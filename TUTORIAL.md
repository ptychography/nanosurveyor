# Step-by-step tutorial on streaming at beamline

## Step 1 - Start up the camera
```
python cin_start_script.py --test
```

## Step 2 - Setup camera
Start CINControl using
```
python cin_gui.py
```
Connect to the camera, and send 
these files to the camera:

...
Disconnect the camera and close CINControl

## Step 3 - Start backend (on phasis)
```
nsbackend --debug examples/backend_phasis.conf
```

## Step 4 - Start frontend
```
nsgui --config examples/frontend_nsptycho.conf
```

## Step 5 - Connect (to backend and camera)
Hit the connect button in the streaming tab.
You should see the backend receiving test patterns

## Step 6 - Open Ptycho Scan in STXM control
Change scanning parameters, e.g. 11x11 scan (10x10 + 21 bg frames).
Now the backend should stop receiving frames.

## Step 7 - Adjust scanning parameters in frontend
It needs to know about the 10x10 scan and the 21 bg frames.
You can also channge other things, e.g. step size, energy ...

## Step 8 - Start new run
Increment the run number in the frontend and hit start.

## Step 9 - Start the ptycho scan in STXM control
before hit the start, make sure you have re-opened the CINControl GUI
(STXM control currently needs it to be responsive), this can be avoided by
changing the main.cfg file of STXM control (in C:/STXM_dev/StxmDir/Microscope Configuration)

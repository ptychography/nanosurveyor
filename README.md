** Nanosurveyor 2.0 **
================

** Copyright Notice **
================
Nanosurveyor Copyright (c) 2017, The Regents of the University of California, through Lawrence Berkeley National Laboratory (subject to receipt of any required approvals from the U.S. Dept. of Energy), 
Benedikt Daurer and Filipe Maia. All rights reserved.

If you have questions about your rights to use or distribute this software, please contact Berkeley Lab's Innovation & Partnerships Office at  IPO@lbl.gov.
NOTICE.  This Software was developed under funding from the U.S. Department of Energy and the U.S. Government consequently retains certain rights. As such, the U.S. Government has been granted for itself and others acting on its behalf a paid-up, nonexclusive, irrevocable, worldwide license in the Software to reproduce, distribute copies to the public, prepare derivative works, and perform publicly and display publicly, and to permit other to do so. 

** License Agreement **
================
Nanosurveyor Copyright (c) 2017, The Regents of the University of California, through Lawrence Berkeley National Laboratory (subject to receipt of any required approvals from the U.S. Dept. of Energy), 
Benedikt Daurer and Filipe Maia. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
(1) Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
(2) Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
(3) Neither the name of the University of California, Lawrence Berkeley National Laboratory, U.S. Dept. of Energy nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
You are under no obligation whatsoever to provide any bug fixes, patches, or upgrades to the features, functionality or performance of the source code ("Enhancements") to anyone; however, if you choose to make your Enhancements available either publicly, or directly to Lawrence Berkeley National Laboratory, without imposing a separate written license agreement for such Enhancements, then you hereby grant the following license: a  non-exclusive, royalty-free perpetual license to install, use, modify, prepare derivative works, incorporate into other computer software, distribute, and sublicense such enhancements or derivative works thereof, in binary and source code form.

** Issue tracker **
===================
Our issue tracker is now on: [https://camera-stxm.atlassian.net/projects/NAN/issues](https://camera-stxm.atlassian.net/projects/NAN/issues)

** Dependencies **
===================
Nanosurveyor depends on the following packages:

* python > 2.7
* pyqt4
* zmq/pyzmq
* numpy
* scipy
* setuptools
* sharp-dev
* arrayfire
* afnumpy

** Installation **
==================
If you have all dependencies are installed, simply run
```sh
python setup.py install
```
or 
```sh
python setup.py install --user
```
The executables will appear in your installation directory.

** Getting started **
=====================
Start the GUI:
```
nsgui
```

Starting the backend:
```
nsbackend
```

** Tutorial **
===============
Step-by-step tutorial on streaming at beamline (last update 2016-02-13)

## Step 1 - Start up the camera
```
python cin_start_script.py --test
```

## Step 2 - Setup camera
Start CINControl using
```
python cin_gui.py
```
Connect to the camera, and send 
these files to the camera:

...
Disconnect the camera and close CINControl

## Step 3 - Start backend (on phasis)
```
nsbackend --debug examples/backend_phasis.conf
```

## Step 4 - Start frontend
```
nsgui --config examples/frontend_nsptycho.conf
```

## Step 5 - Connect (to backend and camera)
Hit the connect button in the streaming tab.
You should see the backend receiving test patterns

## Step 6 - Open Ptycho Scan in STXM control
Change scanning parameters, e.g. 11x11 scan (10x10 + 21 bg frames).
Now the backend should stop receiving frames.

## Step 7 - Adjust scanning parameters in frontend
It needs to know about the 10x10 scan and the 21 bg frames.
You can also channge other things, e.g. step size, energy ...

## Step 8 - Start new run
Increment the run number in the frontend and hit start.

## Step 9 - Start the ptycho scan in STXM control
before hit the start, make sure you have re-opened the CINControl GUI
(STXM control currently needs it to be responsive), this can be avoided by
changing the main.cfg file of STXM control (in C:/STXM_dev/StxmDir/Microscope Configuration)

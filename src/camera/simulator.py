#!/usr/bin/env pypy
"""Simulating a virtual camera that is sending out UDP packets."""
import struct
import socket
import glob
import logging
import datetime
import time
import sys, os
import h5py
import numpy as np
from math import ceil
from PyQt4 import QtCore

from .. import utils

class FccdSimulator(QtCore.QThread):
    """Simulate a FCCD that sends out raw data in form of UDP packets."""
    statusMessage  = QtCore.pyqtSignal(str)
    simulationDone = QtCore.pyqtSignal()

    def __init__(self, filename, address, port, delay=1000, add_random_dark=False):
        QtCore.QThread.__init__(self)

        # Configuration
        self.filename = filename

        # Open CXI file with raw data (to get shape)
        self._dark_key = "entry_1/instrument_1/detector_1/data_dark"
        self._data_key = "entry_1/instrument_1/detector_1/data"
        self.f = h5py.File(filename, 'r')
        self.ndark  = self.f[self._dark_key].shape[0]
        self.ndata  = self.f[self._data_key].shape[0]
        self.ntotal = self.ndark + self.ndata
        self.f.close()

        # Configuration
        self.ip    = address
        self.port  = port
        self.delay = delay
        self.add_random_dark = add_random_dark
        self._stop = False
        
    def createSendPacketsSocket(self):
        """Open a socket for sending UDP packets to the framegrabber."""
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.status = "<font color=\"orange\"> Socket open, packets are send to (%s) on %d " %(self.ip, self.port) + "</font>"
        self.statusMessage.emit(self.status)
        
    def load_frame(self, i):
        """Returns dark/data frame read from opened CXI file."""
        f = h5py.File(self.filename, 'r')        
        if i < self.ndark:
            frame = f[self._dark_key][i,:11640,:].astype(np.uint16).ravel().byteswap().tostring()
        else:
            if (i-self.ndark) < self.ndata:
                data = f[self._data_key][i-self.ndark,:11640,:].astype(np.uint16)
                if self.add_random_dark:
                    dark = f[self._dark_key][int(np.random.random()*self.ndark),:11640,:].astype(np.uint16)
                    data += dark
                frame = (data).ravel().byteswap().tostring()
            else:
                dark = f[self._dark_key][int(np.random.random()*self.ndark),:11640,:].astype(np.uint16)
                frame = (dark).ravel().byteswap().tostring()
        f.close()
        return frame
    
    def make_header(self, port, length, frame_number, packet_number):
        """Returns header."""
        header = struct.pack('!BBHHH', packet_number, 0, port, length, frame_number)
        return header

    def send_frame_in_udp_packets(self, frame, frame_number):
        """Chop frame into small UDP packets and send them out."""
        for i in range(len(frame) // 8184 + 1):
            time.sleep(self.delay/1.e9)
            h = self.make_header(self.port, 8184+8, frame_number, i%256)
            packet = h + frame[i*8184:(i+1)*8184]
            bytes_sent = self.socket.sendto(packet, (self.ip, self.port))
        packet = self.make_header(self.port, 48, frame_number, 0) + 40*"\x00"
        bytes_sent = self.socket.sendto(packet, (self.ip, self.port))
        bytes_sent = self.socket.sendto(packet, (self.ip, self.port))

    def run(self):
        """This triggers the event loop."""
        j = 0
        t0 = datetime.datetime.now()

        # Start the event loop
        while not self._stop:
            
            # Stop producing data when total nr. of frames is reached
            if j >= self.ntotal:
                self._stop = True
                self.simulationDone.emit()

            # Send out UDP packets for given frame
            self.send_frame_in_udp_packets(self.load_frame(j),j)

            # Update counters
            j += 1

            # Update status (speed)
            if not (j % 100):
                self.status = "<font color=\"blue\"> Sending at %.2fHz (frame %d)" %(float(j) / (datetime.datetime.now() - t0).total_seconds(), j) + "</font>"
                self.statusMessage.emit(self.status)

    def stop(self):
        """Stop the event loop."""
        self.status = "<font color=\"red\"> Fccd is stopping the event loop </font>"
        self.statusMessage.emit(self.status)
        self._stop = True

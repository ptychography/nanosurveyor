import sys
import numpy as np
import scipy.special as special
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt

# Append pyptycho to PYTHONPATH, lateron functionality should be moved directly into here
sys.path.append('/home/benedikt/software/pyptycho/')
sys.path.append('/Users/benedikt/phd-project/software/ptychography/pyptycho/')

# Because some of the imports of pytptycho are relative to its parent directory, we also need that one in our path
sys.path.append('/home/benedikt/software')
sys.path.append('/Users/benedikt/phd-project/software/ptychography/')

# Imports from pyptycho
import time, os, datetime, sys
from util import dist, shift, circle
from numpy import *
from scipy import random, ndimage, interpolate
import fftw3
from scipy import polyfit, misc, ndimage, mgrid, rand
from scipy.ndimage.filters import gaussian_filter, maximum_filter, minimum_filter
from processFCCD import fitOverscan, processfCCDFrame
from propagate import propnf
from loadDirFrames import *
from aveDirFrames import *
from mpi4py import MPI
from executeParam import *
import numpy as np
#from piUtils import readSpe

def preprocessing(frame, avgdark, param, scanDir=None, bgScanDir=None, debug=False):
    """Trying to replicate the pre-processing from the pyptycho module using all the functions directly without modification.

    The only difference is that data is not loaded from TIFF file and given directly to this function.

    frame is a single diffraction pattern in TIFF format (1000,1152)
    avgdark is the average dark diffraction pattern in TIFF format (1000,1152)
    param is a dictionary containing all parameters expected by the pyptycho package
    """

    # Take middle part of detector
    frame = frame[500:1500,:]
    avgdark = avgdark[500:1500,:]
    
    # Parameters
    dataPath = param['dataPath']
    scanDate = param['scanDate']
    scanNumber = param['scanNumber']
    scanID = param['scanID']
    bgScanDate = param['bgScanDate']
    bgScanNumber = param['bgScanNumber']
    bgScanID = param['bgScanID']
    sh_sample = int(param['sh_sample_y']),int(param['sh_sample_x']) #size to which the raw data is resampled
    filter_width = float(param['filter_width'])
    ss = float(param['ssy']),float(param['ssx'])
    xpts = int(param['xpts'])
    ypts = int(param['ypts'])
    zd = float(param['zd'])
    dr = float(param['dr'])
    e = float(param['e']) #photon energy
    ccdp = float(param['ccdp'])
    ccdz = float(param['ccdz']) * 1000. #convert to microns
    nexp = int(param['nexp'])
    bin = int(param['bin'])
    png = False #int(param['png'])
    xshift = 2
    yshift = 4
    xc = int(param['xcenter'])# + xshift
    yc = int(param['ycenter'])# + yshift
    t_exp = float(param['t_long']),float(param['t_short'])
    saturation_threshold = float(param['s_threshold'])
    nProcesses = int(param['nProcesses'])
    bl = param['bl']
    xpixnm,ypixnm = np.single(param['pixnm']),np.single(param['pixnm'])
    interferometer = int(param['interferometer'])
    xaxis = param['xaxis']
    yaxis = param['yaxis']
    xaxisNo = str(int(scanID) - 1)
    yaxisNo = str(int(scanID) - 1)
    pThreshold = float(param['probeThreshold'])
    removeOutliers = int(param['removeOutliers'])
    fCCD = int(param['fCCD'])
    dfilter = int(param['lowPassFilter'])
    nl = float(param['nl'])
    cropSize = int(param['sh_crop']) #size to which the raw data is cropped
    noise_threshold = nl
    process = 15
    verbose = True
    fstep = 1
    tiff = 0
    logscale = 1
    indexd = None
    indexo = None
    gpu = int(param['processGPU'])
    fv = int(param['fv'])
    fh = int(param['fh'])
    tr = int(param['transpose'])
    fCCD_version = int(param['fCCD_version'])
    low_pass = int(param['low_pass'])
    multiCore = True
    bsNorm = 1. / float(param['beamStopTransmission'])
    defocus = float(param['defocus'])
    bsXshift = int(param['beamstopXshift'])
    bsYshift = int(param['beamstopYshift'])
    beamstopThickness = int(param['beamstop'])

    if debug:
        filePrefix = 'image'
        frame_offset = 17565
    
    if nexp == 2:
        multi = True
    else: multi = False

    ######################################################################################################
    ###load interferometer data if requested
    if interferometer:
        xaxisDataFile = namePrefix + scanDate + scanNumber + '_' + xaxis + xaxisNo + '.xim'
        yaxisDataFile = namePrefix + scanDate + scanNumber + '_' + yaxis + yaxisNo + '.xim'
        print "X Axis data file: ", xaxisDataFile
        print "Y Axis data file: ", yaxisDataFile
        xdataFile = dataPath + scanDate + '/' + xaxisDataFile
        ydataFile = dataPath + scanDate + '/' + yaxisDataFile
        try: xshiftData = readXIM(xdataFile)
        except:
            print "Could not open interferometer X data file: %s" %xdataFile
            interferometer = 0
        else: xshiftData = (xshiftData - xshiftData.mean()) * 1000. #convert to nanometers
        try: yshiftData = readXIM(ydataFile)
        except:
            print "Could not open interferometer Y data file: %s" %ydataFile
            interferometer = 0
        else: yshiftData = (yshiftData - yshiftData.mean()) * 1000. #convert to nanometers


    ######################################################################################################
    ###calculate the number of pixels to use for raw data (the "crop" size)
    nyn, nxn = sh_sample
    ssy, ssx = ss
    fw = filter_width
    data_ave = np.zeros((nyn,nxn))
    l = e2l(e)
    f = zd * dr / l #zp focal length microns
    na = zd / f / 2. #zp numerical aperture
    xtheta = l / 2. / xpixnm #scattering angle
    nx = 2 * np.round(ccdz * (xtheta) / ccdp)
    ytheta = l / 2. / ypixnm
    ny = 2 * np.round(ccdz * (ytheta) / ccdp)
    sh_pad = ny, nx  #size to which the raw data is zero padded

    verbose = False

    if verbose:
        if fCCD: print "Using fastCCD version: %s" %fCCD_version
        else: print "Not using fastCCD"
        print "Scan date: %s, Scan Number: %s, Scan ID: %s" %(scanDate, scanNumber, scanID)
        print "Output file: %s" %param['dataFile']
        print "Photon energy: %.2f eV" %e
        print "Wavelength: %.2f nm" %l
        print "Zone plate focal length: %.2f mm" %f
        print "sample-CCD distance: %.2f mm" %(ccdz / 1000.)
        print "Requested pixel size (nm): %.2f" %(xpixnm)
        print "Will pad the data to: %i x %i (HxV)" %(nx,ny)
        print "Will downsample data to: %i x %i (HxV)" %(nxn,nyn)
        print "Probe step in pixels (x,y): %.2f,%.2f" %(ssx / xpixnm, ssy / ypixnm)
        print "Intensity threshold percent for probe calculation: %i" %(100. * pThreshold)
        print "Beamstop transmission: %.4f" %(1. / bsNorm)
        print "Beamstop normalization factor: %.2f" %bsNorm

    zpw = round(2. * na * ccdz / ccdp)
    stxmImage = zeros((ypts, xpts))
    stxmMask = circle(nxn,zpw * float(nxn) / float(nx),nxn/2,nxn/2)

    expectedFrames = xpts * ypts * bin * nexp# - nexp * fCCD
    if debug:
        expectedFrames = 10
    if verbose: print "Expecting %i frames." %(expectedFrames)


    ######################################################################################################
    ###load and average the background frames
    ###this alternates long and short frames regardless of "multi", should be fixed
    if True:
        if verbose: print "Averaging background frames..."

        # We already have the average dark frame
        if debug:
            bg, bg_short, nframes = aveDirFrames(bgScanDir, multi, fCCD, fCCD_version)
        else:
            bg = avgdark
            bg_short = None

        #if param['bg_filename'] == '':
        #    bg, bg_short, nframes = aveDirFrames(bgScanDir, multi, fCCD, fCCD_version)
        #else:
        #    bg = np.asarray(readSpe(scanDir + param['bg_filename'])['data'])
        #    nframes = len(bg) / nexp
        #    bg_short = bg[1::2].mean(axis = 0)
        #    bg = bg[2::2].mean(axis = 0)
        #if verbose: print "Done. Averaged %i frames." %nframes

        if fCCD:
            osmask = zeros(bg.shape)
            sh = bg.shape
            if fCCD_version == 1:
                osmask[:,0::12] = 1
                osmask[:,1::12] = 1
            elif fCCD_version == 2:
                #osmask[:,0::12] = 1
                #osmask[:,11::12] = 1
                osmask[0:sh[0]/2,0::12] = 1
                osmask[0:sh[0]/2,11::12] = 1
                osmask[sh[0]/2:sh[0],0::12] = 1
                osmask[sh[0]/2:sh[0],11::12] = 1
                #osmask = vstack((flipud(fliplr(osmask[:,1152:2304])),osmask[:,0:1152]))
            #bg = vstack((flipud(fliplr(bg[:,1152:2304])),bg[:,0:1152]))
            osmask = vstack((osmask[1:467,:],osmask[534:1000,:]))
            #bg = vstack((bg[1:467,:],1. * bg[534:1000,:]))
            dmask = 1 - osmask
            indexd = where(dmask)
            indexo = where(osmask)
            #bg = reshape(bg[indexd],(932,960))
            os_bkg = 0.
    else:
        bg = None
        bg_short = None
        if verbose: print "No background data submitted!"

    t0 = time.time()

    ######################################################################################################
    ###Load the data from the directory of tiff frames
    
    # We already have the frames
    if debug:
        dataStack, loadedFrames = loadDirFrames(scanDir, filePrefix, expectedFrames, cropSize, multi, fCCD, frame_offset)
#        dataStack = dataStack[3]
    dataStack = frame.reshape((1, frame.shape[0], frame.shape[1]))
    nSlices = 1
    # if param['speFile'] == '':
    #     dataStack, loadedFrames = loadDirFrames(scanDir, filePrefix, expectedFrames, cropSize, multi, fCCD, frame_offset)
    # else:
    #     dataStack = np.asarray(readSpe(scanDir + param['speFile'])['data'])
    #     loadedFrames = len(dataStack)

    ######################################################################################################
    ###set up the frame shift data
    pixIndex = [(i / xpts, i % xpts) for i in range(expectedFrames / nexp)]
    if interferometer:
        shiftData = [(-yshiftData[i / xpts, i % xpts] * 1000., xshiftData[i / xpts, i % xpts] * 1000.) for i in range(xpts * ypts)]
    else:
        shiftData = [(-(float(pixIndex[i][0]) - float(ypts - 1) / 2) * ssy, (float(pixIndex[i][1]) - float(xpts - 1) / 2) * ssx) for i in range(expectedFrames / nexp)]

    ######################################################################################################
    ###set up the low pass filter and beamstop normalization
    if fCCD:
        fsh = cropSize, cropSize
        if beamstopThickness == 20:
            bs_r = 31.
        else: bs_r = 40 #was 10 at BL901

        bs = 1. + (bsNorm * circle(cropSize, bs_r, cropSize / 2, cropSize / 2) - (bsNorm + 0.5) * circle(cropSize, 10, cropSize / 2, cropSize / 2))
        bs = 1. + ((bsNorm - 1) * circle(cropSize, bs_r, cropSize / 2, cropSize / 2))
        fNorm = shift(gaussian_filter(bs, sigma = 0.5), bsYshift, bsXshift) #we multiply the data by this to normalize

    else: fsh = dataStack[0].shape; fNorm, indexd, indexo, os_bkg = None, None, None, None
    dy, dx = float(sh_pad[0]) / float(sh_sample[0]),float(sh_pad[1]) / float(sh_sample[1])
    dyn, dxn = int(float(fsh[0]) / dy), int(float(fsh[1]) / dx)

    if dfilter:
        x,y = meshgrid(abs(arange(fsh[1]) - fsh[1] / 2),abs(arange(fsh[0]) - fsh[0] / 2))
        filter = exp(-x**2 / 2. / float(fw)**2 -\
                     y**2 / 2. / float(fw)**2)
        filter = fft.fftshift(filter).astype("float64")
    else: filter = False

    tRead = time.time() - t0
    t0 = time.time()
    #if verbose: print "Read frames at: %.2f Hz" %(float(i) / tRead)
    #if verbose: print "Loaded %i frames" %loadedFrames
    #if expectedFrames == loadedFrames:
    #    if verbose: print "Loaded correct number of frames.  Good!"
    #else:
    #    if verbose: print "Expected %i frames but loaded %i. Exiting!" %(expectedFrames, loadedFrames)
    #    return

    ######################################################################################################
    # ###split up the data stack into chunks and send to the processors
    # if comm.Get_size() > 1: nProcessors = comm.Get_size() - 1
    # else: print "readALSMPI requires at least 2 MPI processes. Exiting."; return
    # if nProcessors == 1:
    #     startIndices = [0]
    #     stopIndices = [expectedFrames - 1]
    # else:
    #     interval = expectedFrames / nProcessors
    #     if interval % 2 != 0: interval += 1  ##make it even for double exposure mode
    #     stopIndices = arange(nProcessors) * interval + interval
    #     startIndices = arange(nProcessors) * interval
    #     if stopIndices[-1] < expectedFrames: stopIndices[-1] = expectedFrames #make the last one longer to get the stragglers
    # for rank in range(1,comm.Get_size()):
    #     i1 = startIndices[rank - 1]
    #     i2 = stopIndices[rank - 1]
    #     indexTuple = i1, i2
    #     if i2 < expectedFrames: 
    #     	nSlices,y,x = i2-i1,0,0 #dataStack[i1:i2,:,:].shape
    #     else: 
    #     	i2 = expectedFrames
    #     	nSlices,y,x = i2-i1,0,0
    #     #print nSlices, i1, i2, expectedFrames, frame_offset
    #     #print rank, fNorm.shape
    #     dataStruct = [bg, sh_pad, sh_sample, filter, xc,\
    #              yc, dfilter, t_exp, saturation_threshold, noise_threshold, stxmMask, \
    #              tiff, False, bl, fCCD, fv, fh, tr, multi, verbose, fCCD_version, fNorm, \
    #              indexTuple, indexd, indexo, cropSize, os_bkg, bg_short, low_pass,nSlices,y,x,\
    #     	 i1,scanDir, filePrefix, expectedFrames, cropSize, frame_offset]
    #     comm.send(dataStruct, rank, tag = 11)
    #     #comm.Send(dataStack[i1:i2,:,:], rank, tag = 11)

    ######################################################################################################
    ###retrieve the chunks back and put them into the right spot in the stack (re-initialized to the new smaller size)
    nPoints = xpts * ypts
    nPoints = 1
    #print nPoints
    #dataStack = zeros((nPoints, nyn, nxn))
    #for rank in range(1, comm.Get_size()):
    #    dataStruct = comm.recv(source = rank, tag = 12)
    #    i1,i2 = dataStruct[22]  ##these are wrong for double exposure mode
    #    if multi: i1, i2 = i1 / 2, i2 / 2
    #    comm.Recv(dataStack[i1:i2], source = rank, tag = 12)

    # tProcess = time.time() - t0
    # if verbose: print "Done!"
    # #if verbose: print "Processed frames at: %.2f Hz" %(float(loadedFrames / nexp) / tProcess)
    # if verbose: print "Total time per frame: %i ms" %(1000. * (tProcess + tRead) / float(expectedFrames))

    ny, nx = sh_pad
    nyn, nxn = sh_sample
    df = dfilter
    nl = noise_threshold

    ###############################################################################################
    ###define the array for the processed data
    #dataStack, loadedFrames = loadDirFrames(scanDir, filePrefix, expectedFrames, cropSize, multi, fCCD, frame_offset + iStart)
    data = np.zeros((len(dataStack), cropSize, cropSize))

    ######################################################################################################
    ###Generate a mask to cover bad regions of the detectors
    a,b,c = dataStack.shape
    dataMask = ones((b,c))
    if fCCD:
        dataMask[500:1000,0:150] = 0.
    dataStack = dataStack * dataMask

    ###############################################################################################
    ###for fastCCD data launch the fCCD pre-processor for overscan removal and background subtraction
    if fCCD:
        if multi:
            for i in range(nSlices):
                data[i,:,:] = processfCCDFrame(dataStack[i,:,:], dataStack[i + 1,:,:], bg, bg_short, os_bkg,\
                    indexd, indexo, t_exp, saturation_threshold,noise_threshold, xc,yc,fNorm,i,cropSize,fCCD_version)
                i += 1
        else:
            for i in range(nSlices):
                data[i,:,:] = processfCCDFrame(dataStack[i,:,:], None, bg, bg_short, os_bkg, indexd, indexo, t_exp, \
                    saturation_threshold, noise_threshold, xc, yc, fNorm, i, cropSize, fCCD_version)

    ###############################################################################################
    #if verbose:
    if verbose: sys.stdout.write("Processing frames...");sys.stdout.flush()

    ###############################################################################################
    ###background subtraction is already done so just remove the final "noise level" offset
    if fCCD:
        ###redefine the center since the fastCCD preprocessor centers the data
        y,x = data[0].shape
        xc,yc = data[0].shape[1] / 2, data[0].shape[0] / 2

    ##############################################################################################
    ###Background subtract and merge for non-fastCCD data
    elif multi:
        nSlices = nSlices / 2
        #data = dataStruct[0]
        data[0::2,:,:] -= bg ##subtract background from long exposures
        data[0::2,:,:] *= (data[0::2,:,:] > nl)
        data[1::2,:,:] -= bg_short ##subtract background from short exposures
        data[1::2,:,:] *= (data[1::2,:,:] > nl)
        mask = (data[0] > saturation_threshold)
        mask = 1. - mask / mask.max()
        data[0::2,:,:] = mask * data[0::2,:,:] + (1 - mask) * data[1::2,:,:] * t_exp[0] / t_exp[1]
        data = data[0::2,:,:]

    else:
        #data = dataStruct[0]
        data -= (bg + nl)
        data *= data > 0.

    dy, dx = float(nyn) / float(ny),float(nxn) / float(nx)
    dyn, dxn = int(float(y) * dy), int(float(x) * dx)
    data -= nl
    data = data * (data > 0.)

    ###############################################################################################
    ####Low Pass Filter
    if df:
        if verbose: print "Applying filter on data transform"

        data = data * (1+0j)

        #############Do it this way to reuse the plan, requires some data copies
        input_array = zeros_like(data[0])
        output_array = zeros_like(data[0])
        fftForwardPlan = fftw3.Plan(input_array,output_array,'forward')
        fftBackwardPlan = fftw3.Plan(input_array,output_array,'backward')
        for i in range(len(data)):
            copyto(input_array, data[i])
            fftw3.guru_execute_dft(fftForwardPlan, input_array, output_array)
            copyto(input_array, output_array * filter)
            fftw3.guru_execute_dft(fftBackwardPlan, input_array, output_array)
            copyto(data[i], output_array)

        data = abs(data) / (data[0].size) #renormalize after transforms

    ###############################################################################################
    ###RESAMPLE and ZERO PAD
    if dyn < dxn:
        q = dxn
    else: q = dyn

    dataStack = zeros((nSlices,nyn, nxn))
    yc = int(float(yc) * float(q) / float(y)) + 1
    xc = int(float(xc) * float(q) / float(x)) + 1

    p1 = nyn / 2 - yc + 1
    p2 = p1 + q
    p3 = nxn / 2 - xc + 1
    p4 = p3 + q

    if p1 < 0 or p3 < 0 or p2 > nyn or p4 > nxn:
        print "Zero-padding failed, try increasing pixels per scan step."
        return

    ##downsample using bivariate spline interpolation
    for i in range(nSlices):
        print data[i].shape
        x, y = np.linspace(0, data[i].shape[0], data[i].shape[0]),np.linspace(0, data[i].shape[1], data[i].shape[1])
        sp = interpolate.RectBivariateSpline(x, y, data[i])
        yn, xn = np.meshgrid(np.linspace(0, data[i].shape[1], q), np.linspace(0, data[i].shape[0], q))
        dataStack[i, p1:p2, p3:p4] = sp.ev(xn.flatten(), yn.flatten()).reshape(q,q)

    if low_pass:
        dataStack *= np.exp(-dist(nyn)**2 / 2. / (nyn / 4)**2)

    ###Zero data points below the noise floor
    dataStack *= (dataStack > nl)

    ####Return
    if fv: dataStack = dataStack[:,::-1,:]
    if fh: dataStack = dataStack[:,:,::-1]
    if tr: dataStack = transpose(dataStack, axes = (0,2,1))
    if fCCD: dataStack = transpose(rot90(transpose(dataStack)))
    #if verbose: print "Sending data from Rank %i" %(comm.Get_rank())
    #comm.send(dataStruct, dest = 0, tag = 12)
    #comm.Send(np.ascontiguousarray(dataStack), dest = 0, tag = 12)

    #return  dataStack[0]
    
    ######################################################################################################
    # ###Calculate the STXM image and make some arrays for the translations
    # ihist = array([(dataStack[i] * (dataStack[i] > 10.)).sum() for i in range(nPoints)])
    # indx = arange(nPoints)
    # stxmImage = reshape(ihist,(ypts,xpts))[::-1,:]

    # #####################################################################################################
    # ###Remove outliers based on STXM intensities
    # x = array(shiftData)[:,1]
    # y = array(shiftData)[:,0]


    # if removeOutliers:
    #     #identify bad files by checking for total intensities more than 3 sigma from the mean
    #     Imax = ihist.mean() + 4.0 * ihist.std()
    #     hifiles = where(ihist > Imax)
    #     Imin = ihist.mean() - 4.0 * ihist.std()
    #     lofiles = where(ihist < Imin)
    #     nanfiles = where(isnan(ihist))
    #     bad_files_indices = concatenate((hifiles[0],lofiles[0],nanfiles[0]))
    #     bad_files_indices.sort() #sort the list to ascending order
    #     if verbose: print "Removing outliers"
    #     k = 0
    #     if len(bad_files_indices) > 0:
    #         for item in bad_files_indices:
    #             i = item / stxmImage.shape[1]
    #             j = item % stxmImage.shape[1]
    #             stxmImage[i,j] = stxmImage.mean()
    #             indx[item] = 0
    #             indx[item+1:nPoints] = indx[item+1:nPoints] - 1
    #             x = delete(x, item - k)
    #             y = delete(y, item - k)
    #             dataStack = delete(dataStack, item - k, axis = 0)
    #             k += 1

    # translationX, translationY = (x - x.min()) * 1e-9, (y - y.min()) * 1e-9

    # ######################################################################################################
    # ###Calculate the probe, center of mass and probe mask
    if verbose: print "Calculating probe and mask"
    dataAve = dataStack.sum(axis = 0) #average dataset
    #locate the center of mass of the average and shift to corner
    dataAve = dataAve * (dataAve > 10.) #threshold, 10 is about 1 photon at 750 eV
    dataSum = dataAve.sum() #sum for normalization
    xIndex = arange(dataAve.shape[1])
    yIndex = arange(dataAve.shape[0])

    xc = np.int(np.round((dataAve.sum(axis = 0) * xIndex).sum() / dataSum))
    yc = np.int(np.round((dataAve.sum(axis = 1) * yIndex).sum() / dataSum))
    #print "Center positions: %.2f, %.2f" %(xc,yc)

    # ##Calculate the probe from the average diffraction pattern
    dataStack = np.roll(np.roll(dataStack, dataStack.shape[1] / 2 - yc, axis = 1), dataStack.shape[2] / 2 - xc, axis = 2)  ##center the data
    # ##zero the center
    # #j,k,l = dataStack.shape
    # #dataStack = dataStack * (1. - circle(k, 9, k / 2, k / 2))
    # dataAve = np.roll(np.roll(dataAve, -yc, axis = 0), -xc, axis = 1) ##center the average data
    # #dataAve = dataAve * (1. - circle(k, 9, k / 2, k / 2))
    # pMask = (dataAve > pThreshold * dataAve.max())
    # p = sqrt(dataAve) * pMask
    # p = fft.ifftshift(fft.ifftn(p)) #we can propagate this to some defocus distance if needed.
    # ###put propagation code here
    # if defocus != 0:
    #     p = propnf(p, defocus * 1000. / xpixnm, l / xpixnm)

    #####################################################################################################
    ##Apply a mask to the data
    #ny,nx = dataStack.shape[1:3]
    #m = np.ones((ny,nx))# - circle(nx, 2, nx / 2, nx / 2)
    #m[:,190:nx] = 0.
    #m[175:205,140:nx] = 0.
    #dataStack *= m

    # ##########################################################
    # ####interpolate STXM image onto the same mesh as the reconstructed image
    # if verbose: print "Interpolating STXM image"
    # yr,xr = ssy * ypts, ssx * xpts
    # y,x = meshgrid(linspace(0,yr,ypts),linspace(0,xr,xpts))
    # y,x = y.transpose(), x.transpose()
    # yp,xp = meshgrid(linspace(0,yr, ypts * (ssy / ypixnm)),linspace(0,xr, xpts * (ssx / xpixnm)))
    # yp,xp = yp.transpose(), xp.transpose()
    # x0 = x[0,0]
    # y0 = y[0,0]
    # dx = x[0,1] - x0
    # dy = y[1,0] - y0
    # ivals = (xp - x0)/dx
    # jvals = (yp - y0)/dy
    # coords = array([ivals, jvals])
    # stxmImageInterp = ndimage.map_coordinates(stxmImage.transpose(), coords)
    # stxmImageInterp = stxmImageInterp * (stxmImageInterp > 0.)

    # stride_x = 1
    # stride_y = 1
    # start_x = 0
    # start_y = 0
    # end_x = xpts
    # end_y = ypts

    # ######################################################################################################
    # ###Generate the CXI file
    # translation = column_stack((translationX, translationY, zeros(translationY.size))) #(x,y,z) in meters
    # corner_position = [float(sh_pad[1]) * ccdp / 2. / 1e6, float(sh_pad[0]) * ccdp / 2. / 1e6, ccdz / 1e6] #(x,y,z) in meters

    # outputFile = param['scanDir'] + param['outputFilename']
    # if verbose: print "Writing data to file: %s" %outputFile

    # ########################################################################################################
    # ########################################################################################################

    # cxiObj = cxi()
    # cxiObj.process = param
    # cxiObj.probe = p
    # cxiObj.beamline = bl
    # cxiObj.energy = e * 1.602e-19
    # cxiObj.ccddata = dataStack
    # cxiObj.probemask = pMask
    # cxiObj.probeRmask = circle(pMask.shape[0], pMask.shape[0] / 4, pMask.shape[0] / 2, pMask.shape[0] / 2)
    # cxiObj.datamean = dataAve
    # cxiObj.illuminationIntensities = dataAve
    # cxiObj.stxm = stxmImage
    # cxiObj.stxmInterp = stxmImageInterp
    # cxiObj.xpixelsize = float(sh_pad[1]) / float(nxn) * float(ccdp) / 1e6
    # cxiObj.ypixelsize = float(sh_pad[0]) / float(nxn) * float(ccdp) / 1e6
    # cxiObj.corner_x, cxiObj.corner_y, cxiObj.corner_z = corner_position
    # cxiObj.translation = translation
    # cxiObj.indices = indx
    # writeCXI(cxiObj, fileName = outputFile)

    # ######################################################################################################
    # ######################################################################################################

    # if verbose: print "Done."
    # if removeOutliers:
    #     if verbose: print "Located %i bad frames..." %(len(bad_files_indices))

    # #os.chmod(outputFile, 0664)
    # return 1

    return dataStack[0]
    

def shift(a, xoffset, yoffset = None):

    if yoffset == None:
        ny, nx = a.shape
        for i in range(0,nx):
            dummy = np.concatenate((a[-xoffset:], a[:-xoffset]))
                
    else:
        shiftxi = int(xoffset)
        shiftyi = int(yoffset)
        shiftxf = xoffset - shiftxi
        shiftyf = yoffset - shiftyi
        gx, gy = np.gradient(a)
        ny,nx = a.shape
        dummy = np.zeros((ny,nx)) + a

        for i in range(0,ny):
            dummy[i] = np.concatenate((dummy[i,-xoffset:], dummy[i,:-xoffset]))

        dummy = np.transpose(dummy)

        for i in range(0,nx):
            dummy[i] = np.concatenate((dummy[i,yoffset:],dummy[i,:yoffset]))

        dummy = np.transpose(dummy)

        if ((shiftxf != 0) * (shiftyf != 0)):
            gx, gy = np.gradient(dummy)
            dummy = dummy - (gx * shiftxf + gy * shiftyf)

    if (dummy.sum() != 0.): return dummy * a.sum() / dummy.sum()
    else: return dummy

def fitOverscan(a,mask):

    ll = a.shape[0]
    xxb = np.matrix(np.linspace(0,1,ll)).T
    w = np.ones((ll))
    w[0:100] = 0
    w = np.matrix(np.diag(w))
    Q = np.matrix(np.hstack((np.power(xxb,8),np.power(xxb,6),np.power(xxb,4),np.power(xxb,2),(xxb * 0. + 1.))))
    bnfw = Q * (Q.T * w * Q).I * (w * Q).T * np.matrix(a * mask)
    bnfw1 = np.rot90(bnfw,2)

    jkjk = np.array(bnfw1) # / rot90(mask,2)
    jkjk1 = np.zeros((2*ll,192))
    jkjk1[::2,0:192] = jkjk[:,::2]
    jkjk1[1::2,0:192] = jkjk[:,1::2]

    tt = np.reshape(np.linspace(1,12*ll,12*ll),(ll,12))
    ttd = tt[:,9::-1]
    tts = tt[:,12:9:-1]

    f = interp1d(tts.flatten(), jkjk1, 'linear',axis = 0,bounds_error = False,fill_value = jkjk1.mean())
    bkg = np.rot90(np.reshape(f(ttd),(ll,192 * 10)),2)
    bkg = f(ttd)
    bkg1 = np.zeros((ll,1920))

    for i in xrange(10):
        bkg1[:,i::10] = bkg[:,i,:]

    return np.rot90(bkg1,2)

def preprocessing_old(frame, dark):
    """Intends to replicate new prepocessing from scripts/processFCCD.py"""

    # Hardcoded params
    saturation_threshold = 5000
    fNorm = 1.
    dSize = 800.
    nyn, nxn = 256,256    
    noise_threshold = 2
    ssy, ssx = (70,70)
    fw = 80
    e = 834.5
    l = 1239.852 / e
    zd = 240
    dr = 60
    xpixnm = 4.66667
    ypixnm = 4.66667
    ccdz = 81.38 * 1000. # microns
    ccdp = 30.
    f = zd * dr / l #zp focal length microns
    na = zd / f / 2. #zp numerical aperture
    xtheta = l / 2. / xpixnm #scattering angle
    nx = 2 * round(ccdz * (xtheta) / ccdp)
    ytheta = l / 2. / ypixnm
    ny = 2 * round(ccdz * (ytheta) / ccdp)
    sh_pad = ny, nx  #size to which the raw data is zero padded
    fCCD_version = 2
    pThreshold = 0.1
    bsNorm = 1./0.042633
    
    print "Using fastCCD version: %s" %fCCD_version
    #print "Scan date: %s, Scan Number: %s, Scan ID: %s" %(scanDate, scanNumber, scanID)
    #print "Output file: %s" %param['dataFile']
    print "Photon energy: %.2f eV" %e
    print "Wavelength: %.2f nm" %l
    print "Zone plate focal length: %.2f mm" %f
    print "sample-CCD distance: %.2f mm" %(ccdz / 1000.)
    print "Requested pixel size (nm): %.2f" %(xpixnm)
    print "Will pad the data to: %i x %i (HxV)" %(nx,ny)
    print "Will downsample data to: %i x %i (HxV)" %(nxn,nyn)
    print "Probe step in pixels (x,y): %.2f,%.2f" %(ssx / xpixnm, ssy / ypixnm)
    print "Intensity threshold percent for probe calculation: %i" %(100. * pThreshold)
    print "Beamstop transmission: %.4f" %(1. / bsNorm)
    print "Beamstop normalization factor: %.2f" %bsNorm
    
    zpw = round(2. * na * ccdz / ccdp)
        
    # Subtract dark
    frame -= dark

    # Take middle part of detector
    frame = frame[500:1500,:]
        
        
    # Default center of frame
    (yc,xc) = frame.shape[0] , frame.shape[1] /2

    if yc > 970:
        xc, yc = xc - 84, yc - 572
    else:
        xc, yc = xc - 84, yc - 500

    data = np.hstack((frame[534:1000,:], np.fliplr(np.flipud(shift(frame[1:467,:],0,2)))))

        #plt.imshow(data, interpolation='none')
        #plt.show()

        
    osmask = np.zeros(data.shape)
    osmask[:,0::12] = 1
    osmask[:,11::12] = 1
    dmask = 1 - osmask
    indexd = np.where(dmask)

    indexo = np.where(osmask)

    dd = np.reshape(data[indexd],(466,10*192))
    os = np.reshape(data[indexo],(466,2*192))
    osmask = os > 0. #ignores the dropped packets, I hop

    smask = dd < saturation_threshold

        #plt.imshow(dd, interpolation='none')
        #plt.show()
        #plt.imshow(os, interpolation='none')
        #plt.show()
        
    bkg1 = fitOverscan(os, osmask)

    dd -= bkg1
    dd = dd * (dd > 0)
        
    ll = dd.shape[0]
    lw = dd.shape[1]

    jk = np.reshape(np.transpose(np.reshape(dd,(ll,10,192)),(0, 1, 2)), (ll * 10, 192))
    jkf=(np.fft.fftn((jk*(np.abs(jk)<4.0))))#; % only when it is background...
    msk = np.abs(jkf) > 400.; msk[0:500,:] = 0 #; % Fourier mask, keep low frequencies.
    jkif = np.reshape(np.transpose(np.reshape(np.fft.ifftn(jkf * msk),(10,ll,192)),(0, 1, 2)),(ll,1920))
    dd -= jkif.real
        
    dd[0,9::10] = 0

    dd =  np.vstack((np.flipud(np.fliplr(dd[:,960:1920])),dd[:,0:960]))
        
    # Version 2
    dd[0:470,:] = shift(dd[0:470,:],0,-8)
    dd[468,8::10] = dd[468,0::10]
    dd[468,9::10] = dd[468,3::10]
    dd[470,0::10] = dd[471,0::10]
    dd[470,1::10] = dd[470,2::10]
    dd[469,:] = (dd[468,:] + dd[470,:]) / 2.
    # temp = dd[:,0:50].mean(axis = 1)
    # dd = dd - reshape(temp,(len(temp),1))
    ##crop
    dd = dd[yc - dSize / 2:yc + dSize / 2, xc - dSize / 2: xc + dSize / 2]
        
    ##remove negatives and multiply by the beamstop scale factor
    dd *= fNorm
    dd -= (fNorm - 1) * noise_threshold
    dd *= dd > noise_threshold

    #plt.imshow(dd, interpolation='none')
    #plt.show()

    if np.isnan(dd.min()): print "Nan encountered!"
    data = dd

    #redefine the center since the fastCCD preprocessor centers the data
    y,x = data.shape
    xc,yc = data.shape[1] / 2, data.shape[0] / 2

    
    dy, dx = float(nyn) / float(ny),float(nxn) / float(nx)
    dyn, dxn = int(float(y) * dy), int(float(x) * dx)
    data -= noise_threshold
    data = data * (data > 0.)

    ####Low Pass Filter
    data = data * (1+0j)

    #############Do it this way to reuse the plan, requires some data copies
    input_array = np.zeros_like(data)
    output_array = np.zeros_like(data)
    fftForwardPlan = fftw3.Plan(input_array,output_array,'forward')
    fftBackwardPlan = fftw3.Plan(input_array,output_array,'backward')
    np.copyto(input_array, data)
    fftw3.guru_execute_dft(fftForwardPlan, input_array, output_array)
    np.copyto(input_array, output_array * filter)
    fftw3.guru_execute_dft(fftBackwardPlan, input_array, output_array)
    np.copyto(data, output_array)

    data = abs(data) / (data[0].size) #renormalize after transforms

    ###############################################################################################
    ###RESAMPLE and ZERO PAD
    if dyn < dxn:
        q = dxn
    else: q = dyn

    dataStack = zeros((nSlices,nyn, nxn))
    yc = int(float(yc) * float(q) / float(y)) + 1
    xc = int(float(xc) * float(q) / float(x)) + 1

    p1 = nyn / 2 - yc + 1
    p2 = p1 + q
    p3 = nxn / 2 - xc + 1
    p4 = p3 + q

    if p1 < 0 or p3 < 0 or p2 > nyn or p4 > nxn:
        print "Zero-padding failed, try increasing pixels per scan step."
        return

    ##downsample using bivariate spline interpolation
    for i in range(nSlices):
        x, y = np.linspace(0, data[i].shape[0], data[i].shape[0]),np.linspace(0, data[i].shape[1], data[i].shape[1])
        sp = interpolate.RectBivariateSpline(x, y, data[i])
        yn, xn = np.meshgrid(np.linspace(0, data[i].shape[1], q), np.linspace(0, data[i].shape[0], q))
        dataStack[i, p1:p2, p3:p4] = sp.ev(xn.flatten(), yn.flatten()).reshape(q,q)

    if low_pass:
        dataStack *= np.exp(-dist(nyn)**2 / 2. / (nyn / 4)**2)

    ###Zero data points below the noise floor
    dataStack *= (dataStack > nl)

    ####Return
    if fv: dataStack = dataStack[:,::-1,:]
    if fh: dataStack = dataStack[:,:,::-1]
    if tr: dataStack = transpose(dataStack, axes = (0,2,1))
    if fCCD: dataStack = transpose(rot90(transpose(dataStack)))
    if verbose: print "Sending data from Rank %i" %(comm.Get_rank())
    comm.send(dataStruct, dest = 0, tag = 12)
    comm.Send(np.ascontiguousarray(dataStack), dest = 0, tag = 12)




    return data
    


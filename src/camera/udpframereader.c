#include <Python.h>
#include <sys/socket.h>
#include <stdint.h>
#include <arpa/inet.h>
#include <assert.h>

// Copy data and swap bytes. Both pointers must be 64bit aligned and size must be multiple of 8
static void memcpy_ntohs(void *restrict dst, const void *restrict src, size_t n){
  // 
  assert(n & 0x7 == 0);
  assert((uintptr_t)dst & (uintptr_t)0x7 == 0);
  assert((uintptr_t)src & (uintptr_t)0x7 == 0);
  uint64_t * restrict to = (uint64_t * restrict)dst;
  uint64_t * restrict from = (uint64_t * restrict)src;
  while(n >= 8){
    *to = ((*from << 8) & 0xFF00FF00FF00FF00) | ((*from >> 8) & 0x00FF00FF00FF00FF);
    to++;
    from++;
    n-=8;
  }
}

static int guess_packet_n(int packet_small_n, int prev_packet_n, int n_packets){
  // The package_small_n is only 8 bits. We'll have to
  // estimate the higher bits
  int packet_n = 0;
  if(prev_packet_n == -1){
    // We don't know any better so let's just assume the 
    // highs are 0
    packet_n = packet_small_n;
  }else if(abs(packet_small_n - (prev_packet_n % 256)) < 100){
    // The new low bits are similar to the previous.
    // Keep the same high bits
    packet_n = 256 * (prev_packet_n/256) + (packet_small_n);
  }else if((packet_small_n - (prev_packet_n % 256)) > 100){
    // The new low bits are much higher than the previous.
    // We probably got a few delayed packages from a previous batch
    packet_n = 256*(prev_packet_n/256 - 1) + packet_small_n;
  }else if((packet_small_n - (prev_packet_n % 256)) < -100){
    // The new low bits are much lower than the previous.
    // We probably got a few packages from a new batch
    packet_n = 256*(prev_packet_n/256 + 1) + packet_small_n;
  }
  while(packet_n < 0){
    packet_n += 256;
  }
  while(packet_n >= n_packets){
    packet_n -= 256;
  }
  return packet_n;
}

static PyObject *
ufr_read_frame(PyObject *self, PyObject *args)
{
  // We're going to assume all packets are at most 8192 bytes
  const int packet_size = 8192;
  const int header_size = 8;
  int sock;
  int size;
  PyObject* out;
  PyObject* frame_n_out;
  int status = 0;
  // The frame we'll eventually return
  static uint8_t * restrict frame; 
  // The frame we're currently building
  int frame_n = -1;

  if (!PyArg_ParseTuple(args, "ii", &sock, &size))
    return NULL;
  Py_BEGIN_ALLOW_THREADS
  // The buffer where we first receive the data
  uint8_t * restrict buffer = malloc(packet_size);
    
  // The total number of packets we'll receive
  int n_packets = 1+(size-1)/(packet_size-header_size);
  // The number of packages we have received for this frame
  int packets_received = 0;
  int prev_packet_n = 0;
  while(1){
    ssize_t bytes_recv = recv(sock, buffer, packet_size, 0);
    if(bytes_recv < 0){
      // If we get an error during recv return None, error_value
      status = ntohs(bytes_recv);
      break;
    }
    int fn = ntohs(*(uint16_t*)(buffer+6));
    if(fn != frame_n){
      if(fn < frame_n && fn != 0){
	// We got some left overs from a previous frame. Ignore it.
	//	printf("Got old packets for frame %d\n", fn);
	continue;
      }
      /*
	if(packets_received){
      	printf("Dropping %d packets\n", packets_received);
	printf("Dropping frame %d. Starting frame %d\n", frame_n, fn);
	}
      */
      // Allocate a new frame
      free(frame);
      // We are allocating more than we need because sometimes the guess_packet_n
      // makes the wrong guess and thinks we have the last packet when we don't
      // and tries to write a full packet as the last packet.
      // If we just had the frame size allocated we would overflow.
      // In any case if the packet number is guessed wrong the frame will be gibberish
      frame = malloc(n_packets*(packet_size-header_size));
      frame_n = fn;
      packets_received = 0;
      prev_packet_n = -1;
    }

    int packet_small_n = buffer[0];
    int packet_n = guess_packet_n(packet_small_n, prev_packet_n, n_packets);
    prev_packet_n = packet_n;

    // Copy data appropriately, and swap endianess
    memcpy_ntohs(frame+packet_n*(packet_size-header_size),
		 buffer+header_size,bytes_recv-header_size);
    packets_received++;

    if(packets_received == n_packets){
      // We are done!
      break;
    }
  }  
  free(buffer);
  Py_END_ALLOW_THREADS
  if(status == 0){
    out = PyBuffer_FromReadWriteMemory(frame, size);
    frame_n_out = Py_BuildValue("i", frame_n);
    return PyTuple_Pack(2, out, frame_n_out);
  }else{
    // If we get an error during recv return None, error_value
    return PyTuple_Pack(2, Py_None, Py_BuildValue("i", status));
  }
}

static PyMethodDef UDPReaderMethods[] = {
  {"read_frame",  ufr_read_frame, METH_VARARGS,
   "read_frame(socket_fd, frame_nbytes)\n\n"
   "Returns the first frame read from the given socket file descriptor.\n"
   "The function will not return until it manages to successfully\n"
   "read a whole frame without dropping any packets.\n"
   "\n"
   "Parameters\n"
   "----------\n"
   "socket_fd : int\n"
   "    The socket file descriptor usually obtained from socket.fileno()\n"
   "frame_nbytes : int\n"
   "    The size, in bytes, of the frame to read.\n"
   "\n"
   "Returns\n"
   "----------\n"
   "frame : buffer\n"
   "    The frame that was read or None in case of failure.\n"
   "    N.B.: This buffer is freed() in subsequent call to this function!\n"
   "frame_number : int\n"
   "    The frame number, according to the packet headers.\n"
  },
  {NULL, NULL, 0, NULL}        /* Sentinel */
};

PyMODINIT_FUNC
initudpframereader(void)
{
  (void) Py_InitModule("udpframereader", UDPReaderMethods);
}


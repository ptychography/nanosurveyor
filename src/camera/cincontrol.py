import logging
import socket
import time
from PyQt4 import QtCore
from PyQt4 import QtNetwork

class CINController(QtCore.QThread):
    
    statusMessage = QtCore.pyqtSignal(str)
    newParam_int      = QtCore.pyqtSignal(str, str, int)
    newParam_float      = QtCore.pyqtSignal(str, str, float)
    newParam_str      = QtCore.pyqtSignal(str, str, str)
    startNewRun   = QtCore.pyqtSignal()

    def __init__(self, params):
        QtCore.QThread.__init__(self)

        # A copy of the params dictionary
        self.params = params
                        
    def createSendCommandsSocket(self):
        # A socket for sending commands to the camera
        self.cam_cmdsocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.cam_cmdsocket.bind(('localhost', self.params['cam_port']))
        self.cam_cmdaddr = (self.params['cam_ip'], self.params['cam_port'])
        self.status = "<font color=\"orange\"> CINController is sending commands to the camera on %d" %(self.params['cam_port']) + "</font>"
        self.statusMessage.emit(self.status)
        
    def createReceiveCommandsSocket_qtcpserver(self):
        # A socket for receiving commands from the frontend using QTCP server
        self.qtcpserver = QtNetwork.QTcpServer()
        self.qtcpserver.newConnection.connect(self.acceptRemoteConnection)
        print self.params['cin_ip'], self.params['stxm_port']
        hostAddress = QtNetwork.QHostAddress(self.params['cin_ip'])
        self.qtcpserver.listen(hostAddress, self.params['stxm_port'])
        self.status = "<font color=\"orange\"> CINController is listening to commands on %d, using QTCPServer" %(self.params['stxm_port']) + "</font>"
        self.statusMessage.emit(self.status)

    def acceptRemoteConnection(self):
        self.qtcpclient = self.qtcpserver.nextPendingConnection()
        self.qtcpclient.readyRead.connect(self.answer_command_qtcp)
        print "cin client: ", self.qtcpclient.peerAddress().toString(), self.qtcpclient.peerPort()
        
    def answer_command_qtcp(self):
        newline = "\n\r"
        while self.qtcpclient.canReadLine():
            cmd = self.qtcpclient.readLine()
            cmd = str(QtCore.QString(cmd))
            if len(cmd) == 0:
                return self.qtcpclient.send(self.response(""))

            #sub command
            cmd = str(QtCore.QString(cmd))
            resp_command = cmd

            try:
                index = cmd.index(" ")
                resp_command = cmd[0:index].strip()
                values = cmd[index+1:].strip()
                if resp_command == "setCapturePath":
                   if len(values.strip()) > 0:
                     self.statusMessage.emit("Save output to:" + values.strip())
                     #self.newParam_str.emit('cxiwrite', 'saveDir', values.strip())
                     #self.startNewRun.emit()

                if resp_command == "sendScanInfo":
                    #print cmd
                    p = values.split(",")
                    param_dict = {}
                    for pm in p:
                        update_param = pm.strip().split(" ")
                        param_dict[update_param[0]] = update_param[1]
                        self.status = "<font color=\"orange\"> Data from STXM: {}" %(param_dict) + "</font>"
                    self.statusMessage.emit("Parsing: " + str(param_dict))
                    self.newParam_float.emit('metadata', 'scanStep', float(param_dict['step_size_y'])*1000.0)

                    self.newParam_float.emit('metadata', 'pixelSizeX', float(30e-6))
                    self.newParam_float.emit('metadata', 'pixelSizeY', float(30e-6))


                    self.newParam_int.emit('metadata', 'scanNx', int(param_dict['num_pixels_x']))
                    self.newParam_int.emit('metadata', 'scanNy', int(param_dict['num_pixels_y']))
                    self.newParam_int.emit('background','nframes', int(param_dict['background_pixels_x']) * int(param_dict['background_pixels_y']))
                    self.newParam_float.emit('metadata', 'energy', float(param_dict['energy'])*1.602*1e-19)
                    self.startNewRun.emit()
            except:
                pass
          
            result = self.response(resp_command)
            self.qtcpclient.write(result)
            
    def response(self, cmd):
        return(b'Command: ' + cmd + b'\n\r')
                            
    def run(self):
        """Start the event loop."""
        self.status = "<font color=\"blue\"> CINController is starting the event loop </font>"
        self.statusMessage.emit(self.status)
        self.evtloop = QtCore.QEventLoop()
        self.evtloop.exec_()
                
    def stop(self):
        """
        Stop the event loop.
        """
        self.status = "<font color=\"red\"> CINController is stopping the event loop </font>"
        self.statusMessage.emit(self.status)
        try:
            self.evtloop.exit()
        except:
            print "There is no evtloop"
    
    def startCapture(self, val=None):
        """
        Old: trigger saving to file
        New: turn on streaming
        """
        logging.debug("turning the streaming mode on")
        self.streamon = True

    def stopCapture(self, val=None):
        """
        Old: trigger not saving to file
        New: turn off streaming
        """
        logging.debug("turning the streaming mode off")
        self.streamoff = False

    def setExp(self, val=None):
        """
        set the exposure time
        """
        pass

    def swtrig(self, val=None):
        """
        start external trigger mode
        """
        pass

        
    # Commands to the Camera
    # ----------------------
    def resetCounter(self, val=None):
        """
        reset the frame counter
        
        void MainWindow::on_pushButtonResetFrameCount_clicked()
        {
        QByteArray datagram;
        append16(datagram, 0x8001, 0x0106);
        commandSocket->writeDatagram(datagram.data(), datagram.size(), cameraControlIP, cameraControlPortNum);
        MyLib::msleep(1);
        }

        """
        self.cam_cmdsocket.sendto(msg, self.cam_cmdaddr)
        

    def uploadConfig(self, val=None):
        """
        upload configuration file to the camera.
        """
        pass


def main():
    cin = CINController()
    cin.start()

if __name__ == '__main__':
    main()

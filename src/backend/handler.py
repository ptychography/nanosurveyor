import logging
import time
import zmq
import numpy as np
import sys, os
import socket
import argparse
import datetime
import h5py
import imp
import subprocess
from scipy.misc import imsave

from .. import utils
from ..comm.zmqsocket import ZmqSocket

class Handler:
    """
    This is the main component of the backend. It runs an event loop that is handling data and control commands 
    and is distributing tasks across multiple modules. See docs/handler.pdf for details.
    """
    def __init__(self, args):
                
        # Configuration
        self.conf = imp.load_source('backend_conf', args.config)
        self.debug = args.debug or (self.conf.verbose >= 2)

        # Shared directories (Logging,  temporary files, ...)
        self.setStreamingDir()
        self.setLoggingDir()
        self.setDebuggingDir()
        self.setTemporaryDir()
        
        # Submodules
        self.slurm = not args.noslurm
        self.start_workers = not args.noworkers
        self.submodules = []
        
        # Configuration
        self.reset()
        self.parameters = None
        self.offset  = 0
        self.runcounter = 0
        self.backgroundCommands = {'startRun':0, 'finish': False}
        self.dataCommands       = {'startRun':0, 'finish': False}
        self.preprocCommands    = {'startRun':0}
        self.ptychoCommands     = {'startRun':0, 'interrupt':False}
        self.cxiwriteCommands   = {'startRun':0, 'finishRaw': False, 'finishProc':False}

        self.status = {'backgroundConfigured': False,
                       'backgroundFinished': False,
                       'dataConfigured': False,
                       'dataFinished': False,
                       'preprocConfigured': False,
                       'ptychoConfigured': False,
                       'ptychoInitialized': False,
                       'ptychoFinished': False,
                       'cxiwriteConfigured': False,
                       'cxiwriteFinished':False}

        self.setStatus()
        self.plist = ['experiment', 'metadata', 'cxiwrite', 'background', 'data', 'preproc', 'ptycho', 'grabber', 'backend']

    def setStreamingDir(self):
        """Set streaming directory to $HOME/.nanosurveyor/streaming/ ."""
        if '.nanosurveyor' not in os.listdir(os.environ['HOME']):
            os.mkdir(os.environ['HOME'] + '/.nanosurveyor')
        if 'streaming' not in os.listdir(os.environ['HOME'] + '/.nanosurveyor'):
            os.mkdir(os.environ['HOME'] + '/.nanosurveyor/streaming')
                
    def setLoggingDir(self):
        """Set logging directory to $HOME/.nanosurveyor/streaming/log/ ."""
        if 'log' not in os.listdir(os.environ['HOME'] + '/.nanosurveyor/streaming'):
            os.mkdir(os.environ['HOME'] + '/.nanosurveyor/streaming/log')
        self.logdir = os.environ['HOME'] + '/.nanosurveyor/streaming/log'

    def setDebuggingDir(self):
        """Set debugging directory to $HOME/.nanosurveyor/streaming/debug/ ."""
        if 'debug' not in os.listdir(os.environ['HOME'] + '/.nanosurveyor/streaming'):
            os.mkdir(os.environ['HOME'] + '/.nanosurveyor/streaming/debug')
        self.debugdir = os.environ['HOME'] + '/.nanosurveyor/streaming/debug'

    def setTemporaryDir(self):
        """Set temporary directory to $HOME/.nanosurveyor/streaming/tmp/ ."""
        if 'tmp' not in os.listdir(os.environ['HOME'] + '/.nanosurveyor/streaming'):
            os.mkdir(os.environ['HOME'] + '/.nanosurveyor/streaming/tmp')
        self.tmpdir = os.environ['HOME'] + '/.nanosurveyor/streaming/tmp'
        
    def createInputDataSocket(self):
        """Connects a data socket (SUB) in order to receive raw data."""
        self.input_socket = ZmqSocket(zmq.SUB)
        self.input_socket.connect_socket("tcp://%s:%d" %(self.conf.input_ip, self.conf.input_port))
        self.input_socket.subscribe("rawframe")
        self.input_socket.register()
        print utils.INFO +  "Backend is reading data from %s on port %d " %(self.conf.input_ip, self.conf.input_port)
        
    def createWorkerDataSockets(self):
        """Connects multiple data sockets (DEALER) in order to dibstribute worker tasks across modules (Background, Preproc, Data, Ptycho)."""
        self.preproc_socket = ZmqSocket(zmq.DEALER)
        self.preproc_socket.set_identity('preproc')
        self.preproc_socket.bind("tcp://*:%d" %(self.conf.preproc_port))
        self.preproc_socket.register()
        print utils.INFO + "Backend is connected to a worker for preprocessing of data at %s on port %d" %(self.conf.preproc_ip, self.conf.preproc_port)
                                           
        self.background_socket = ZmqSocket(zmq.DEALER)
        self.background_socket.set_identity('background')
        self.background_socket.bind("tcp://*:%d" %(self.conf.background_port))
        self.background_socket.register()
        print utils.INFO + "Backend is connected to a worker for accumulation of background at %s on port %d" %(self.conf.background_ip, self.conf.background_port)

        self.data_socket = ZmqSocket(zmq.DEALER)
        self.data_socket.set_identity('data')
        self.data_socket.bind("tcp://*:%d" %(self.conf.data_port))
        self.data_socket.register()
        print utils.INFO + "Backend is connected to a worker for accumulation of data at %s on port %d" %(self.conf.data_ip, self.conf.data_port)
        
    def createWorkerReadSockets(self):
        """Binds a ptycho read socket (DEALER) in order to receive reconstruction."""
        self.ptycho_readsocket = ZmqSocket(zmq.DEALER)
        self.ptycho_readsocket.set_identity('ptycho')
        self.ptycho_readsocket.bind("tcp://*:%d" %(self.conf.ptycho_readport))
        self.ptycho_readsocket.register()
        print utils.INFO + "Backend is reading data from  a worker for ptychographic reconstruction at %s on port %d" %(self.conf.ptycho_ip, self.conf.ptycho_readport)

    def createWorkerSendSockets(self):
        """Binds different send socket (PUB) in order to send information to workers."""
        self.ptycho_sendsocket = ZmqSocket(zmq.PUB)
        self.ptycho_sendsocket.bind("tcp://*:%d" %self.conf.ptycho_sendport)
        print utils.INFO + "Backend is sending data for ptycho worker on port %d" %self.conf.ptycho_sendport

        self.cxiwrite_sendsocket = ZmqSocket(zmq.PUB)
        self.cxiwrite_sendsocket.bind("tcp://*:%d" %self.conf.cxiwrite_sendport)
        print utils.INFO + "Backend is sending data for cxiwrite worker on port %d" %self.conf.cxiwrite_sendport
        
    def createWorkerControlSockets(self):
        """Binds control sockets (REP) in order to distribute control commands across modules (Background, Data, Ptycho)."""
        self.background_ctrlsocket = ZmqSocket(zmq.REP)
        self.background_ctrlsocket.bind("tcp://*:%d" %(self.conf.background_ctrlport))
        self.background_ctrlsocket.register()
        print utils.INFO + "Backend is waiting for requests from the background worker on port %d" %(self.conf.background_ctrlport)

        self.data_ctrlsocket = ZmqSocket(zmq.REP)
        self.data_ctrlsocket.bind("tcp://*:%d" %(self.conf.data_ctrlport))
        self.data_ctrlsocket.register()
        print utils.INFO + "Backend is waiting for requests from the data worker on port %d" %(self.conf.data_ctrlport)

        self.preproc_ctrlsocket = ZmqSocket(zmq.REP)
        self.preproc_ctrlsocket.bind("tcp://*:%d" %(self.conf.preproc_ctrlport))
        self.preproc_ctrlsocket.register()
        print utils.INFO + "Backend is waiting for requests from a preproc worker on port %d" %(self.conf.preproc_ctrlport)
        
        self.ptycho_ctrlsocket = ZmqSocket(zmq.REP)
        self.ptycho_ctrlsocket.bind("tcp://*:%d" %(self.conf.ptycho_ctrlport))
        self.ptycho_ctrlsocket.register()
        print utils.INFO + "Backend is waiting for requests from the ptycho worker on port %d" %(self.conf.ptycho_ctrlport)

        self.cxiwrite_ctrlsocket = ZmqSocket(zmq.REP)
        self.cxiwrite_ctrlsocket.bind("tcp://*:%d" %(self.conf.cxiwrite_ctrlport))
        self.cxiwrite_ctrlsocket.register()
        print utils.INFO + "Backend is waiting for requests from the cxiwrite worker on port %d" %(self.conf.cxiwrite_ctrlport)

        self.frontend_ctrlsocket = ZmqSocket(zmq.REP)
        self.frontend_ctrlsocket.bind("tcp://*:%d" %(self.conf.frontend_ctrlport))
        self.frontend_ctrlsocket.register()
        print utils.INFO + "Backend is waiting for requests from the frontend on port %d" %self.conf.frontend_ctrlport
        
    def createViewingDataSocket(self):
        """Connects data socket (PUSH) in order to send data back to the frontend."""
        self.view_socket = ZmqSocket(zmq.PUSH)
        self.view_socket.connect_socket("tcp://%s:%d" %(self.conf.input_ip, self.conf.view_port))
        print utils.INFO + "Backend is providing data for viewing on port %d" %self.conf.view_port

    def startWorkers(self):
        """Launch submodules"""
        if self.slurm:
            self.startSlurmWorkers()
        else:
            self.startPythonWorkers()
        
    def startSlurmWorkers(self):
        """Launch submodules as SLURM jobs."""
        slurm_cmd = "sbatch --nodelist %s -p %s --job-name %s -o %s/slurm_%s.out "
        with open(self.tmpdir + '/mpi_preproc.sh', 'w') as f:
            f.write("#!/bin/sh\n#SBATCH --ntasks=%d\nmpirun nspreproc -i %s -p %s -l %s" 
                    %(self.conf.nworkers, self.conf.host_ip, self.conf.preproc_port, self.logdir))
        with open(self.tmpdir + '/mpi_ptycho.sh', 'w') as f:
            f.write("#!/bin/sh\n#SBATCH --ntasks=%d\nmpirun nsptycho -i %s -r %s -s %s -l %s" 
                    %(self.conf.nptychoworkers, self.conf.host_ip, self.conf.ptycho_sendport, self.conf.ptycho_readport, self.logdir))
        commands = []
        commands.append(slurm_cmd %(self.conf.preproc_ip, self.conf.slurm_partition, "preproc", self.logdir, "preproc") + self.tmpdir + '/mpi_preproc.sh')
        commands.append(slurm_cmd %(self.conf.background_ip, self.conf.slurm_partition, "background", self.logdir, "background") + "nsbackground -i %s -p %s -c %s -l %s" 
                        %(self.conf.host_ip, self.conf.background_port, self.conf.background_ctrlport, self.logdir))
        commands.append(slurm_cmd %(self.conf.data_ip, self.conf.slurm_partition, "data", self.logdir, "data") + "nsdata -i %s -p %s -c %s -l %s" 
                        %(self.conf.host_ip, self.conf.data_port, self.conf.data_ctrlport, self.logdir))
        if self.conf.use_mpi:
            commands.append(slurm_cmd %(self.conf.ptycho_ip, self.conf.slurm_partition, "ptycho", self.logdir, "ptycho") + self.tmpdir + '/mpi_ptycho.sh')
        else:
            commands.append(slurm_cmd %(self.conf.ptycho_ip, self.conf.slurm_partition, "ptycho", self.logdir, "ptycho") + "nsptycho -i %s -r %s -s %s -l %s" 
                            %(self.conf.host_ip, self.conf.ptycho_sendport, self.conf.ptycho_readport, self.logdir))
        commands.append(slurm_cmd %(self.conf.cxiwrite_ip, self.conf.slurm_partition, "cxiwrite", self.logdir, "cxiwrite") + "nscxiwrite -i %s -r %s -c %s -l %s" 
                        %(self.conf.host_ip, self.conf.cxiwrite_sendport, self.conf.cxiwrite_ctrlport, self.logdir))            
        for cmd in commands:
            self.dprint("This jobs should be started: " + cmd)
            if self.start_workers:
                self.dprint("Submiting batch job:" + cmd)
                os.system(cmd)

    def startPythonWorkers(self):
        """Launch submodules as Python processes."""
        commands = []
        ssh_cmd = ''
        commands.append(ssh_cmd + "mpirun -n %d nspreproc -i %s -p %s -l %s"
                        %(self.conf.nworkers, self.conf.host_ip, self.conf.preproc_port, self.logdir))
        commands.append(ssh_cmd + "nsbackground -i %s -p %s -c %s -l %s" 
                        %(self.conf.host_ip, self.conf.background_port, self.conf.background_ctrlport, self.logdir))
        commands.append(ssh_cmd + "nsdata -i %s -p %s -c %s -l %s" 
                        %(self.conf.host_ip, self.conf.data_port, self.conf.data_ctrlport, self.logdir))
        if self.conf.use_mpi:
            commands.append(ssh_cmd + "mpirun -n %d nsptycho -i %s -r %s -s %s -l %s"
                            %(self.conf.nptychoworkers, self.conf.host_ip, self.conf.ptycho_sendport, self.conf.ptycho_readport, self.logdir))
        else:
            commands.append(ssh_cmd + "nsptycho -i %s -r %s -s %s -c %s -l %s" 
                            %(self.conf.host_ip, self.conf.ptycho_sendport, self.conf.ptycho_readport, self.conf.ptycho_ctrlport, self.logdir))
        commands.append(ssh_cmd + "nscxiwrite -i %s -r %s -c %s -l %s" 
                        %(self.conf.host_ip, self.conf.cxiwrite_sendport, self.conf.cxiwrite_ctrlport, self.logdir))
        for cmd in commands:
            if self.debug:
                cmd +=  ' -d'
            self.dprint("This jobs should be started: " + cmd)
            if self.start_workers:
                self.dprint("Starting Python process:" + cmd)
                date =  str(datetime.date.today()).replace('-', '')
                outfile = open(self.logdir + '/%s_main_%s.out' %(cmd.split(' ')[0], date), 'a', buffering=1)
                errfile = open(self.logdir + '/%s_main_%s.err' %(cmd.split(' ')[0], date), 'a', buffering=1)
                self.submodules.append(subprocess.Popen(cmd.split(), shell=False, stdout=outfile, stderr=errfile))

    def reset(self):
        """"Resets flags/counter, triggered when new run is requested."""
        self.meanBackground = None
        self.stdBackground  = None
        self.wait_for_background = False
        self.isRecording = False
        self.nframes = 0

    def updateStatus(self, sdict):
        """Updates the status of given submodules"""
        for k,v in sdict.iteritems():
            self.status[k] = v
        self.setStatus()
        
    def updateRunNr(self):
        """Updates the run number such that submodules will know about it."""
        self.runcounter += 1
        self.backgroundCommands['startRun'] = self.runcounter
        self.dataCommands['startRun']       = self.runcounter
        self.preprocCommands['startRun']    = self.runcounter
        self.ptychoCommands['startRun']     = self.runcounter
        self.cxiwriteCommands['startRun']   = self.runcounter

    def setParameters(self, params):
        """Overwrite parameters for new run."""
        self.parameters = params
        for p in self.plist:
            setattr(self, p, self.parameters[p])
        self.dataFrames = self.metadata['scanNx'] * self.metadata['scanNy']
        self.setPositions()
        
    def setStatus(self):
        """Overwrite current status of all submoduls."""
        for p in self.status:
            setattr(self, p, self.status[p])
        
    def setPositions(self):
        """Defines array of positions based on 'scanNx', 'scanNy' and ''scanStep'."""
        x = np.arange(0, self.metadata['scanNx']*self.metadata['scanStep'], self.metadata['scanStep'])
        y = np.arange(0, self.metadata['scanNy']*self.metadata['scanStep'], self.metadata['scanStep'])
        xx, yy = np.meshgrid(x, y)
        self.positions = 1e-9*np.vstack([xx.flat, yy.flat]).T

    def isBackground(self, fid):
        """Returns True if current frame (based on fid) is a background frame 
        (skipping the first background frame, because it is always bad)."""
        return (int(fid) >= self.offset + int(self.background['dropFirstFrame'])) and (int(fid) <= (self.offset + self.background['nframes'] - 1))

    def isLastBackground(self, fid):
        """Returns True if current frame (based on fid) is the last background frame."""
        return (int(fid) == (self.offset + self.background['nframes'] - 1))

    def loadBackground(self):
        """Loads an old background from the temporary directory."""
        if 'background.h5' in os.listdir(self.tmpdir):
            with h5py.File(self.tmpdir + '/background.h5', 'r') as file:
                self.meanBackground = file["data/mean"][:].astype(np.uint16)
                self.stdBackground  = file["data/std"][:].astype(np.uint16)

    def isData(self, fid):
        """Returns True if current frame (based on fid) is a data frame."""
        above_lower_bound = (int(fid) >= (self.offset + self.background['nframes']))
        below_upper_bound = (int(fid) <= (self.offset + self.background['nframes'] + self.dataFrames - 1))
        return (above_lower_bound and below_upper_bound)
    
    def index(self, fid):
        """Returns data/background index."""
        if self.isData(fid):
            return int(fid) - (self.offset + self.background['nframes'])
        if self.isBackground(fid):
            return int(fid) - self.offset

    def posx(self, fid):
        """Returns current (based on fid) scanning position in x [nm]."""
        return self.positions[self.index(fid),0]*1e9
    
    def posy(self, fid):
        """Returns current (based on fid) scanning position in x [nm]."""
        return self.positions[self.index(fid),1]*1e9

    def dprint(self, msg):
        """Print wrapper that prints only in debug mode."""
        if self.debug:
            print msg

    def ddump(self, img, label='test'):
        """Dump images to PNG file when in debug mode."""
        if self.debug:
            if np.issubdtype(img.dtype, np.complex):
                imsave(self.debugdir + '/' + label+'_abs'+'.png', np.abs(img))
                imsave(self.debugdir + '/' + label+'_phase'+'.png', np.angle(img))
            elif np.issubdtype(img.dtype, np.float):
                imsave(self.debugdir + '/' + label+'.png', img)
            else:
                print "Could not dump image of type ", img.dtype

    def run(self):
        """This triggers the event loop."""
        nraw, nclean = 0,0
        update = 100
        traw0 = tclean0 = time.time()
        self.isRunning = True
        print utils.START + "Backend is starting the event loop"

        # Start the event loop
        while self.isRunning:
            try:
                # Receive requests from the frontend
                if self.frontend_ctrlsocket.activity():
                    request, jdict = self.frontend_ctrlsocket.recv_cmd()
		    self.dprint("The frontend has sent a request: %s" %request)
                    if request == 'newRun':
			self.dprint("The frontend has send a json dict with parameters: ")
                        self.dprint(jdict)
                        self.reset()
                        self.setParameters(jdict)
                        self.loadBackground()
                        self.updateRunNr()
                        self.isRecording = True
                        self.frontend_ctrlsocket.send_cmd('config', self.status)
                    if request == 'interrupt':
                        self.dprint('Received request from frontend to interrupt current run')
                        self.ptychoCommands['interrupt'] = True
                        self.isRecording = False
                        self.frontend_ctrlsocket.send_cmd('update', self.status)    
                    if request == 'getUpdate':
                        self.frontend_ctrlsocket.send_cmd('update', self.status)  

                # Receive raw frames from the frontend
                if (not self.wait_for_background) and (self.input_socket.activity()):
                    self.input_socket.recv() # drop key
                    fid, frame = self.input_socket.recv_multipart()
                    nraw += 1
                    self.dprint("Received frame with id = %s" %fid)
                    if self.isRecording:
                        if self.isBackground(fid):
                            self.dprint("Send frame to background worker, id = %s" %fid)
                            self.background_socket.send_multipart([fid, frame])
                            self.cxiwrite_sendsocket.send('rawframe_dark', zmq.SNDMORE)
                            self.cxiwrite_sendsocket.send_multipart([str(self.index(fid)), frame])
                        if self.isLastBackground(fid):
                            time.sleep(.1) # This is to make sure the last background is added before the mean is returned
                            self.dprint("Send request for mean/std to background worker, id = %s" %fid)
                            self.backgroundCommands['finish'] = True
                            self.wait_for_background = True
                            self.nframes = 0
                        if self.isData(fid):
                            self.dprint("Send frame to preproc worker, id = %s" %fid)
                            self.preproc_socket.send('clean', zmq.SNDMORE)
                            self.preproc_socket.send_frame_and_background(fid, frame, self.meanBackground, self.stdBackground)
                            self.cxiwrite_sendsocket.send('rawframe_data', zmq.SNDMORE)
                            self.cxiwrite_sendsocket.send_multipart([str(self.index(fid)), frame])
                            
                    else:
                        self.offset = int(fid) + 1 # Next expected frame nr.
                        self.dprint("Send frame to preproc worker, id = %s" %fid)
                        self.preproc_socket.send_multipart(['descramble', fid, frame])

                # Receive request reply from background worker
                if self.background_ctrlsocket.activity():
                    request, status = self.background_ctrlsocket.recv_cmd()
                    self.updateStatus(status)
                    if request == 'getConfig':
                        self.background_ctrlsocket.send_cmd('config', self.parameters)
                    if request == 'getUpdate':
                        self.background_ctrlsocket.send_cmd('update', self.backgroundCommands)
                        
                # Receive data from the background worker
                if self.background_socket.activity():
                    self.dprint("Received data from background worker")
                    background = self.background_socket.recv_array()
                    self.meanBackground = background[0]
                    self.stdBackground  = background[1]
                    self.wait_for_background = False
                    self.backgroundCommands['finish'] = False

                # Receive request reply from data worker
                if self.data_ctrlsocket.activity():
                    request, status = self.data_ctrlsocket.recv_cmd()
                    self.updateStatus(status)
                    if request == 'getConfig':
                        self.data_ctrlsocket.send_cmd('config', self.parameters)
                    if request == 'getUpdate':
                        self.data_ctrlsocket.send_cmd('update', self.dataCommands)

                # Receive data from the data worker
                if self.data_socket.activity():
                    key = self.data_socket.recv()
                    self.dprint("Received data from data worker: %s" %key)
                    if key == 'illumination':
                        illumination = self.data_socket.recv_array()
                        self.ptycho_sendsocket.send("illumination", zmq.SNDMORE)
                        self.ptycho_sendsocket.send_array(illumination)
                        self.cxiwrite_sendsocket.send("illumination", zmq.SNDMORE)
                        self.cxiwrite_sendsocket.send_array(illumination)
                        self.dataCommands['finish'] = False
                    elif key == 'image':
                        image = self.data_socket.recv_array()
                        self.view_socket.send("stxm", zmq.SNDMORE)
                        self.view_socket.send_array(image.reshape((self.metadata['scanNy'], self.metadata['scanNx'])))

                # Receive request reply from a preproc worker
                if self.preproc_ctrlsocket.activity():
                    request, status = self.preproc_ctrlsocket.recv_cmd()
                    self.updateStatus(status)
                    if request == 'getConfig':
                        self.preproc_ctrlsocket.send_cmd('config', self.parameters)
                    if request == 'getUpdate':
                        self.preproc_ctrlsocket.send_cmd('update', self.preprocCommands)
                        print "Handler sent update", self.preprocCommands
                        
                # Receive clean data from a preproc worker
                if self.preproc_socket.activity():
                    fid, cleanframe = self.preproc_socket.recv_frame()
                    self.dprint("Received data from a preproc worker, id = %d" %int(fid))
                    nclean += 1

                    if self.isRecording and self.isData(fid):
                        self.dprint("Sending clean frame to ptycho worker, index = %d" %self.index(fid))
                        self.ptycho_sendsocket.send("frame", zmq.SNDMORE)
                        self.ptycho_sendsocket.send_frame(str(self.index(fid)), cleanframe)
                        self.data_socket.send_frame(str(self.index(fid)), cleanframe)
                        self.nframes += 1
                        self.dprint("Sending clean frame to the frontend, index = %d" %self.index(fid))
                        self.view_socket.send("cleanframe", zmq.SNDMORE)
                        self.view_socket.send_frame_and_pos(str(self.index(fid)), cleanframe, 
                                                            "%.2f nm" %self.posx(fid), 
                                                            "%.2f nm" %self.posy(fid))
                        self.dprint("Sending clean frame to cxiwriter, index = %d" %self.index(fid))
                        self.cxiwrite_sendsocket.send("frame", zmq.SNDMORE)
                        self.cxiwrite_sendsocket.send_frame(str(self.index(fid)), cleanframe)
                        self.ddump(cleanframe, label='cleanframe%03d' %self.index(fid))
                        
                        # Tell data worker that we have enough data frames for estimating the illumination
                        if (self.nframes == (self.background['nframes'] - 1)):
                            self.dataCommands['finish'] = True
                            
                        # Tell cxiwrite worker to close raw/processed files
                        #if (self.nframes == (self.dataFrames - 1)):
                        #    self.cxiwriteCommands['finishRaw'] = True
                        #    self.cxiwriteCommands['finishProc'] = True
                            
                    else:
                        self.dprint("Sending clean frame to the frontend, id = %s" %fid)
                        self.view_socket.send("cleanframe", zmq.SNDMORE)
                        self.view_socket.send_frame_and_pos(fid, cleanframe, "--", "--")

                # Receive request from ptycho worker
                if self.ptycho_ctrlsocket.activity():
                    request, status = self.ptycho_ctrlsocket.recv_cmd()
                    self.updateStatus(status)
                    if request == 'getConfig':
                        self.ptycho_ctrlsocket.send_cmd('config', self.parameters)
                    elif request == 'getUpdate':
                        self.ptycho_ctrlsocket.send_cmd('update', self.ptychoCommands)

                # Receiving images from the ptycho worker
                if self.ptycho_readsocket.activity():
                    self.dprint("Received data from the ptycho worker")
                    image, illumination, iteration, moduleError, overlapError = self.ptycho_readsocket.recv_recons()
                    self.view_socket.send("reconstruction", zmq.SNDMORE)
                    self.view_socket.send_recons(image, illumination, iteration, moduleError, overlapError)

                # Receive request from cxiwrite worker
                if self.cxiwrite_ctrlsocket.activity():
                    request, status = self.cxiwrite_ctrlsocket.recv_cmd()
                    self.updateStatus(status)
                    if request == 'getConfig':
                        self.cxiwrite_ctrlsocket.send_cmd('config', self.parameters)
                    elif request == 'getUpdate':
                        self.cxiwrite_ctrlsocket.send_cmd('update', self.cxiwriteCommands)

                # Update reading speed of raw frames
                if (nraw == update):
                    traw1 = time.time()
                    self.dprint(utils.RATE + "Reading raw data at %.2fHz" %(nraw/(traw1-traw0)))
                    traw0 = traw1
                    nraw = 0
                
                # Update reading speed of clean frames
                if (nclean == update):
                    tclean1 = time.time()
                    self.dprint(utils.RATE + "Reading clean data at %.2fHz" %(nclean/(tclean1-tclean0)))
                    tclean0 = tclean1
                    nclean = 0

            except zmq.ZMQError as e:
                print "An error has occured: %s \n Stopping the backend ..." %e
                self.stop()

    def stopWorkers(self):
        """Stop Slurm/Python workers"""
        if self.slurm:
            self.stopSlurmWorkers()
        else:
            self.stopPythonWorkers()

    def stopSlurmWorkers(self):
        """Cancel batch jobs"""
        slurm_cmd = "scancel -n %s"
        workers = ["preproc", "background", "data", "ptycho", "cxiwrite"]
        for w in workers:
            self.dprint("Cancelling batch job: " + w)
            os.system(slurm_cmd %w)

    def stopPythonWorkers(self):
        """Kill Python processes"""
        for w in self.submodules:
            self.dprint("Killing Python process: " +  str(w.pid))
            subprocess.call(['kill', '-9', '%d' %w.pid])
 
    def closeSockets(self):
        """Closing data, read, send and ctrl sockets"""
        self.input_socket.close()
        self.preproc_socket.close()
        self.background_socket.close()
        self.data_socket.close()
        self.ptycho_readsocket.close()
        self.ptycho_sendsocket.close()
        self.view_socket.close()
        self.background_ctrlsocket.close()
        self.data_ctrlsocket.close()
        self.ptycho_ctrlsocket.close()
        self.cxiwrite_ctrlsocket.close()

    def stop(self):
        """Stop the event loop and  workers, close sockets"""
        print utils.STOP + "Backend is stopping the event loop"
        self.isRunning = False
        self.stopWorkers()
        self.closeSockets()
            
    def __del__(self):
        """Close sockets on deletion"""
        self.closeSockets()

def parse_cmdl_args():
    """Parsing command line arguments"""
    parser = argparse.ArgumentParser(description='nsbackend')
    parser.add_argument("config", type=str, help="Configuration file")
    parser.add_argument("--noslurm",   action='store_true', help='Do not use SLURM for worker processes.')
    parser.add_argument("--noworkers", action='store_true', help='Do not start the worker processes')
    parser.add_argument("--debug",     action='store_true', help='Debugging mode')
    if(len(sys.argv) == 1):
        parser.print_help()
    return parser.parse_args()

def main():
    """Entry point to start backend handler."""
    args = parse_cmdl_args()
    handler = Handler(args)
    handler.createInputDataSocket()
    handler.createWorkerDataSockets()
    handler.createWorkerReadSockets()
    handler.createWorkerSendSockets()
    handler.createWorkerControlSockets()
    handler.createViewingDataSocket()
    handler.startWorkers()
    try:
        handler.run()
    except KeyboardInterrupt:
        handler.stop()
    except Exception as e:
        print e
        handler.stop()

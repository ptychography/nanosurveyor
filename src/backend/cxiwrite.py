import numpy as np
import zmq
import time
import h5py
import sys
import datetime
import argparse
import json

from .. import utils
from ..io.cxiwriter import CXIWriter
from ..comm.zmqsocket import ZmqSocket

class Cxiwrite:
    """Writing collected/procesed data to file."""
    def __init__(self, args):
        self.createSockets(args)
        self.isWaitingForReply = False
        self.metalist = ['scanNx', 'scanNy', 'scanStep', 'arraySize', 'pixelSizeX', 'pixelSizeY', 'distance', 'energy']
        self.bglist = ['nframes']
        self.writelist = ['saveDir', 'saveRaw', 'saveProcessed', 'saveReconstructed']
        self.slist = ['Configured', 'Finished']
        self.startRun = False
        self.currentRun = None
        self.finishRaw = False
        self.finishProc =  False
        self.finishRecons = False
        self.isOpenRaw = False
        self.isOpenProc = False
        self.isOpenRecons = False
        self.isConfigured = False
        self.isFinished = False
        self.subscribed = []

    def createSockets(self, args):
        """Connects a read (SUB) and control (REQ) socket in order to communicate with handler."""        
        self.read_socket = ZmqSocket(zmq.SUB)
        self.read_socket.connect_socket("tcp://%s:%d" %(args.handlerip, args.readport))
        self.read_socket.register()
        print utils.INFO + "Cxiwrite worker is waiting for data on port %d" %args.readport

        self.control_socket = ZmqSocket(zmq.REQ)
        self.control_socket.connect_socket("tcp://%s:%d" %(args.handlerip, args.ctrlport))
        self.control_socket.register()
        print utils.INFO + "Cxiwrite worker is waiting for commands on port %d" %args.ctrlport
                
    def setCommands(self, commands):
        """Updates commands received from the handler."""
        for k,v in commands.iteritems():
            setattr(self, k, v)

    def getStatus(self):
        """Return the current status of the cxiwrite module."""
        status = {}
        for s in self.slist:
            status['cxiwrite' + s] = getattr(self, 'is' + s)
        return status
        
    def setParameters(self, params):
        """Overwrites config parameters givenv a JSON dict"""
        for p in self.metalist:
            setattr(self, p, params['metadata'][p])
        for p in self.bglist:
            setattr(self, p, params['background'][p])
        for p in self.writelist:
            setattr(self, p, params['cxiwrite'][p])
        self.setPositions()
        self.cornerPos = [self.arraySize/2 * self.pixelSizeX, self.arraySize/2 * self.pixelSizeY, self.distance]
        self.ndata = self.scanNx * self.scanNy
        self.nrows = 11640
        self.nadc  = 192

    def setPositions(self):
        """Defines array of positions based on 'scanNx', 'scanNy' and ''scanStep'."""
        x = np.arange(0, self.scanNx*self.scanStep, self.scanStep)
        y = np.arange(0, self.scanNy*self.scanStep, self.scanStep)
        xx, yy = np.meshgrid(x, y)
        self.positions = 1e-9*np.vstack([xx.flat, yy.flat, np.zeros(xx.shape).flat]).T

    def saveParameters(self, jdict):
        """Save parameters in a configuration file."""
        paramfile = self.saveDir + '/%05d_param.conf' %(self.currentRun)
        with open(paramfile, 'w') as json_file:
            json.dump(jdict, json_file, sort_keys=True, indent=4, separators=(',', ': '))

    def startNewRun(self):
        """Returns True if a new run has been started."""
        return (self.startRun and (self.startRun != self.currentRun))

    def configureWriter(self):
        """Configures and initialized a CXI writer."""
        # Subscribe to different datasets based on configuration
        if self.saveRaw:
            self.subscribed.append('rawframe_dark')
            self.subscribed.append('rawframe_data')
        if self.saveProcessed:
            self.subscribed.append('frame')
            self.subscribed.append('illumination')
        if self.saveReconstructed:
            self.subscribed.append('recons')
        for k in self.subscribed:
            print utils.INFO + "Cxiwriter worker is subscribed to ", k
            self.read_socket.subscribe(k)

        # Initialize a CXI writer to write raw data
        if self.saveRaw:
            filename = self.saveDir + '/%05d_raw.cxi' %self.currentRun
            self.cxifile_raw = CXIWriter(filename)
            self.cxifile_raw.write_metadata(self.energy, self.distance, 
                                            self.pixelSizeX, self.pixelSizeY, self.cornerPos)
            self.cxifile_raw.write_translations(self.positions)
            self.cxifile_raw.write_dataframes(shape=(self.ndata,   self.nrows, self.nadc), dtype='uint16')
            self.cxifile_raw.write_darkframes(shape=(self.nframes, self.nrows, self.nadc), dtype='uint16')
            self.isOpenRaw = True
            print utils.INFO + "Cxiwrite worker has initialized writing raw data"

        # Initialize a CXI writer to write processed data
        if self.saveProcessed:
            filename = self.saveDir + '/%05d_proc.cxi' %self.currentRun
            self.cxifile_proc = CXIWriter(filename)
            self.cxifile_proc.write_metadata(self.energy, self.distance, 
                                             self.pixelSizeX, self.pixelSizeY, self.cornerPos)
            self.cxifile_proc.write_translations(self.positions)
            self.cxifile_proc.write_dataframes(shape=(self.ndata, self.arraySize, self.arraySize))
            self.isOpenProc = True
            print utils.INFO + "Cxiwrite worker has initialized writing processed data"

        # Initialize a CXI writer to write reconstructed data
        if self.saveReconstructed:
            filename = self.saveDir + '/%05d_recons.cxi' %self.currentRun
            self.cxifile_recons = CXIWriter(filename)
            self.cxifile_recons.write_metadata(self.energy, self.distance, 
                                               self.pixelSizeX, self.pixelSizeY, self.cornerPos)
            print utils.INFO + "Cxiwrite worker has initialized writing reconstructed data"

    def run(self):
        """This triggers the event loop."""
        self.isRunning = True
        print utils.START + "Write worker is starting the event loop"

        # Start the event loop
        while self.isRunning:

            # Send request over control socket (ask what to do)
            if not self.isWaitingForReply:
                if self.startNewRun():
                    self.control_socket.send_cmd('getConfig', self.getStatus())
                    self.currentRun = self.startRun
                else:
                    self.control_socket.send_cmd('getUpdate', self.getStatus())
                self.isWaitingForReply = True

            # Receive request reply over control socket (telling the module what to do)
            if self.control_socket.activity():
                cmd, jdict = self.control_socket.recv_cmd()
                if jdict is not None:
                    if cmd == 'config':
                        self.setParameters(jdict)
                        self.saveParameters(jdict)
                        self.configureWriter()
                        self.isConfigured = True
                    if cmd == 'update':
                        self.setCommands(jdict)
                self.isWaitingForReply = False

            # Receive new data
            if self.read_socket.activity() and self.isConfigured:
                key = self.read_socket.recv()
                if key == "rawframe_dark":
                    fid, rawframe = self.read_socket.recv_multipart()
                    self.cxifile_raw.insert_darkframe(int(fid), np.fromstring(rawframe, '<u2').reshape(self.nrows, self.nadc))
                    self.cxifile_raw.flush()
                if key == "rawframe_data":
                    fid, rawframe = self.read_socket.recv_multipart()
                    self.cxifile_raw.insert_dataframe(int(fid), np.fromstring(rawframe, '<u2').reshape(self.nrows, self.nadc))
                    self.cxifile_raw.flush()
                elif key == "frame":
                    print "Saving frame ", int(fid)
                    fid, frame = self.read_socket.recv_frame()
                    self.cxifile_proc.insert_dataframe(int(fid), frame)
                    self.cxifile_proc.flush()
                elif key == "illumination":
                    print "Saving illumination"
                    illumination_array = self.read_socket.recv_array()
                    self.cxifile_proc.write_illumination(illumination_array[0])
                    self.cxifile_proc.write_Fillumination_mask(illumination_array[1].astype(np.bool))
                    self.cxifile_proc.write_Fillumination_intensities(illumination_array[2].real)
                    self.cxifile_proc.flush()
                elif key == "recons":
                    print "Saving reconstruction"

            # Close CXI files
            if self.finishRaw and self.saveRaw and self.isOpenRaw:
                print utils.INFO + "Closing CXI file with raw data"
                self.cxifile_raw.close()
                self.isOpenRaw = False
            if self.finishProc and self.saveProcessed and self.isOpenProc:
                print utils.INFO + "Closing CXI file with processed data"
                self.cxifile_proc.close()
                self.isOpenProc = False
                
    def stop(self):
        """Stop the event loop and close sockets."""
        if self.isOpenRaw:
            self.cxifile_raw.close()
        if self.isOpenProc:
            self.cxifile_proc.close()
        self.isRunning = False
        self.read_socket.close()
        print utils.STOP + "Write worker stopped the event loop"

def parse_cmdl_args():
    """Parsing command line arguments"""
    parser = argparse.ArgumentParser(description='nscxiwrite')
    parser.add_argument("-i", "--handlerip", type=str, default="127.0.0.1", help='Handler (ip), default: localhost')
    parser.add_argument("-r", "--readport",  type=int, default=6008, help="Read Port")
    parser.add_argument("-c", "--ctrlport",  type=int, default=7006, help="Control port")
    parser.add_argument("-l", "--logdir",    type=str, default='./', help="Location where log files are saved")
    parser.add_argument("-d", "--debugging", action='store_true',    help='Debugging mode, more print outs')
    return parser.parse_args()

def main():
    """Entry point to start data handler."""
    args = parse_cmdl_args()
    date =  str(datetime.date.today()).replace('-', '')
    sys.stdout = open(args.logdir + '/cxiwrite_%s.out' %date, 'a', buffering=1)
    sys.stderr = open(args.logdir + '/cxiwrite_%s.err' %date, 'a', buffering=1)
    worker = Cxiwrite(args)
    try:
        worker.run()
    except KeyboardInterrupt:
        worker.stop()    

import numpy as np
import logging
import zmq
import time
import h5py
import argparse
import os, sys
import datetime
from mpi4py import MPI

from .. import utils
from ..camera import fccd
from ..camera import pyptycho_wrapper
from ..io import confparser
from ..comm.zmqsocket import ZmqSocket

class Preproc:
    """Preprocessing/cleaning of raw frames."""
    def __init__(self, args):
        self.comm = MPI.COMM_WORLD
        self.size = self.comm.Get_size()
        self.rank = self.comm.Get_rank()
        self.createSockets(args)
        self.isWaitingForReply = False
        self.reset()
        self.slist = ['Configured', 'Finished']
        self.startRun = False
        self.currentRun = None

    def createSockets(self, args):
        """Connects a data (DEALER) and control (REQ) socket in order to communicate with handler."""
        self.data_socket = ZmqSocket(zmq.DEALER)
        self.data_socket.connect_socket("tcp://%s:%d"  %(args.handlerip, args.dataport))
        self.data_socket.register()
        print utils.INFO + "Preproc worker (rank = %s) is waiting for data on port %d" %(self.rank, args.dataport)
        self.control_socket = ZmqSocket(zmq.REQ)
        self.control_socket.connect_socket("tcp://%s:%d"  %(args.handlerip, args.ctrlport))
        self.control_socket.register()
        print utils.INFO + "Preproc worker (rank = %s) is waiting for commands on port %d" %(self.rank, args.ctrlport)

    def reset(self):
        """Resets the local flags to start new run."""
        self.isConfigured = False
        self.isFinished = False
        self._adcmask = None

    def setCommands(self, commands):
        """Updates commands received from the handler."""
        for k,v in commands.iteritems():
            setattr(self, k, v)

    def getStatus(self):
        """Return the current status of the preproc module."""
        status = {}
        for s in self.slist:
            status['preproc' + s] = getattr(self, 'is' + s)
        return status
    
    def setParameters(self, params):
        """Overwrites config parameters given a JSON dict"""
        self.experiment = params['experiment']
        self.metadata   = params['metadata']
        self.params     = params['preproc']
        if self.params['method'] == 'pyptycho':
            paramfile = self.params['oldparam']
            self.oldparam = confparser.OldParam(paramfile).param

    def configure(self):
        """Configures a detector object and passing all necessary configuration for pre-processing"""
        self.detector = fccd.FCCD(nrows=1000, width=2000, rotate=self.params['rotate'])
        
    def startNewRun(self):
        """Returns True if a new run has been started."""
        return (self.startRun and (self.startRun != self.currentRun))
    
    def cleanFrame(self, frame, bgmean, bgstd):
        """Returns clean frames after going through pre-processing pipeline."""
        if self._adcmask is None:
            self._adcmask = self.detector.adcmask(bgstd) 

        # Default pre-processing
        if self.params['method'] == 'default':
            
            # Descrambling to clock format (dark-subtracted data)
            clockframe = self.detector.descramble(frame - bgmean, self._adcmask, mask=True)

            # Preprocessing (based on MATLAB code)
            frame = self.detector.preprocessing(clockframe)

        # Pre-precossing based on pyptycho code
        if self.params['method'] == 'pyptycho':

            # Descrambling to clock format (dark and data separately)
            clockdata = self.detector.descramble(frame)
            clockdark = self.detector.descramble(bgmean)
    
            # Convert back to TIFF format (as expected by pytptycho)
            tiffdata = self.detector.assemble2(clockdata)
            tiffdark = self.detector.assemble2(clockdark)

            # Preprocessing using pyptycho
            frame = pyptycho_wrapper.preprocessing(tiffdata, tiffdark, self.oldparam)

        # Make a copy of the cleanframe
        cleanframe = np.copy(frame.astype(np.float32))
        return cleanframe
    
    def run(self):
        """This triggers the event loop."""
        self.isRunning = True
        print utils.START + "Preproc worker is starting the event loop"

        # Start the event loop
        while self.isRunning:
            
            # Send request over control socket (ask what to do)
            if not self.isWaitingForReply:
                if self.startNewRun():
                    self.reset()
                    self.control_socket.send_cmd('getConfig', self.getStatus())
                    self.currentRun = self.startRun
                else:
                    self.control_socket.send_cmd('getUpdate', self.getStatus())
                self.isWaitingForReply = True

            # Receive request reply over control socket (telling the module what to do)
            if self.control_socket.activity():
                cmd, jdict = self.control_socket.recv_cmd()
                if jdict is not None:
                    if cmd == 'config':
                        self.setParameters(jdict)
                        self.configure()
                        self.isConfigured = True
                    if cmd == 'update':
                        self.setCommands(jdict)
                self.isWaitingForReply = False

            # Receive raw data and return clean data
            if self.data_socket.activity():
                request = self.data_socket.recv()
                if request == 'clean':
                    fid, rawframe, bgmean, bgstd = self.data_socket.recv_frame_and_background()
                    print utils.TIME + "Preproc worker (rank = %d) received a raw frame, id = %s" %(self.rank, fid)
                    outframe = self.cleanFrame(np.fromstring(rawframe, '<u2'), bgmean, bgstd)
                if request == 'descramble':
                    fid, rawframe = self.data_socket.recv_multipart()
                    outframe = self.detector.descramble(np.fromstring(rawframe, '<u2'))
                self.data_socket.send_frame(fid, outframe)
                print utils.TIME + "Preproc worker (rank = %d) sent a processed frame, id = %s" %(self.rank, fid)
                
    def stop(self):
        """Stop the event loop and close sockets."""
        self.isRunning = False
        self.data_socket.close()
        print utils.STOP + "Preproc worker stopped the event loop"

def parse_cmdl_args():
    """Parsing command line arguments"""
    parser = argparse.ArgumentParser(description='nspreproc')
    parser.add_argument("-i", "--handlerip", type=str, default="127.0.0.1", help='Handler (ip), default: localhost')
    parser.add_argument("-p", "--dataport",  type=int, default=6002, help="Data port, default: 6002")
    parser.add_argument("-c", "--ctrlport",  type=int, default=7002, help="Control port")
    parser.add_argument("-l", "--logdir",    type=str, default='./', help="Location where log files are saved")
    parser.add_argument("-d", "--debugging", action='store_true',    help='Debugging mode, more print outs')
    return parser.parse_args()

def main():
    """Entry point to start preproc handler."""
    args = parse_cmdl_args()
    date = str(datetime.date.today()).replace('-', '')
    sys.stdout.flush()
    sys.stderr.flush()
    sys.stdout = open(args.logdir + '/preproc_%s.out' %date, 'a', buffering=1)
    sys.stderr = open(args.logdir + '/preproc_%s.err' %date, 'a', buffering=1)
    worker = Preproc(args)
    try:
        worker.run()
    except KeyboardInterrupt:
        sys.stdout.close()
        sys.stderr.close()
        worker.stop()

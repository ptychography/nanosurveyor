import os, glob
import argparse

def parse_cmdl_args():
    """Parsing command line arguments"""
    parser = argparse.ArgumentParser(description='nsdebug')
    parser.add_argument("-o", "--output", type=str, nargs='+', help='Show output')
    parser.add_argument("-e", "--errors", type=str, nargs='+', help='Show errors')
    return parser.parse_args()

def main():
    """Entry point for logger script."""
    args = parse_cmdl_args()
    logdir = os.environ['HOME'] + "/.nanosurveyor/streaming/log/"
    logs = ""
    if args.output is not None:
        for o in args.output:
            logs += (" " + sorted(glob.glob(logdir + o + "_*.out"))[-1])
    if args.errors is not None:
        for e in args.errors:
            logs += (" " + sorted(glob.glob(logdir + e + "_*.err"))[-1])
    os.system('tail -f ' + logs)

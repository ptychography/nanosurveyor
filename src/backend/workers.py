import os
import sys
import imp
import argparse
import subprocess
import time

class Starter:
    """Starting backend modules as python processes."""
    def __init__(self, args):
        self.submodules = []
        self.conf = imp.load_source('backend_conf', args.config)
        self.logdir = os.environ['HOME'] + '/.nanosurveyor/streaming/log'

    def startWorkers(self):
        """Start workers as python processes."""
        commands = []
        commands.append("mpirun -n %d nspreproc -i %s -p %s -l %s"
                        %(self.conf.nworkers, self.conf.host_ip, self.conf.preproc_port, self.logdir))
        commands.append("nsbackground -i %s -p %s -c %s -l %s" 
                        %(self.conf.host_ip, self.conf.background_port, self.conf.background_ctrlport, self.logdir))
        commands.append("nsdata -i %s -p %s -c %s -l %s" 
                        %(self.conf.host_ip, self.conf.data_port, self.conf.data_ctrlport, self.logdir))
        commands.append("nsptycho -i %s -m %s -p %s -c %s -l %s" 
                        %(self.conf.host_ip, self.conf.meta_port, self.conf.ptycho_port, self.conf.ptycho_ctrlport, self.logdir))
        for cmd in commands:
            print "Starting Python process:", cmd
            self.submodules.append(subprocess.Popen(cmd.split(), shell=False))

    def stopWorkers(self):
        """Kill Python processes"""
        for w in self.submodules:
            print "Killing Python process: ", w.pid
            subprocess.call(['kill', '-9', '%d' %w.pid])

def parse_cmdl_args():
    """Parsing command line arguments"""
    parser = argparse.ArgumentParser(description='nsbackend')
    parser.add_argument("config", type=str, help="Configuration file")
    if(len(sys.argv) == 1):
        parser.print_help()
    return parser.parse_args()

def main():
    """Entry point for worker startup script."""
    args = parse_cmdl_args()
    s = Starter(args)
    s.startWorkers()
    while True:
        try:
            pass
        except KeyboardInterrupt:
            s.stopWorkers()
            print "Waiting 5 seconds before exiting ..."
            time.sleep(5)
            sys.exit()
        except Exception as e:
            print e
            s.stopWorkers()

if __name__ == '__main__':
    main()

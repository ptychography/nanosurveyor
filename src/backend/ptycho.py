import sys
import time
import threading
import zmq
import argparse
import datetime
from mpi4py import MPI

import os

if ("OMPI_COMM_WORLD_LOCAL_RANK" in os.environ):
  print os.environ["OMPI_COMM_WORLD_LOCAL_RANK"]
  os.environ["CUDA_VISIBLE_DEVICES"] = os.environ["OMPI_COMM_WORLD_LOCAL_RANK"]
os.environ["IPYTHONDIR"] = "/tmp"


# This is to make sure numpy/afnumpy/SHARP are not required to run the frontend
try:
    import numpy as np
    import afnumpy as af
    import afnumpy.fft as fft
    import sharp
except:
    pass

from .. import utils
from ..comm.zmqsocket import ZmqSocket

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()
isLeader = (rank == 0)

class Solver:
    """
    Wrap around the SHARP solver. Initializes the reconstruction.
    Starts the engine.
    """
    def __init__(self, engine,communicator,strategy):
        self.m_engine = engine
        self.m_communicator = communicator
        self.m_opt = sharp.Options.getOptions()
        self.strategy = strategy

    def initialize(self, frames, mean_bg, translations, reciprocal_size,
                   illumination, illumination_mask, illumination_intensities, frame_weights):
        """Initialize the reconstruction. """
        self.m_engine.setTotalFrameCount(frames.shape[0]) 
        self.m_engine.setFrames(frames)
        self.m_engine.setMeanBackground(mean_bg)
        self.m_engine.setReciprocalSize(reciprocal_size)
        self.m_engine.setTranslations(translations[self.strategy.myFrames(),:])
        self.m_engine.setAllTranslations(translations)
        self.m_engine.setMyFrameWeights(frame_weights)
        self.m_engine.setIllumination(illumination)
        self.m_engine.setIlluminationMask(sharp.af2thrust(illumination_mask))
        self.m_engine.setIlluminationIntensities(sharp.af2thrust(illumination_intensities))
        self.m_engine.setCommunicator(self.m_communicator)
        return self.m_engine.initialize()

    def run(self, iterations):
        """Run the engine for a given nr. of iterations."""
        tic = time.clock()
        self.m_engine.iterate(iterations)
        toc = time.clock()
        if self.m_communicator.isLeader():
            if self.m_opt.time_solver:
                print "Iteration time: %g us\n", toc-tic
            
    def writeImage(self, prefix):
        """Write the results to a CXI file."""
        if self.m_communicator.isLeader():
            if len(self.m_opt.output_file) > 0:
                self.m_input_output.writeCXI(self.m_engine,self.m_opt.output_file)
            else:
                self.m_input_output.writeCXI(self.m_engine)

class Engine(sharp.CudaEngine):
    """The reconstruction engine. Defines what happens in every iteration of the reconstruction."""
    def __init__(self):
        sharp.CudaEngine.__init__(self)
        self.overlapResidual = 0.
        self.dataResidual  = 0.
        self.stop = False

    def init_shapes(self):
        """Defines shapes of frames, images."""
        self.frames_shape = (self.m_nframes, self.m_frame_width, self.m_frame_height)
        self.crop = self.m_frame_width/2

    def init_objects(self):
        """Create afnumpy objects."""
        self.frames_data    = af.zeros(self.frames_shape, dtype=af.complex64)
        self.image_object   = sharp.range2af(self.m_image)
        self.frames_iterate = sharp.range2af(self.m_frames_iterate)
        self.frames_object  = af.copy(self.frames_iterate)
        self.illumination   = sharp.range2af(self.m_illumination)
        self.frames_temp    = sharp.range2af(self.m_frames)
        self.frame_weights  = sharp.range2af(self.m_frame_weights)
        self.m_frames_norm  = 1. 
        
    def iterate(self, steps):
        """Runs the engine for a given nr. of iteration steps."""

        for i in range(steps):

            # Stop the engine on request
            if self.stop:
                break

            # True if residials should be calculated
            calculateResidual = (self.m_iteration % self.m_output_period == 0)

            # Data Projector
            if calculateResidual:
                self.dataResidual = self.dataProjector(self.m_frames_iterate, self.frames_data, True)
                frames_norm = af.sqrt(af.sum(af.abs(self.frames_temp * (self.frame_weights).reshape((self.m_nframes,1,1)))))
                #print rank, self.dataResidual / frames_norm
                if frames_norm:
                    self.dataResidual /= frames_norm
            else:
                self.dataProjector(self.m_frames_iterate, self.frames_data)

            # First update (RAAR)
            self.axpbypcz(self.frames_iterate, self.frames_object, self.frames_data, self.frames_iterate, self.m_beta, -self.m_beta,1.-2.* self.m_beta)

            # True if synchronization should be done
            do_sync = (((self.m_iteration % self.m_global_period) == 0) or ((i == steps - 1)))

            # OverlapProjector
            if calculateResidual:
                self.overlapResidual = self.overlapProjector(self.frames_data,
                                                        self.frames_object,
                                                        self.image_object,
                                                        do_sync, True)
                #print rank, self.overlapResidual / frames_norm
                if frames_norm:
                    self.overlapResidual /= frames_norm
            else:
                self.overlapProjector(self.frames_data,
                                      self.frames_object,
                                      self.image_object,
                                      do_sync)

            # Refine Illumination
            if(self.m_iteration > 0 and self.m_illumination_refinement_period > 0 and
               (self.m_iteration % self.m_illumination_refinement_period) == 0):
               self.refineIllumination(self.frames_data,
                                       self.image_object)

            # Second update (RAAR)#
            self.axpby(self.frames_iterate, self.frames_object, self.frames_iterate, 1.0, 2.* self.m_beta)

            # Print Resdiuals
            if self.m_comm.isLeader() and calculateResidual:
                if self.m_has_solution:
                    tmp1 = af.array(self.m_illuminated_area.real * self.solution.conj() * self.image_object)
                    phi = af.sum(tmp1)
                    tmp2 = af.array(self.m_illuminated_area.real * af.abs(self.image_object)**2)
                    image_norm_2 = af.sum(tmp2)
                    imageResidual = (self.m_image_norm_2 * image_norm_2 - (af.abs(phi)**2)) / (self.m_image_norm_2 * image_norm_2)
                    if imageResidual <= 0:
                        imageResidual = 0 
                    msg = "iter = %d, data = %06g, overlap = %06g, image = %06g"
                    print utils.TIME + msg %(self.m_iteration, self.dataResidual, self.overlapResidual, imageResidual)
                else:
                    msg = "iter = %d, data = %06g, overlap = %06g"
                    print utils.TIME + msg %(self.m_iteration, self.dataResidual, self.overlapResidual)

            # Update iteration counter
            # ------------------------
            self.m_iteration += 1

class Reconstructor:
    """The reconstruction module (ptycho)."""
    def __init__(self, args):
        self.createSockets(args)
        self.debugging = True #args.debugging
        self.opt = sharp.Options.getOptions()
        self.engine = Engine()
        self.isWaitingForReply = False
        self.reset()
        self.startRun = False
        self.currentRun = None
        self.hasIllumination = False
        self.received_a_command = False
        self.finish_recons = False
        
        # List of parameters
        self.metalist   = ['scanNx', 'scanNy', 'scanStep', 'arraySize', 
                           'energy', 'distance', 'pixelSizeX', 'pixelSizeY', 'flipYpositions']
        self.ptycholist = ['iterStep', 'iterTotal', 'update', 
                           'sharpIlluminationRefine', 'sharpFourierMask', 'sharpRoundPixel']

        # List of status flags
        self.slist = ['Configured', 'Initialized', 'Finished']

    def dprint(self, msg):
        """Print debugging messages"""
        if self.debugging:
            rank_str = " (%d) " %rank
            print utils.TIME + rank_str + msg
        
    def createSockets(self, args):
        """
        Connect to reading socket (SUB) in order to recv frames and initial illumination.
        Bind to data socket (DEALER) in order to send reconstructed images.
        Bind to control socket (REP) in order to recv commands from the backend handler.

        """
        self.read_socket = ZmqSocket(zmq.SUB)
        self.read_socket.connect_socket("tcp://%s:%d" %(args.handlerip, args.readport))
        self.read_socket.register()
        self.read_socket.subscribe("illumination")
        self.read_socket.subscribe("frame")
        print utils.INFO + "(%d) Recons worker is waiting for data on port %d" %(rank, args.readport)

        self.send_socket = ZmqSocket(zmq.DEALER)
        self.send_socket.connect_socket("tcp://%s:%d" %(args.handlerip, args.sendport))
        self.send_socket.register()
        print utils.INFO + "(%d) Recons worker is sending data on port %d" %(rank, args.sendport)

        self.control_socket = ZmqSocket(zmq.REQ)
        self.control_socket.connect_socket("tcp://%s:%d" %(args.handlerip, args.ctrlport))
        self.control_socket.register()
        print utils.INFO + "(%d) Recons worker is waiting for commands on port %d" %(rank, args.ctrlport)
                
        
    def reset(self):
        """Reset the ptycho module."""
        self.iteration = 0
        self.received  = 0
        self.interrupt = False
        self.isConfigured = False
        self.isInitialized = False
        self.isFinished = False
        self.isSaved = False
        self.dprint("ptycho module has been reset")

    def setCommands(self, commands):
        """Overwrite commands."""
        for k,v in commands.iteritems():
            setattr(self, k, v)

    def getStatus(self):
        """Return current status of ptycho module."""
        status = {}
        for s in self.slist:
            status['ptycho' + s] = getattr(self, 'is' + s)
        return status

    def setParameters(self, params):
        """Set parameters."""
        for p in self.metalist:
            setattr(self, p, params['metadata'][p])
        for p in self.ptycholist:
            setattr(self, p, params['ptycho'][p])            
        self.wavelength = 1.98644521e-25/self.energy
        self.dprint("Parameters have been set")

    def startNewRun(self):
        """Return True if new run has been requested."""
        return (self.startRun and (self.startRun != self.currentRun))
    
    def prepareReconstruction(self):
        """Prepare for SHARP reconstruction (options, positions, ...)"""
        self.dprint("Preparing for SHARP")
        self.setOptions()
        self.crop = self.arraySize/2
        #if self.illuminationFile:
        #    self.loadIllumination(self.illuminatonFile)
        self.setPositions()
        self.iterations = self.opt.iterations
        self.reciprocal_size = (self.pixelSizeX * self.arraySize/(self.distance * self.wavelength),
                                self.pixelSizeY * self.arraySize/(self.distance * self.wavelength))
        # The line below is wrong, was there a reason for this???
        #self.reciprocal_size = (self.pixelSizeX * (960.0/float(self.arraySize)) * self.arraySize/(self.distance * self.wavelength),
        #                        self.pixelSizeY * (960.0/float(self.arraySize)) * self.arraySize/(self.distance * self.wavelength))
        self.mean_background = af.zeros((self.arraySize, self.arraySize), dtype=af.float32)
        self.std_background  = af.zeros((self.arraySize, self.arraySize), dtype=af.float32)

        # Strategy
        self.strategy = sharp.Strategy(self.communicator)
        self.strategy.setTranslation(self.positions)
        self.strategy.calculateDecomposition()
        self.myframes = self.strategy.myFrames()
        self.dprint("I got to work on %d frames" %len(self.myframes))

        # Initialize a gpu array for frames
        self.frames   = af.zeros((len(self.myframes), self.arraySize, self.arraySize), dtype=np.float32)
        self.frame_weights = af.zeros(len(self.myframes), af.complex64)
        self.validframes   = np.zeros(len(self.myframes)).astype(np.bool)
        self.dprint("Finished preparations for SHARP")

    def setOptions(self):
        """Define options for SHARP reconstruction."""
        self.dprint("Setting SHARP options")
        options = ['']
        if self.sharpIlluminationRefine:
            options.append('-r %d' %self.sharpIlluminationRefine)
        if self.sharpFourierMask:
            options.append('-M')
        options.append('-T')
        options.append('2')
        #options.append('-I')
        options.append('-D')
        options.append('test.cxi')
        print options
        self.opt.parse_args(options)
        self.engine.setWrapAround(self.opt.wrapAround)
        self.communicator = sharp.Communicator(options, self.engine)
        self.engine.setInputOutput(sharp.InputOutput(options, self.communicator))
        self.dprint("Finished setting SHARP options")
        
    def setIllumination(self, array):
        """Define illumination."""
        self.illumination = af.array(array[0], dtype=af.complex64)
        self.Fillumination_mask = af.array(array[1], dtype=af.complex64)
        self.Fillumination_intensities = af.array(np.ascontiguousarray(array[2].real))
        self.dprint("Illuminations has been set.")

    def setPositions(self):
        """Define scanning positions."""
        x = np.arange(0, self.scanNx*self.scanStep, self.scanStep)
        y = np.arange(0, self.scanNy*self.scanStep, self.scanStep)
        xx, yy = np.meshgrid(x, y)
        if self.flipYpositions:
          cpu_pos = np.ascontiguousarray(1e-9*np.vstack([xx.flat, yy.flat[::-1], np.zeros(xx.shape).flat]).T)
        else:
          cpu_pos = np.ascontiguousarray(1e-9*np.vstack([xx.flat, yy.flat, np.zeros(xx.shape).flat]).T)
        self.positions = af.array(cpu_pos)
        
    def initSolver(self):
        """Prepare for the initialization of the solver. """
        if self.isInitialized:
            return
        
        # Fill empty frames with bright field frames
        #self.dprint("Fill empty frames with bright field images")
        #sh = self.frames.shape
        #if np.any(~self.validframes):
        #    brightframes = af.array(np.ones((sh[0] - self.received, sh[1], sh[2])) * self.Fillumination_intensities, dtype=af.float32)
        #    self.frames[af.where(af.array(~self.validframes))[0],:,:] = brightframes
        
        # Initialize the solver
        self.dprint("Initializing solver")
        self.solver = Solver(self.engine, self.communicator, self.strategy)
        if self.solver.initialize(self.frames, self.mean_background, self.positions,
                                  self.reciprocal_size, self.illumination,
                                  self.Fillumination_mask, self.Fillumination_intensities,
                                  self.frame_weights) == -1:
            raise RuntimeError('Solver/Engine has failed to initialize')
        self.dprint("Solver/Engine has been initialized")
        self.isInitialized = True
        self.engine.init_shapes()
        self.engine.init_objects()
        
    def send_recons(self):
        """Send current result of reconstruction to the backend handler."""
        if isLeader:
            image = np.ascontiguousarray(self.engine.image_object[self.crop:-self.crop, self.crop:-self.crop])
            illumination = np.array(self.engine.illumination).reshape((self.arraySize, self.arraySize))
            self.send_socket.send_recons(image, illumination, str(self.engine.m_iteration), 
                                         str(self.engine.dataResidual), 
                                         str(self.engine.overlapResidual))

    def continue_reconstruction(self):
        """Continue reconstructiion if more iterations to be done (while new frames are still arriving)"""
        received_total = comm.allreduce(self.received, op=MPI.SUM)
        if (received_total % self.update):
            return False
        if self.iteration >= self.iterTotal:
            return True
        else:
            self.dprint("Run %d more iterations of SHARP" %self.iterStep)
            self.engine.calculateImageScale()
            self.solver.run(self.iterStep)
            self.iteration += self.iterStep
            self.send_recons()
            return False
        
    def stop_reconstruction(self):
        """Stop the current reconstruction"""
        if self.interrupt:
            print "Stopping reconstruction"
            self.isConfigured = False
            self.isInitialized = False
            self.isRunning = False    # Is this correct?
            self.engine.stop = True   # Is this correct?
                                
    def update_frames(self, index, frame):
        """Update frames that SHARP is using for the reconstruction"""
        if self.isConfigured:
            self.frames[int(index),:,:] = af.array(np.fft.fftshift(frame), dtype=np.float32)
            self.frame_weights[int(index)] = 1.
            self.validframes[int(index)] = True
            self.received += 1
        #if self.isInitialized:
        #    self.engine.setMyFrameWeights(self.frame_weights)
        #    self.engine.calculateImageScale()

    def on_recv_command(self, cmd, jdict):
        """Slot for receiving commands on rank 0."""
        comm.bcast(cmd, root=0)
        comm.bcast(jdict, root=0)
        if cmd == 'config':
            self.reset()
            print jdict
            self.setParameters(jdict)
            self.prepareReconstruction()
            self.isConfigured = True
        elif cmd == 'update':
            self.setCommands(jdict)

    def run(self):
        """The event loop of the reconstruction module."""
        self.isRunning = True
        print utils.START + "(%d) Recons worker is starting the event loop" %rank

        cmd = None
        jdict = None

        while self.isRunning:

            # Send request over control socket (asking what to do)
            if isLeader and not self.isWaitingForReply:
                if self.startNewRun():
                    self.control_socket.send_cmd("getConfig", self.getStatus())
                    self.currentRun = self.startRun
                else:
                    self.control_socket.send_cmd("getUpdate", self.getStatus())
                self.isWaitingForReply = True

            # Receive request reply over control socket (telling SHARP what to do)
            if isLeader and self.control_socket.activity():
                cmd, jdict = self.control_socket.recv_cmd()
                if jdict is not None:
                    self.received_a_command = True
                self.isWaitingForReply = False
                
            # All workers have to react upon new commands received
            self.received_a_command = comm.bcast(self.received_a_command, root=0)
            if self.received_a_command:
                self.on_recv_command(comm.bcast(cmd, root=0), comm.bcast(jdict, root=0))
            self.received_a_command = False
            
            # Receive frames and initial illumination
            if self.read_socket.activity():
                key = self.read_socket.recv()
                if key == 'illumination':
                    self.dprint("Recons worker receveived illumination")
                    self.setIllumination(self.read_socket.recv_array())
                    self.hasIllumination = True
                if key == 'frame':
                    index, frame = self.read_socket.recv_frame()
                    if int(index) in self.myframes:
                        self.dprint("Received new frame, index = %d" %int(index))
                        self.update_frames(self.myframes.index(int(index)), frame)


            if self.isInitialized:
               self.engine.setMyFrameWeights(self.frame_weights)
               self.engine.calculateImageScale()
                        
            # Wait for all workers to have the illumination, then initialize solver
            if not self.isInitialized and comm.allreduce(self.hasIllumination, op=MPI.LAND):
                self.initSolver()
                        
            # Keep SHARP running
            if self.isInitialized:
                self.isFinished = self.continue_reconstruction()
                

            # Stop the reconstruction
            self.stop_reconstruction()


    def stop(self):
        """Stop the reconstruction module."""
        self.isRunning = False
        self.engine.stop = True
        self.read_socket.close()
        self.send_socket.close()
        self.control_socket.close()
        print utils.STOP + "Recons worker is stopping the event loop"
        self.dprint("Reconstruction worker has finished")

def parse_cmdl_args():
    """Parsing command line arguments"""
    parser = argparse.ArgumentParser(description='nsrecons')
    parser.add_argument("-i", "--handlerip", type=str, default="127.0.0.1", help='Handler (ip), default: localhost')
    parser.add_argument("-s", "--sendport",  type=int, default=6005, help="Send port, default: 6005")
    parser.add_argument("-c", "--ctrlport",  type=int, default=7005, help="Control port, default: 7005")
    parser.add_argument("-r", "--readport",  type=int, default=6007, help="Read port, default: 6007")
    parser.add_argument("-l", "--logdir",    type=str, default='./', help="Location where log files are saved")
    parser.add_argument("-d", "--debugging", action='store_true',    help='Debugging mode, more print outs')
    return parser.parse_args()

def main():
    """Entry point to start ptycho handler."""
    args = parse_cmdl_args()
    date =  str(datetime.date.today()).replace('-', '')
    sys.stdout = open(args.logdir + '/ptycho_%s.out' %date, 'a', buffering=1)
    sys.stderr = open(args.logdir + '/ptycho_%s.err' %date, 'a', buffering=1)
    worker = Reconstructor(args)
    try:
        worker.run()
    except KeyboardInterrupt:
        worker.stop()

import numpy as np
import zmq
import time
import h5py
import sys
import datetime
import argparse

from .. import utils
from .. import camera
from ..algorithms import stat, stxm, source
from ..comm.zmqsocket import ZmqSocket

class Data:
    """Accumulation of data frames."""
    def __init__(self, args):
        self.createSockets(args)
        self.isWaitingForReply = False
        self.bright = stat.Stats()
        self.stxm = stxm.STXM()
        self.metalist = ['arraySize', 'scanNx', 'scanNy', 'flipYpositions']
        self.datalist = ['brightThreshold', 'fmaskThreshold']
        self.slist = ['Configured', 'Finished']
        self.startRun = False
        self.currentRun = None
        self.finish = False
        self.isConfigured = False
        self.isFinished = False

    def createSockets(self, args):
        """Connects a data (DEALER) and control (REQ) socket in order to communicate with handler."""        
        self.data_socket = ZmqSocket(zmq.DEALER)
        self.data_socket.connect_socket("tcp://%s:%d" %(args.handlerip, args.port))
        self.data_socket.register()
        print utils.INFO + "Data worker is waiting for data on port %d" %args.port
        self.control_socket = ZmqSocket(zmq.REQ)
        self.control_socket.connect_socket("tcp://%s:%d" %(args.handlerip, args.ctrlport))
        self.control_socket.register()
        print utils.INFO + "Data worker is waiting for commands on port %d" %args.ctrlport
                
    def reset(self):
        """Resets the accumualators of the STXM and stat module."""
        self.bright.reset()
        self.stxm.reset(self.ndata)
        self.finish = False
        self.isConfigured = False
        self.isFinished = False

    def setCommands(self, commands):
        """Updates commands received from the handler."""
        for k,v in commands.iteritems():
            setattr(self, k, v)

    def getStatus(self):
        """Return the current status of the data module."""
        status = {}
        for s in self.slist:
            status['data' + s] = getattr(self, 'is' + s)
        return status
        
    def setParameters(self, params):
        """Overwrites config parameters given a JSON dict"""
        for p in self.metalist:
            setattr(self, p, params['metadata'][p])
        for p in self.datalist:
            setattr(self, p, params['data'][p])
        self.ndata = self.scanNx*self.scanNy

    def startNewRun(self):
        """Returns True if a new run has been started."""
        return (self.startRun and (self.startRun != self.currentRun))

    def accumulate(self, index, x):
        """Add new data to STXM and bright accumulators."""
        maxval = x.max()
        maxval = max(maxval, self.bright.max())
        self.bright.add(x * (x > maxval*self.brightThreshold))
        self.stxm.add(index, x)
        
    def getImage(self):
        """Return combined STXM image (complex array)."""
        img = self.stxm.bfimage() * np.exp(1j*self.stxm.diffimage())
        if self.flipYpositions:
            img = np.flipud(mg)
        return img

    def getIllumination(self):
        """Return illumination, Fillumination mask and Fillumination intensities."""
        norm = self.bright.n()
        norm[norm == 0] = 1.
        illumination, Fillumination_mask, Fillumination_intensities = source.guess_illumination(self.bright.sum(), self.stxm.mask(), norm)
        return np.array([illumination, Fillumination_mask, Fillumination_intensities])

    def run(self):
        """This triggers the event loop."""
        self.isRunning = True
        print utils.START + "Data worker is starting the event loop"

        # Start the event loop
        while self.isRunning:

            # Send request over control socket (ask what to do)
            if not self.isWaitingForReply:
                if self.startNewRun():
                    self.control_socket.send_cmd('getConfig', self.getStatus())
                    self.currentRun = self.startRun
                else:
                    self.control_socket.send_cmd('getUpdate', self.getStatus())
                self.isWaitingForReply = True

            # Receive request reply over control socket (telling the module what to do)
            if self.control_socket.activity():
                cmd, jdict = self.control_socket.recv_cmd()
                if jdict is not None:
                    if cmd == 'config':
                        self.setParameters(jdict)
                        self.reset()
                        self.isConfigured = True
                    if cmd == 'update':
                        self.setCommands(jdict)
                self.isWaitingForReply = False

            # Receive new frames and send STXM image back
            if self.data_socket.activity():
                index, frame = self.data_socket.recv_frame()
                self.accumulate(int(index), frame)
                self.data_socket.send('image', zmq.SNDMORE)
                self.data_socket.send_array(self.getImage())

            # Finish generatation of initial illumination
            if self.finish and (not self.isFinished):
                self.data_socket.send('illumination', zmq.SNDMORE)
                self.data_socket.send_array(self.getIllumination())
                self.isFinished = True
            
    def stop(self):
        """Stop the event loop and close sockets."""
        self.isRunning = False
        self.data_socket.close()
        print utils.STOP + "Data worker stopped the event loop"

def parse_cmdl_args():
    """Parsing command line arguments"""
    parser = argparse.ArgumentParser(description='nsdata')
    parser.add_argument("-i", "--handlerip", type=str, default="127.0.0.1", help='Handler (ip), default: localhost')
    parser.add_argument("-p", "--port",      type=int, default=6004, help="Port")
    parser.add_argument("-c", "--ctrlport",  type=int, default=7004, help="Control port")
    parser.add_argument("-l", "--logdir",    type=str, default='./', help="Location where log files are saved")
    parser.add_argument("-d", "--debugging", action='store_true',    help='Debugging mode, more print outs')
    return parser.parse_args()

def main():
    """Entry point to start data handler."""
    args = parse_cmdl_args()
    date =  str(datetime.date.today()).replace('-', '')
    sys.stdout = open(args.logdir + '/data_%s.out' %date, 'a', buffering=1)
    sys.stderr = open(args.logdir + '/data_%s.err' %date, 'a', buffering=1)
    worker = Data(args)
    try:
        worker.run()
    except KeyboardInterrupt:
        worker.stop()    

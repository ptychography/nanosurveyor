import numpy as np
import zmq
import time
import h5py
import argparse
import datetime
import sys, os

from .. import utils
from ..algorithms import stat
from ..comm.zmqsocket import ZmqSocket

class Background:
    """Accumulation of background frames"""
    def __init__(self, args):
        self.createSockets(args)
        self.isWaitingForReply = False
        self.stat = stat.Stats()
        self.reset()
        self.bglist = ['nframes']
        self.slist  = ['Configured', 'Finished']
        self.startRun   = False
        self.currentRun = None
        
    def createSockets(self, args):
        """Connects a data (DEALER) and control (REQ) socket in order to communicate with handler."""
        self.data_socket = ZmqSocket(zmq.DEALER)
        self.data_socket.connect_socket("tcp://%s:%d" %(args.handlerip, args.dataport))
        self.data_socket.register()
        print utils.INFO + "Background worker is waiting for data on port %d" %args.dataport
        
        self.control_socket = ZmqSocket(zmq.REQ)
        self.control_socket.connect_socket("tcp://%s:%d" %(args.handlerip, args.ctrlport))
        self.control_socket.register()
        print utils.INFO + "Background worker is waiting for commands on port %d" %args.ctrlport

    def reset(self):
        """Resets the local variable to start over the accumulation of background frames for new runs."""
        self.stat.reset()
        self.finish = False
        self.isConfigured = False
        self.isFinished = False

    def setCommands(self, commands):
        """Updates commands received from the handler."""
        for k,v in commands.iteritems():
            setattr(self, k, v)

    def getStatus(self):
        """Return the current status of the backround module."""
        status = {}
        for s in self.slist:
            status['background' + s] = getattr(self, 'is' + s)
        return status

    def setParameters(self, params):
        """Overwrites config parameters given a JSON dict"""
        for p in self.bglist:
            setattr(self, p, params['background'][p])

    def startNewRun(self):
        """Returns True if a new run has been started."""
        return (self.startRun and (self.startRun != self.currentRun))
                
    def getBackground(self):
        """Returns mean/std background in stacked array if data is available."""
        if self.stat.n().any():
            return np.vstack([self.stat.mean(), self.stat.std()])
        else:
            return None
        
    def saveBackground(self, filename):
        """Saves avg/std background into temporary directory if data is available.."""
        if self.stat.n().any():
            with h5py.File(filename, 'w') as file:
                file["data/mean"] = self.stat.mean()
                file["data/std"]  = self.stat.std()
        else:
            print "Could not save background, no data available!"
           
    def run(self):
        """This triggers the event loop."""
        self.isRunning = True
        print utils.START + "Background worker is starting the event loop"

        # Start the event loop
        while self.isRunning:

            # Send request over control socket (asking what to do)
            if not self.isWaitingForReply:
                if self.startNewRun():
                    self.reset()
                    self.control_socket.send_cmd("getConfig", self.getStatus())
                    self.currentRun = self.startRun
                else:
                    self.control_socket.send_cmd("getUpdate", self.getStatus())
                self.isWaitingForReply = True

            # Receive request reply over control socket (telling the module what to do)
            if self.control_socket.activity():
                cmd, jdict = self.control_socket.recv_cmd()
                if jdict is not None:
                    if cmd == 'config':
                        self.setParameters(jdict)
                        self.isConfigured = True
                    if cmd == 'update':
                        self.setCommands(jdict)
                self.isWaitingForReply = False

            # Receive new frames
            if self.isConfigured and self.data_socket.activity():
                fid, rawframe = self.data_socket.recv_multipart()
                print utils.TIME + "Received new frame, id = ", int(fid)
                self.stat.add(np.fromstring(rawframe, '<u2').astype(np.int))
                
            # Finish averaging and return mean/std
            if self.finish and not self.isFinished:
                bg = self.getBackground()
                if bg is not None:
                    self.data_socket.send_array(bg)
                    self.saveBackground(os.environ['HOME'] + '/.nanosurveyor/streaming/tmp/background.h5')
                else:
                    print "Could not send background, no data available!"
                self.isFinished = True

    def stop(self):
        """Stop the event loop and close sockets."""
        self.isRunning = False
        self.data_socket.close()
        self.control_socket.close()
        print utils.STOP + "Background worker stopped the event loop"

def parse_cmdl_args():
    """Parsing command line arguments"""
    parser = argparse.ArgumentParser(description='nsbackground')
    parser.add_argument("-i", "--handlerip", type=str, default="127.0.0.1", help='Handler (ip), default: localhost')
    parser.add_argument("-p", "--dataport",  type=int, default=6003, help="Data port")
    parser.add_argument("-c", "--ctrlport",  type=int, default=7003, help="Control port")
    parser.add_argument("-l", "--logdir",    type=str, default='./', help="Location where log files are saved")
    parser.add_argument("-d", "--debugging", action='store_true',    help='Debugging mode, more print outs')
    return parser.parse_args()

def main():
    """Entry point to start background handler."""
    args = parse_cmdl_args()
    date =  str(datetime.date.today()).replace('-', '')
    sys.stdout = open(args.logdir + '/background_%s.out' %date, 'a', buffering=1)
    sys.stderr = open(args.logdir + '/background_%s.err' %date, 'a', buffering=1)
    worker = Background(args)
    try:
        worker.run()
    except KeyboardInterrupt:
        worker.stop()    


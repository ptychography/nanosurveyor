from ringbuffer import RingBuffer
from colormaps import *
import datetime
TIME  = "[" + str(datetime.datetime.now()) + "] "
START = "\033[32m[start]\033[0m " + TIME
STOP  = "\033[31m[stop]\033[0m  " + TIME
INFO  = "\033[33m[info]\033[0m  " + TIME
RATE  = "\033[35m[rate]\033[0m  " + TIME


"""
This module includes algorithms related to the source.
"""
import numbers
import numpy as np

def guess_illumination(datasum, datamask, norm):
    """Returns estimates for illumination, Fillumination_mask and Fillumination_intensities."""
    if isinstance(norm, numbers.Number):
        norm = np.ones(datasum.shape)*norm
    Fillumination = datasum
    Fillumination_mask = datamask.astype(np.int)
    Fillumination_intensities = np.zeros_like(Fillumination)        
    Fillumination_intensities[Fillumination.real > 0] = Fillumination.real[Fillumination.real > 0] / norm[Fillumination.real > 0]
    Fillumination = np.sqrt(Fillumination / norm)
    Fillumination = np.fft.fftshift(Fillumination)
    Fillumination_intensities = np.fft.fftshift(Fillumination_intensities)
    Fillumination_mask = np.fft.fftshift(Fillumination_mask)
    illumination = np.fft.ifft2(Fillumination)
    illumination = np.fft.fftshift(illumination) / np.sqrt(np.sum(np.abs(illumination ** 2)))
    return illumination, Fillumination_mask, Fillumination_intensities

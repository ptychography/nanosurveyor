import numpy as np

class STXM:
    """A class for populating STXM images.
    
    Example
    -------
    TODO
    """
    def __init__(self, msk_threshold=0.01, N=100):
        """Initialize STXM accumulators.

        Parameters
        ----------
        msk_threshold : Optional[float]
             Threshold for the mask, default = 0.01
        N : Optional[int]
             Nr. of data points, default = 100
        """
        self.msk_threshold = msk_threshold
        self.reset(N)
        
    def reset(self, N):
        """Resets STXM accumulators.

        Parameters
        ----------
        N : int
             Nr. of data points.
        """
        self._msk = False
        self._max = 0.
        self._bf  = np.zeros(N)
        self._df  = np.zeros(N)
        self._dx  = np.zeros(N)
        self._dy  = np.zeros(N)

    def diff(self, x, msk=None):
        """Returns differential contrast in x,y.
        
        Parameters
        ----------
        x : array_type
            image
        msk : Optinal[array_type]
            boolean mask 
        """
        ny, nx = x.shape
        top    = x[:ny/2]
        bottom = x[ny/2:]
        right  = x[:,:nx/2]
        left   = x[:,nx/2:]
        if msk is not None:
            top    = top[msk[:ny/2]]
            bottom = bottom[msk[ny/2:]]
            right  = right[msk[:,:nx/2]] 
            left   = left[msk[:,nx/2:]]
        dx = right.sum() - left.sum()
        dy = top.sum() - bottom.sum()
        return dx, dy
        
    def add(self, i, x):
        """Adds new data to accumulators.
        
        Parameters
        ----------
        i : int
            Index.
        x : array_type
            New data ndarray.
        """
        self._max = max(self._max, x.max())
        self._msk |= (x > self._max*self.msk_threshold)
        self._bf[i] = x[self._msk].sum()
        self._df[i] = x[~self._msk].sum()
        self._dx[i], self._dy[i] = self.diff(x)

    def bfimage(self):
        """Returns a brightfield STXM image."""
        bf = self._bf.copy()
        visited = (bf != 0)
        bf[~visited] = bf[visited].mean()
        return bf

    def dfimage(self):
        """Returns a darkfield STXM image."""
        df = self._df.copy()
        visited = (df != 0)
        df[~visited] = df[visited].mean()
        return df

    def diffimage(self):
        """Returns a combined differential STXM image."""
        dx, dy = self._dx.copy(), self._dy.copy()
        visited = (dx != 0) & (dy != 0)
        dx[~visited] = dx[visited].mean()
        dy[~visited] = dy[visited].mean()
        return np.sqrt(dx**2 + dy**2)

    def mask(self):
        """Returns the accumulated mask."""
        return self._msk

import numpy as np
import matplotlib.pyplot as plt

class Stats:
    """A class for accumulation of ndarrays providing statistics (mean, std).

    Example
    -------
    import numpy as np
    stat = Stats()
    for i in range(100):
        stat.add(np.random.random((10,10))
    print stat.mean()
    print stat.std()
    """
    def __init__(self):
        """Initialize stat accumulators."""
        self.reset()
        
    def reset(self):
        """Resets accumulators to zeros."""
        self._x   = 0.
        self._x2  = 0.
        self._n   = 0.
        self._N   = 0.
        self._max = 0.

    def add(self, x):
        """Adds new data to accumulators.


        Parameters
        ----------
        x : array_type
             New data.
        """
        self._x  += x
        self._x2 += (x**2)
        self._n  += (x > 0)
        self._N  += 1
        self._max = max(self._max, x.max())
        
    def mean(self):
        """Returns the mean of accumulated data."""
        self._n[self._n == 0.] = 1
        return self._x / self._n

    def std(self):
        """Returns the std of accumulated data."""
        self._n[self._n == 0.] = 1
        return np.sqrt((self._x2 / self._n) - (self.mean()**2))

    def max(self):
        """Returns the maximum value of accumulated data."""
        return self._max

    def sum(self):
        """Returns the summed array of accumululated data."""
        return self._x

    def n(self):
        """Returns the number of non-zero pixels as an array."""
        return self._n

    def N(self):
        """Returns the total number of arrays added."""
        return self._N

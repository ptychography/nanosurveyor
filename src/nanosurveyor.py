#!/usr/bin/env python
"""Nanosurveyor main file"""
import argparse
import logging
from frontend.nanosurveyor_gui import NanoSurveyorGUI
from PyQt4.QtCore import *
from PyQt4.QtGui import *

def parse_cmdl_args():
    """Parsing command line arguments"""
    parser = argparse.ArgumentParser(description='Nanosurveyor')
    parser.add_argument("-v", "--verbose", action='store_true', help='output extra information')
    parser.add_argument("-d", "--debug",   action='store_true', help='output debug messages')
    parser.add_argument("-c", "--config", type=str, default=None, help="Configuration file")
    return parser.parse_args()

def main():
    """Starting the Nanosurveyor GUI"""
    args = parse_cmdl_args()
    level = logging.WARNING
    if args.verbose:
        level = logging.INFO
    if args.debug:
        level = logging.DEBUG
    logging.basicConfig(format='%(filename)s:%(lineno)d %(message)s', level=level)
    
    app = QApplication([])
    frame = NanoSurveyorGUI(args)
    frame.show()
    app.exec_()

if __name__ == "__main__":
    main()

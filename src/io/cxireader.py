import h5py

class CXIReader:
    """A class for reading CXI files.

    Example
    -------
    TODO
    """
    def __init__(self, filename):
        """Opens a CXI file in read mode.

        Parameters
        ----------
        filename : str
             Name of CXI file.
        """
        self._f = h5py.File(filename, "r")
        
    def close(self):
        self._f.close()

        
class CXIRawReader(CXIReader):
    """A class for reading raw CXI files.

    Example
    -------
    TODO
    """
    def __init__(self, filename):
        """Opens a raw CXI file in read mode.

        Parameters
        ----------
        filename : str
             Name of CXI file.
        """
        CXIReader.__init__(self, filename)
        self._dark = self._f["entry_1/instrument_1/detector_1/data_dark"]
        self._data = self._f["entry_1/instrument_1/detector_1/data"]
        
    def ndark(self):
        return self._dark.shape[0]

    def ndata(self):
        return self._data.shape[0]
        
    def getData(self, i):
        return self._data[i]

    def getDark(self, i):
        return self._dark[i]
        

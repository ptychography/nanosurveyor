import numpy as np

class NewParam:
    def __init__(self, confdict):
        self.detside = int(confdict["metadata"]["arraySize"])
        self.scanx   = int(confdict["metadata"]["scanNx"])
        self.scany   = int(confdict["metadata"]["scanNy"])
        self.scanstep   = float(confdict["metadata"]["scanStep"])
        self.energy     = float(confdict["metadata"]["energy"])
        self.distance   = float(confdict["metadata"]["distance"])
        self.pixelsizex = float(confdict["metadata"]["pixelSizeX"])
        self.pixelsizey = float(confdict["metadata"]["pixelSizeY"])        
        self.brightthreshold = float(confdict["data"]["brightThreshold"])
        self.fmaskthreshold  = float(confdict["data"]["fmaskThreshold"])
        self.initframes = int(confdict["background"]["nframes"])

class OldParam:
    def __init__(self, paramfile):
        self.param = {}
        f = open(paramfile, 'r')
        for item in f:
            pStringList = [item.split('=')[0],item.split('=')[1].rstrip('\n')]
            if len(pStringList) != 2:
                pass
            else:
                self.param[pStringList[0]] = pStringList[1]
        


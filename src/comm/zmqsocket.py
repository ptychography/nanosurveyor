"""
Provides a wrapper for a ZeroMQ socket. Adapted from PyZeroMQt.
"""
import zmq, numpy
from zmqcontext import ZmqContext

class ZmqSocket:
    """Wrapper around a zmq socket."""
    def __init__(self, _type, context=None):
        context = ZmqContext.instance()
        self._socket = context.socket(_type)
        self._poller = zmq.Poller()
        self.subscriptions = []

    def __del__(self):
        """Close socket on deletion"""
        self.close()

    def close(self):
        """Manually close socket"""
        self._socket.close()
        
    def set_identity(self, name):
        """Set zmq socket identity"""
        self._socket.setsockopt(zmq.IDENTITY, name)

    def identity(self):
        """Return the zmq socket identity"""
        return self._socket.getsockopt(zmq.IDENTITY)
        
    def bind(self, addr):
        """Bind socket to address"""
        self._socket.bind(addr)

    def connect_socket(self, addr, tunnel=None):
        """Connect socket to endpoint, possible using an ssh tunnel
        The tunnel argument specifies the hostname of the ssh server to
        tunnel through.
        """
        if(tunnel):
            from zmq import ssh
            #print "Password", str(tunnel.split(',')[1])
            ssh.tunnel_connection(self._socket, addr, tunnel.split(',')[0], password=str(tunnel.split(',')[1]))
        else:
            self._socket.connect(addr)
            
    def subscribe(self, title):
        """Subscribe to a broadcast with the given title"""
        if title in self.subscriptions:
            return
        self._socket.setsockopt(zmq.SUBSCRIBE, title)
        self.subscriptions.append(title)

    def unsubscribe(self, title):
        """Unsubscribe to a broadcast with the given title"""
        self._socket.setsockopt(zmq.UNSUBSCRIBE, title)
        self.subscriptions.remove(title)

    def register(self):
        """Register socket to poller"""
        self._poller.register(self._socket, zmq.POLLIN)

    def activity(self, timeout=1):
        """Callback run when there's activity on the socket"""
        return (self._socket in dict(self._poller.poll(timeout)))
    
    def recv(self, flags=0):
        """Receive a message on the socket"""
        return self._socket.recv(flags)

    def recv_json(self, flags=0):
        """Receive and json decode a message on the socket"""
        md = self._socket.recv_json(flags)
        return md

    def recv_multipart(self):
        """Receive a multipart message on the socket"""
        return self._socket.recv_multipart()

    def recv_cmd(self, flags=0):
        """Receive a cmd"""
        cmd = self.recv()
        return cmd, self.recv_json(flags)

    def recv_array(self, flags=0, copy=True, track=False):
        """Receive a numpy array"""
        md = self._socket.recv_json(flags=flags)
        msg = self._socket.recv(flags=flags, copy=copy, track=track)
        buf = buffer(msg)
        return  numpy.ndarray(shape=md['shape'], dtype=md['dtype'], buffer=buf, strides=md['strides'])

    def recv_frame(self, *args, **kwargs):
        """Receive a frame with an id"""
        fid   = self.recv()
        frame = self.recv_array(*args, **kwargs)
        return fid, frame

    def recv_frame_and_background(self, *args, **kwargs):
        """Receive a frame with id and background"""
        fid = self.recv()
        frame = self.recv()
        bgmean = self.recv_array(*args, **kwargs)
        bgstd  = self.recv_array(*args, **kwargs)
        return fid, frame, bgmean, bgstd

    def recv_frame_and_pos(self, *args, **kwargs):
        """Receive a frame with an id and positions"""
        posx = self.recv()
        posy = self.recv()
        fid, frame = self.recv_frame(*args, **kwargs)
        return fid, frame, posx, posy

    def recv_recons(self, *args, **kwargs):
        """Receive an image with an iteration number"""
        iteration = self.recv()
        moduleError = self.recv()
        overlapError = self.recv()
        image = self.recv_array(*args, **kwargs)
        illumination = self.recv_array(*args, **kwargs)
        return image, illumination, iteration, moduleError, overlapError
    
    def send(self, *args, **kwargs):
        """Send a message on the socket"""
        return self._socket.send(*args, **kwargs)

    def send_json(self, flags=0):
        """Send and json decode a message on the socket"""
        return self._socket.send_json(flags)

    def send_multipart(self, *args, **kwargs):
        """Send a list of messages as a multipart message on the socket"""
        return self._socket.send_multipart(*args, **kwargs)

    def send_ident(self, ident):
        """Send an empty identifier and wait for more to send"""
        return self.send(ident, zmq.SNDMORE)

    def send_cmd(self, cmd, jdict, *args, **kwargs):
        """Send a cmd together with a dict as a json decoded string"""
        self.send(cmd, zmq.SNDMORE)
        return self.send_json(jdict, *args, **kwargs)

    def send_array(self, array, flags=0, copy=True, track=False):
        """Send a numpy array with metadata"""
        md = dict(
            dtype=str(array.dtype),
            shape=array.shape,
            strides=array.strides,
        )
        self._socket.send_json(md, flags|zmq.SNDMORE)
        return self._socket.send(array, flags, copy=copy, track=track)

    def send_frame(self, fid, frame, *args, **kwargs):
        """Send a frame with an id"""
        self.send(fid, zmq.SNDMORE)
        self.send_array(frame, *args, **kwargs)

    def send_frame_and_background(self, fid, frame, bgmean, bgstd, *args, **kwargs):
        """Send a frame with an id and background data"""
        self.send_multipart([fid, frame], zmq.SNDMORE, *args, **kwargs)
        self.send_array(bgmean, zmq.SNDMORE, *args, **kwargs)
        self.send_array(bgstd, *args, **kwargs)
        
    def send_frame_and_pos(self, fid, frame, posx, posy, *args, **kwargs):
        """Send a frame with an id and positions"""
        self.send(posx, zmq.SNDMORE)
        self.send(posy, zmq.SNDMORE)
        self.send_frame(fid, frame, *args, **kwargs)

    def send_recons(self, image, illumination, iteration, moduleError, overlapError, *args, **kwargs):
        """Send an image with an iteration number"""
        self.send(iteration, zmq.SNDMORE)
        self.send(moduleError, zmq.SNDMORE)
        self.send(overlapError, zmq.SNDMORE)
        self.send_array(image, zmq.SNDMORE, *args, **kwargs)
        self.send_array(illumination, *args, **kwargs)
        
        

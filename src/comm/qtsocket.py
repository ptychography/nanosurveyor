"""
Provides a wrapper for a ZeroMQ socket. Adapted from PyZeroMQt. Provides Qt signal handling
"""
import zmq
from PyQt4 import QtCore
from zmqcontext import ZmqContext
from zmqsocket  import ZmqSocket

class QtSocket(QtCore.QObject, ZmqSocket):
    """Wrapper around a zmq socket. Provides Qt signal handling"""
    ready_read  = QtCore.pyqtSignal()
    #ready_write = QtCore.pyqtSignal()
    def __init__(self, _type, parent=None, **kwargs):
        QtCore.QObject.__init__(self, parent, **kwargs)
        ZmqSocket.__init__(self, _type)
        
        fd = self._socket.getsockopt(zmq.FD)
        self._notifier = QtCore.QSocketNotifier(fd, QtCore.QSocketNotifier.Read, self)
        self._notifier.activated.connect(self.activity)
        #self._socket.setsockopt(RCVHWM, 10)

    def activity(self):
        """Callback run when there's activity on the socket"""
        #self._notifier.setEnabled(False)
        while(self._socket.getsockopt(zmq.EVENTS) & zmq.POLLIN):
            self.ready_read.emit()
            #self._notifier.setEnabled(True)

    def disconnect(self):
        self._notifier.activated.disconnect()

        

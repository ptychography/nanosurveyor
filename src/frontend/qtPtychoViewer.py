import sys, os, time
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QTAgg as NavigationToolbar
from matplotlib.figure import Figure
from PIL import Image
from aux.imageIO import numpy2qimage, imload, imsave
from aux.util import congrid, rot_ave
from aux.util import e2l, l2e
import numpy as np
from matplotlib import rc, rcParams
from aux.util import filtered_auto
from reportlab.pdfgen import canvas
from subprocess import call
from qtDiffInspector import imageWindow, plotWindow
from aux.readCXI import *
from scipy.misc import toimage
from reportlab.lib.pagesizes import letter, A4
import datetime
import matplotlib.cm as cm

class headerPopup(QMainWindow):

    def __init__(self):
        super(headerPopup, self).__init__()
        self.setCentralWidget(self.pointsWidget())
        self.setWindowTitle('Ptychography Meta Data')

    def pointsWidget(self):
        widget = QWidget()
        Grid = QGridLayout(widget)
        Grid.setSpacing(5)
        Grid.setMargin(5)

class infoWidget(QFrame):
    def __init__(self, parent = None):
        QFrame.__init__(self, parent)

        frame = QFrame(self)
        grid = QGridLayout(frame)
        grid.setSpacing(0)
        grid.setHorizontalSpacing(2)
        grid.setMargin(0)
        self.plotSize = QSize(256,192)
        self.plotType = 'histogram'
        self.energy = 0.
        self.wavelength = 0.
        self.pixelSize = 0.
        self.ccdZ = 0.
        self.roiSize = 0

        #################################################################################
        self.main_frame = QWidget()

        # Create the mpl Figure and FigCanvas objects.
        # 5.5x4 inches, 100 dots-per-inch
        #
        rcParams['font.size'] = 10
        self.dpi = 100
        self.fig = Figure((6.0, 4.0), dpi=self.dpi,facecolor='white')
        self.fig.subplots_adjust(left = 0.12, bottom=0.1, right = 0.97, top = 0.93)
        self.canvas = FigureCanvas(self.fig)
        self.canvas.setParent(self.main_frame)

        # Since we have only one plot, we can use add_axes
        # instead of add_subplot, but then the subplot
        # configuration tool in the navigation toolbar wouldn't
        # work.
        #
        self.axes = self.fig.add_subplot(111)

        # Bind the 'pick' event for clicking on one of the bars
        #
        #self.canvas.mpl_connect('pick_event', self.on_pick)

        # Create the navigation toolbar, tied to the canvas
        #
        self.mpl_toolbar = NavigationToolbar(self.canvas, self.main_frame)
        ################################################################################

        #self.info = self.infoWidget()
        self.control = self.controlWidget()
        self.diffThumbnail = QLabel()
        self.diffThumbnail.setGeometry(10, 10, 128, 128)

        grid.addWidget(self.canvas,0,0,1,2,Qt.AlignTop | Qt.AlignHCenter)
        grid.addWidget(self.mpl_toolbar,1,0,1,2,Qt.AlignTop | Qt.AlignLeft)
        grid.addWidget(self.diffThumbnail,2,0,1,1,Qt.AlignVCenter | Qt.AlignHCenter)
        grid.addWidget(self.control,2,1,1,1,Qt.AlignTop | Qt.AlignRight)
        self.setLayout(grid)
        self.roiBox.setCurrentIndex(0)
        self.roiBoxChanged()

    def controlWidget(self):

        widget = QWidget(self)
        grid = QGridLayout(widget)
        grid.setSpacing(0)
        grid.setMargin(0)

        self.roiLabel = QLabel("ROI:")
        self.roiBox = QComboBox()
        self.roiBox.addItem("Bounding Box")
        self.roiBox.addItem("Center+Size")
        self.roiBox.setMaximumSize(128,24)
        self.roiBox.setMinimumSize(128,24)
        self.roiSizeLabel = QLabel("Size")
        self.roiSizeEdit = QLineEdit()
        self.roiSizeEdit.setMaximumSize(64,24)
        self.roiCenterXLabel = QLabel("X Center")
        self.roiCenterXEdit = QLineEdit()
        self.roiCenterXEdit.setMaximumSize(64,24)
        self.roiCenterYLabel = QLabel("Y Center")
        self.roiCenterYEdit = QLineEdit()
        self.roiCenterYEdit.setMaximumSize(64,24)
        self.roiLeftLabel = QLabel("Left")
        self.roiLeftEdit = QLineEdit()
        self.roiLeftEdit.setMaximumSize(64,24)
        self.roiRightLabel = QLabel("Right")
        self.roiRightEdit = QLineEdit()
        self.roiRightEdit.setMaximumSize(64,24)
        self.roiTopLabel = QLabel("Top")
        self.roiTopEdit = QLineEdit()
        self.roiTopEdit.setMaximumSize(64,24)
        self.roiBottomLabel = QLabel("Bottom")
        self.roiBottomEdit = QLineEdit()
        self.roiBottomEdit.setMaximumSize(64,24)

        self.applyRoiButton = QPushButton()
        self.applyRoiButton.setText('Apply')
        self.applyRoiButton.setMinimumSize(128,24)
        self.applyRoiButton.setMaximumSize(128,24)

        self.mouseCenterButton = QPushButton()
        self.mouseCenterButton.setText('Center at Mouse')
        self.mouseCenterButton.clicked.connect(self.mouseCenter)
        self.mouseCenterButton.setMinimumSize(128,24)
        self.mouseCenterButton.setMaximumSize(128,24)

        self.energyLabel = QLabel("Energy (eV)")
        self.energyEdit = QLineEdit()
        self.energyEdit.setMaximumSize(64,24)
        self.wavelengthLabel = QLabel("Wavelength (nm)")
        self.wavelengthEdit = QLineEdit()
        self.wavelengthEdit.setMaximumSize(64,24)
        self.pixelSizeLabel = QLabel('Pixel Size ('+u'\u03BC'+'m)')
        self.pixelSizeEdit = QLineEdit()
        self.pixelSizeEdit.setMaximumSize(64,24)
        self.ccdZLabel = QLabel("Detector Distance (mm)")
        self.ccdZEdit = QLineEdit()
        self.ccdZEdit.setMaximumSize(64,24)

        self.plotBox = QComboBox()
        self.plotBox.addItem("Plot Pixels")
        self.plotBox.addItem("Plot Q")
        self.plotBox.setMaximumSize(128,24)
        self.plotBox.setMinimumSize(128,24)

        self.hLine = QFrame()
        self.hLine.setFrameShape(QFrame.HLine)
        self.hLine.setFrameShadow(QFrame.Sunken)
        self.hLine.setMinimumSize(QSize(self.plotSize.width(),10))
        self.hLine2 = QFrame()
        self.hLine2.setFrameShape(QFrame.HLine)
        self.hLine2.setFrameShadow(QFrame.Sunken)
        self.hLine2.setMinimumSize(QSize(self.plotSize.width(),10))
        self.hLine3 = QFrame()
        self.hLine3.setFrameShape(QFrame.HLine)
        self.hLine3.setFrameShadow(QFrame.Sunken)
        self.hLine3.setMinimumSize(QSize(self.plotSize.width(),10))

        self.roiSizeEdit.textChanged.connect(self.roiSizeChanged)
        self.roiCenterXEdit.textChanged.connect(self.roiCenterXChanged)
        self.roiCenterYEdit.textChanged.connect(self.roiCenterYChanged)
        self.roiLeftEdit.textChanged.connect(self.roiLeftChanged)
        self.roiRightEdit.textChanged.connect(self.roiRightChanged)
        self.roiTopEdit.textChanged.connect(self.roiTopChanged)
        self.roiBottomEdit.textChanged.connect(self.roiBottomChanged)
        self.roiBox.activated.connect(self.roiBoxChanged)
        self.applyRoiButton.clicked.connect(self.refresh)
        self.connect(self.energyEdit, SIGNAL('returnPressed()'), self.energyChanged)
        self.connect(self.wavelengthEdit, SIGNAL('returnPressed()'), self.wavelengthChanged)
        self.connect(self.pixelSizeEdit, SIGNAL('returnPressed()'), self.pixelSizeChanged)
        self.connect(self.ccdZEdit, SIGNAL('returnPressed()'), self.ccdZChanged)
        self.energyEdit.setText(str(0.00))
        self.wavelengthEdit.setText(str(0.00))
        self.pixelSizeEdit.setText(str(0.00))
        self.ccdZEdit.setText(str(0.00))

        grid.addWidget(self.roiLabel,0,0,1,2,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.roiBox,0,2,1,2,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.roiSizeLabel,1,0,1,1,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.roiSizeEdit,1,1,1,1,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.mouseCenterButton,1,2,1,2,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.roiCenterXLabel,2,0,1,1,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.roiCenterXEdit,2,1,1,1,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.roiCenterYLabel,2,2,1,1,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.roiCenterYEdit,2,3,1,1,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.roiLeftLabel,3,0,1,1,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.roiLeftEdit,3,1,1,1,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.roiRightLabel,3,2,1,1,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.roiRightEdit,3,3,1,1,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.roiTopLabel,4,0,1,1,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.roiTopEdit,4,1,1,1,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.roiBottomLabel,4,2,1,1,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.roiBottomEdit,4,3,1,1,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.applyRoiButton,5,2,1,2,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.hLine3,6,0,1,4,Qt.AlignTop | Qt.AlignHCenter)
        grid.addWidget(self.energyLabel,7,1,1,2,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.energyEdit,7,3,1,1,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.wavelengthLabel,8,1,1,2,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.wavelengthEdit,8,3,1,1,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.pixelSizeLabel,9,1,1,2,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.pixelSizeEdit,9,3,1,1,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.ccdZLabel,10,1,1,2,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.ccdZEdit,10,3,1,1,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.plotBox,11,2,1,2,Qt.AlignVCenter | Qt.AlignRight)

        return widget



    def mouseCenter(self):

        self.roiCenterX = int(self.xVal.text())
        self.roiCenterXEdit.setText(str(self.roiCenterX))
        self.roiCenterY = int(self.yVal.text())
        self.roiCenterYEdit.setText(str(self.roiCenterY))
        self.refresh()

    def pixelSizeChanged(self):

        try: self.pixelSize = round(float(self.pixelSizeEdit.text()),2)
        except ValueError: print "Pixel size must be a floating point number"
        else:
            self.pixelSizeEdit.setText(str(round(self.pixelSize,2)))

    def ccdZChanged(self):

        try: self.ccdZ = round(float(self.ccdZEdit.text()),2)
        except ValueError: print "CCD Z-distance must be a floating point number"
        else:
            self.ccdZEdit.setText(str(round(self.ccdZ,2)))

    def energyChanged(self):

        try: self.energy = round(float(self.energyEdit.text()),2)
        except ValueError: print "Energy must be a floating point number"
        else:
            self.wavelength = e2l(self.energy)
            self.wavelengthEdit.setText(str(round(self.wavelength,2)))
            self.energyEdit.setText(str(round(self.energy,2)))

    def wavelengthChanged(self):

        try: self.wavelength = round(float(self.wavelengthEdit.text()),2)
        except ValueError: print "Wavelength must be a floating point number"
        else:
            self.energy = e2l(self.wavelength)
            self.energyEdit.setText(str(round(self.energy,2)))
            self.wavelengthEdit.setText(str(round(self.wavelength,2)))

    def roiBoxChanged(self):

        if self.roiBox.currentIndex() == 0:
            self.roiSizeEdit.setEnabled(False)
            self.roiCenterXEdit.setEnabled(False)
            self.roiCenterYEdit.setEnabled(False)
            self.roiLeftEdit.setEnabled(True)
            self.roiRightEdit.setEnabled(True)
            self.roiTopEdit.setEnabled(True)
            self.roiBottomEdit.setEnabled(True)
        elif self.roiBox.currentIndex() == 1:
            self.roiSizeEdit.setEnabled(True)
            self.roiCenterXEdit.setEnabled(True)
            self.roiCenterYEdit.setEnabled(True)
            self.roiLeftEdit.setEnabled(False)
            self.roiRightEdit.setEnabled(False)
            self.roiTopEdit.setEnabled(False)
            self.roiBottomEdit.setEnabled(False)

    def roiSizeChanged(self):

        try: self.roiSize = int(str(self.roiSizeEdit.text()))
        except ValueError: print "ROI Size must be an integer"
        #else: print self.roiSize

    def roiCenterXChanged(self):

        try: self.roiCenterX = int(str(self.roiCenterXEdit.text()))
        except ValueError: print "Center X must be an integer"
        #else: print self.roiCenterX

    def roiCenterYChanged(self):

        try: self.roiCenterY = int(str(self.roiCenterYEdit.text()))
        except ValueError: print "Center Y must be an integer"
        #else: print self.roiCenterY

    def roiLeftChanged(self):

        try: self.roiLeft = int(str(self.roiLeftEdit.text()))
        except ValueError: print "ROI Left must be an integer"
        #else: print self.roiLeft

    def roiRightChanged(self):

        try: self.roiRight = int(str(self.roiRightEdit.text()))
        except ValueError: print "ROI Right must be an integer"
        #else: print self.roiRight

    def roiTopChanged(self):

        try: self.roiTop = int(str(self.roiTopEdit.text()))
        except ValueError: print "ROI Top must be an integer"
        #else: print self.roiTop

    def roiBottomChanged(self):

        try: self.roiBottom = int(str(self.roiBottomEdit.text()))
        except ValueError: print "ROI Bottom must be an integer"
        #else: print self.roiBottom

    def setHistPlot(self):

        self.plotType = 'histogram'
        self.refresh()

    def setHorLinePlot(self):

        self.plotType = 'horLine'
        self.refresh()

    def setVerLinePlot(self):

        self.plotType = 'verLine'
        self.refresh()

    def setRadialPlot(self):

        self.plotType = 'radial'
        self.refresh()

    def setSpecialLinePlot(self):

        self.plotType = 'specialLine'
        self.refresh()

    def updatePlot(self):

        self.plot.drawPath()

    def setRoiText(self):

        self.roiLeftEdit.setText(str(self.roiLeft))
        self.roiRightEdit.setText(str(self.roiRight))
        self.roiTopEdit.setText(str(self.roiTop))
        self.roiBottomEdit.setText(str(self.roiBottom))
        self.roiCenterXEdit.setText(str(self.roiCenterX))
        self.roiCenterYEdit.setText(str(self.roiCenterY))
        self.roiSizeEdit.setText(str(self.roiSize))

    def refresh(self):

        self.emit(SIGNAL("refresh"), ())

#class plotWindow(QFrame):
#    def __init__(self, parent = None, size = None):
#        QFrame.__init__(self, parent)
#
#        self.imageSize = size
#        self.gradient = True
#        self.penColor = Qt.black
#        self.gradientColor = Qt.gray
#        self.setBackgroundRole(QPalette.Base)
#        self.path = QPainterPath()
#        self.penSize = 3
#        self.borderColor = Qt.gray
#        self.borderWidth = 3
#
#        if self.gradient:
#            if self.imageSize.height() > self.imageSize.width():
#                self.gradientMap = QLinearGradient(self.penSize, 0, self.imageSize.width()-self.penSize, 0)
#            else:
#                self.gradientMap = QLinearGradient(0, self.imageSize.height()-self.penSize, 0, 0)
#            self.gradientMap.setColorAt(0.0, Qt.white)
#            self.gradientMap.setColorAt(1.0, self.gradientColor)
#
#    def minimumSizeHint(self):
#        return self.imageSize
#
#    def sizeHint(self):
#        return self.imageSize
#
#    def paintEvent(self, event):
#        painter = QPainter()
#        painter.begin(self)
#        painter.setRenderHint(QPainter.Antialiasing,True)
#        painter.setBrush(QBrush(Qt.white))
#        painter.setPen(QPen(self.borderColor, self.penSize,
#                Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin))
#        painter.drawRect(0, 0, self.imageSize.width(), self.imageSize.height())
#        painter.setPen(QPen(Qt.blue, 2.0,
#                Qt.SolidLine, Qt.RoundCap))
#        #if self.gradient: painter.setBrush(QBrush(self.gradientMap))
#        painter.drawPath(self.path)
#        painter.end()

    def drawPath(self):

        self.update()

class qtPtychoViewerMain(QMainWindow):
    def __init__(self, parent = None, dataFile = None):

        super(qtPtychoViewerMain, self).__init__()

        self.histNBins = 10
        self.__thumbSize = 600
        self.mainImageSize = QSize(self.__thumbSize, self.__thumbSize)
        self.setWindowTitle('qtDiffInspector')
        self.sidePlotSize = QSize(96,self.__thumbSize)
        self.bottomPlotSize = QSize(self.__thumbSize,96)
        self.selectedRect = QRect()
        self.selectedRectTL = QPoint()
        self.selectedRectBR = QPoint()
        self.dataRect = QRect()
        self.dataStartRect = QRect()
        self.imageRect = QRect()
        self.dataRectTL = QPoint()
        self.dataRectBR = QPoint()
        self.imStartPt = QPoint()
        self.__zoomX = self.__thumbSize
        self.__zoomY = self.__thumbSize
        self.dataMin = None
        self.dataMax = None
        self.dataX, self.dataY = 0,0
        self.dataSlice = None
        self.plotType = 'horLine'
        self.dataXform = None
        self.gotXform = False
        self.drawFilter = False
        self.drawPolygon = False
        self.isComplex = False
        self.filePath = os.environ['PTYCHODATAROOT']
        self.homePath = os.environ['PYPTYCHOROOT'] + '/pyptycho/gui/'
        self.fileList = []
        self.fileIndex = 0
        self.fileTypes = 'tiff','tif','TIFF','TIF','npy','xim','hdr', 'cxi'
        self.display = 0
        self.cmap = 'bone'
        self.frameIndex = 0
        self.nFrames = 0

        self.setCentralWidget(self.inspectorWidget())
        self.layout().setSizeConstraint(QLayout.SetFixedSize)

        open = QAction(QIcon(self.homePath + 'icons/open.png'), 'Open', self)
        open.setShortcut('Ctrl+O')
        open.setStatusTip('Open File')
        self.connect(open, SIGNAL('triggered()'), self.chooseFile)

        saveData = QAction('Save Data', self)
        saveData.setShortcut('Ctrl+S')
        saveData.setStatusTip('Save Data File')
        self.connect(saveData, SIGNAL('triggered()'), self.saveFile)

        saveImage = QAction('Save Image', self)
        saveImage.setShortcut('Ctrl+I')
        saveImage.setStatusTip('Save Image File')
        self.connect(saveImage, SIGNAL('triggered()'), self.saveImage)

        reload = QAction('Reload', self)
        reload.setShortcut('Ctrl+R')
        reload.setStatusTip('Reload File')
        self.connect(reload, SIGNAL('triggered()'), self.reload)

        exit = QAction(QIcon(self.homePath + 'icons/exit.png'), 'Exit', self)
        exit.setShortcut('Ctrl+Q')
        exit.setStatusTip('Exit application')
        self.connect(exit, SIGNAL('triggered()'), SLOT('close()'))

        openPolygon = QAction('Open Polygon', self)
        openPolygon.setStatusTip('Open Polygon')
        self.connect(openPolygon, SIGNAL('triggered()'), self.openPolygon)

        savePolygon = QAction('Save Polygon', self)
        savePolygon.setStatusTip('Save Polygon')
        self.connect(savePolygon, SIGNAL('triggered()'), self.savePolygon)

        applyPolygon = QAction('Apply Polygon', self)
        applyPolygon.setStatusTip('Apply Polygon')
        self.connect(applyPolygon, SIGNAL('triggered()'), self.applyPolygon)

        clearPolygon = QAction('Clear Polygon', self)
        clearPolygon.setStatusTip('Clear Polygon')
        self.connect(clearPolygon, SIGNAL('triggered()'), self.clearPolygon)

        fitPolynomial = QAction('Fit Polynomial', self)
        fitPolynomial.setStatusTip('Fit Polynomial')
        self.connect(fitPolynomial, SIGNAL('triggered()'), self.fitPolynomial)

        levels = QAction('Levels', self)
        levels.setStatusTip('Set Thresholds')
        self.connect(levels, SIGNAL('triggered()'), self.levels)

        self.statusBar()

    def savePolynomial(self):
        pass

    def fitPolynomial(self):
        self.mainImage.polygonFit = QPolygon()
        if self.mainImage.polygon.size() > 1:
            xImagePixelValues = []
            yImagePixelValues = []
            for i in range(self.mainImage.polygon.size()):
                xImagePixelValues.append(self.mainImage.polygon.point(i).x())
                yImagePixelValues.append(self.mainImage.polygon.point(i).y())

            this_x_size = self.dataRect.width() #num pixels in current data sub-array
            this_y_size = self.dataRect.height()
            xpix = np.single(np.array(xImagePixelValues))-(float(self.mainImageSize.width())/2.-float(self.__zoomX)/2.)
            ypix = np.single(np.array(yImagePixelValues))-(float(self.mainImageSize.height())/2.-float(self.__zoomY)/2.)
            xDataPixelValues = (((xpix/float(self.__zoomX))*float(this_x_size)).round()).astype('int')+self.dataRect.left() - 1
            yDataPixelValues = (((ypix/float(self.__zoomY))*float(this_y_size)).round()).astype('int')+self.dataRect.top() - 1

            fitfunc = lambda p, x: p[0]*sin(np.pi * x / 180. + p[1]) + p[2] # Target function
            errfunc = lambda p, x, y: fitfunc(p, x) - y # Distance to the target function
            p0 = [1000.,1.,1.] # Initial guess for the parameters
            p1, success = optimize.leastsq(errfunc, p0[:], args = (yDataPixelValues, xDataPixelValues))
            xfitDataPixelValues = fitfunc(p1, yDataPixelValues)


            #pfit = np.polyfit(yDataPixelValues,xDataPixelValues,9)
            #xfitDataPixelValues = np.polyval(pfit,yDataPixelValues)
            xfitImagePixelValues = ((np.single(xfitDataPixelValues + 1 - self.dataRect.left()) / float(this_x_size) *\
                                     float(self.__zoomX) + (float(self.mainImageSize.width())/2. -\
                                                            float(self.__zoomX)/2.))).round().astype('int')
            for i in range(len(xfitImagePixelValues)):
                self.mainImage.polygonFit.prepend(QPoint(xfitImagePixelValues[i],yImagePixelValues[i]))
            self.updateImage()
            if self.STXMdata:
                xMotorPoints = np.array(self.STXMdata.xPoints)[np.array(xDataPixelValues)]
                yMotorPoints = np.array(self.STXMdata.yPoints)[np.array(yDataPixelValues)]
                pfit = np.polyfit(yMotorPoints,xMotorPoints, 9)
                xfitMotorPoints = np.polyval(pfit, yMotorPoints)
                print pfit
                print "Actual - Fit - Residual"
                for i in range(len(xfitMotorPoints)):
                    print xMotorPoints[i],xfitMotorPoints[i],abs(xMotorPoints[i]-xfitMotorPoints[i])

        else:
            print "Polynomial fit requires more than 1 point"

    def openPolygon(self):

        pass

    def savePolygon(self):

        pass

    def applyPolygon(self):

        pass

    def clearPolygon(self):

        pass

    def levels(self):

        self.diffLevels = levelsWindow(data = self.data)
        self.connect(self.diffLevels, SIGNAL("Levels"), self.applyLevels)
        self.connect(self.diffLevels, SIGNAL("Reset"),self.clear)
        self.diffLevels.show()
        sys.exit(self.exec_())

    def applyLevels(self):

        if self.diffLevels.maskButton.isChecked():
            print "Applying mask with values [lo,hi]=[%.2f,%.2f]" %(self.diffLevels.lowValue,self.diffLevels.hiValue)
            self.mask = (self.data > self.diffLevels.lowValue) * (self.data < self.diffLevels.hiValue)
        else:
            self.mask = np.ones(self.data.shape)
            self.dataMin = self.diffLevels.lowValue
            self.dataMax = self.diffLevels.hiValue
        self.updateImage()
        self.imageClick(x1 = self.xpt, x2 = None, y1 = self.ypt, y2 = None)

    def keyPressEvent(self, event):

        self.mainImage.key = event.key()

    def keyReleaseEvent(self, event):

        self.mainImage.key = None

    def inspectorWidget(self):

        widget = QWidget(self)
        grid = QGridLayout(widget)
        grid.setSpacing(4)
        grid.setMargin(4)

        self.tB = self.toolBar()
        self.dW = self.displayWidget()
        grid.addWidget(self.tB, 0, 0, 1, 1, Qt.AlignVCenter | Qt.AlignLeft)
        grid.addWidget(self.dW, 1, 0, 1, 1, Qt.AlignVCenter | Qt.AlignLeft)
        return widget

    def displayWidget(self):

        widget = QWidget(self)
        grid = QGridLayout(widget)
        grid.setSpacing(4)
        grid.setMargin(4)
        self.mainImage = imageWindow(widget,size = self.mainImageSize,\
        pressFunction = self.imageClick, moveFunction = self.imageDrag)
        self.mainImage.setMouseTracking(True)
        self.bottomPlot = plotWindow(widget,size = self.bottomPlotSize)
        self.bottomPlot.printValues = True
        self.sidePlot = plotWindow(widget,size = self.sidePlotSize)

        bg = "QLabel {background-color:white}"
        self.sidePlot.setStyleSheet( bg )

        bg = "QLabel {background-color:white}"
        self.bottomPlot.setStyleSheet( bg )

        self.connect(self.mainImage, SIGNAL("clearToggles"), self.clearToggles)
        self.connect(self.mainImage, SIGNAL("calcPoints"), self.fitPolynomial)

        self.sideInfo = infoWidget(widget)
        self.connect(self.sideInfo, SIGNAL("refresh"), self.placeHolder)#self.updateInfoPlot)
        self.connect(self.sideInfo,SIGNAL("refresh"),self.updateROI)

        self.logButton = QRadioButton()
        self.logButton.setText('Log Scale')
        self.logButton.clicked.connect(self.logButtonClicked)

        self.zoomButton = QPushButton()
        self.zoomButton.setText('Zoom')
        self.zoomButton.clicked.connect(self.zoom)

        self.scaleButton = QPushButton()
        self.scaleButton.setText('Autoscale')
        self.scaleButton.setGeometry(10,10,100,30)
        self.scaleButton.clicked.connect(self.autoscale)

        grid.addWidget(self.mainImage, 1, 0,1,1)#, Qt.AlignTop | Qt.AlignHCenter)
        grid.addWidget(self.sidePlot, 1, 1,1,1)#, Qt.AlignTop | Qt.AlignHCenter)
        grid.addWidget(self.bottomPlot, 2, 0,3,1)#, Qt.AlignTop | Qt.AlignHCenter)
        grid.addWidget(self.sideInfo, 1, 2, 4, 2, Qt.AlignTop | Qt.AlignHCenter)
        grid.addWidget(self.logButton, 2,1,1,1,Qt.AlignBottom | Qt.AlignHCenter)
        grid.addWidget(self.zoomButton, 3,1,1,1,Qt.AlignBottom | Qt.AlignHCenter)
        grid.addWidget(self.scaleButton, 4,1,1,1,Qt.AlignBottom | Qt.AlignHCenter)
        #self.setLayout(grid)
	widget.setLayout(grid)
        return widget

    def placeHolder(self):

        pass

    def logButtonClicked(self):

        self.updateImage()


    def updateROI(self):

        if self.sideInfo.roiBox.currentIndex() == 0:
            self.dataRect.setLeft(self.sideInfo.roiLeft)
            self.dataRect.setRight(self.sideInfo.roiRight)
            self.dataRect.setTop(self.sideInfo.roiTop)
            self.dataRect.setBottom(self.sideInfo.roiBottom)
        elif self.sideInfo.roiBox.currentIndex() == 1:
            self.dataRect.setLeft(self.sideInfo.roiCenterX - self.sideInfo.roiSize / 2)
            self.dataRect.setRight(self.sideInfo.roiCenterX + self.sideInfo.roiSize / 2)
            self.dataRect.setTop(self.sideInfo.roiCenterY - self.sideInfo.roiSize / 2)
            self.dataRect.setBottom(self.sideInfo.roiCenterY + self.sideInfo.roiSize / 2)

        if (self.dataRect.width() > self.dataRect.height()):
            factor = float(self.dataRect.height())/float(self.dataRect.width())
            self.__zoomX = self.mainImageSize.width()
            self.__zoomY = factor * self.__zoomX
        elif (self.dataRect.width() < self.dataRect.height()):
            factor = float(self.dataRect.width())/float(self.dataRect.height())
            self.__zoomY = self.mainImageSize.height()
            self.__zoomX = factor * self.__zoomY
        else:
            self.__zoomX, self.__zoomY = self.mainImageSize.width(),self.mainImageSize.height()
        self.setRoiText()
        self.updateImage()

    def toolBar(self):

        widget = QWidget(self)
        grid = QGridLayout(widget)
        grid.setSpacing(0)
        grid.setMargin(0)

        self.openButton = QPushButton(QIcon(self.homePath + 'icons/open.png'),'')
        self.openButton.clicked.connect(self.chooseFile)
        self.openButton.setMaximumSize(32, 32)
        #self.openButton.setMinimumSize(32, 32)
        self.openButton.setToolTip('Open New File')

        self.saveButton = QPushButton(QIcon(self.homePath + 'icons/save.png'),'')
        self.saveButton.clicked.connect(self.saveFile)
        self.saveButton.setMaximumSize(32, 32)
        self.saveButton.setToolTip('Save Data File')

        self.saveImageButton = QPushButton(QIcon(self.homePath + 'icons/saveImage.png'),'')
        self.saveImageButton.clicked.connect(self.saveImage)
        self.saveImageButton.setMaximumSize(32, 32)
        self.saveImageButton.setToolTip('Save Image File')

        self.printButton = QPushButton(QIcon(self.homePath + 'icons/print.png'),'')
        self.printButton.clicked.connect(self.printFile)
        self.printButton.setMaximumSize(32, 32)
        self.printButton.setToolTip('Print')

        self.pdfButton = QPushButton(QIcon(self.homePath + 'icons/pdf.png'),'')
        self.pdfButton.clicked.connect(self.genPDF)
        self.pdfButton.setMaximumSize(32, 32)
        self.pdfButton.setToolTip('Generate PDF')

        self.vLine = QFrame(self)
        self.vLine.setFrameShape(QFrame.VLine)
        self.vLine.setFrameShadow(QFrame.Sunken)
        self.vLine.setMinimumSize(QSize(2,32))

        self.clearButton = QPushButton(QIcon(self.homePath + 'icons/clear.png'),'')
        self.clearButton.clicked.connect(self.clear)
        self.clearButton.setMaximumSize(32, 32)
        self.clearButton.setToolTip('Reset')

        self.reloadButton = QPushButton(QIcon(self.homePath + 'icons/reload.png'),'')
        self.reloadButton.clicked.connect(self.reload)
        self.reloadButton.setMaximumSize(32, 32)
        self.reloadButton.setToolTip('Reload Current File')

        setHist = QAction('Histogram', self,triggered=self.setHistPlot)
        setHist.setStatusTip('Histogram Plot')

        setRadial = QAction('Radial Average', self, triggered=self.setRadialPlot)
        setRadial.setStatusTip('Radial Average')

        setHorLine = QAction('Horizontal Lineout', self, triggered=self.setHorLinePlot)
        setHorLine.setStatusTip('Horizontal Lineout')

        setVerLine = QAction('Vertical Lineout', self,triggered=self.setVerLinePlot)
        setVerLine.setStatusTip('Vertical Lineout')

        setSpecialLine = QAction('Slice', self,triggered=self.setSpecialLinePlot)
        setSpecialLine.setStatusTip('Slice')

        setImageReal = QAction('Image Real Part', self,triggered=self.setImageReal)
        setImageReal.setStatusTip('Display Image Real Part')

        setImageImag = QAction('Image Imaginary Part', self,triggered=self.setImageImag)
        setImageImag.setStatusTip('Display Image Imaginary Part')

        setImageComplex = QAction('Complex Image', self,triggered=self.setImageComplex)
        setImageComplex.setStatusTip('Display Complex Image')

        setImagePhase = QAction('Image Phase', self,triggered=self.setImagePhase)
        setImagePhase.setStatusTip('Display Image Phase')

        setImageMag = QAction('Image Magnitude', self,triggered=self.setImageMag)
        setImageMag.setStatusTip('Display Image Intensity')

        setProbePhase = QAction('Probe Phase', self,triggered=self.setProbePhase)
        setProbePhase.setStatusTip('Display Probe Phase')

        setProbeMag = QAction('Probe Magnitude', self,triggered=self.setProbeMag)
        setProbeMag.setStatusTip('Display Probe Magnitude')

        setDataMean = QAction('Data Mean', self,triggered=self.setDataMean)
        setDataMean.setStatusTip('Display Data Mean')

        setData = QAction('Data Frame', self,triggered=self.setData)
        setData.setStatusTip('Display Data Frame')

        setIllumination = QAction('Illumination Pattern', self,triggered=self.setIllumination)
        setIllumination.setStatusTip('Display Illumination Pattern')

        setSTXM = QAction('STXM', self,triggered=self.setSTXM)
        setSTXM.setStatusTip('Display STXM Image')

        setBG = QAction('Background', self,triggered=self.setBG)
        setBG.setStatusTip('Display Calculated Background Data')

        setStartImage = QAction('Start Image', self,triggered=self.setStartImage)
        setStartImage.setStatusTip('Display Start Image')

        setGray = QAction(QIcon(self.homePath + 'icons/reload.png'),'Gray', self,triggered=self.setGray)
        #setGray = QAction(QIcon(QPixmap.fromImage(numpy2qimage(np.outer(np.arange(0,1,0.01),np.ones(10)),cmap = 'gray'))),'Gray', self,triggered=self.setGray)
        setGray.setStatusTip('Colormap: gray')

        setJet = QAction('Jet', self,triggered=self.setJet)
        setJet.setStatusTip('Colormap: jet')

        setHot = QAction('Hot', self,triggered=self.setHot)
        setHot.setStatusTip('Colormap: hot')

        setStern = QAction('Stern', self,triggered=self.setStern)
        setStern.setStatusTip('Colormap: Stern')

        setEarth = QAction('Earth', self,triggered=self.setEarth)
        setEarth.setStatusTip('Colormap: Earth')

        setBone = QAction('Bone', self,triggered=self.setBone)
        setBone.setStatusTip('Colormap: Bone')

        self.imageTypeButton = QPushButton()
        self.imageTypeButton.setText('Image')
        self.imageTypeButton.setMinimumSize(64, 26)

        imageMenu = QMenu()
        imageMenu.addAction(setImageReal)
        imageMenu.addAction(setImageImag)
        imageMenu.addAction(setImageComplex)
        imageMenu.addAction(setImagePhase)
        imageMenu.addAction(setImageMag)
        imageMenu.addAction(setProbePhase)
        imageMenu.addAction(setProbeMag)
        imageMenu.addAction(setDataMean)
        imageMenu.addAction(setData)
        imageMenu.addAction(setIllumination)
        imageMenu.addAction(setSTXM)
        imageMenu.addAction(setBG)
        imageMenu.addAction(setStartImage)
        self.imageTypeButton.setMenu(imageMenu)

        self.plotTypeButton = QPushButton()
        self.plotTypeButton.setText('Plot')
        self.plotTypeButton.setMinimumSize(64, 26)

        plotMenu = QMenu()
        plotMenu.addAction(setHist)
        plotMenu.addAction(setHorLine)
        plotMenu.addAction(setVerLine)
        plotMenu.addAction(setRadial)
        plotMenu.addAction(setSpecialLine)
        self.plotTypeButton.setMenu(plotMenu)

        self.colorMapButton = QPushButton()
        self.colorMapButton.setText('Color map')
        self.colorMapButton.setMinimumSize(64, 26)

        colorMenu = QMenu()
        colorMenu.addAction(setGray)
        colorMenu.addAction(setJet)
        colorMenu.addAction(setHot)
        colorMenu.addAction(setStern)
        colorMenu.addAction(setEarth)
        colorMenu.addAction(setBone)
        self.colorMapButton.setMenu(colorMenu)

        self.fftshiftButton = QPushButton(QIcon(self.homePath + 'icons/fftshift.png'),'')
        self.fftshiftButton.clicked.connect(self.fftshift)
        self.fftshiftButton.setMaximumSize(32,32)
        self.fftshiftButton.setToolTip('0 Frequency Shift Data')

        self.fftButton = QPushButton(QIcon(self.homePath + 'icons/fft.png'),'')
        self.fftButton.clicked.connect(self.fft)
        self.fftButton.setMaximumSize(32, 32)
        self.fftButton.setToolTip('Fourier Transform Data')

        self.unzoomButton = QPushButton(QIcon(self.homePath + 'icons/unzoom.png'),'')
        self.unzoomButton.clicked.connect(self.unzoom)
        self.unzoomButton.setMaximumSize(32, 32)
        self.unzoomButton.setToolTip('Zoom Out')

        self.filterButton = QPushButton(QIcon(self.homePath + 'icons/filter.png'),'')
        self.filterButton.clicked.connect(self.setFilter)
        self.filterButton.setMaximumSize(32, 32)
        self.filterButton.setToolTip('Draw FFT Filter')
        self.filterButton.setCheckable(True)

        self.polygonButton = QPushButton(QIcon(self.homePath + 'icons/polygon.png'),'')
        self.polygonButton.clicked.connect(self.setPolygon)
        self.polygonButton.setMaximumSize(32, 32)
        self.polygonButton.setToolTip('Draw Free Polygon')
        self.polygonButton.setCheckable(True)

        self.pointsButton = QPushButton(QIcon(self.homePath + 'icons/points.png'),'')
        self.pointsButton.clicked.connect(self.setPoints)
        self.pointsButton.setMaximumSize(32, 32)
        self.pointsButton.setToolTip('Draw Points')
        self.pointsButton.setCheckable(True)

        self.headerButton = QPushButton()
        self.headerButton.setText('Header')
        self.headerButton.clicked.connect(self.headerClicked)
        self.headerButton.setMaximumSize(64,32)
        self.headerButton.setToolTip('Display Header Information')

        self.filesButton = QPushButton(QIcon(self.homePath + 'icons/contents.png'),'')
        self.filesButton.clicked.connect(self.fileViewer)
        self.filesButton.setMaximumSize(32, 32)
        self.filesButton.setToolTip('Show Working Directory')

        self.nextButton = QPushButton(QIcon(self.homePath + 'icons/next.png'),'')
        self.nextButton.clicked.connect(self.openNext)
        self.nextButton.setMaximumSize(32, 32)
        self.nextButton.setToolTip('Open Next File in Directory')

        self.previousButton = QPushButton(QIcon(self.homePath + 'icons/previous.png'),'')
        self.previousButton.clicked.connect(self.openPrevious)
        self.previousButton.setMaximumSize(32, 32)
        self.previousButton.setToolTip('Open Previous File in Directory')

        self.frameLabel = QLabel()
        self.frameLabel.setText("Frame Number")
        self.frameEdit = QLineEdit()
        self.frameEdit.setMaximumSize(64,24)
        self.frameEdit.setText('0')

        self.nextFrameButton = QPushButton(QIcon(self.homePath + 'icons/next.png'),'')
        self.nextFrameButton.clicked.connect(self.openNextFrame)
        self.nextFrameButton.setMaximumSize(32, 32)
        self.nextFrameButton.setToolTip('Display Next Data Frame')

        self.previousFrameButton = QPushButton(QIcon(self.homePath + 'icons/previous.png'),'')
        self.previousFrameButton.clicked.connect(self.openPreviousFrame)
        self.previousFrameButton.setMaximumSize(32, 32)
        self.previousFrameButton.setToolTip('Display Previous Data Frame')

        if sys.platform == 'darwin': grid.setHorizontalSpacing(13)
        else: grid.setHorizontalSpacing(1)
        grid.addWidget(self.openButton,0,0,1,1,Qt.AlignVCenter | Qt.AlignLeft)
        grid.addWidget(self.saveButton,0,1,1,1,Qt.AlignVCenter | Qt.AlignLeft)
        grid.addWidget(self.saveImageButton,0,2,1,1,Qt.AlignVCenter | Qt.AlignLeft)
        grid.addWidget(self.printButton,0,3,1,1,Qt.AlignVCenter | Qt.AlignLeft)
        grid.addWidget(self.pdfButton,0,4,1,1,Qt.AlignVCenter | Qt.AlignLeft)
        grid.addWidget(self.vLine,0,5,1,1,Qt.AlignVCenter | Qt.AlignLeft)
        grid.addWidget(self.previousButton,0,6,1,1, Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.nextButton,0,7,1,1, Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.unzoomButton,0,8,1,1, Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.clearButton,0,9,1,1,Qt.AlignVCenter | Qt.AlignLeft)
        grid.addWidget(self.reloadButton,0,10,1,1, Qt.AlignVCenter | Qt.AlignLeft)
        grid.addWidget(self.plotTypeButton,0,15,1,1, Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.imageTypeButton,0,14,1,1, Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.colorMapButton,0,16,1,1, Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.frameLabel,0,17,1,1, Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.frameEdit,0,18,1,1, Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.previousFrameButton,0,19,1,1, Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.nextFrameButton,0,20,1,1, Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.fftshiftButton,0,11,1,1, Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.fftButton,0,12,1,1, Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.headerButton,0,13,1,1, Qt.AlignVCenter | Qt.AlignRight)
        # grid.addWidget(self.filterButton,0,13,1,1, Qt.AlignVCenter | Qt.AlignRight)
        # grid.addWidget(self.polygonButton,0,14,1,1, Qt.AlignVCenter | Qt.AlignRight)
        # grid.addWidget(self.pointsButton,0,15,1,1, Qt.AlignVCenter | Qt.AlignRight)
        #grid.addWidget(self.filesButton,0,0,1,1, Qt.AlignVCenter | Qt.AlignRight)
        #self.setLayout(grid)
	widget.setLayout(grid)
        return widget

    def headerClicked(self):
        self.w = headerPopup()
        #self.w.setGeometry(QRect(100, 100, 400, 400))
        self.w.show()


    def openNextFrame(self):

        if (self.frameIndex < self.nFrames - 1): self.frameIndex += 1
        self.frameEdit.setText(str(self.frameIndex))
        if self.display == 5:
            self.data = self.cxi.ccddata[self.frameIndex]
            self.updateImage()

    def openPreviousFrame(self):

        if (self.frameIndex > 0): self.frameIndex -= 1
        self.frameEdit.setText(str(self.frameIndex))
        if self.display == 5:
            self.data = self.cxi.ccddata[self.frameIndex]
            self.updateImage()

    def setGray(self):

        self.cmap = 'gray'
        self.updateImage()

    def setHot(self):

        self.cmap = 'hot'
        self.updateImage()

    def setJet(self):

        self.cmap = 'jet'
        self.updateImage()

    def setStern(self):

        self.cmap = 'stern'
        self.updateImage()

    def setEarth(self):

        self.cmap = 'earth'
        self.updateImage()

    def setBone(self):

        self.cmap = 'bone'
        self.updateImage()

    def setImageReal(self):
        self.gotXform = False
        self.display = 0
        if self.cxi.image != None:
            self.data = self.cxi.image.real
            self.checkShape()
            self.dataShape = self.data.shape
            self.dataRect.setRect(0,0,self.dataShape[1]+1,self.dataShape[0]+1)
            self.unzoom()
        else: pass

    def setImageImag(self):
        self.gotXform = False
        self.display = 0
        if self.cxi.image != None:
            self.data = self.cxi.image.imag
            self.checkShape()
            self.dataShape = self.data.shape
            self.dataRect.setRect(0,0,self.dataShape[1]+1,self.dataShape[0]+1)
            self.unzoom()
        else: pass

    def setImageComplex(self):

        pass

    def setImagePhase(self):
        self.gotXform = False
        self.display = 0
        if self.cxi.image != None:
            #self.data = np.unwrap(np.angle(self.cxi.image),discont = 1.5 * np.pi)
            self.data = np.angle(self.cxi.image)
            self.checkShape()
            self.dataShape = self.data.shape
            self.dataRect.setRect(0,0,self.dataShape[1]+1,self.dataShape[0]+1)
            self.unzoom()
        else: pass

    def setImageMag(self):
        self.gotXform = False
        self.display = 0
        if self.cxi.image != None:
            self.data = abs(self.cxi.image)
            self.checkShape()
            self.dataShape = self.data.shape
            self.dataRect.setRect(0,0,self.dataShape[1]+1,self.dataShape[0]+1)
            self.unzoom()
        else: pass

    def setProbePhase(self):
        self.gotXform = False
        self.display = 2
        if self.probe != None:
            self.data = np.angle(self.probe)
            self.checkShape()
            self.dataShape = self.data.shape
            self.dataRect.setRect(0,0,self.dataShape[1]+1,self.dataShape[0]+1)
            self.unzoom()
        else: pass

    def setProbeMag(self):
        self.gotXform = False
        self.display = 3
        if self.probe != None:
            self.data = np.abs(self.probe)
            self.checkShape()
            self.dataShape = self.data.shape
            self.dataRect.setRect(0,0,self.dataShape[1]+1,self.dataShape[0]+1)
            self.unzoom()
        else: pass

    def setDataMean(self):
        self.gotXform = False
        self.display = 4
        if self.cxi.datamean != None:
            self.data = self.cxi.datamean
            self.checkShape()
            self.dataShape = self.data.shape
            self.dataRect.setRect(0,0,self.dataShape[1]+1,self.dataShape[0]+1)
            self.unzoom()
        else: pass

    def setData(self):
        self.gotXform = False
        self.display = 5
        if self.cxi.ccddata != None:
            self.data = self.cxi.ccddata[self.frameIndex]# - np.sqrt(self.cxi.bg)
            self.checkShape()
            self.dataShape = self.data.shape
            self.dataRect.setRect(0,0,self.dataShape[1]+1,self.dataShape[0]+1)
            self.unzoom(click = False)
        else: pass

    def setIllumination(self):
        self.gotXform = False
        self.display = 6
        if self.probe != None:
            self.data = abs(self.probe)
            self.checkShape()
            self.dataShape = self.data.shape
            self.dataRect.setRect(0,0,self.dataShape[1]+1,self.dataShape[0]+1)
            self.unzoom()
        else: pass

    def setSTXM(self):
        self.gotXform = False
        self.display = 0
        if self.cxi.stxm != None:
            if self.cxi.stxmInterp == None: self.data = self.cxi.stxm
            else: self.data = self.cxi.stxmInterp
            self.checkShape()
            self.dataShape = self.data.shape
            self.dataRect.setRect(0,0,self.dataShape[1]+1,self.dataShape[0]+1)
            self.unzoom()
        else: pass

    def setBG(self):
        self.gotXform = False
        self.display = 8
        if self.cxi.bg != None:
            self.data = self.cxi.bg
            self.checkShape()
            self.dataShape = self.data.shape
            self.dataRect.setRect(0,0,self.dataShape[1]+1,self.dataShape[0]+1)
            self.unzoom()
        else: pass

    def setStartImage(self):
        self.gotXform = False
        self.display = 9
        if self.cxi.startImage != None:
            self.data = abs(self.cxi.startImage)
            self.checkShape()
            self.dataShape = self.data.shape
            self.dataRect.setRect(0,0,self.dataShape[1]+1,self.dataShape[0]+1)
            self.unzoom()
        else: pass

    def openNext(self):

        if (self.fileIndex + 1) < self.nFiles:
            self.openFile(self.filePath + self.fileList[self.fileIndex + 1])
        else:
            pass

    def openPrevious(self):

        if (self.fileIndex - 1) > 0:
            self.openFile(self.filePath + self.fileList[self.fileIndex - 1])
        else:
            pass

    def fileViewer(self):

        window = Window(path = self.filePath)
        window.show()
        sys.exit(self.exec_())

    def setFilter(self):

        if self.filterButton.isChecked() == False:
            self.mainImage.drawFilter = False
            self.clearToggles()
        else:
            self.mainImage.drawFilter = True
            self.mainImage.drawPolygon = False
            self.mainImage.drawPoints = False
            self.mainImage.polygon = QPolygon()
            self.polygonButton.setChecked(False)
            self.pointsButton.setChecked(False)

    def setPolygon(self):

        if self.polygonButton.isChecked() == False:
            self.mainImage.drawPolygon = False
            self.clearToggles()
        else:
            self.mainImage.drawPolygon = True
            self.mainImage.drawFilter = False
            self.mainImage.drawPoints = False
            self.filterButton.setChecked(False)
            self.pointsButton.setChecked(False)
            self.mainImage.polygon = QPolygon()
            self.mainImage.update()

    def setPoints(self):

        if self.pointsButton.isChecked() == False:
            self.mainImage.drawPoints = False
            self.mainImage.points = QPolygon()
            self.clearToggles()
        else:
            self.mainImage.drawPoints = True
            self.mainImage.drawFilter = False
            self.mainImage.drawPolygon = False
            self.filterButton.setChecked(False)
            self.polygonButton.setChecked(False)
            self.mainImage.polygon = QPolygon()
            self.mainImage.update()

    def clearToggles(self):
        self.mainImage.drawFilter = False
        self.mainImage.dragFilter = False
        self.mainImage.filter = QRect()
        self.filterButton.setChecked(False)
        self.mainImage.drawPolygon = False
        self.mainImage.polygon = QPolygon()
        self.mainImage.polygonFit = QPolygon()
        self.polygonButton.setChecked(False)
        self.mainImage.drawPoints = False
        self.mainImage.points = QPolygon()
        self.pointsButton.setChecked(False)
        self.mainImage.pointsRectList = []
        self.mainImage.update()

    def unzoom(self, click = True, autoscale = True):
        self.mask = np.ones(self.data.shape)
        if self.dataShape[1] > self.dataShape[0]:
            self.__zoomX, self.__zoomY = self.__thumbSize, round(float(self.dataShape[0]) / float(self.dataShape[1]) * self.__thumbSize)
        else:
            self.__zoomX, self.__zoomY = round(float(self.dataShape[1]) / float(self.dataShape[0]) * self.__thumbSize), self.__thumbSize
        self.sideInfo.roiLeft = 0
        self.sideInfo.roiTop = 0
        self.sideInfo.roiRight = self.dataShape[1]
        self.sideInfo.roiBottom = self.dataShape[0]
        self.sideInfo.roiCenterX = self.dataShape[1]/2
        self.sideInfo.roiCenterY = self.dataShape[0]/2
        self.sideInfo.roiSize = min(self.dataShape[0],self.dataShape[1])
        self.logOffset = 0.0001 * self.data.max()
        self.dataRect.setRect(0,0,self.dataShape[1]+1,self.dataShape[0]+1)
        if autoscale: self.dataMin, self.dataMax = self.data.min(),self.data.max()
        self.mainImage.box.setRect(0,0,self.dataShape[1]+1,self.dataShape[0]+1)
        if click: self.imageClick(self.mainImageSize.width() / 2, 0,self.mainImageSize.height() / 2,0)
        self.updatePlots()
        self.setRoiText()
        self.updateImage()

    def fft(self):

        if self.mainImage.gotFilter:
            self.dataXform = abs(filtered_auto(self.data, diameter = self.filterWidth,\
                bp = True, center = (self.filterY,self.filterX)))
            self.gotXform = True
            self.autoscale()
        else:
            self.dataXform = abs(np.fft.fftshift(np.fft.fftn(self.data[\
                                                             self.dataRect.top():self.dataRect.bottom(),self.dataRect.left():\
            self.dataRect.right()]*self.mask[self.dataRect.top():\
            self.dataRect.bottom(),self.dataRect.left():self.dataRect.right()])))
            self.gotXform = True
            self.autoscale()
        self.mainImage.gotFilter = False
        self.mainImage.drawFilter = False
        self.mainImage.drawPolygon = False
        #self.mainImage.gotPolygon = False
        self.polygonButton.setChecked(False)
        self.filterButton.setChecked(False)

    def fftshift(self):

        self.data = np.fft.fftshift(self.data)
        self.mask = np.fft.fftshift(self.mask)
        self.updateImage()

    def setHistPlot(self):

        self.plotType = 'histogram'
        self.updateInfoPlot()

    def setHorLinePlot(self):

        self.plotType = 'horLine'
        self.updateInfoPlot()

    def setVerLinePlot(self):

        self.plotType = 'verLine'
        self.updateInfoPlot()

    def setRadialPlot(self):

        self.plotType = 'radial'
        self.updateInfoPlot()

    def setSpecialLinePlot(self):

        self.plotType = 'specialLine'
        self.updateInfoPlot()

    def saveFile(self):

        initialPath = QDir.currentPath() + '/untitled.tiff'

        fileName = QFileDialog.getSaveFileName(self, "Save As",
            initialPath,
            "%s Files (*.%s);;All Files (*)" %('TIF', 'tif'))
        if fileName:
            #im = Image.fromarray(self.data[self.dataRect.top():self.dataRect.bottom(),self.dataRect.left():self.dataRect.right()])
            temp = self.data #self.data[self.dataRect.top():self.dataRect.bottom(),self.dataRect.left():self.dataRect.right()]
            im = toimage(temp.astype('single'), high = temp.max(), low = temp.min(), mode = 'F')
            im.save(str(fileName))
        return False

    def clear(self):

        self.logButton.setChecked(False)
        if self.dataShape[1] > self.dataShape[0]:
            self.__zoomX, self.__zoomY = self.__thumbSize, round(float(self.dataShape[0]) / float(self.dataShape[1]) * self.__thumbSize)
        else:
            self.__zoomX, self.__zoomY = round(float(self.dataShape[1]) / float(self.dataShape[0]) * self.__thumbSize), self.__thumbSize
        self.logOffset = 0.0001 * self.data.max()
        self.dataRect.setRect(0,0,self.dataShape[1]+1,self.dataShape[0]+1)
        self.mainImage.box.setRect(0,0,self.dataShape[1]+1,self.dataShape[0]+1)
        self.dataXform = None
        self.gotXform = False
        self.imageClick(self.mainImageSize.width() / 2, 0,self.mainImageSize.height() / 2,0)
        self.mask = np.ones(self.data.shape)
        self.autoscale()
        self.updatePlots()

    def saveImage(self):

        initialPath = QDir.currentPath() + '/untitled.png'

        fileName = QFileDialog.getSaveFileName(self, "Save As",
            initialPath,
            "%s Files (*.%s);; %s Files (*.%s);; All Files (*)" % ('PNG','png','TIFF','tiff'))
        if fileName:
            if fileName.split('.')[-1::][0] == 'png':
                if self.logButton.isChecked():
                    return imsave(np.log(self.data[self.dataRect.top():\
                    self.dataRect.bottom(),self.dataRect.left():self.dataRect.right()] + self.logOffset),\
                        str(fileName),ct = self.cmap)
                else:
                    return imsave(np.clip(self.data[self.dataRect.top():self.dataRect.bottom(),\
                                          self.dataRect.left():self.dataRect.right()],self.dataMin,self.dataMax),\
                        str(fileName),ct = self.cmap)
            elif fileName.split('.')[-1::][0] == 'tiff':
                temp = Image.fromarray(abs(self.data[self.dataRect.top():self.dataRect.bottom(),\
                                           self.dataRect.left():self.dataRect.right()]))
                temp.save(str(fileName))
            else: print "Unrecognized file format: %s" %(fileName.split('.')[-1::][0])

        return False

    def numpyToImage(self, a, cmap):

        vmin, vmax = a.min(), a.max()
        if cmap == "bone":
            im = toimage(np.uint8(cm.bone((a.clip(vmin,vmax)-vmin)/(vmax-vmin))*255))
        elif cmap=='stern':
            im = toimage(np.uint8(cm.gist_stern((a.clip(vmin,vmax)-vmin)/(vmax-vmin))*255))
        elif cmap=='gray':
            im = toimage(np.uint8(cm.gray((a.clip(vmin,vmax)-vmin)/(vmax-vmin))*255))
        elif cmap=='gist_yarg':
            im = toimage(np.uint8(cm.gist_yarg((a.clip(vmin,vmax)-vmin)/(vmax-vmin))*255))
        elif cmap=='earth':
            im = toimage(np.uint8(cm.gist_earth((a.clip(vmin,vmax)-vmin)/(vmax-vmin))*255))
        elif cmap=='hot':
            im = toimage(np.uint8(cm.hot((a.clip(vmin,vmax)-vmin)/(vmax-vmin))*255))
        elif cmap=='jet':
            im = toimage(np.uint8(cm.jet((a.clip(vmin,vmax)-vmin)/(vmax-vmin))*255))
        elif cmap=='bone_r':
            im = toimage(np.uint8(cm.bone_r((a.clip(vmin,vmax)-vmin)/(vmax-vmin))*255))

        return im

    def printFile(self):

        self.lineSpace = 15
        self.pageWidth = 620.
        self.pageHeight = 820.
        self.margin = 40
        self.hSpace, self.vSpace = 40, 40
        sh = self.cxi.image.shape
        imageWidth = self.pageWidth / 2 - self.margin - self.hSpace / 2
        imageHeight = sh[0] * (imageWidth / sh[1])

        def hello(c):
            c.drawString(100,500,"Hello World")

        fileFormat = 'pdf'
        fileName = QDir.currentPath() + '/tmp.' + fileFormat

        if fileName:
            c = canvas.Canvas(fileName, pagesize = letter)
            c.drawInlineImage(self.numpyToImage(np.abs(self.cxi.image)**2, 'bone_r'), self.margin,self.margin, width = imageWidth,height = imageHeight)
            c.drawInlineImage(self.numpyToImage(np.angle(self.cxi.image), 'bone_r'), self.margin + imageWidth + self.hSpace, \
                self.margin, width = imageWidth, height = imageHeight)
            c.drawString(self.margin, 3 * self.lineSpace - 10 + self.margin + imageHeight, str(self.path))
            c.drawString(self.margin, 2 * self.lineSpace - 10 + self.margin + imageHeight, datetime.datetime.now().ctime())
            c.drawString(self.margin, self.lineSpace - 10 + self.margin + imageHeight, "Intensity")
            c.drawString(self.margin + imageWidth + self.hSpace, self.lineSpace - 10 + self.margin + imageHeight, "Phase")
            c.showPage()
            c.save()
            #call(["evince",fileName])
            call(["lpr", "-P",  "HP_LaserJet_400_color_M451dn", fileName])
            call(["rm", fileName])

    def genPDF(self):

        self.lineSpace = 15
        self.pageWidth = 620.
        self.pageHeight = 820.
        self.margin = 40
        self.hSpace, self.vSpace = 40, 40
        sh = self.cxi.image.shape
        imageWidth = self.pageWidth / 2 - self.margin - self.hSpace / 2
        imageHeight = sh[0] * (imageWidth / sh[1])

        def hello(c):
            c.drawString(100,500,"Hello World")

        fileFormat = 'pdf'
        fileName = 'tmp.pdf' #QDir.currentPath() + '/tmp.' + fileFormat

        if fileName:
            c = canvas.Canvas(fileName, pagesize = letter)
            c.drawInlineImage(self.numpyToImage(np.abs(self.cxi.image)**2, 'bone_r'), self.margin,self.margin, width = imageWidth,height = imageHeight)
            c.drawInlineImage(self.numpyToImage(np.angle(self.cxi.image), 'bone_r'), self.margin + imageWidth + self.hSpace,\
                self.margin, width = imageWidth, height = imageHeight)
            c.drawString(self.margin, 3 * self.lineSpace - 10 + self.margin + imageHeight, str(self.path))
            c.drawString(self.margin, 2 * self.lineSpace - 10 + self.margin + imageHeight, datetime.datetime.now().ctime())
            c.drawString(self.margin, self.lineSpace - 10 + self.margin + imageHeight, "Intensity")
            c.drawString(self.margin + imageWidth + self.hSpace, self.lineSpace - 10 + self.margin + imageHeight, "Phase")
            c.showPage()
            c.save()
            call(["evince",fileName])
            #call(["lpr", "-P",  "HP_LaserJet_400_color_M451dn", fileName])
            call(["rm", fileName])

    def updateInfoPlot(self):

        dummy = (self.sideInfo.energy * self.sideInfo.wavelength * self.sideInfo.pixelSize * self.sideInfo.ccdZ != 0.)
        q = (self.sideInfo.plotBox.currentIndex() == 1)

        if self.plotType == 'histogram':
            if self.gotXform:
                histVec, binEdges = np.histogram(self.dataXform[self.dataRect.top():self.dataRect.bottom(),\
                                                 self.dataRect.left():self.dataRect.right()], self.histNBins)
            else:
                histVec, binEdges = np.histogram(self.data[self.dataRect.top():self.dataRect.bottom(),\
                                                 self.dataRect.left():self.dataRect.right()], self.histNBins)
            histVec = np.log10(histVec)
            binEdges = binEdges / 1000.
            self.sideInfo.axes.clear()
            self.sideInfo.axes.grid(True,color = 'blue')
            self.sideInfo.axes.bar(binEdges[:-1],histVec,binEdges.max()/20.)
            self.sideInfo.axes.set_xlabel(r'Pixel Values ($\times$1000)')
            self.sideInfo.axes.set_ylabel(r'log$_{10}$(\# of Pixels)')
            self.sideInfo.axes.set_title(r'Histogram')
            self.sideInfo.canvas.draw()
        elif self.plotType == 'horLine':
            if self.gotXform: hVec = self.dataXform[self.dataY,self.dataRect.left():self.dataRect.right()]
            else: hVec = self.data[self.dataY,self.dataRect.left():self.dataRect.right()]
            x = np.arange(self.dataRect.left(),self.dataRect.right())
            self.sideInfo.axes.clear()
            self.sideInfo.axes.grid(True,color = 'blue')
            if dummy * q:
                x = 1. / self.sideInfo.wavelength * (x - self.dataShape[1] / 2.) * self.sideInfo.pixelSize / self.sideInfo.ccdZ / 1000.
                y = 1. / self.sideInfo.wavelength * (-self.dataY + self.dataShape[0]/2.) * self.sideInfo.pixelSize / self.sideInfo.ccdZ/1000.
                self.sideInfo.axes.set_xlabel(r'$Q_{x}$ (nm$^{-1}$)')
                self.sideInfo.axes.set_title(r'Horizontal Slice: $Q_{y}$ = %.4f' %(y))
            else:
                self.sideInfo.axes.set_xlabel(r'Horizontal Pixel Location')
                self.sideInfo.axes.set_title(r'Horizontal Slice: Y = %i' %self.dataY)
            self.sideInfo.axes.plot(x,hVec)
            self.sideInfo.axes.set_ylabel(r'Data Value')
            self.sideInfo.axes.axis((x.min(),x.max(),hVec.min(),hVec.max()+0.05*hVec.max()))
            self.sideInfo.canvas.draw()
        elif self.plotType == 'verLine':
            if self.gotXform: vVec = self.dataXform[self.dataRect.top():self.dataRect.bottom(),self.dataX]
            else: vVec = self.data[self.dataRect.top():self.dataRect.bottom(),self.dataX]
            y = np.arange(self.dataRect.top(),self.dataRect.bottom())
            self.sideInfo.axes.clear()
            self.sideInfo.axes.grid(True,color = 'blue')
            if dummy * q:
                x = 1. / self.sideInfo.wavelength * (-self.dataX + self.dataShape[1] / 2.) * self.sideInfo.pixelSize / self.sideInfo.ccdZ / 1000.
                y = 1. / self.sideInfo.wavelength * (y - self.dataShape[0]/2.) * self.sideInfo.pixelSize / self.sideInfo.ccdZ / 1000.
                self.sideInfo.axes.set_xlabel(r'$Q_{y}$ (nm$^{-1}$)')
                self.sideInfo.axes.set_title(r'Vertical Slice: $Q_{x}$ = %.4f' %(x))
            else:
                self.sideInfo.axes.set_xlabel(r'Vertical Pixel Location')
                self.sideInfo.axes.set_title(r'Vertical Slice: X = %i' %self.dataX)
            self.sideInfo.axes.plot(y,vVec)
            self.sideInfo.axes.set_ylabel(r'Data Value')
            self.sideInfo.axes.set_title(r'Vertical Slice')
            self.sideInfo.axes.axis((y.min(),y.max(),vVec.min(),vVec.max()+0.05*vVec.max()))
            self.sideInfo.canvas.draw()
        elif self.plotType == 'specialLine':
            sVec = self.dataSlice
            p = np.sqrt((self.xSlice - self.xSlice[0]).astype('float32')**2 +\
                        (self.ySlice - self.ySlice[0]).astype('float32')**2).astype('int16')
            self.sideInfo.axes.clear()
            self.sideInfo.axes.grid(True,color = 'blue')
            self.sideInfo.axes.plot(p,sVec)
            self.sideInfo.axes.set_xlabel(r'Slice Pixel Distance')
            self.sideInfo.axes.set_ylabel(r'Data Value')
            self.sideInfo.axes.set_title(r'Slice')
            self.sideInfo.axes.axis((p.min(),p.max(),sVec.min(),sVec.max()+0.05*sVec.max()))
            self.sideInfo.canvas.draw()
        elif self.plotType == 'radial':
            if self.gotXform: rVec, a = rot_ave(self.dataXform[self.dataRect.top():self.dataRect.bottom()+\
                                                                                   1,self.dataRect.left():self.dataRect.right()+1],1.,1.)
            else:
                if dummy * q:
                    rVec, a = rot_ave(self.data[self.dataRect.top():self.dataRect.bottom()+\
                                                                    1,self.dataRect.left():self.dataRect.right()+1],\
                        self.sideInfo.pixelSize / 1000., self.sideInfo.ccdZ)
                    a = 2. * a / self.sideInfo.wavelength
                else:
                    rVec, a = rot_ave(self.data[self.dataRect.top():self.dataRect.bottom()+\
                                                                    1,self.dataRect.left():self.dataRect.right()+1],1.,1.)
                    a = a / a.max() * len(self.data) * np.sqrt(2.)
            self.sideInfo.axes.clear()
            self.sideInfo.axes.grid(True,color = 'blue')
            self.sideInfo.axes.loglog(a,rVec)
            if dummy * q: self.sideInfo.axes.set_xlabel(r'Spatial Frequency (nm$^{-1}$)')
            else: self.sideInfo.axes.set_xlabel(r'Pixel Distance')
            self.sideInfo.axes.set_ylabel(r'Average Intensity (CCD Counts)')
            self.sideInfo.axes.set_title(r'Azimuthal Average')
            self.sideInfo.axes.axis((a.min(),a.max(),rVec.min(),rVec.max()+0.05*rVec.max()))
            self.sideInfo.canvas.draw()

    def updatePlots(self):

        self.bottomPlot.drawPath()
        self.sidePlot.drawPath()

    def autoscale(self):

        if self.mainImage.gotROI:
            this_x_size = self.dataRect.width() #num pixels in current data sub-array
            this_y_size = self.dataRect.height()

            #get size of new data sub-array by scaling with fractional size of image selection
            new_x_size=int(round(float(this_x_size)*float(self.selectedRect.width())/float(self.__zoomX)))
            new_x_size = new_x_size - new_x_size % 4 #size must be divisible by 4 for QImage conversion
            new_y_size=int(round(float(this_y_size)*float(self.selectedRect.height())/float(self.__zoomY)))
            new_y_size = new_y_size - new_y_size % 4
            xpix=float(self.selectedRect.left())-(float(self.mainImageSize.width())/2.-float(self.__zoomX)/2.)
            ypix=float(self.selectedRect.top())-(float(self.mainImageSize.height())/2.-float(self.__zoomY)/2.)
            y1 = int(round((ypix/float(self.__zoomY))*float(this_y_size - 1)))+self.dataRect.top()
            x1 = int(round((xpix/float(self.__zoomX))*float(this_x_size - 1)))+self.dataRect.left()
            y2 = y1 + new_y_size
            x2 = x1 + new_x_size
        else:
            x1 = self.dataRect.left()
            x2 = self.dataRect.left() + self.dataRect.width()
            y1 = self.dataRect.top()
            y2 = self.dataRect.top() + self.dataRect.height()
        if self.gotXform: self.dataMin = self.dataXform[y1:y2,x1:x2].min()
        else: self.dataMin = self.data[y1:y2,x1:x2].min()
        if self.gotXform: self.dataMax = self.dataXform[y1:y2,x1:x2].max()
        else: self.dataMax = self.data[y1:y2,x1:x2].max()
        self.updateImage()

    def zoom(self):

        if self.mainImage.gotROI:
            #update the bounding box in data space and generate the subarray
            this_x_size = self.dataRect.width() #num pixels in current data sub-array
            this_y_size = self.dataRect.height()

            #get size of new data sub-array by scaling with fractional size of image selection
            new_x_size=int(round(float(this_x_size)*float(self.selectedRect.width())/float(self.__zoomX)))
            new_x_size = new_x_size - new_x_size % 4 #size must be divisible by 4 for QImage conversion
            new_y_size=int(round(float(this_y_size)*float(self.selectedRect.height())/float(self.__zoomY)))
            new_y_size = new_y_size - new_y_size % 4
            xpix=float(self.selectedRect.left())-(float(self.mainImageSize.width())/2.-float(self.__zoomX)/2.)
            ypix=float(self.selectedRect.top())-(float(self.mainImageSize.height())/2.-float(self.__zoomY)/2.)
            y1 = int(round((ypix/float(self.__zoomY))*float(this_y_size - 1)))+self.dataRect.top()
            x1 = int(round((xpix/float(self.__zoomX))*float(this_x_size - 1)))+self.dataRect.left()
            y2 = y1 + new_y_size
            x2 = x1 + new_x_size

            self.dataRect.setLeft(x1)
            self.dataRect.setRight(x2)
            self.dataRect.setTop(y1)
            self.dataRect.setBottom(y2)
            imageWidth = float(self.selectedRect.width())
            imageHeight = float(self.selectedRect.height())

            if (new_x_size > new_y_size):
                factor = float(self.mainImageSize.width()) / imageWidth
                self.__zoomX, self.__zoomY = self.mainImageSize.width(), int(round(factor * imageHeight))
            elif (new_y_size > new_x_size):
                factor = float(self.mainImageSize.height()) / imageHeight
                self.__zoomX, self.__zoomY = int(round(factor * imageWidth)), self.mainImageSize.height()
            else:
                self.__zoomX, self.__zoomY = self.mainImageSize.width(),self.mainImageSize.height()
            self.imageRect.setLeft(self.__thumbSize / 2 - self.__zoomX / 2)
            self.imageRect.setRight(self.__thumbSize / 2 + self.__zoomX / 2)
            self.imageRect.setTop(self.__thumbSize / 2 - self.__zoomY / 2)
            self.imageRect.setBottom(self.__thumbSize / 2 + self.__zoomY / 2)
            self.setRoiText()
            self.updateImage()
            #self.updateInfo()

    def updateImage(self):
        if self.gotXform == False:
            if self.logButton.isChecked(): self.image = numpy2qimage(np.log(\
                self.data[self.dataRect.top():self.dataRect.bottom(),\
                self.dataRect.left():self.dataRect.right()] * self.mask[self.dataRect.top():self.dataRect.bottom(),\
                                                              self.dataRect.left():self.dataRect.right()] +\
                self.logOffset),cmap = self.cmap).scaled(self.__zoomX, self.__zoomY)
            else: self.image = numpy2qimage(np.clip(self.data[self.dataRect.top():self.dataRect.bottom(),\
                                                    self.dataRect.left():self.dataRect.right()],self.dataMin,self.dataMax) * self.mask[self.dataRect.top():self.dataRect.bottom(),\
                                                                                                                             self.dataRect.left():self.dataRect.right()],cmap = self.cmap).scaled(self.__zoomX, self.__zoomY)
        else:
            if self.logButton.isChecked(): self.image = numpy2qimage(np.log(\
                self.dataXform[self.dataRect.top():self.dataRect.bottom(),\
                self.dataRect.left():self.dataRect.right()] +\
                self.logOffset),cmap = self.cmap).scaled(self.__zoomX, self.__zoomY)
            else: self.image = numpy2qimage(np.clip(self.dataXform[self.dataRect.top():self.dataRect.bottom(),\
                                                    self.dataRect.left():self.dataRect.right()],self.dataMin, self.dataMax),cmap = self.cmap).scaled(self.__zoomX, self.__zoomY)
        self.mainImage.imStartPt.setX(self.mainImageSize.width() / 2 - self.__zoomX / 2)
        self.mainImage.imStartPt.setY(self.mainImageSize.height() / 2 - self.__zoomY / 2)
        self.mainImage.displayImage(self.image)

    def reload(self):

        self.dataXform = None
        self.gotXform = False
        self.openFile(str(self.path))

    def chooseFile(self):
        """ Provides a dialog window to allow the user to specify an image file.
            If a file is selected, the appropriate function is called to process
            and display it.
        """
        dataFile = QFileDialog.getOpenFileName(self,
            "Choose a data file to open", self.filePath, "CXI (*.cxi)")
            #"Choose a data file to open", "CXI (*.cxi)")

        if dataFile != '':
            self.openFile(str(dataFile))
            self.path = dataFile

    def checkShape(self):

        pass
#        if (self.data.shape[0] % 2) != 0: self.data = self.data[1:self.data.shape[0],:]
#        if (self.data.shape[1] % 2) != 0: self.data = self.data[:,1:self.data.shape[1]]
#        if ((self.data.shape[0] / 2) % 2) != 0: self.data = self.data[2:self.data.shape[0],:]
#        if ((self.data.shape[1] / 2) % 2) != 0: self.data = self.data[:,2:self.data.shape[1]]

    def openFile(self, dataFile):
        self.STXMdata = None
        if os.path.exists(dataFile): self.path = dataFile
        else:
            print "qtDiffInspector Error: no such data file"
            return
        if dataFile.split('.')[len(dataFile.split('.'))-1]=='cxi':
            self.cxi = readCXI(dataFile)
            if self.cxi.image != None:
                self.display = 0
                self.data = abs(self.cxi.image)
            elif self.cxi.stxm != None:
                self.display = 0
                self.data = self.cxi.stxmInterp
            else:
                self.display = 5
                self.data = self.cxi.ccddata[0]
            if self.cxi.energy != None:
                l = e2l(self.cxi.energy)*1e-9
                NA = np.sqrt(self.cxi.corner[0]**2 + self.cxi.corner[1]**2) / np.sqrt(2.) / self.cxi.corner[2]
                self.pixm = l / 2. / NA
                self.scanStepm = self.cxi.translation[1][0] - self.cxi.translation[0][0]

        else:
            self.data = None
        if self.data != None:
            if len(self.cxi.imageProbe.shape) > 2: self.probe = self.cxi.imageProbe[0,0]
            else: self.probe = self.cxi.imageProbe
            #print "Data starting shape:", self.data.shape
            self.checkShape()
            #print "Data finishing shape:", self.data.shape
            self.dataShape = self.data.shape
            if self.dataShape[1] > self.dataShape[0]:
                self.__zoomX, self.__zoomY = self.__thumbSize, round(float(self.dataShape[0]) / float(self.dataShape[1]) * self.__thumbSize)
            else:
                self.__zoomX, self.__zoomY = round(float(self.dataShape[1]) / float(self.dataShape[0]) * self.__thumbSize), self.__thumbSize
            self.mask = np.ones(self.data.shape)
            self.dataXform = None
            self.gotXform = False
            self.setWindowTitle('qtDiffInspector: '+ dataFile)
            self.logOffset = 0.0001 * self.data.max()
            self.dataRect.setRect(0,0,self.dataShape[1]+1,self.dataShape[0]+1)
            self.mainImage.box.setRect(0,0,self.dataShape[1]+1,self.dataShape[0]+1)
            self.fileName = dataFile.split('/')[-1::][0]
            self.filePath = dataFile.split(self.fileName)[0]
            self.getFileList(sorted(os.listdir(self.filePath)))
            self.fileCreated = time.ctime(os.path.getctime(dataFile))
            self.fileModified = time.ctime(os.path.getmtime(dataFile))
            self.roiLeft = self.dataRect.left()
            self.roiRight = self.dataRect.right()
            self.roiTop = self.dataRect.top()
            self.roiBottom = self.dataRect.bottom()
            if self.cxi.process == None: pixm = 1.
            else: pixm = np.single(self.cxi.process['pixnm'])/1e9
            y,x = np.array((self.cxi.translation[:,1], self.cxi.translation[:,0]))
            self.nFrames = len(self.cxi.ccddata)
            self.yPos = ((y - y.max() / 2) / pixm).round() + self.dataShape[1] / 2
            self.xPos = ((x - x.max() / 2) / pixm).round() + self.dataShape[0] / 2
            self.imageClick(self.mainImageSize.width() / 2, 0,self.mainImageSize.height() / 2,0, mplot = True)
            self.updatePlots()
            self.updateInfo()
            self.autoscale()
            self.updateROI()
            self.imageRect.setLeft(self.__thumbSize / 2 - self.__zoomX / 2)
            self.imageRect.setRight(self.__thumbSize / 2 + self.__zoomX / 2)
            self.imageRect.setTop(self.__thumbSize / 2 - self.__zoomY / 2)
            self.imageRect.setBottom(self.__thumbSize / 2 + self.__zoomY / 2)




    def getFileList(self,fileList):

        self.fileList = []
        for filename in fileList:
            if filename.split('.')[::-1][0] in self.fileTypes:
                self.fileList.append(filename)
        i = 0
        for filename in self.fileList:
            if filename.find(self.fileName) == 0:
                self.fileIndex = i
            else: i += 1
        self.nFiles = len(self.fileList)

    def imageDrag(self, x1,x2, y1,y2):

        if self.mainImage.gotSlice:
            pass
        else:
            x, y = x1 - x2, y1 - y2
            dy = round(float(y) / float(self.__zoomY) * float(self.dataRect.height()))
            dx = round(float(x) / float(self.__zoomX) * float(self.dataRect.width()))

            yT = self.dataStartRect.top() + dy
            yB = self.dataStartRect.bottom() + dy
            xL = self.dataStartRect.left() + dx
            xR = self.dataStartRect.right() + dx

            if yT < 0:
                yT = 0.
                yB = self.dataRect.height() - 1
            elif yB > self.dataShape[0]:
                yT = self.dataShape[0] - self.dataRect.height()
                yB = self.dataShape[0] - 1

            if xL < 0:
                xL = 0.
                xR = self.dataRect.width() - 1
            elif xR > self.dataShape[1]:
                xL = self.dataShape[1] - self.dataRect.width()
                xR = self.dataShape[1] - 1


            self.dataRect.setTop(yT)
            self.dataRect.setLeft(xL)
            self.dataRect.setBottom(yB)
            self.dataRect.setRight(xR)
            self.setRoiText()
            self.updateImage()

    def setRoiText(self):

        self.sideInfo.roiLeft = self.dataRect.left()
        self.sideInfo.roiRight = self.dataRect.right()
        self.sideInfo.roiTop = self.dataRect.top()
        self.sideInfo.roiBottom = self.dataRect.bottom()
        self.sideInfo.roiCenterX = (self.dataRect.right() + self.dataRect.left()) / 2
        if (self.sideInfo.roiCenterX) % 2 != 0: self.sideInfo.roiCenterX += 1
        self.sideInfo.roiCenterY = (self.dataRect.top() + self.dataRect.bottom()) / 2
        if (self.sideInfo.roiCenterY) % 2 != 0: self.sideInfo.roiCenterY += 1
        self.sideInfo.setRoiText()

    def imageClick(self, x1 = None, x2 = None, y1 = None, y2 = None, mplot = False):
        if self.mainImage.gotSlice != True:
            x,y = x1, y1
            self.xpt,self.ypt=x,y
            sidePlotPath = QPainterPath()
            bottomPlotPath = QPainterPath()
            this_x_size = self.dataRect.width() #num pixels in current data sub-array
            this_y_size = self.dataRect.height()
            xpix=float(x)-(float(self.mainImageSize.width())/2.-float(self.__zoomX)/2.)
            ypix=float(y)-(float(self.mainImageSize.height())/2.-float(self.__zoomY)/2.)
            self.dataY = int((ypix/float(self.__zoomY))*float(this_y_size - 1))+self.dataRect.top()
            self.dataX = int((xpix/float(self.__zoomX))*float(this_x_size - 1))+self.dataRect.left()

            if mplot:
                if self.display == 0:
                    stxmIndexX = int(float(self.dataX) / float(self.data.shape[1]) * float(self.cxi.stxm.shape[1]))
                    stxmIndexY = int(float(self.dataY) / float(self.data.shape[0]) * float(self.cxi.stxm.shape[0]))
                    sys.stdout.write("STXM point (H,V): %i,%i \r" %(stxmIndexX + 1,self.cxi.stxm.shape[0] - stxmIndexY))
                    sys.stdout.flush()
                    stxmSh = self.cxi.stxm.shape
                    pointIndex = (stxmSh[0] - 1 - stxmIndexY) * stxmSh[1] + stxmIndexX
                    if self.cxi.indices != None:
                        self.frameIndex = self.cxi.indices[pointIndex]
                        if self.frameIndex != 0:
                            self.frameEdit.setText(str(self.frameIndex))
                            self.sideInfo.diffThumbnail.setPixmap(QPixmap.fromImage(numpy2qimage(np.log(self.cxi.ccddata[self.frameIndex] + \
                                0.001))).scaled(256,256))
                        else:
                            self.sideInfo.diffThumbnail.setPixmap(QPixmap.fromImage(numpy2qimage(0. * self.cxi.ccddata[0] +\
                                                                                                     0.001)).scaled(256,256))

            if self.gotXform:
                vVector = congrid(self.dataXform[self.dataRect.top():self.dataRect.top()+\
                                                                     self.dataRect.height(),self.dataX],(self.sidePlotSize.height()+1,))
                vVector -= vVector.min()
                hVector = congrid(self.dataXform[self.dataY,self.dataRect.left():self.dataRect.left()+\
                                                                                 self.dataRect.width()],(self.bottomPlotSize.width()+1,))
                hVector -= hVector.min()
            elif self.isComplex:
                vVector = congrid(abs(self.data[self.dataRect.top():self.dataRect.top()+\
                                                                    self.dataRect.height(),self.dataX]),(self.sidePlotSize.height()+1,))
                vVector -= vVector.min()
                hVector = congrid(abs(self.data[self.dataY,self.dataRect.left():self.dataRect.left()+\
                                                                                self.dataRect.width()]),(self.bottomPlotSize.width()+1,))
                hVector -= hVector.min()
            else:
                vVector = congrid(self.data[self.dataRect.top():self.dataRect.top()+\
                    self.dataRect.height(),self.dataX] * self.mask[self.dataRect.top():self.dataRect.top()+\
                    self.dataRect.height(),self.dataX],(self.sidePlotSize.height()+1,))
                vVector -= vVector.min()
                hVector = congrid(self.data[self.dataY,self.dataRect.left():self.dataRect.left()+\
                    self.dataRect.width()] * self.mask[self.dataY,self.dataRect.left():self.dataRect.left()+\
                    self.dataRect.width()],(self.bottomPlotSize.width()+1,))
                hVector -= hVector.min()
            vVector = vVector / vVector.max() * (self.sidePlotSize.width() - 4.)
            hVector = hVector / hVector.max() * (self.bottomPlotSize.height() - 4.)
            sidePlotPath.moveTo(2,0)
            for i in range(len(vVector)):
                sidePlotPath.lineTo(vVector[i], i)
            sidePlotPath.lineTo(2,self.sidePlotSize.height())
            self.sidePlot.path = sidePlotPath
            bottomPlotPath.moveTo(0,self.bottomPlotSize.height()-2)
            for i in range(len(hVector)):
                bottomPlotPath.lineTo(i,self.bottomPlotSize.height() - 2. - hVector[i])
            bottomPlotPath.lineTo(self.bottomPlotSize.width(),self.bottomPlotSize.height() - 2)
            self.bottomPlot.path = bottomPlotPath
            self.bottomPlot.maxValue = self.data[self.dataY,self.dataRect.left():self.dataRect.left()+\
                                                                                 self.dataRect.width()].max()
            self.bottomPlot.minValue = self.data[self.dataY,self.dataRect.left():self.dataRect.left()+\
                                                                                 self.dataRect.width()].min()
            self.updatePlots()

        if self.mainImage.gotFilter:
            x = (self.mainImage.filter.left() + self.mainImage.filter.right())/ 2
            y = (self.mainImage.filter.top() + self.mainImage.filter.bottom())/ 2
            self.filterX = float(x) / float(self.mainImageSize.width()) * float(self.dataRect.width()) + self.dataRect.left()-1
            self.filterY = float(y) / float(self.mainImageSize.height()) * float(self.dataRect.height()) + self.dataRect.top()-1
            if (self.mainImage.filter.width() > self.mainImage.filter.height()):
                self.filterWidth = self.mainImage.filter.width()
            else:
                self.filterWidth = self.mainImage.filter.height()

        self.dataStartRect.setLeft(self.dataRect.left())
        self.dataStartRect.setRight(self.dataRect.right())
        self.dataStartRect.setTop(self.dataRect.top())
        self.dataStartRect.setBottom(self.dataRect.bottom())

        if self.mainImage.gotROI:
            if x1 < x2:
                self.selectedRect.setLeft(x1)
                self.selectedRect.setRight(x2)
            else:
                self.selectedRect.setLeft(x2)
                self.selectedRect.setRight(x1)
            if y1 < y2:
                self.selectedRect.setTop(y1)
                self.selectedRect.setBottom(y2)
            else:
                self.selectedRect.setTop(y2)
                self.selectedRect.setBottom(y1)

            self.selectedRectTL.setX(self.selectedRect.left())
            self.selectedRectTL.setY(self.selectedRect.top())
            self.selectedRectBR.setX(self.selectedRect.right())
            self.selectedRectBR.setY(self.selectedRect.bottom())
        elif self.mainImage.gotSlice:
            sliceX1 = float(x1)/float(self.mainImageSize.width()) * float(self.dataRect.width()) + float(self.dataRect.left())
            sliceY1 = float(y1)/float(self.mainImageSize.height()) * float(self.dataRect.height()) + float(self.dataRect.top())
            sliceX2 = float(x2)/float(self.mainImageSize.width()) * float(self.dataRect.width()) + float(self.dataRect.left())
            sliceY2 = float(y2)/float(self.mainImageSize.height()) * float(self.dataRect.height()) + float(self.dataRect.top())
            l = int(np.sqrt(abs(sliceY2 - sliceY1)**2 + abs(sliceX2 - sliceX1)**2))
            self.xSlice = np.linspace(sliceX1,sliceX2,l).astype('int16')
            self.ySlice = np.linspace(sliceY1,sliceY2,l).astype('int16')
            if self.gotXform: self.dataSlice = self.dataXform[(self.ySlice,self.xSlice)]
            else: self.dataSlice = self.data[(self.ySlice,self.xSlice)]

        if mplot:
            if self.plotType != 'histogram':
                if self.plotType != 'radial': self.updateInfoPlot()

    def updateInfo(self):

#        self.shapeVal.setText('%ix%i' %(self.dataRect.width()-1,self.dataRect.height()-1))
#        self.sideInfo.maxVal.setText('%.2f' %(self.data[self.dataRect.top():self.dataRect.bottom(),self.dataRect.left():self.dataRect.right()]).max())
#        self.sideInfo.minVal.setText('%.2f' %(self.data[self.dataRect.top():self.dataRect.bottom(),self.dataRect.left():self.dataRect.right()]).min())
#        self.sideInfo.meanVal.setText('%.2f' %(self.data[self.dataRect.top():self.dataRect.bottom(),self.dataRect.left():self.dataRect.right()]).mean())
#        self.sideInfo.stdVal.setText('%.2f' %(self.data[self.dataRect.top():self.dataRect.bottom(),self.dataRect.left():self.dataRect.right()]).std())
#        self.sideInfo.totVal.setText('%i' %(self.data[self.dataRect.top():self.dataRect.bottom(),self.dataRect.left():self.dataRect.right()]).sum())
#        self.sideInfo.fileNameVal.setText(self.fileName)
#        self.sideInfo.fileCreatedVal.setText(self.fileCreated)
#        self.sideInfo.fileModifiedVal.setText(self.fileModified)
        self.updateInfoPlot()

        self.sideInfo.roiLeftEdit.setText(str(self.roiLeft))
        self.sideInfo.roiRightEdit.setText(str(self.roiRight))
        self.sideInfo.roiTopEdit.setText(str(self.roiTop))
        self.sideInfo.roiBottomEdit.setText(str(self.roiBottom))





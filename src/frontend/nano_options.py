import PyQt4
import PyQt4.QtGui
from PyQt4.QtCore import *
from PyQt4.QtGui import *
import paramiko
import time
import select
import threading
import os
from stat import S_IMODE, S_ISDIR, S_ISREG
import multiprocessing, time, random, sys

try:
    import SocketServer
except ImportError:
    import socketserver as SocketServer


g_verbose = False

def verbose(s):
    if g_verbose:
        print(s)

def executeSSHCommand(cmd, server, username, password, num_row):
    
    try:
        tmp_list = []
        result = ""
        tmp_list.append(num_row)
        tmp_list.append(result)
        cmd_salloc = 'salloc -p batch -n 4 --time=02:00:00'
        
        full_cmd = '\n' + cmd_salloc +  '\n' + cmd + '\nexit\nexit\n'

        #time.sleep(2)
        #PyQt4.QtGui.qApp.processEvents()
        #time.sleep(2)
            
        # Test local ssh connection
            
        local_ssh = paramiko.SSHClient()
        local_ssh.load_system_host_keys()

        local_ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        local_ssh.connect(server,username=username,password=password,allow_agent=False)

        channel = local_ssh.invoke_shell()
        stdin = channel.makefile('wb')
        stdout = channel.makefile('rb')

        stdin.write(full_cmd)

        result = stdout.read()
        tmp_list[1] = result
        
        stdout.close()
        stdin.close()
        local_ssh.close()
        
    except Exception as error:
        tmp_list.append(False)
        return tmp_list
    else:
        tmp_list.append(True)
        return tmp_list

class ForwardServer (SocketServer.ThreadingTCPServer):
    daemon_threads = True
    allow_reuse_address = True


class Handler (SocketServer.BaseRequestHandler):

    def handle(self):
        try:
            chan = self.ssh_transport.open_channel('direct-tcpip',
                                                   (self.chain_host, self.chain_port),
                                                   self.request.getpeername())
        except Exception as e:
            verbose('Incoming request to %s:%d failed: %s' % (self.chain_host,
                                                              self.chain_port,
                                                              repr(e)))
            return

        if chan is None:
            verbose('Incoming request to %s:%d was rejected by the SSH server.' %
                    (self.chain_host, self.chain_port))
            return

        verbose('Connected!  Tunnel open %r -> %r -> %r' % (self.request.getpeername(),
                                                            chan.getpeername(), (self.chain_host, self.chain_port)))
        while True:
            r, w, x = select.select([self.request, chan], [], [])
            if self.request in r:
                data = self.request.recv(1024)
                if len(data) == 0:
                    break
                chan.send(data)
            if chan in r:
                data = chan.recv(1024)
                if len(data) == 0:
                    break
                self.request.send(data)

        peername = self.request.getpeername()
        chan.close()
        self.request.close()
        verbose('Tunnel closed from %r' % (peername,))


def forward_tunnel(local_port, remote_host, remote_port, transport):
    # this is a little convoluted, but lets me configure things for the Handler
    # object.  (SocketServer doesn't give Handlers any way to access the outer
    # server normally.)
    class SubHander (Handler):
        chain_host = remote_host
        chain_port = remote_port
        ssh_transport = transport
    fw = ForwardServer(('', local_port), SubHander) #.serve_forever()
    th = threading.Thread(target=fw.serve_forever)
    th.daemon = True
    th.start()


class Login(QDialog):
    def __init__(self):
        QDialog.__init__(self)
        self.machineName = QLineEdit(self)
        self.machineName.setText('phasis.lbl.gov')

        self.textName = QLineEdit(self)
        self.textPass = QLineEdit(self)
        self.textPass.setEchoMode(QLineEdit.Password)

        self.buttonLogin = QPushButton('Login', self)
        self.buttonLogin.clicked.connect(self.handleLogin)

        self.buttonCancel = QPushButton('Cancel', self)
        self.buttonCancel.clicked.connect(self.reject)


        layout = QVBoxLayout(self)

        layout.addWidget(self.machineName)
        layout.addWidget(self.textName)
        layout.addWidget(self.textPass)

        layout.addWidget(self.buttonLogin)
        layout.addWidget(self.buttonCancel)

    def handleLogin(self):
        if (len(self.textName.text()) > 0 and
            len(self.textPass.text()) > 0 and
            len(self.machineName.text()) > 0):
            self.accept()
        else:
            QMessageBox.warning(self, 'Error', 'Bad user or password')


class NanoOptions:

    def __init__(self, stdOutEdit, procsList):
        self.ssh = None
        self.ftp = None
        self.stdOutEdit = stdOutEdit
        self.procsList = procsList
        
        self.pool = multiprocessing.Pool()
        self.result_list = []
        
    def sendCommandButtonClicked(self, commandString):
        #print "Sending command..."
        self.sendSSHCommand(commandString)

    def connect2server(self):

        if self.is_connected() == True:
            return True

        self.stdOutEdit.append('Connecting...')

        login = Login()

        if login.exec_() != QDialog.Accepted:
            self.stdOutEdit.append('Connection attempt to phasis cancelled...')
            return False

        self.ssh = paramiko.SSHClient()
        self.ssh.load_system_host_keys()

        username = str(login.textName.text())
        password = str(login.textPass.text())
        server = str(login.machineName.text())

        self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        try:
            self.ssh.connect(server,username=username,password=password,allow_agent=False)
            self.stdOutEdit.append('Connected!')
        except paramiko.AuthenticationException:
            self.stdOutEdit.append('Could not connect to server!')
            return False

        #set up port connections
        #local_port = 9001
        #remote_machine = "n0000.phasis"
        #remote_port = 9002
        #forward_tunnel(local_port, remote_machine, remote_port, self.ssh.get_transport())

        self.stdOutEdit.append('Opening ftp channel...')
        self.ftp = self.ssh.open_sftp()
        #self.stdConEdit.append('Done!')

        #self.ftp.close()
        #self.ssh.close()
		
        if not os.path.isdir('/tmp/ptycho'):
            print "Created tmp dir!"
            os.mkdir('/tmp/ptycho/')

        self.getRemoteDir('/', '/tmp/ptycho/',False)

        self.stdOutEdit.append('Done!')

        return True

    def is_connected(self):
        transport = self.ssh.get_transport() if self.ssh else None
        return transport and transport.is_active()
            
    def sendSSHCommand(self, cmd):
        
        login = Login()

        if login.exec_() != QDialog.Accepted:
            self.stdOutEdit.append('Connection attempt to phasis cancelled...')
            return False

        username = str(login.textName.text())
        password = str(login.textPass.text())
        server = str(login.machineName.text())
        
        self.stdOutEdit.append('Executing command %s' % cmd)
        self.stdOutEdit.append('Waiting for response...')
        num_row = self.procsList.count()
        self.pool.apply_async(func=executeSSHCommand, args=(cmd, server, username, password, num_row,), callback=self.receiveResult)
        item = QListWidgetItem(cmd)
        item.setForeground(Qt.darkGray)
        self.procsList.addItem(item)
        
            
    def receiveResult(self, result):
        self.result_list.append(result)
        if (result[2]):
            self.procsList.item(result[0]).setForeground(Qt.darkGreen)
        else:
            self.procsList.item(result[0]).setForeground(Qt.darkRed)


    def showResultsButtonClicked(self):
        resWindow = QDialog()
        resWindow.setFixedSize(800,600)
        resWindow.move(100,100)
        resWindow.setWindowTitle('Current process results')
        Grid = QGridLayout(resWindow)
        Grid.setSpacing(0)
        Grid.setMargin(2)
        
        stdLabel = QLabel()
        stdLabel.setText('Current processes results')
        stdResEdit = QTextEdit()
        stdResEdit.setReadOnly(True)
        stdResEdit.setMinimumSize(800,600)
        stdResEdit.setLineWrapMode(0)
        
        Grid.addWidget(stdLabel,1,0,Qt.AlignTop | Qt.AlignLeft)
        Grid.addWidget(stdResEdit,4,0, Qt.AlignTop | Qt.AlignLeft)
        
        resWindow.setLayout(Grid)
        
        for i in range(len(self.result_list)):
            result = self.result_list[i]
            num_proc = result[0]
            output = result[1]
            status = result[2]
            stdResEdit.append("<div style=\"font-size:14pt;font-weight:bold;\">" + "Process number: " + str(num_proc) + "</div>")
            stdResEdit.append(str(status))
            stdResEdit.append('Output:')
            for line in output.splitlines():
                stdResEdit.append(line)
                
        resWindow.exec_()

    def copyRemoteDataset(src_dataset, dest_dataset):
        """ Copy dataset from remote location to local destination """
        
        self.stdOutEdit.append('Copying result...')
        remote_path = src_dataset #str(self.dirEdit.text()) + "/" + "NS_141017001.cxi"
        local_path = dest_dataset #"/tmp/NS_141017001.cxi"

        print remote_path, local_path
        self.ftp.get(remote_path, local_path)
        self.stdOutEdit.append('Done!')

        #self.tabviewer.openFile(local_path)
        #self.stdConEdit.append('Result can be visualized on the second tab!')

    def copyRemoteDataset():
        """
            TODO create UI to select Remote Dataset & Local directory.
            then call copy command with paths..
        """
        
        remote_dataset = str(self.dirEdit.text()) + "/" + "NS_141017001.cxi"
        local_dataset = "/tmp/NS_141017001.cxi"
        copyRemoteDataset(remote_dataset, local_dataset)

        #ask whether the new file should be opened
        """ Simple messagebox to ask whether to show the copied file """
        #self.tabviewer.openFile(local_path)
        #self.stdConEdit.append('Result can be visualized on the second tab!')


    def closeConnection(self):
        if self.is_connected():
            self.ssh.close()
	    self.ftp.close()
            self.stdOutEdit.append('Disconnected!')
        else:
            self.stdOutEdit.append('Not connected!')

    def chooseDirRemote(self,dirEdit):
        self.dirEdit = dirEdit;
        self.remoteFileDialog = QFileDialog()
        self.remoteFileDialog.setFileMode(QFileDialog.Directory)
        self.remoteFileDialog.setOption(QFileDialog.ShowDirsOnly, True)
        self.remoteFileDialog.setDirectory(self.dirEdit.text())
        self.remoteFileDialog.directoryEntered.connect(self.updateRemoteDir)
        self.updateRemoteDir('/tmp/ptycho')
        self.remoteFileDialog.exec_()
        res = self.remoteFileDialog.directory()
        dir = res.absolutePath()
        if dir != '':
            self.dirEdit.setText(dir + '/')
    
    def loadCXIRemote(self,dirEdit):
        self.dirEdit = dirEdit
        self.remoteFileDialog = QFileDialog()
        self.remoteFileDialog.setDirectory("/tmp/ptycho/")
        self.remoteFileDialog.selectFilter("CXI (*.cxi)")
        self.remoteFileDialog.directoryEntered.connect(self.updateRemoteDirCXI)
        self.updateRemoteDir('/tmp/ptycho')
        self.remoteFileDialog.exec_()
        res = self.remoteFileDialog.selectedFiles()

        fileName = str(res[0])

        if fileName.find('.cxi') != -1:
            self.get(fileName.split('/tmp/ptycho')[1], fileName, preserve_mtime=False)
        return fileName

    def loadConfigRemote(self,dirEdit):
        self.dirEdit = dirEdit
        self.remoteFileDialog = QFileDialog()
        self.remoteFileDialog.setDirectory("/tmp/ptycho/")
        self.remoteFileDialog.selectFilter("TXT (*.txt)")
        self.remoteFileDialog.directoryEntered.connect(self.updateRemoteDirTXT)
        self.updateRemoteDir('/tmp/ptycho')
        self.remoteFileDialog.exec_()
        res = self.remoteFileDialog.selectedFiles()
        fileName = str(res[0])
        return fileName

    def saveConfigRemote(self,dirEdit,param):
        self.dirEdit = dirEdit
        self.remoteFileDialog = QFileDialog()
        self.remoteFileDialog.setFileMode(QFileDialog.Directory)
        self.remoteFileDialog.setOption(QFileDialog.ShowDirsOnly, True)
        self.remoteFileDialog.setDirectory("/tmp/ptycho/" + self.dirEdit.text())
        self.remoteFileDialog.directoryEntered.connect(self.updateRemoteDir)
        self.updateRemoteDir('/tmp/ptycho')
        self.remoteFileDialog.exec_()
        res = self.remoteFileDialog.directory()
        fileName = str(res.absolutePath().split('/tmp/ptycho')[1] + '/param.txt')
        self.saveParamFileRemote(fileName, param)

    def saveParamFileRemote(self, remoteName, param):
        localName = '/tmp/param.txt'
        try: 
            f = open(localName, 'w')
        except:
            print("Failed to write parameter file."); return 0
        else:
            for key in param.keys():
                f.write(key + '=' + param[key] + '\n')
            f.close()
        try:
            self.ftp.put(localName, remoteName)
        except:
            return 0
        return 1

    def updateRemoteDir(self, directory):
        if str(directory).find('/tmp/ptycho') == -1:
            self.dirEdit.setText('/tmp/ptycho')
            self.remoteFileDialog.setDirectory('/tmp/ptycho')
            return
        self.getRemoteDir(str(directory.split('/tmp/ptycho')[1]+'/'), '/tmp/ptycho/',False, False)

    def updateRemoteDirTXT(self, directory):
        if str(directory).find('/tmp/ptycho') == -1:
            self.dirEdit.setText('/tmp/ptycho')
            self.remoteFileDialog.setDirectory('/tmp/ptycho')
            return
        self.getRemoteDir(str(directory.split('/tmp/ptycho')[1]+'/'), '/tmp/ptycho/',False, True, 1)

    def updateRemoteDirCXI(self, directory):
        if str(directory).find('/tmp/ptycho') == -1:
            self.dirEdit.setText('/tmp/ptycho')
            self.remoteFileDialog.setDirectory('/tmp/ptycho')
            return
        self.getRemoteDir(str(directory.split('/tmp/ptycho')[1]+'/'), '/tmp/ptycho/',False, True, 2)

    def getRemoteDir(self, remotedir, localdir, preserve_mtime=True, file = False, filetype = 1):
        
        wtcb = WTCallbacks()
        self.readRemoteDir(remotedir, wtcb.file_cb, wtcb.dir_cb, False, file)

        for dname in wtcb.dlist:
            for subdir in self.path_advance(dname):
                try:
                    os.mkdir(self.reparent(localdir, subdir))
                    wtcb.dlist.append(subdir)
                except OSError:     # dir exists
                    pass

	if file and filetype == 1:
	    for fname in wtcb.flist:
                if fname.find('.txt') != -1:
                    self.get(fname, self.reparent(localdir, fname), preserve_mtime=preserve_mtime)  #Don't need to actually copy the file, just need the name of directories
	if file and filetype == 2:
	    for fname in wtcb.flist:
	        if fname.find('.cxi') != -1:
	            #self.get(fname, self.reparent(localdir, fname), preserve_mtime=preserve_mtime)  #Don't need to actually copy the file, just need the name of directories
		    open(self.reparent(localdir, fname), 'w').close()

    def readRemoteDir(self, remotepath, fcallback, dcallback, recurse=True, file=False):
        for entry in self.ftp.listdir(remotepath):
            pathname = os.path.join(remotepath, entry)
            mode = self.ftp.stat(pathname).st_mode
            if S_ISDIR(mode):
                dcallback(pathname)
                if recurse:
                    self.readRemoteDir(pathname, fcallback, dcallback)
            if file:
	        if S_ISREG(mode):
                    fcallback(pathname)

    def path_advance(self, thepath, sep=os.sep):
        pre = ''
        if thepath[0] == sep:
            pre = sep
        curpath = ''
        parts = thepath.split(sep)
        if pre:
            if parts[0]:
                parts[0] = pre + parts[0]
            else:
                parts[1] = pre + parts[1]
        for part in parts:
            curpath = os.path.join(curpath, part)
            if curpath:
                yield curpath
    def reparent(self, newparent, oldpath):
        if oldpath[0] == os.sep:
            oldpath = '.' + oldpath
        return os.path.join(newparent, oldpath)
    
    def get(self, remotepath, localpath=None, callback=None, preserve_mtime=False):
        if not localpath:
            localpath = os.path.split(remotepath)[1]

        if preserve_mtime:
            sftpattrs = self.ftp.stat(remotepath)
	#self.stdOutEdit.append('Copying file from server...')
        self.ftp.get(remotepath, localpath, callback=callback)
        if preserve_mtime:
            os.utime(localpath, (sftpattrs.st_atime, sftpattrs.st_mtime))


class WTCallbacks(object):
    def __init__(self):
        self.flist = []
        self.dlist = []

    def file_cb(self, pathname):
        self.flist.append(pathname)

    def dir_cb(self, pathname):
        self.dlist.append(pathname)


from PyQt4 import QtCore, QtGui, uic
import pyqtgraph as pg
import numpy as np
import argparse
import time
import os, sys
import zmq
import json

from ..comm.qtsocket import QtSocket
from ..comm.zmqsocket import ZmqSocket
from .. import utils
from guicontrol import *

currdir = os.path.dirname(os.path.realpath(__file__))
Ui_mainwindow, base = uic.loadUiType(currdir + '/ui/streamviewer.ui')
Ui_info,       base = uic.loadUiType(currdir + '/ui/infoWidget.ui')
Ui_visual,     base = uic.loadUiType(currdir + '/ui/visWidget.ui')
Ui_viscontrol, base = uic.loadUiType(currdir + '/ui/visControl.ui')
Ui_network,    base = uic.loadUiType(currdir + '/ui/networkWidget.ui')
Ui_simulation, base = uic.loadUiType(currdir + '/ui/simulationWidget.ui')
Ui_connection, base = uic.loadUiType(currdir + '/ui/connectionWidget.ui')
Ui_status,     base = uic.loadUiType(currdir + '/ui/statusWidget.ui')

class controlDialog(QtGui.QDialog):
    """A class for the parameter dialog."""
    def __init__(self, params, parent=None):
        super(controlDialog, self).__init__(parent)
        uic.loadUi(currdir + '/ui/controlDialog.ui', self)
        self.params = params
        self.setNetworkParameters()
        self.setSimulationParameters()
        self.setBasicParameters()

    def setNetworkParameters(self):
        """Overwrites network parameters based on config file."""
        self.backendAddress.setText(self.params['backend']['address'])
        self.backendSshTunnel.setChecked(self.params['backend']['tunnel'])
        self.backendViewPort.setValue(self.params['backend']['viewport'])
        self.backendControlPort.setValue(self.params['backend']['ctrlport'])
        self.grabberInputPort.setValue(self.params['grabber']['inport'])
        self.grabberOutputPort.setValue(self.params['grabber']['outport'])

    def setSimulationParameters(self):
        """Overwrites simulation parameters based on config file."""
        self.rawDataFile.setText(self.params['simulation']['rawfile'])
        self.simulationOn.setChecked(self.params['simulation']['ison'])
        self.cameraAddress.setText(self.params['simulation']['cam_address'])
        self.cameraPort.setValue(self.params['simulation']['cam_port'])
        self.cameraDelay.setValue(self.params['simulation']['cam_delay'])
        
    def setBasicParameters(self):
        """Overwrites basic parameters based on config file."""
        self.xScanPoints.setText(str(self.params['metadata']['scanNx']))
        self.yScanPoints.setText(str(self.params['metadata']['scanNy']))
        self.scanStep.setText(str(self.params['metadata']['scanStep']))
        self.backendBgFrames.setText(str(self.params['background']['nframes']))
        self.backendInitFrames.setText(str(self.params['data']['initFrames']))
        self.arraySize.setText(str(self.params['metadata']['arraySize']))
        self.dataBrightThreshold.setText(str(self.params['data']['brightThreshold']))
        self.dataFmaskThreshold.setText(str(self.params['data']['fmaskThreshold']))
        self.ptychoIterationStep.setText(str(self.params['ptycho']['iterStep']))
        self.ptychoUpdate.setText(str(self.params['ptycho']['update']))
        self.ptychoEnergy.setText(str(self.params['metadata']['energy']))
        self.ptychoDistance.setText(str(self.params['metadata']['distance']))
        self.ptychoPixelSizeX.setText(str(self.params['metadata']['pixelSizeX']))
        self.ptychoPixelSizeY.setText(str(self.params['metadata']['pixelSizeY']))
        self.ptychoSharpFourierMask.setChecked(self.params['ptycho']['sharpFourierMask'])
        self.ptychoSharpRoundPixel.setChecked(self.params['ptycho']['sharpRoundPixel'])
        self.ptychoSharpIlluminationRefine.setText(str(self.params['ptycho']['sharpIlluminationRefine']))
        self.ptychoIterTotal.setText(str(self.params['ptycho']['iterTotal']))
        
    def getNetworkParameters(self):
        """Overwrites network parameters based on user input."""
        self.params['backend']['address']  = str(self.backendAddress.text())
        self.params['backend']['tunnel']   = self.backendSshTunnel.isChecked()
        self.params['backend']['viewport'] = int(self.backendViewPort.value())
        self.params['backend']['ctrlport'] = int(self.backendControlPort.value())
        self.params['grabber']['inport']   = int(self.grabberInputPort.value())
        self.params['grabber']['outport']  = int(self.grabberOutputPort.value())

    def getSimulationParameters(self):
        """Overwrites simulation parameters based on user input."""
        self.params['simulation']['rawfile']      = str(self.rawDataFile.text())
        self.params['simulation']['ison']         = self.simulationOn.isChecked()
        self.params['simulation']['cam_address']  = str(self.cameraAddress.text())
        self.params['simulation']['cam_port']     = int(self.cameraPort.value())
        self.params['simulation']['cam_delay']    = int(self.cameraDelay.value())
        
    def getBasicParameters(self):
        """Overwrites basic parameters based on user input."""
        self.params['metadata']['scanNx']                   = int(self.xScanPoints.text())
        self.params['metadata']['scanNy']                   = int(self.yScanPoints.text())
        self.params['metadata']['scanStep']                 = float(self.scanStep.text())
        self.params['background']['nframes']                = int(self.backendBgFrames.text())
        self.params['data']['initFrames']                   = int(self.backendInitFrames.text())
        self.params['metadata']['arraySize']                = int(self.arraySize.text())
        self.params['data']['brightThreshold']              = float(self.dataBrightThreshold.text())
        self.params['data']['fmaskThreshold']               = float( self.dataFmaskThreshold.text())
        self.params['ptycho']['iterStep']                   = int(self.ptychoIterationStep.text())
        self.params['ptycho']['update']                     = int(self.ptychoUpdate.text())
        self.params['metadata']['energy']                   = float(self.ptychoEnergy.text())
        self.params['metadata']['distance']                 = float(self.ptychoDistance.text())
        self.params['metadata']['pixelSizeX']               = float(self.ptychoPixelSizeX.text())
        self.params['metadata']['pixelSizeY']               = float(self.ptychoPixelSizeY.text())
        self.params['ptycho']['sharpFourierMask']           = self.ptychoSharpFourierMask.isChecked()
        self.params['ptycho']['sharpRoundPixel']            = self.ptychoSharpRoundPixel.isChecked()
        self.params['ptycho']['sharpIlluminationRefine']    = int(self.ptychoSharpIlluminationRefine.text())
        self.params['ptycho']['iterTotal']                  = int(self.ptychoIterTotal.text())
                
    def getValues(self):
        """Returns updated parameter dict."""
        self.getNetworkParameters()
        self.getSimulationParameters()
        self.getBasicParameters()
        return self.params
    
    def accept(self):
        super(controlDialog, self).accept()

class ConnectionWidget(QtGui.QWidget, Ui_connection):
    def __init__(self, style=None):
        QtGui.QWidget.__init__(self)
        self.setupUi(self)
        self.parameters.setIconSize(QtCore.QSize(20,20))
        if style == 'black':
            self.parameters.setIcon(QtGui.QIcon(currdir + '/icons/settings_red.svg'))
        else:
            self.parameters.setIcon(QtGui.QIcon(currdir + '/icons/settings.svg'))

class StatusWidget(QtGui.QWidget, Ui_status):
    def __init__(self):
        QtGui.QWidget.__init__(self)
        self.setupUi(self)

class SimulationWidget(QtGui.QWidget, Ui_simulation):
    def __init__(self):
        QtGui.QWidget.__init__(self)
        self.setupUi(self)

class InfoWidget(QtGui.QWidget, Ui_info):
    def __init__(self):
        QtGui.QWidget.__init__(self)
        self.setupUi(self)
        self.moduleStatus.setIconSize(QtCore.QSize(16,16))
        self.backgroundStatus.setIconSize(QtCore.QSize(16,16))
        self.illuminationStatus.setIconSize(QtCore.QSize(16,16))
        self.ptychoInitialized.setIconSize(QtCore.QSize(16,16))
        self.ptychoFinished.setIconSize(QtCore.QSize(16,16))
        
    def setButtonIcon(self, button, status):
        if status:
            button.setIcon(QtGui.QIcon(currdir + '/icons/status.svg'))
        else:
            button.setIcon(QtGui.QIcon())
        
    def setModuleStatus(self, status):
        self.setButtonIcon(self.moduleStatus, status)

    def setBackgroundStatus(self, status):
        self.setButtonIcon(self.backgroundStatus, status)

    def setIlluminationStatus(self, status):
        self.setButtonIcon(self.illuminationStatus, status)
        
    def setPtychoInitialized(self, status):
        self.setButtonIcon(self.ptychoInitialized, status)

    def setPtychoFinished(self, status):
        self.setButtonIcon(self.ptychoFinished, status)

class NetworkWidget(QtGui.QWidget, Ui_network):
    def __init__(self, history=100, nr_of_plots=2, labels=None, labelx='x', labely='y', 
                 ymin=0, ymax=1, xlog=False, ylog=False):
        QtGui.QWidget.__init__(self)
        self.setupUi(self)
        
        # Nr. of line plots
        self.n = nr_of_plots
        
        # Buffers
        self._x = utils.RingBuffer(history)
        self._y = [utils.RingBuffer(history) for i in range(self.n)]

        # Colors
        self.colors = [(255,128,0), (128,255,0), (0,255,255), (255,0,255)]

        # Labels for legend
        if labels is None:
            labels = nr_of_plots*['']

        # Prepare plot
        self.ploty = self.n*[None]
        self.setCanvas()
        self.setAxis(labelx, labely, ymin, ymax, xlog, ylog)
        self.legend = self.canvas.addLegend(size=(120,10), offset=5)
        self.setPlot()
        self.setLegend(labels)
        self.canvas.enableAutoRange(self.canvas.getViewBox().XYAxes)
        
        # Signal for data source
        self.source_signal = None

    def setCanvas(self):
        self.canvas.plotItem.setContentsMargins(10,10,10,10)
        self.canvas.setMouseEnabled(x=False, y=False)
        self.canvas.setMenuEnabled(False)
        
    def setAxis(self,labelx, labely, ymin=0, ymax=1, xlog=False, ylog=False, pady=45):
        self.canvas.setLabel('bottom', labelx)
        self.canvas.setLabel('left', labely)
        self.canvas.getAxis('left').setWidth(45)
        self.canvas.setRange(yRange=(ymin,ymax))
        self.canvas.setLogMode(x=xlog, y=ylog)
        
    def setPlot(self):
        for i in range(self.n):
            self.ploty[i] = self.canvas.plot(x=None, y=None, clear=False, pen=self.colors[i])
        
    def setLegend(self, labels):
        self.labels = labels
        for i in range(self.n):
            self.legend.addItem(self.ploty[i], labels[i])
        
    def removeLegend(self):
        for i in range(self.n):
            self.legend.removeItem(self.labels[i])

    def clearBuffers(self):
        self._x.clear()
        [self._y[i].clear() for i in range(self.n)]

    def showXAxis(self, show=True):
        self.canvas.showAxis('bottom', show)
            
    def disconnect(self):
        if self.source_signal is not None:
            self.source_signal.disconnect(self.update_buffers)
        
    def connect(self, signal):
        self.source_signal = signal
        self.source_signal.connect(self.update_buffers)

    def setSource(self, sourceid):
        self.selectPlot.setCurrentIndex(self.selectPlot.findText(sourceid))

    def update_buffers(self, x, *y):
        self._x.append(x)
        for i in range(self.n):
            self._y[i].append(y[i])
            
    def replot(self):
        if not len(self._x):
            return
        self.canvas.clear()
        for i in range(self.n):
            self.ploty[i] = self.canvas.plot(x=np.array(self._x), y=np.array(self._y[i]),
                                             clear=False, pen=self.colors[i])

class VisControlWidget(QtGui.QWidget, Ui_viscontrol):
    def __init__(self):
        QtGui.QWidget.__init__(self)
        self.setupUi(self)
        self.multiview.setIconSize(QtCore.QSize(20,20))
        self.multiview.clicked.connect(self.onMultiView)
        self.singleview.setIconSize(QtCore.QSize(20,20))
        self.singleview.clicked.connect(self.onSingleView)
        self.setMultiView(True)

    def onMultiView(self, clicked):
        self.setMultiView(True)

    def onSingleView(self, clicked):
        self.setMultiView(False)

    def setMultiView(self, active=True):
        if active:
            self.multiview.setIcon(QtGui.QIcon(currdir + '/icons/4grid_active.svg'))
            self.singleview.setIcon(QtGui.QIcon(currdir + '/icons/1grid_inactive.svg'))
        else:
            self.multiview.setIcon(QtGui.QIcon(currdir + '/icons/4grid_inactive.svg'))
            self.singleview.setIcon(QtGui.QIcon(currdir + '/icons/1grid_active.svg'))
                
class VisualWidget(QtGui.QWidget, Ui_visual):
    def __init__(self, enableAmplitude=True, enablePhase=True, enableComplex=False):
        QtGui.QWidget.__init__(self)
        self.setupUi(self)
        
        # Header
        #self.logaxis.setIcon(QtGui.QIcon(currdir + '/icons/logaxis.svg'))
        #self.logaxis.setIconSize(QtCore.QSize(16,16))
        #self.logaxis.clicked.connect(self.toggleLogAxis)
        #self.amplitude.setIcon(QtGui.QIcon(currdir + '/icons/amplitude.svg'))
        #self.amplitude.setIconSize(QtCore.QSize(16,16))
        #self.amplitude.setEnabled(enableAmplitude)
        #self.amplitude.clicked.connect(self.useAmplitude)
        #self.phase.setIcon(QtGui.QIcon(currdir + '/icons/phase.svg'))
        #self.phase.setIconSize(QtCore.QSize(16,16))
        #self.phase.setEnabled(enablePhase)
        #self.phase.clicked.connect(self.usePhase)
        #self.complex.setIcon(QtGui.QIcon(currdir + '/icons/complex.svg'))
        #self.complex.setIconSize(QtCore.QSize(16,16))
        #self.complex.setEnabled(enableComplex)

        # Image
        self.image = np.zeros((256,256))
        self.showAmplitude = True
        self.showPhase = False
        
        # Canvas
        self.canvas.ci.layout.setContentsMargins(0, 0, 0, 0)
        self.canvas.ci.layout.setSpacing(0)
        self.canvas.ci.border = (0,0,0)
        self.view = self.canvas.addViewBox(row=0, col=0, lockAspect=True, enableMouse=False, invertY=True)
        self.imageitem = pg.ImageItem(self.image, autoDownsample=True)
        self.view.addItem(self.imageitem)
        self.view.autoRange(padding=0.012)
        self.view.enableAutoRange(self.view.XYAxes)

        # Colormap
        self.setColorMap('cubehelix')

        # Logarithmic axis
        self.setLogAxis(False)
        
        # Levels
        self.autolevel = True
        self.vmin = None
        self.vmax = None

        # Signal for data source
        self.source_signal = None

    def disconnect(self):
        if self.source_signal is not None:
            self.source_signal.disconnect()
        
    def connect(self, signal):
        self.source_signal = signal
        self.source_signal.connect(self.setData)

    def setSource(self, sourceid):
        self.selectImage.setCurrentIndex(self.selectImage.findText(sourceid))
        
    @QtCore.pyqtSlot(str)
    def setColorMap(self, name):
        self.cmap = getattr(utils, name)()

    @QtCore.pyqtSlot()
    def toggleLogAxis(self):
        self.log = (not self.log)

    @QtCore.pyqtSlot()
    def setLogAxis(self, log):
        self.log = log
        
    #@QtCore.pyqtSlot()
    #def toggleAutoContrast(self):
    #    self.setVmin(None)
    #    self.setVmax(None)
            
    @QtCore.pyqtSlot()
    def useAmplitude(self):
        self.showAmplitude = True
        self.showPhase = False
        self.setImage()
        self.replot()

    @QtCore.pyqtSlot()
    def usePhase(self):
        self.showAmplitude = False
        self.showPhase = True
        self.setImage()
        self.replot()

    def setImage(self):
        if self.showPhase:
            self.image = np.angle(self.data)
        elif self.showAmplitude:
            self.image = np.abs(self.data)
        else:
            pass
        
    def setData(self, data):
        self.data = data
        self.setImage()

    def setVmin(self, vmin):
        self.vmin = vmin
        
    def setVmax(self, vmax):
        self.vmax = vmax

    def updateLevels(self):
        if self.vmin is None: # and not self.autolevel:
            if self.log:
                vmin = np.log10(self.image.min() + 1e-9)
            else:
                vmin = self.image.min()
        else:
            if self.log:
                vmin = np.log10(self.vmin + 1e-9)
            else:
                vmin = self.vmin
        if self.vmax is None: # and not self.autolevel:
            if self.log:
                vmax = np.log10(self.image.max())
            else:
                vmax = self.image.max()
        else:
            if self.log:
                vmax = np.log10(self.vmax)
            else:
                vmax = self.vmax
        return vmin, vmax
        
    def replot(self):
        if self.log:
            image = np.log10(self.image).transpose()
        else:
            image = self.image.transpose()
        self.imageitem.setImage(image, autolevels=False, lut=self.cmap)
        self.imageitem.setLevels(tuple(self.updateLevels()))
        self.view.autoRange()
        
class StreamViewer(QtGui.QMainWindow, Ui_mainwindow):
    """Class for streaming GUI."""
    statusMessage = QtCore.pyqtSignal(str)

    def __init__(self, args):
        QtGui.QMainWindow.__init__(self)
        self.setupUi(self)
        self.setWindowTitle("NanoSurveyor 2.0 - Streaming")

        # Define which stylesheet to use
        self.setStyle(args.style)
            
        # Read default params from file
        paramfile = args.config
        if paramfile is None:
            paramfile = os.path.dirname(os.path.realpath(__file__)) + '/default.conf'
        with open(paramfile) as json_file:
            self.jsonstring = json_file.read()
            self.params = json.loads(self.jsonstring)    

        # GUI Control
        self.guicontrol = Guicontrol(self.params)
            
        # Status of backend modules
        self.status = None
        self.backendConnected = False

        # Status Panel
        self.statusFrame.layout().removeWidget(self.statusPanel)
        self.statusPanel.setParent(None)
        self.statusPanel = StatusWidget()
        self.statusFrame.layout().addWidget(self.statusPanel)

        # Connection Panel
        self.connectionFrame.layout().removeWidget(self.connectionPanel)
        self.connectionPanel.setParent(None)
        self.connectionPanel = ConnectionWidget(args.style)
        self.connectionFrame.layout().addWidget(self.connectionPanel)
        
        # Simulation Panel
        self.simulationFrame.layout().removeWidget(self.simulationPanel)
        self.simulationPanel.setParent(None)
        self.simulationPanel = SimulationWidget()
        self.simulationFrame.layout().addWidget(self.simulationPanel)
        self.toggleSimulationPanel()

        # Network/Monitoring Panel
        self.networkFrame.layout().removeWidget(self.networkPanel)
        self.networkPanel.setParent(None)
        self.networkPanel = NetworkWidget(self.params['ptycho']['iterTotal'],
                                           nr_of_plots=2, labels=['Data', 'Overlap'],
                                           labelx='Nr. of Iterations', labely='Error metric', 
                                           ylog=True)
        self.networkFrame.layout().addWidget(self.networkPanel)
        self.networkFrame.setAutoFillBackground(True)
        self.networkPanel.selectPlot.currentIndexChanged.connect(self.onSelectPlotSource)
        self.networkPanel.setSource('Reconstruction errors')

        # Info Panel
        self.infoFrame.layout().removeWidget(self.infoPanel)
        self.infoPanel.setParent(None)
        self.infoPanel = InfoWidget()
        self.infoFrame.layout().addWidget(self.infoPanel)

        # VisControl panel
        self.viscontrolFrame.layout().removeWidget(self.viscontrolPanel)
        self.viscontrolPanel.setParent(None)
        self.viscontrolPanel = VisControlWidget()
        self.viscontrolFrame.layout().addWidget(self.viscontrolPanel)
        self.viscontrolPanel.multiview.clicked.connect(self.setMultiView)
        self.viscontrolPanel.singleview.clicked.connect(self.setSingleView)
        
        # Initialize 4 visualization Panels
        self.visFrames = [self.imageFrame, self.illuminationFrame, self.stxmFrame, self.dataFrame]
        self.visPanels = [self.imagePanel, self.illuminationPanel, self.stxmPanel, self.dataPanel]
        self.visTitles = ['Reconstruction', 'Illumination', 'STXM Image', 'Data frames']
        self.enablePhase   = [True, True, True, False]
        self.enableComplex = [True, True, True, False]
        self.visMinLevels  = [None, None, None, None]
        self.visMaxLevels  = [None, None, None, None]
        self.visColorMaps  = ['hot', 'hot', 'hot', 'hot']
        #self.visColorMaps  = ['cubehelix', 'jet', 'cubehelix', 'hot']
        self.visLogAxis    = [False, False, False, False]
        for i in range(4):
            self.visFrames[i].layout().removeWidget(self.visPanels[i])
            self.visPanels[i].setParent(None)
            self.visPanels[i] = VisualWidget(enablePhase=self.enablePhase[i],
                                             enableComplex=self.enableComplex[i])
            self.visPanels[i].setLogAxis(self.visLogAxis[i])
            self.visPanels[i].setVmin(self.visMinLevels[i])
            self.visPanels[i].setVmax(self.visMaxLevels[i])
            self.visPanels[i].setColorMap(self.visColorMaps[i])
            self.visFrames[i].layout().addWidget(self.visPanels[i])
            self.visFrames[i].setAutoFillBackground(True)
            self.visPanels[i].selectImage.currentIndexChanged.connect(self.onSelectImageSource)
            self.visPanels[i].setSource(self.visTitles[i])

        # Initializations connections/timers/threads
        self.init_connections()
        self.init_timer()
        self.threads = []
            
    def setStyle(self, theme='default'):
        """Defines the style of the GUI."""
        if theme == 'default':
            filename = os.path.dirname(os.path.realpath(__file__)) + '/stylesheets/default.qss'
        elif theme == 'black':
            filename = os.path.dirname(os.path.realpath(__file__)) + '/stylesheets/black.qss'
        elif theme == 'white':
            filename = os.path.dirname(os.path.realpath(__file__)) + '/stylesheets/white.qss'
        with open(filename) as f:
            self.style = f.read()
        self.setStyleSheet(self.style)
        
    def init_connections(self):
        """Connecting main GUI buttons."""
        self.connectionPanel.connect.clicked.connect(self.connectAll)
        self.connectionPanel.disconnect.clicked.connect(self.disconnectAll)
        self.connectionPanel.parameters.clicked.connect(self.controlParameters)
        self.connectionPanel.interrupt.clicked.connect(self.interrupt)
        self.simulationPanel.start.clicked.connect(self.simulateNewRun)
        self.guicontrol.statusMessage.connect(self.appendStatus)
        self.guicontrol.frameinfo.connect(self.setFrameInfo)
        self.guicontrol.reconsinfo.connect(self.setReconsInfo)
        self.guicontrol.simulationStopped.connect(self.simulationPanel.start.setEnabled)
        self.guicontrol.moduleStatus.connect(self.updateModuleStatus)
        self.guicontrol.backgroundStatus.connect(self.updateBackgroundStatus)
        self.guicontrol.illuminationStatus.connect(self.updateIlluminationStatus)
        self.guicontrol.ptychoStatus.connect(self.updatePtychoStatus)

    def init_timer(self):
        """Initialize timers for replotting and sending heartbeats."""
        self.replot_timer = QtCore.QTimer()
        self.replot_timer.setInterval(self.params["interface"]["replotIntervall"])
        self.replot_timer.timeout.connect(self.replot)
        self.replot_timer.start()
        self.heartbeat = QtCore.QTimer()
        self.heartbeat.setInterval(self.params["interface"]["heartbeatIntervall"])
        self.heartbeat.timeout.connect(self.functionUpdate)
        self.heartbeat.start()
        
    def replot(self):
        """Call replot functions of all subplots."""
        for plot in self.visPanels:
            plot.replot()
        self.guicontrol.checkTransfer()
        self.networkPanel.replot()

    def functionUpdate(self):
        """Send a heartbeat signal to backend."""
        if self.backendConnected:
            self.guicontrol.sendCommand('getUpdate')
            
    @QtCore.pyqtSlot()
    def controlParameters(self):
        """Slot that opens params dialog and stores new values on closing."""
        controlDiag = controlDialog(self.params)
        if controlDiag.exec_():
            self.params = controlDiag.getValues()
            self.toggleSimulationPanel()

    def toggleSimulationPanel(self):
        """Toggle the visibilty of the simulation panel."""
        if self.params["simulation"]["ison"]:
            self.simulationFrame.setVisible(True)
            self.rightFrame.layout().setRowStretch(0,1)
            self.rightFrame.layout().setRowStretch(1,9)
            self.rightFrame.layout().setRowStretch(2,1)
            self.rightFrame.layout().setRowStretch(3,10)
        else:
            self.simulationFrame.setVisible(False)
            self.rightFrame.layout().setRowStretch(0,1)
            self.rightFrame.layout().setRowStretch(1,10)
            self.rightFrame.layout().setRowStretch(2,0)
            self.rightFrame.layout().setRowStretch(3,10)

            
    @QtCore.pyqtSlot()
    def setMultiView(self):
        """Slot that toggles maximization/minimization of subplots."""
        self.centralwidget.layout().setRowStretch(2,10)
        self.centralwidget.layout().setRowStretch(3,10)
        self.centralwidget.layout().setColumnStretch(0,10)
        self.centralwidget.layout().setColumnStretch(1,10)
        self.centralwidget.layout().setColumnStretch(2,12)
        for i in range(1,4):
            self.visFrames[i].setVisible(True)

            
    @QtCore.pyqtSlot()
    def setSingleView(self):
        """Slot that toggles maximization/minimization of subplots."""
        self.centralwidget.layout().setRowStretch(2,0)
        self.centralwidget.layout().setRowStretch(3,0)
        self.centralwidget.layout().setRowStretch(2,20)
        self.centralwidget.layout().setColumnStretch(0,0)
        self.centralwidget.layout().setColumnStretch(1,0)
        self.centralwidget.layout().setColumnStretch(0,20)
        for i in range(1,4):
            self.visFrames[i].setVisible(False)

    @QtCore.pyqtSlot(int)
    def onSelectImageSource(self, index):
        """Disconnect from current source and re-connect new source."""
        vispanel =  self.sender().parent()
        vispanel.disconnect()
        sourceid = self.sender().itemText(index)
        if sourceid == 'Reconstruction':
            sig = self.guicontrol.reconsdata
        elif sourceid == 'STXM Image':
            sig = self.guicontrol.stxmdata
        elif sourceid == 'Data frames':
            sig = self.guicontrol.framedata
        elif sourceid == 'Illumination':
            sig = self.guicontrol.illumdata
        vispanel.connect(sig)

    @QtCore.pyqtSlot(int)
    def onSelectPlotSource(self, index):
        """Disconnect from current source and re-connect new source."""
        plotpanel =  self.sender().parent()
        plotpanel.disconnect()
        sourceid = self.sender().itemText(index)
        if sourceid == 'Reconstruction errors':
            plotpanel.clearBuffers()
            plotpanel.connect(self.guicontrol.reconsinfo)
            plotpanel.removeLegend()
            plotpanel.showXAxis(True)
            plotpanel.setPlot()
            plotpanel.setLegend(['Data', 'Overlap'])
            plotpanel.setAxis('Nr. of iterations', 'Error metric', ymin=-1, ymax=0, ylog=True)

        elif sourceid == 'Download/Upload rates':
            plotpanel.clearBuffers()
            plotpanel.connect(self.guicontrol.transferinfo)
            plotpanel.removeLegend()
            plotpanel.showXAxis(False)
            plotpanel.setPlot()
            plotpanel.setLegend(['Download', 'Upload'])
            plotpanel.setAxis('', 'Download/Upload [MB/s]', ymin=0, ymax=100)

    @QtCore.pyqtSlot(str,str,str)
    def setFrameInfo(self,fid,posx,posy):
        """Slot that processes incoming frame info."""
        self.infoPanel.framenr.setText(fid)
        
    @QtCore.pyqtSlot(int, float, float)
    def setReconsInfo(self,iteration, moduleError, overlapError):
        """Slot that processes incoming reconstruction info."""
        self.infoPanel.iteration.setText(str(iteration))

    @QtCore.pyqtSlot(bool)
    def updateModuleStatus(self, status):
        self.infoPanel.setModuleStatus(status)

    @QtCore.pyqtSlot(bool)
    def updateBackgroundStatus(self, status):
        self.infoPanel.setBackgroundStatus(status)

    @QtCore.pyqtSlot(bool)
    def updateIlluminationStatus(self, status):
        self.infoPanel.setIlluminationStatus(status)

    @QtCore.pyqtSlot(bool, bool)
    def updatePtychoStatus(self, initialized, finished):
        self.infoPanel.setPtychoInitialized(initialized)
        self.infoPanel.setPtychoFinished(finished)

    @QtCore.pyqtSlot(str)
    def appendStatus(self, status):
        """Slot that processes incoming status."""
        if status == "connect":
            self.connectionPanel.connect.setEnabled(False)
            self.connectionPanel.disconnect.setEnabled(True)
            self.simulationPanel.start.setEnabled(True)
            self.backendConnected = True
            self.statusReceived = True
        elif status == "disconnect":
            self.connectionPanel.connect.setEnabled(True)
            self.connectionPanel.disconnect.setEnabled(False)
            self.connectionPanel.interrupt.setEnabled(False)
            self.simulationPanel.start.setEnabled(False)
            self.backendConnected = False
        else:
            self.statusPanel.status.append(status)
    
    @QtCore.pyqtSlot()
    def connectAll(self):
        """Slot connecting all guicontrol signals to streamviewer actions."""
        self.guicontrol.connectAll()
        
    @QtCore.pyqtSlot()
    def disconnectAll(self):
        """Slot disconnecting all guicontrol signals."""
        self.guicontrol.disconnectAll()
        
    @QtCore.pyqtSlot()
    def simulateNewRun(self):
        """Slot that triggers new run."""
        self.guicontrol.startSimulation()
        self.connectionPanel.interrupt.clicked.connect(self.guicontrol.stopSimulation)
        self.connectionPanel.interrupt.setEnabled(True)
        self.simulationPanel.start.setEnabled(False)

    @QtCore.pyqtSlot()
    def interrupt(self):
        """Slot that stops the current run."""
        self.connectionPanel.interrupt.setEnabled(False)
        self.guicontrol.interrupt()

    def resizeEvent(self, event):
        """Overwrites QtGui.QMainWindow.resizeEvent and 
        additionally calls QtGui.QApplication.processEvents()."""
        QtGui.QMainWindow.resizeEvent(self, event)
        QtGui.QApplication.processEvents()

def parse_cmdl_args():
    """Parsing command line arguments"""
    parser = argparse.ArgumentParser(description='nstream')
    parser.add_argument("-c", "--config", type=str, default=None, help="Configuration file")
    parser.add_argument("-s", "--style", type=str, default='default', help="Style, e.g. black, white, default")
    return parser.parse_args()

def main():
    """Entry point to start streaming tab of GUI."""
    args = parse_cmdl_args()
    app = QtGui.QApplication([])
    frame = StreamViewer(args)
    frame.resize(1300,830)
    frame.show()
    app.exec_()


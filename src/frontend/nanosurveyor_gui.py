#! /usr/bin/python
from PyQt4 import QtCore, QtGui, uic
from qtDiffInspector import qtDiffInspectorMain
from qtPtychoViewer import qtPtychoViewerMain
from aux.readXIM import *
from scipy import interpolate
import sys
from aux.readCXI import readCXI
from multiprocessing import Process
import os
import datetime
import numpy as np
from aux.executeParam import *
from aux.util import e2l
from aux.param import Param
import subprocess
import logging
import time
import select
import threading
import nano_options
from ..frontend.streamviewer import StreamViewer
from ..frontend.simulationviewer import SimulationViewer

if "PYPTYCHOROOT" not in os.environ:
  os.environ["PYPTYCHOROOT"] = os.path.dirname(os.path.realpath(__file__))

if "PTYCHODATAROOT" not in os.environ: 
  os.environ["PTYCHODATAROOT"] = os.path.dirname(os.path.realpath(__file__))

p = Param()
param = p.param
DATAPATH = os.environ['PTYCHODATAROOT'] + '/'
HOME = os.environ['PYPTYCHOROOT'] + '/gui/'
TMPDIR = os.environ['PTYCHODATAROOT'] + '/tmp/'
#TRANSMISSIONDATAFILE = os.environ['PYPTYCHOROOT'] + '/pyptycho/examples/Si_data.npy'
TRANSMISSIONDATAFILE = os.environ['PYPTYCHOROOT'] + '/pyptycho/examples/Si_data_5mu.npy'
#print TMPDIR

def loadBSData(dataFile):
    """
    Takes a numpy file with (energy, transmission)
    returns numpy array(energy,transmission)
    """
    return np.load(dataFile)

def getBSTransmission(energy, transmissionData):
    
    f = interpolate.interp1d(transmissionData[:,0],transmissionData[:,1])
    return f(energy)

class DetachableTabWidget(QtGui.QTabWidget):

    """ Subclass of QTabWidget which provides the ability to detach
    tabs and making floating windows from them which can be reattached.
    """

    def __init__(self, *args, **kwargs):
        super(DetachableTabWidget, self).__init__(*args, **kwargs)
        self.setTabBar(_DetachableTabBar())

    def detach_tab(self, i):
        """ Make floating window of tab.

        :param i: index of tab to detach
        """
        teared_widget = self.widget(i)
        widget_name = self.tabText(i)

        # Shift index to the left and remove tab.
        self.setCurrentIndex(self.currentIndex() - 1 if self.currentIndex() > 0 else 0)
        self.removeTab(i)

        # Store widgets window-flags and close event.
        teared_widget._flags = teared_widget.windowFlags()
        teared_widget._close = teared_widget.closeEvent

        # Make stand-alone window.
        teared_widget.setWindowFlags(QtCore.Qt.Window)
        teared_widget.show()

        # Redirect windows close-event into reattachment.
        teared_widget.closeEvent = lambda event: self.attach_tab(teared_widget, widget_name, event, i)

    def attach_tab(self, widget, name, event, index):
        """ Attach widget when receiving QtCore.SIGNAL from child-window.

        :param widget: :class:`QtGui.QWidget`
        :param name: name of attached widget
        :param event: close Event
        """
        widget.setWindowFlags(widget._flags)
        widget.closeEvent = widget._close
        #self.addTab(widget, name)
        self.insertTab(index, widget, name)
        self.setCurrentWidget(widget)
        event.ignore()


class _DetachableTabBar(QtGui.QTabBar):

    def __init__(self, *args, **kwargs):
        super(_DetachableTabBar, self).__init__(*args, **kwargs)
        self._start_drag_pos = None
        self._has_dragged = False
        self.setMovable(True)

    def mousePressEvent(self, event):
        # Keep track of where drag-starts.
        self._start_drag_pos = event.globalPos()
        super(_DetachableTabBar, self).mousePressEvent(event)

    def mouseMoveEvent(self, event):
        # If tab is already detached, do nothing.
        if self._has_dragged:
            return

        # Detach-tab if drag in y-direction is large enough.
        if abs((self._start_drag_pos - event.globalPos()).y()) >= QtGui.QApplication.startDragDistance()*8:
            self._has_dragged = True
            self.parent().detach_tab(self.currentIndex())

    def mouseReleaseEvent(self, event):
        self._has_dragged = False


class ptychoServerViewer(QtGui.QFrame):

    def __init__(self, con, tab_tmp, parent = None):

        super(ptychoServerViewer, self).__init__()
        
        self.tabviewer = tab_tmp

        self.getToday()
        self.inputEdit = self.inputWidget()
        self.outputEdit = self.outputWidget()

	
        self.connectEdit = self.connectWidget()
        self.reconEdit = self.reconWidget()
        self.reconEdit.setTabEnabled(0,False)
        self.reconEdit.setTabEnabled(1,False)
        self.dirWidget = self.controlWidget()
        self.bsWidget = self.beamstopWidget()
        #sys.stdout = OutLog(self.stdOutEdit, sys.stdout)

        #TODO: refactor - stdConEdit gets initialized in self.connectWidget()
        self.options = nano_options.NanoOptions(self.stdOutEdit, self.procsList)
        
        
        tabLabel1 = QtGui.QLabel()
        tabLabel1.setText("Pre-Processor")
        tabLabel2 = QtGui.QLabel()
        tabLabel2.setText("Reconstruction")

        self.hLine = QtGui.QFrame()
        self.hLine.setFrameShape(QtGui.QFrame.HLine)
        self.hLine.setFrameShadow(QtGui.QFrame.Sunken)
        self.hLine.setMinimumSize(QtCore.QSize(512 + 168,10))

        self.space = QtGui.QFrame()
        self.space.setMinimumSize(QtCore.QSize(208,360))

        w = QtGui.QWidget()
        wg = QtGui.QGridLayout(w)
        wg.addWidget(self.dirWidget,0,0,1,3, QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        wg.addWidget(self.hLine,1,0,1,2,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        wg.addWidget(tabLabel1,2,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        wg.addWidget(tabLabel2,2,1,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        wg.addWidget(self.inputEdit, 3,0,5,2, QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        wg.addWidget(self.reconEdit, 3,1,1,1, QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        wg.addWidget(self.bsWidget, 4,1,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        wg.addWidget(self.connectEdit,5,1,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        wg.addWidget(self.outputEdit, 0,2,9,1, QtCore.Qt.AlignTop | QtCore.Qt.AlignRight)
        self.setLayout(wg)

        self.calcPupil()
        self.processList = []

        #try: self.loadParamFromFile(TMPDIR + 'param.txt')
        #except: pass

        #if self.loadParamFromFile(TMPDIR + 'param.txt'): self.setParam(param)

        self.updateBetaValueLabel()
        self.updateEtaValueLabel()
        self.bsData = loadBSData(TRANSMISSIONDATAFILE)
        self.set5mu()
        self.updateEnergy()
        

    def connectWidget(self):

        widget = QtGui.QWidget()
        Grid = QtGui.QGridLayout(widget)
        Grid.setSpacing(0)
        Grid.setMargin(2)

        self.modeLabel = QtGui.QLabel()
        self.modeLabel.setText('Running mode:')
        
        self.buttonGroup = QtGui.QButtonGroup(self)
        self.radio1 = QtGui.QRadioButton("Local")
        self.radio1.setChecked(1)
        self.buttonGroup.addButton(self.radio1)
        self.radio2 = QtGui.QRadioButton("Server Mode 1")
        self.buttonGroup.addButton(self.radio2)
        self.radio3 = QtGui.QRadioButton("Server Mode 2")
        self.buttonGroup.addButton(self.radio3)
        self.buttonGroup.buttonClicked.connect(self.updateMode)
        self.radio3.setEnabled(False)

        #self.stdLabel = QtGui.QLabel()
        #self.stdLabel.setText('Connection to server')
        #self.stdConEdit = QtGui.QTextEdit()
        #self.stdConEdit.setReadOnly(True)
        #self.stdConEdit.setMinimumSize(300,230)
        
        self.closeConnectionButton = QtGui.QPushButton()
        self.closeConnectionButton.setText('Disconnect')
        self.closeConnectionButton.clicked.connect(self.closeConnection)
        self.closeConnectionButton.setMinimumSize(110,24)
        self.closeConnectionButton.setMaximumSize(110,24)
        self.closeConnectionButton.setEnabled(False)
        
        Grid.addWidget(self.modeLabel,0,0,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.radio1,1,0,QtCore.Qt.AlignCenter | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.radio2,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignCenter)
        Grid.addWidget(self.radio3,2,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignCenter)
        Grid.addWidget(self.closeConnectionButton,3,0,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft) 
        #Grid.addWidget(self.stdLabel,3,0,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        #Grid.addWidget(self.stdConEdit,4,0, QtCore.Qt.AlignTop | QtCore.Qt.AlignRight)

        return widget

    def updateMode(self):
        if self.radio1.isChecked(): #Local
            #self.closeConnectionButton.setEnabled(False)
            self.dirEdit.setText('/')
            self.openDirButton.clicked.disconnect()
            self.openDirButton.clicked.connect(self.chooseDir)
            self.modeLabel.setText('Local')
            self.loadConfigButton.clicked.disconnect()
            self.loadConfigButton.clicked.connect(self.loadConfig)
            self.saveConfigButton.clicked.disconnect()
            self.saveConfigButton.clicked.connect(self.saveConfig)
            self.loadCXIButton.clicked.disconnect()
            self.loadCXIButton.clicked.connect(self.loadCXI)
            self.reconEdit.setTabEnabled(0,False)
            self.reconEdit.setTabEnabled(1,False)
            self.sendCommandButton.setEnabled(False)
            
        if self.radio2.isChecked(): #Server mode 1
            if not self.options.is_connected():
                if self.connect2server():
                    self.dirEdit.setText('/tmp/ptycho/')
                    self.openDirButton.clicked.disconnect()
                    self.openDirButton.clicked.connect(lambda: self.options.chooseDirRemote(self.dirEdit))
                    self.modeLabel.setText('Remote')
                    self.loadConfigButton.clicked.disconnect()
                    self.loadConfigButton.clicked.connect(self.loadConfigRemote)
                    self.saveConfigButton.clicked.disconnect()
                    self.saveConfigButton.clicked.connect(self.saveConfigRemote)
                    self.loadCXIButton.clicked.disconnect()
                    self.loadCXIButton.clicked.connect(self.loadCXIRemote)
                    self.reconEdit.setTabEnabled(0,False)
                    self.reconEdit.setTabEnabled(1,False)
                    self.sendCommandButton.setEnabled(False)
                else:
                    self.radio1.setChecked(True)
                    
            else:
                self.dirEdit.setText('/tmp/ptycho/')
                self.openDirButton.clicked.disconnect()
                self.openDirButton.clicked.connect(lambda: self.options.chooseDirRemote(self.dirEdit))
                self.modeLabel.setText('Remote')
                self.loadConfigButton.clicked.disconnect()
                self.loadConfigButton.clicked.connect(self.loadConfigRemote)
                self.saveConfigButton.clicked.disconnect()
                self.saveConfigButton.clicked.connect(self.saveConfigRemote)
                self.loadCXIButton.clicked.disconnect()
                self.loadCXIButton.clicked.connect(self.loadCXIRemote)
                self.reconEdit.setTabEnabled(0,False)
                self.reconEdit.setTabEnabled(1,False)
                self.sendCommandButton.setEnabled(False)

#	if self.radio3.isChecked(): #Server mode 2
#	    if not self.options.is_connected():
#	        if self.connect2server():
#	            if not os.path.isdir('/tmp/ptycho'):
#	                os.mkdir('/tmp/ptycho/')
#	            self.dirEdit.setText('/tmp/ptycho/')
#	            self.openDirButton.clicked.disconnect()
#	            self.openDirButton.clicked.connect(lambda: self.options.chooseDirRemote(self.dirEdit))
#	            self.modeLabel.setText('Remote')
#	            self.loadConfigButton.clicked.disconnect()
#	            self.loadConfigButton.clicked.connect(self.loadConfigRemote)
#	            self.saveConfigButton.clicked.disconnect()
#	            self.saveConfigButton.clicked.connect(self.saveConfigRemote)
#	            self.loadCXIButton.clicked.disconnect()
#	            self.loadCXIButton.clicked.connect(self.loadCXIRemote)
#	            self.reconEdit.setTabEnabled(0,False)
#	            self.reconEdit.setTabEnabled(1,False)
#	            self.sendCommandButton.setEnabled(False)

    def saveConfigRemote(self):
	self.options.saveConfigRemote(self.dirEdit,param)

    def loadConfigRemote(self):
	fileName = self.options.loadConfigRemote(self.dirEdit)

	if fileName.find('.txt') != -1:        
	    self.loadParamFromFile(fileName)
	    self.updateEnergy()
	    self.reconEdit.setTabEnabled(0,True)
	    self.reconEdit.setTabEnabled(1,True)
	    self.sendCommandButton.setEnabled(True)    

    def loadCXIRemote(self):

	fileName = self.options.loadCXIRemote(self.dirEdit)

	#fileName = QFileDialog.getOpenFileName(self, "Open CXI File", DATAPATH, "CXI (*.cxi)")
        #print os.path.dirname(str(fileName)) + '/'
        #self.dirEdit.setText(os.path.dirname(str(fileName)) + '/')

	if fileName.find('.cxi') != -1:
	    self.loadParamFromFile(fileName)
            self.updateEnergy()
	    self.reconEdit.setTabEnabled(0,True)
	    self.reconEdit.setTabEnabled(1,True)
	    self.sendCommandButton.setEnabled(True)

    def connect2server(self):
        out = self.options.connect2server()
	if out:
	    self.sendCommandButton.setEnabled(True)
	    self.closeConnectionButton.setEnabled(True)
	    return out

    def closeConnection(self):
        self.options.closeConnection()
        self.sendCommandButton.setEnabled(False)
        self.closeConnectionButton.setEnabled(False)
        
    def sendCommandButtonClicked(self):
        if self.radio1.isChecked() == True:
            self.execute()
            return
        else:
            print "Sending command..."
            self.updateCommandString()
            self.options.sendCommandButtonClicked(self.commandString)
            
    def showResultsButtonClicked(self):
        self.options.showResultsButtonClicked()

    def copyRemoteDataset():
        """
            TODO create UI to select Remote Dataset & Local directory.
            then call copy command with paths..
        """
        
        remote_dataset = str(self.dirEdit.text()) + "/" + "NS_141017001.cxi"
        local_dataset = "/tmp/NS_141017001.cxi"
        self.options.copyRemoteDataset(remote_dataset, local_dataset)

        #ask whether the new file should be opened
        """ Simple messagebox to ask whether to show the copied file """
        self.tabviewer.openFile(local_path)
        self.stdOutEdit.append('Result can be visualized on the second tab!')


    def getToday(self):

        self.year = datetime.date.today().year
        self.yearStr = str(self.year - 2000)
        self.month = datetime.date.today().month
        if self.month < 10: self.monthStr = '0' + str(self.month)
        else: self.monthStr = str(self.month)
        self.day = datetime.date.today().day
        if self.day < 10: self.dayStr = '0' + str(self.day)
        else: self.dayStr = str(self.day)
        self.todaysDateStr = self.yearStr + self.monthStr + self.dayStr

    def startServer(self):

        pass

    def stopServer(self):

        pass

    def updateCommandString(self):
        self.updateParameters()	
        self.commandString = parseParam(param)
        self.stdCommandEdit.clear()
        self.stdCommandEdit.append(self.commandString)
        
        #def execute(self, fileName = False):
    def updateParameters(self):
        self.calcPixnm()

        param["process"] = str(1 * self.preprocessButton.isChecked())
        param["reconstruct"] = str(1 * self.reconButton.isChecked())
        param["dataPath"] = str(self.dirEdit.text())#DATAPATH
        param["scanDate"] = str(self.dateEdit.text())
        param['Year'] = '20' + param['scanDate'][0:2]
        param['Month'] = param['scanDate'][2:4]
        param['Day'] = param['scanDate'][4:6]
        param["scanNumber"] = str(self.scanNumEdit.text())
        param["scanID"] = str(self.scanIDEdit.text())
        param["bgScanDate"] = str(self.dateEdit.text())
        param["bgScanNumber"] = str(self.bgScanNumEdit.text())
        param["bgScanID"] = str(self.bgScanIDEdit.text())
        param["bin"] = str(self.binEdit.text())
        param["pixnm"] = str(self.pixnmEdit.text())
        param["sh_sample_y"] = str(self.sampleEdit.text())
        param["sh_sample_x"] = str(self.sampleEdit.text())
        param["ssx"] = str(self.xStepSizeEdit.text())
        param["ssy"] = str(self.yStepSizeEdit.text())
        param["xpts"] = str(self.xPtsEdit.text())
        param["ypts"] = str(self.yPtsEdit.text())
        param["zd"] = str(self.zdEdit.text())
        param["dr"] = str(self.drEdit.text())
        param["e"] = str(self.enEdit.text())
        param["ccdp"] = str(self.ccdpEdit.text())
        param["ccdz"] = str(self.ccdzEdit.text())
        if str(self.shortExpEdit.text()) == '0': param["nexp"] = '1'
        else: param["nexp"] = '2'
        param["nl"] = str(self.ntEdit.text())
        param["xcenter"] = str(self.xcEdit.text())
        param["ycenter"] = str(self.ycEdit.text())
        param["t_long"] = str(self.longExpEdit.text())
        param["t_short"] = str(self.shortExpEdit.text())
        param["s_threshold"] = str(self.stEdit.text())
        param["bl"] = str(self.blEdit.text())
        param["cxi"] = '1'
        param["gpu"] = str(1 * self.gpuButton.isChecked())
        param["nIterations"] = str(self.itEdit.text())
        param["probeMask"] = str(1 * self.pmButton.isChecked())
        param["pR"] = str(1 * self.prButton.isChecked())
        param["bR"] = str(1 * self.brButton.isChecked())
        param["useStxm"] = str(1 * self.useStxmButton.isChecked())
        param["usePrevious"] = str(1 * self.usePreviousButton.isChecked())
        param["illuminationIntensities"] = str(1 * self.iiButton.isChecked())
        param["fv"] = str(1 * self.vInvertButton.isChecked())
        param["fh"] = str(1 * self.hInvertButton.isChecked())
        param["transpose"] = str(1 * self.transposeButton.isChecked())
        #param["fCCD_version"] = '2'
        param["fCCD_version"] = str(2 - self.fCCD_OldSoftwareButton.isChecked())
        param["removeOutliers"] = str(1 * self.outliersButton.isChecked())
        param["removePhasePlane"] = '1'
        param['ssn'] = str(self.pixStepEdit.text())
        nCPU = 1
        value = self.nCPUBox.currentText()
        if value!='':
            nCPU = int(value)
        param['nCPU'] = str(nCPU + 1)
        param['nBlocks'] = str(int(np.sqrt(nCPU)))
        param['nGPU'] = str(self.nGPUBox.currentText())
        param['loopSize'] = str(self.loopSizeEdit.text())
        param['interferometer'] = str(1 * self.interferometerButton.isChecked())
        param['useCluster'] = str(1 * self.phasisButton.isChecked())
        param['low_pass'] = str(1 * self.filterButton.isChecked())
        param['beamStopTransmission'] = str(self.bsEdit.text())
        #param['beta'] = str(self.betaEdit.text())
        param['beta'] = str(float(self.betaSlider.value())/100)
        self.updateBetaValueLabel()
        #param['eta'] = str(self.etaEdit.text())
        param['eta'] = str(float(self.etaSlider.value())/100)
        self.updateEtaValueLabel()
        param['defocus'] = str(self.defocusEdit.text())
        param['illuminationPeriod'] = str(self.probeIntervalEdit.text())
        param['backgroundPeriod'] = str(self.backgroundIntervalEdit.text())
        param['probeFile'] = str(self.probeEdit.text())
        param['illuminationIntensitiesFile'] = str(self.iiEdit.text())
        param['beamstopXshift'] = str(self.xshiftEdit.text())
        param['beamstopYshift'] = str(self.yshiftEdit.text())

        if self.speFileEdit.text() == '':
            param["version"] = '2'
            #param['dataFile'] = param['bl'].upper() + '_' + param['scanDate'] + param['scanNumber'] + '.cxi' #CXI file name
            if param['bl'] == 'K': param['bgScanDir'] = param['dataPath'] + param['scanDate'] + '/' + \
                 'K' + param['scanDate'] + param['bgScanNumber'] + '/' + param['bgScanID'] + '/'
            else: param['bgScanDir'] = param['dataPath'] + param['scanDate'] + '/' + \
                 param['scanDate'] + param['bgScanNumber'] + '/' + param['bgScanID'] + '/'
            if param['bl'] == 'K': param['scanDir'] = param['dataPath']+ param['scanDate'] + '/'  + 'K' + param['scanDate'] + \
                param['scanNumber'] + '/' + param['scanID'] + '/'
            else: param['scanDir'] = param['dataPath'] + param['scanDate'] + '/' + param['scanDate'] + param['scanNumber'] + '/' + param['scanID'] + '/'
        else:
            #param["version"] = '1'
            param["version"] = '2'
            param['dataPath'] = str(self.speDirEdit.text())
            param['speFile'] = str(self.speFileEdit.text())
            #param['dataFile'] = str(param['speFile']).split('.SPE')[0] + '.cxi'
            param['bg_filename'] = str(self.speBGEdit.text())
            param['scanDir'] = param['dataPath']
            param['bgScanDir'] = param['dataPath']

        ##Filename logic.  If the User does not select an input file the filename is generated automatically from scan/data/ID
        ##If the user does not define the output file name then it is set to the input file name by default.
        if str(self.inputFileEdit.text()) == '':
            param['dataFile'] = param['bl'].upper() + '_' + param['scanDate'] + param['scanNumber'] + '.cxi' #CXI file name
        else:
            param['dataFile'] = str(self.inputFileEdit.text())
        if str(self.outFileEdit.text()) == '': param['outputFilename'] = param['dataFile']
        else: param['outputFilename'] = str(self.outFileEdit.text())

        param['zpw'] = str(self.pupilEdit.text())
        param['fCCD']=str(1 * self.fCCDButton.isChecked())
        param['probeThreshold'] = str(self.ptEdit.text())
        if (str(self.filterEdit.text()) == '') or (str(self.filterEdit.text()) == '0'):
            param["lowPassFilter"] = '0'
        else:
            param["lowPassFilter"] = '1'
            param['filter_width'] = str(self.filterEdit.text())

        #if fileName == False:
        #    saveParamFile(TMPDIR + 'param.txt', param)
        #    if self.phasisButton.isChecked():
        #        saveParamFile(os.environ['NSRECONSERVERROOT'] + 'param.txt', param)
        #    else:
        #        self.p = Process(target=runParam, args=(param,))
        #        self.p.start()
        #else:
        #    saveParamFile(fileName, param)
        #
        ##save parameter list to temp dir

    def execute(self, fileName = False):
        self.updateCommandString()	

        if fileName == False:
            if self.phasisButton.isChecked():
                print "Using ETA: %s" %param['eta']
                saveParamFile(os.environ['NSRECONSERVERROOT'] + 'param.txt', param)
            else:
                self.commandString = parseParam(param)
                self.p = Process(target=runParam, args=(self.commandString,))
                self.p.start()
        else:
            saveParamFile(fileName, param)

        ##save parameter list to temp dir
        saveParamFile(TMPDIR + 'param.txt', param)


    def controlWidget(self):

        widget = QtGui.QWidget()
        Grid = QtGui.QGridLayout(widget)
        Grid.setSpacing(5)
        Grid.setMargin(5)

        self.dirLabel = QtGui.QLabel()
        self.dirLabel.setText("Base Data Directory")
        self.dirEdit = QtGui.QLineEdit()
        self.dirEdit.setMinimumSize(350,24)
        #self.dirEdit.setText(DATAPATH)
        self.dirEdit.setText('/home/')

        self.openDirButton = QtGui.QPushButton(QtGui.QIcon(HOME + 'icons/open.png'),'')
        self.openDirButton.clicked.connect(self.chooseDir)
        self.openDirButton.setMaximumSize(32, 32)
        self.openDirButton.setToolTip('Select Data Base Directory')
        
        self.modeLabel = QtGui.QLabel()
        self.modeLabel.setText("Local")

        Grid.addWidget(self.dirLabel,0,0,1,1, QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.dirEdit,0,1,1,1, QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.openDirButton,0,2,1,1, QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.modeLabel,0,3,1,1, QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        return widget

    def chooseDir(self):

        """ Provides a dialog window to allow the user to specify an image file.
            If a file is selected, the appropriate function is called to process
            and display it.
        """
        #dir = QFileDialog.getExistingDirectory(self, "Choose the base data directory")
        #print('Browsing local files...')
        dir = QFileDialog.getExistingDirectory(self, "Choose the base data directory",self.dirEdit.text())

        if dir != '':
            self.dirEdit.setText(dir + '/')

    def chooseProbeFile(self):

        """ Provides a dialog window to allow the user to specify an image file.
            If a file is selected, the appropriate function is called to process
            and display it.
        """
        probeFile = QFileDialog.getOpenFileName(self, "Choose probe file",  "Numpy (*.npy)")

        if dir != '':
            self.probeEdit.setText(probeFile)

    def chooseIlluminationIntensitiesFile(self):

        """ Provides a dialog window to allow the user to specify an image file.
            If a file is selected, the appropriate function is called to process
            and display it.
        """
        iiFile = QFileDialog.getOpenFileName(self, "Choose illumination intensities file",  "Numpy (*.npy)")

        if dir != '':
            self.iiEdit.setText(iiFile)

    def inputWidget(self):

        tab_widget = QtGui.QTabWidget()

        tab1 = self.simpleInputWidget()
        tab2 = self.advancedInputWidget()

        tab_widget.addTab(tab1, "Basic")
        tab_widget.addTab(tab2, "Advanced")

        # layout manager
        vbox = QtGui.QVBoxLayout()
        vbox.addWidget(tab_widget)
        return tab_widget

    def reconWidget(self):

        tab_widget = QtGui.QTabWidget()

        tab1 = self.simpleReconWidget()
        tab2 = self.advancedReconWidget()

        tab_widget.addTab(tab1, "Basic")
        tab_widget.addTab(tab2, "Advanced")

        # layout manager
        vbox = QtGui.QVBoxLayout()
        vbox.addWidget(tab_widget)
        return tab_widget

    def beamstopWidget(self):

        widget = QtGui.QWidget()
        Grid = QtGui.QGridLayout(widget)
        Grid.setSpacing(5)
        Grid.setMargin(5)

        self.bsSelectButton = QtGui.QPushButton()
        self.bsSelectButton.setText('Beamstop Thickness')
        self.bsSelectButton.setMinimumSize(128, 26)

        set5mu = QtGui.QAction('5 micron', self,triggered=self.set5mu)
        set5mu.setStatusTip('5 micron')

        set10mu = QtGui.QAction('10 micron', self,triggered=self.set10mu)
        set10mu.setStatusTip('10 micron')

        bsMenu = QtGui.QMenu()
        bsMenu.addAction(set5mu)
        bsMenu.addAction(set10mu)
        self.bsSelectButton.setMenu(bsMenu)

        self.bsLabel = QtGui.QLabel()
        self.bsLabel.setText("Beamstop Transmission")
        self.bsEdit = QtGui.QLineEdit()
        self.bsEdit.setMaximumSize(100,24)
        self.bsEdit.setText('0.01')

        self.xshiftLabel = QtGui.QLabel()
        self.xshiftLabel.setText("Beamstop X shift")
        self.xshiftEdit = QtGui.QLineEdit()
        self.xshiftEdit.setMaximumSize(100,24)
        self.xshiftEdit.setText('-3')

        self.yshiftLabel = QtGui.QLabel()
        self.yshiftLabel.setText("Beamstop Y shift")
        self.yshiftEdit = QtGui.QLineEdit()
        self.yshiftEdit.setMaximumSize(100,24)
        self.yshiftEdit.setText('-5')

        Grid.addWidget(self.bsLabel,2,1,1,1,QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.bsEdit,2,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.bsSelectButton,0,0,1,2,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.xshiftEdit,3,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.xshiftLabel,3,1,1,1,QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.yshiftEdit,4,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.yshiftLabel,4,1,1,1,QtCore.Qt.AlignVCenter| QtCore.Qt.AlignLeft)

        return widget

    def updateBeamstop(self, dataFile):
        self.bsData = loadBSData(dataFile)
        self.updateBSTransmission()

    def set5mu(self):

        self.updateBeamstop(os.environ['PYPTYCHOROOT'] + '/pyptycho/examples/Si_data_5mu.npy')

    def set10mu(self):

        self.updateBeamstop(os.environ['PYPTYCHOROOT'] + '/pyptycho/examples/Si_data_10mu.npy')

    def simpleReconWidget(self):

        widget = QtGui.QWidget()
        Grid = QtGui.QGridLayout(widget)
        Grid.setSpacing(5)
        Grid.setMargin(5)

        self.itLabel = QtGui.QLabel()
        self.itLabel.setText("Number of Iterations")
        self.itEdit = QtGui.QLineEdit()
        self.itEdit.setMinimumSize(128,24)
        self.itEdit.setText('100')
        self.itEdit.textChanged.connect(self.updateCommandString)

        self.gpuButton = QtGui.QCheckBox()
        self.gpuButton.setText('Use GPU')
        self.gpuButton.setChecked(True)
        self.gpuButton.stateChanged.connect(self.updateCommandString)

        self.pmButton = QtGui.QCheckBox()
        self.pmButton.setText('Use Probe Mask')
        self.pmButton.setChecked(True)
        self.pmButton.stateChanged.connect(self.updateCommandString)

        self.prButton = QtGui.QCheckBox()
        self.prButton.setText('Use Probe Retrieval')
        self.prButton.setChecked(True)
        self.prButton.stateChanged.connect(self.updateCommandString)

        self.iiButton = QtGui.QCheckBox()
        self.iiButton.setText('Use Illumination Intensities')
        self.iiButton.setChecked(False)

        self.brButton = QtGui.QCheckBox()
        self.brButton.setText('Use Background Retrieval')
        self.brButton.stateChanged.connect(self.updateCommandString)

        self.useStxmButton = QtGui.QCheckBox()
        self.useStxmButton.setText('Use STXM Image')
        self.useStxmButton.stateChanged.connect(self.updateCommandString)

        self.usePreviousButton = QtGui.QCheckBox()
        self.usePreviousButton.setText('Use Previous Image')
        self.usePreviousButton.stateChanged.connect(self.updateCommandString)

        self.hLine = QtGui.QFrame()
        self.hLine.setFrameShape(QtGui.QFrame.HLine)
        self.hLine.setFrameShadow(QtGui.QFrame.Sunken)
        self.hLine.setMinimumSize(QtCore.QSize(256,10))

        #self.executeButton = QtGui.QPushButton()
        #self.executeButton.setText('Run')
        #self.executeButton.clicked.connect(self.execute)
        #self.executeButton.setMinimumSize(128,24)
        #self.executeButton.setMaximumSize(128,24)

        self.preprocessButton = QtGui.QCheckBox()
        self.preprocessButton.setText('Pre-Process')
        self.preprocessButton.setChecked(True)
        self.preprocessButton.stateChanged.connect(self.updateCommandString)

        self.reconButton = QtGui.QCheckBox()
        self.reconButton.setText('Reconstruct')
        self.reconButton.setChecked(True)
        self.reconButton.stateChanged.connect(self.updateCommandString)

        Grid.addWidget(self.itEdit,0,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.itLabel,0,1,1,1,QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.gpuButton,1,0,1,2,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.pmButton,2,0,1,2,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.prButton,3,0,1,2,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.brButton,4,0,1,2,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.iiButton,5,0,1,2,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.useStxmButton,6,0,1,2,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.usePreviousButton,7,0,1,2,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.hLine,8,0,1,2,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        #Grid.addWidget(self.executeButton,12,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.preprocessButton,10,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.reconButton,9,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)


        return widget

    def advancedReconWidget(self):

        widget = QtGui.QWidget()
        Grid = QtGui.QGridLayout(widget)
        Grid.setSpacing(5)
        Grid.setMargin(5)

        self.phasisButton = QtGui.QCheckBox()
        self.phasisButton.setText('Use Phasis Cluster')
        self.phasisButton.setChecked(False)
        self.phasisButton.clicked.connect(self.updateGPUBox)
        self.phasisButton.clicked.connect(self.updateCPUBox)
        self.phasisButton.stateChanged.connect(self.updateCommandString)
	

        self.filterButton = QtGui.QCheckBox()
        self.filterButton.setText('Low-pass filter the image')
        self.filterButton.setChecked(False)
        self.filterButton.stateChanged.connect(self.updateCommandString)

        self.nGPUBox = QtGui.QComboBox()
        self.nGPUBox.setMaximumSize(128,24)
        self.nGPUBox.setMinimumSize(128,24)
        self.updateGPUBox()
        self.nGPUBox.setCurrentIndex(3)
        self.nGPULabel = QtGui.QLabel("Number of GPUs")
        self.nGPUBox.currentIndexChanged.connect(self.updateCommandString)


        self.nCPUBox = QtGui.QComboBox()
        self.nCPUBox.setMaximumSize(128,24)
        self.nCPUBox.setMinimumSize(128,24)
        self.updateCPUBox()
        self.nCPUBox.setCurrentIndex(3)
        self.nCPULabel = QtGui.QLabel("Number of CPUs")
        self.nCPUBox.currentIndexChanged.connect(self.updateCommandString)

        self.loopSizeLabel = QtGui.QLabel()
        self.loopSizeLabel.setText("Iterations per MPI loop")
        self.loopSizeEdit = QtGui.QLineEdit()
        self.loopSizeEdit.setMinimumSize(128,24)
        self.loopSizeEdit.setText('10')
        self.loopSizeEdit.textChanged.connect(self.updateCommandString)

        self.probeIntervalLabel = QtGui.QLabel()
        self.probeIntervalLabel.setText("Probe Interval")
        self.probeIntervalEdit = QtGui.QLineEdit()
        self.probeIntervalEdit.setMinimumSize(128,24)
        self.probeIntervalEdit.setText('2')

        self.backgroundIntervalLabel = QtGui.QLabel()
        self.backgroundIntervalLabel.setText("Background Interval")
        self.backgroundIntervalEdit = QtGui.QLineEdit()
        self.backgroundIntervalEdit.setMinimumSize(128,24)
        self.backgroundIntervalEdit.setText('2')

        #self.betaLabel = QtGui.QLabel()
        #self.betaLabel.setText("Beta (-2,2)")
        #self.betaEdit = QtGui.QLineEdit()
        #self.betaEdit.setMaximumSize(100,24)
        #self.betaEdit.setText('0.6')
        
        self.betaLabel = QtGui.QLabel()
        self.betaLabel.setText("Beta Value")
        self.betaSlider = QtGui.QSlider()
        self.betaSlider.setMinimumSize(128,24)
        self.betaSlider.setOrientation(QtCore.Qt.Horizontal)
        self.betaSlider.setRange(-200,200)
        self.betaSlider.setTickInterval(10)
        self.betaSlider.setSingleStep(5)
        self.betaSlider.setValue(60)
        self.betaValueLabel = QtGui.QLabel()
        self.betaSlider.valueChanged.connect(self.updateCommandString)

        #self.etaLabel = QtGui.QLabel()
        #self.etaLabel.setText("Eta (-2,2)")
        #self.etaEdit = QtGui.QLineEdit()
        #self.etaEdit.setMaximumSize(100,24)
        #self.etaEdit.setText('0.6')

        self.etaLabel = QtGui.QLabel()
        self.etaLabel.setText("Eta Value")
        self.etaSlider = QtGui.QSlider()
        self.etaSlider.setOrientation(QtCore.Qt.Horizontal)
        self.etaSlider.setMinimumSize(128,24)
        self.etaSlider.setRange(-200,200)
        self.etaSlider.setTickInterval(10)
        self.etaSlider.setSingleStep(5)
        self.etaSlider.setValue(60)
        self.etaValueLabel = QtGui.QLabel()
        self.etaSlider.valueChanged.connect(self.updateCommandString)

        self.probeLabel = QtGui.QLabel()
        self.probeLabel.setText("Probe input file")
        self.probeEdit = QtGui.QLineEdit()
        self.probeEdit.setMaximumSize(128,24)
        self.probeEdit.setText('')

        self.iiLabel = QtGui.QLabel()
        self.iiLabel.setText("II input file")
        self.iiEdit = QtGui.QLineEdit()
        self.iiEdit.setMaximumSize(128,24)
        self.iiEdit.setText('')

        self.chooseProbeButton = QtGui.QPushButton(QtGui.QIcon(HOME + 'icons/open.png'),'')
        self.chooseProbeButton.clicked.connect(self.chooseProbeFile)
        self.chooseProbeButton.setMaximumSize(32, 32)
        self.chooseProbeButton.setToolTip('Select Probe File')

        self.chooseiiButton = QtGui.QPushButton(QtGui.QIcon(HOME + 'icons/open.png'),'')
        self.chooseiiButton.clicked.connect(self.chooseIlluminationIntensitiesFile)
        self.chooseiiButton.setMaximumSize(32, 32)
        self.chooseiiButton.setToolTip('Select Illumination Intensities File')

        Grid.addWidget(self.nGPULabel,0,1,1,3,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.nGPUBox,0,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.nCPULabel,1,1,1,3,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.nCPUBox,1,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.loopSizeEdit,2,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.loopSizeLabel,2,1,1,3,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        #Grid.addWidget(self.betaEdit,3,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.betaSlider,3,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.betaValueLabel,3,1,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.betaLabel,3,2,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        #Grid.addWidget(self.betaLabel,3,1,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        
        Grid.addWidget(self.etaSlider,4,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.etaValueLabel,4,1,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.etaLabel,4,2,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        
        #Grid.addWidget(self.etaEdit,4,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        #Grid.addWidget(self.etaLabel,4,1,1,3,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)

        Grid.addWidget(self.probeIntervalEdit,5,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.probeIntervalLabel,5,1,1,3,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.backgroundIntervalEdit,6,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.backgroundIntervalLabel,6,1,1,3,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.probeEdit,7,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.probeLabel,7,2,1,3,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.iiEdit,8,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.iiLabel,8,2,1,3,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.chooseProbeButton,7,1,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.chooseiiButton,8,1,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.phasisButton,9,0,7,2,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.filterButton,12,0,7,2,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)

        return widget

    def updateBetaValueLabel(self):

        self.betaValueLabel.setText(str(float(self.betaSlider.value())/100))

    def updateEtaValueLabel(self):

        self.etaValueLabel.setText(str(float(self.etaSlider.value())/100))

    def updateGPUBox(self):

        for i in range(self.nGPUBox.count()):
            self.nGPUBox.removeItem(0)

        nGPUs = self.phasisButton.isChecked() * 12 + 4
        for i in range(nGPUs):
            self.nGPUBox.addItem(str(i + 1))

        self.nGPUBox.setCurrentIndex(3)


    def updateCPUBox(self):

        for i in range(self.nCPUBox.count()):
            self.nCPUBox.removeItem(0)

        if self.phasisButton.isChecked():
            self.nCPUBox.addItem('1')
            self.nCPUBox.addItem('4')
            self.nCPUBox.addItem('9')
            self.nCPUBox.addItem('16')
            self.nCPUBox.addItem('25')
            self.nCPUBox.addItem('36')
            self.nCPUBox.addItem('49')
        else:
            self.nCPUBox.addItem('1')
            self.nCPUBox.addItem('4')
            self.nCPUBox.addItem('9')
            self.nCPUBox.addItem('16')

        self.nCPUBox.setCurrentIndex(3)

    def advancedInputWidget(self):

        widget = QtGui.QWidget()
        Grid = QtGui.QGridLayout(widget)
        Grid.setSpacing(5)
        Grid.setMargin(5)

        self.sampleLabel = QtGui.QLabel()
        self.sampleLabel.setText("Final Array Size")
        self.sampleEdit = QtGui.QLineEdit()
        self.sampleEdit.setMaximumSize(100,24)
        self.sampleEdit.setText('256')

        self.filterLabel = QtGui.QLabel()
        self.filterLabel.setText("Filter Width in Pixels")
        self.filterEdit = QtGui.QLineEdit()
        self.filterEdit.setMaximumSize(100,24)
        self.filterEdit.setText('60')

        self.pupilLabel = QtGui.QLabel()
        self.pupilLabel.setText("ZP Pupil Width")
        self.pupilEdit = QtGui.QLineEdit()
        self.pupilEdit.setMaximumSize(100,24)
        self.pupilEdit.setText('60')
        self.connect(self.pupilEdit, QtCore.SIGNAL('returnPressed()'), self.calcCCDZ)

        self.ccdzLabel = QtGui.QLabel()
        self.ccdzLabel.setText("CCD Z")
        self.ccdzEdit = QtGui.QLineEdit()
        self.ccdzEdit.setMaximumSize(100,24)
        self.ccdzEdit.setText('79.55')
        self.connect(self.ccdzEdit, QtCore.SIGNAL('returnPressed()'), self.calcPupil)

        self.ccdpLabel = QtGui.QLabel()
        self.ccdpLabel.setText("CCD Pixel Size (um)")
        self.ccdpEdit = QtGui.QLineEdit()
        self.ccdpEdit.setMaximumSize(100,24)
        self.ccdpEdit.setText('20')
        self.connect(self.ccdpEdit, QtCore.SIGNAL('returnPressed()'), self.calcPupil)

        self.zdLabel = QtGui.QLabel()
        self.zdLabel.setText("Zone Plate Diameter (um)")
        self.zdEdit = QtGui.QLineEdit()
        self.zdEdit.setMaximumSize(100,24)
        self.zdEdit.setText('90')
        self.connect(self.zdEdit, QtCore.SIGNAL('returnPressed()'), self.calcPupil)

        self.drLabel = QtGui.QLabel()
        self.drLabel.setText("Outer Zone Width (nm)")
        self.drEdit = QtGui.QLineEdit()
        self.drEdit.setMaximumSize(100,24)
        self.drEdit.setText('250')
        self.connect(self.drEdit, QtCore.SIGNAL('returnPressed()'), self.calcPupil)

        self.xcLabel = QtGui.QLabel()
        self.xcLabel.setText("X Center Pixel")
        self.xcEdit = QtGui.QLineEdit()
        self.xcEdit.setMaximumSize(100,24)
        self.xcEdit.setText('400')

        self.ycLabel = QtGui.QLabel()
        self.ycLabel.setText("Y Center Pixel")
        self.ycEdit = QtGui.QLineEdit()
        self.ycEdit.setMaximumSize(100,24)
        self.ycEdit.setText('400')

        self.ntLabel = QtGui.QLabel()
        self.ntLabel.setText("Noise Threshold")
        self.ntEdit = QtGui.QLineEdit()
        self.ntEdit.setMaximumSize(100,24)
        self.ntEdit.setText('10')

        self.stLabel = QtGui.QLabel()
        self.stLabel.setText("Saturation Threshold")
        self.stEdit = QtGui.QLineEdit()
        self.stEdit.setMaximumSize(100,24)
        self.stEdit.setText('20000')

        self.ptLabel = QtGui.QLabel()
        self.ptLabel.setText("Probe Threshold")
        self.ptEdit = QtGui.QLineEdit()
        self.ptEdit.setMaximumSize(100,24)
        self.ptEdit.setText('0.2')

        self.filterLabel = QtGui.QLabel()
        self.filterLabel.setText("Filter RMS Width (pixels)")
        self.filterEdit = QtGui.QLineEdit()
        self.filterEdit.setMaximumSize(100,24)
        self.filterEdit.setText('100')

        self.blLabel = QtGui.QLabel()
        self.blLabel.setText("Beamline")
        self.blEdit = QtGui.QLineEdit()
        self.blEdit.setMaximumSize(100,24)
        self.blEdit.setText('ns')

        self.speFileEdit = QtGui.QLineEdit()
        self.speFileEdit.setMaximumSize(100,24)
        self.speFileEdit.setText('')

        self.loadSpeFileButton = QtGui.QPushButton()
        self.loadSpeFileButton.setText('Load SPE Data')
        self.loadSpeFileButton.clicked.connect(self.loadSpeData)
        self.loadSpeFileButton.setMinimumSize(128,24)
        self.loadSpeFileButton.setMaximumSize(128,24)

        self.speBGEdit = QtGui.QLineEdit()
        self.speBGEdit.setMaximumSize(100,24)
        self.speBGEdit.setText('')

        self.outFileEdit = QtGui.QLineEdit()
        self.outFileEdit.setMaximumSize(100,24)
        self.outFileEdit.setText('')
        self.outFileLabel = QtGui.QLabel()
        self.outFileLabel.setText("Output Filename")

        self.inputFileEdit = QtGui.QLineEdit()
        self.inputFileEdit.setMaximumSize(100,24)
        self.inputFileEdit.setText('')
        self.inputFileLabel = QtGui.QLabel()
        self.inputFileLabel.setText("Input Filename")

        self.loadSpeBGFileButton = QtGui.QPushButton()
        self.loadSpeBGFileButton.setText('Load SPE BG')
        self.loadSpeBGFileButton.clicked.connect(self.loadSpeBG)
        self.loadSpeBGFileButton.setMinimumSize(128,24)
        self.loadSpeBGFileButton.setMaximumSize(128,24)

        self.speDirEdit = QtGui.QLineEdit()
        self.speDirEdit.setMaximumSize(100,24)
        self.speDirEdit.setText('')

        self.loadSpeDirButton = QtGui.QPushButton()
        self.loadSpeDirButton.setText('Load SPE Dir')
        self.loadSpeDirButton.clicked.connect(self.loadSpeDir)
        self.loadSpeDirButton.setMinimumSize(128,24)
        self.loadSpeDirButton.setMaximumSize(128,24)

        self.outliersButton = QtGui.QCheckBox()
        self.outliersButton.setText('Remove Outliers')

        self.fCCDButton = QtGui.QCheckBox()
        self.fCCDButton.setText('fastCCD')
        self.fCCDButton.setChecked(True)

        self.hInvertButton = QtGui.QCheckBox()
        self.hInvertButton.setText('Flip Hor')

        self.vInvertButton = QtGui.QCheckBox()
        self.vInvertButton.setText('Flip Ver')

        self.transposeButton = QtGui.QCheckBox()
        self.transposeButton.setText('Transpose')

        self.fCCD_OldSoftwareButton = QtGui.QCheckBox()
        self.fCCD_OldSoftwareButton.setText('Old fCCD Software')

        self.interferometerButton = QtGui.QCheckBox()
        self.interferometerButton.setText('Use IFM Data')

        Grid.addWidget(self.sampleEdit,0,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.sampleLabel,0,1,1,1,QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.pupilEdit,1,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.pupilLabel,1,1,1,1,QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.ccdzEdit,2,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.ccdzLabel,2,1,1,1,QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.ccdpEdit,3,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.ccdpLabel,3,1,1,1,QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.zdEdit,4,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.zdLabel,4,1,1,1,QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.drEdit,5,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.drLabel,5,1,1,1,QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.xcEdit,6,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.xcLabel,6,1,1,1,QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.ycEdit,7,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.ycLabel,7,1,1,1,QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.ntEdit,8,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.ntLabel,8,1,1,1,QtCore.Qt.AlignVCenter| QtCore.Qt.AlignLeft)
        Grid.addWidget(self.stEdit,9,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.stLabel,9,1,1,1,QtCore.Qt.AlignVCenter| QtCore.Qt.AlignLeft)
        Grid.addWidget(self.filterEdit,10,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.filterLabel,10,1,1,1,QtCore.Qt.AlignVCenter| QtCore.Qt.AlignLeft)
        Grid.addWidget(self.ptEdit,11,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.ptLabel,11,1,1,1,QtCore.Qt.AlignVCenter| QtCore.Qt.AlignLeft)
        Grid.addWidget(self.blEdit,12,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.blLabel,12,1,1,1,QtCore.Qt.AlignVCenter| QtCore.Qt.AlignLeft)

        Grid.addWidget(self.outFileEdit, 13, 0, 1, 1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.outFileLabel, 13, 1, 1, 1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.inputFileEdit, 14, 0, 1, 1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.inputFileLabel, 14, 1, 1, 1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.speFileEdit,15,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.loadSpeFileButton,15,1,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.speBGEdit,16,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.loadSpeBGFileButton,16,1,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.speDirEdit,17,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.loadSpeDirButton,17,1,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.outliersButton,18,1,1,2,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.fCCDButton,18,0,1,2,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.hInvertButton,19,0,1,2,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.vInvertButton,19,1,1,2,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.transposeButton,20,0,1,2,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.fCCD_OldSoftwareButton,21,0,1,3,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.interferometerButton,22,0,1,3,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)


        return widget

    def calcCCDZ(self):

        zd = self.zdEdit.text().toFloat()[0]
        dr = self.drEdit.text().toFloat()[0]
        l = e2l(self.enEdit.text().toFloat()[0])
        f = zd * dr / l #zp focal length microns
        na = zd / f / 2. #zp numerical aperture
        self.ccdzEdit.setText(str(np.round(self.ccdpEdit.text().toFloat()[0] * self.pupilEdit.text().toFloat()[0] / (2. * na)/1000.,2)))

    def calcPupil(self):

        zd = self.zdEdit.text().toFloat()[0]
        if zd == 0: return
        dr = self.drEdit.text().toFloat()[0]
        l = e2l(self.enEdit.text().toFloat()[0])
        f = zd * dr / l #zp focal length microns
        na = zd / f / 2. #zp numerical aperture
        self.pupilEdit.setText(str(round(1000. * self.ccdzEdit.text().toFloat()[0] * (2. * na) / self.ccdpEdit.text().toFloat()[0],2)))

    def loadSpeData(self):

        fileName = QFileDialog.getOpenFileName(self,
            "Open SPE file", DATAPATH, "SPE (*.SPE);; SPE (*.spe)")

        self.speFile = str(fileName).split('/')[-1::1][0]
        self.speDir = str(fileName).split(self.speFile)[0]
        self.speFileEdit.setText(self.speFile)
        self.speDirEdit.setText(self.speDir)
        param['dataFile'] = str(self.speFile).split('.SPE')[0] + '.cxi'
        self.outFileEdit.setText(param['dataFile'])

    def loadSpeBG(self):

        fileName = QFileDialog.getOpenFileName(self,
            "Open SPE file", DATAPATH, "SPE (*.SPE);; SPE (*.spe)")

        self.speBGEdit.setText(str(fileName).split('/')[-1::1][0])

    def loadSpeDir(self):

        self.speDir = QFileDialog.getExistingDirectory(self, "Load Directory",
            DATAPATH,
            QFileDialog.ShowDirsOnly
            | QFileDialog.DontResolveSymlinks)

        self.speDirEdit.setText(self.speDir)

    def simpleInputWidget(self):

        widget = QtGui.QWidget()
        serverGrid = QtGui.QGridLayout(widget)
        serverGrid.setSpacing(5)
        serverGrid.setMargin(5)

        self.loadCXIButton = QtGui.QPushButton()
        self.loadCXIButton.setText('Load CXI')
        self.loadCXIButton.clicked.connect(self.loadCXI)
        self.loadCXIButton.setMinimumSize(100,24)
        self.loadCXIButton.setMaximumSize(100,24)

        self.loadScanButton = QtGui.QPushButton()
        self.loadScanButton.setText('Load Scan')
        self.loadScanButton.clicked.connect(self.loadScan)
        self.loadScanButton.setMinimumSize(100,24)
        self.loadScanButton.setMaximumSize(100,24)

        self.loadConfigButton = QtGui.QPushButton()
        self.loadConfigButton.setText('Load Config')
        self.loadConfigButton.clicked.connect(self.loadConfig)
        self.loadConfigButton.setMinimumSize(100,24)
        self.loadConfigButton.setMaximumSize(100,24)

        self.saveConfigButton = QtGui.QPushButton()
        self.saveConfigButton.setText('Save Config')
        self.saveConfigButton.clicked.connect(self.saveConfig)
        self.saveConfigButton.setMinimumSize(100,24)
        self.saveConfigButton.setMaximumSize(100,24)

        self.dateLabel = QtGui.QLabel()
        self.dateLabel.setText("Scan Date (YYMMDD)")
        self.dateEdit = QtGui.QLineEdit()
        self.dateEdit.setMinimumSize(100,24)
        self.dateEdit.setMaximumSize(100,24)
        self.dateEdit.setText(self.todaysDateStr)

        self.scanNumLabel = QtGui.QLabel()
        self.scanNumLabel.setText("Scan Number")
        self.scanNumEdit = QtGui.QLineEdit()
        self.scanNumEdit.setMinimumSize(100,24)
        self.scanNumEdit.setMaximumSize(100,24)
        self.scanNumEdit.setText('001')

        self.scanIDLabel = QtGui.QLabel()
        self.scanIDLabel.setText("Scan ID")
        self.scanIDEdit = QtGui.QLineEdit()
        self.scanIDEdit.setMinimumSize(100,24)
        self.scanIDEdit.setMaximumSize(100,24)
        self.scanIDEdit.setText('001')

        self.bgScanNumLabel = QtGui.QLabel()
        self.bgScanNumLabel.setText("BG Scan Number")
        self.bgScanNumEdit = QtGui.QLineEdit()
        self.bgScanNumEdit.setMinimumSize(100,24)
        self.bgScanNumEdit.setMaximumSize(100,24)
        self.bgScanNumEdit.setText('001')

        self.bgScanIDLabel = QtGui.QLabel()
        self.bgScanIDLabel.setText("BG Scan ID")
        self.bgScanIDEdit = QtGui.QLineEdit()
        self.bgScanIDEdit.setMinimumSize(100,24)
        self.bgScanIDEdit.setMaximumSize(100,24)
        self.bgScanIDEdit.setText('001')

        self.binLabel = QtGui.QLabel()
        self.binLabel.setText("Frames Per Point (bin)")
        self.binEdit = QtGui.QLineEdit()
        self.binEdit.setMinimumSize(100,24)
        self.binEdit.setMaximumSize(100,24)
        self.binEdit.setText('1')

        self.pixStepLabel = QtGui.QLabel()
        self.pixStepLabel.setText("Pixels Per Step")
        self.pixStepEdit = QtGui.QLineEdit()
        self.pixStepEdit.setMinimumSize(100,24)
        self.pixStepEdit.setMaximumSize(100,24)
        self.pixStepEdit.setText('20')
        self.connect(self.pixStepEdit, QtCore.SIGNAL('returnPressed()'), self.calcPixnm)

        self.pixnmLabel = QtGui.QLabel()
        self.pixnmLabel.setText("Pixel Size (nm)")
        self.pixnmEdit = QtGui.QLineEdit()
        self.pixnmEdit.setMinimumSize(100,24)
        self.pixnmEdit.setMaximumSize(100,24)
        self.pixnmEdit.setText('5.0')
        self.connect(self.pixnmEdit, QtCore.SIGNAL('returnPressed()'), self.calcPixPerStep)

        self.longExpLabel = QtGui.QLabel()
        self.longExpLabel.setText("Long Exposure Time (ms)")
        self.longExpEdit = QtGui.QLineEdit()
        self.longExpEdit.setMinimumSize(100,24)
        self.longExpEdit.setMaximumSize(100,24)
        self.longExpEdit.setText('100')

        self.shortExpLabel = QtGui.QLabel()
        self.shortExpLabel.setText("Short Exposure Time (ms)")
        self.shortExpEdit = QtGui.QLineEdit()
        self.shortExpEdit.setMinimumSize(100,24)
        self.shortExpEdit.setMaximumSize(100,24)
        self.shortExpEdit.setText('0')

        self.xStepSizeLabel = QtGui.QLabel()
        self.xStepSizeLabel.setText("X Step size (nm)")
        self.xStepSizeEdit = QtGui.QLineEdit()
        self.xStepSizeEdit.setMinimumSize(100,24)
        self.xStepSizeEdit.setMaximumSize(100,24)
        self.xStepSizeEdit.setText('100')
        self.connect(self.xStepSizeEdit, QtCore.SIGNAL('returnPressed()'), self.xStepSizeChanged)

        self.yStepSizeLabel = QtGui.QLabel()
        self.yStepSizeLabel.setText("Y Step size (nm)")
        self.yStepSizeEdit = QtGui.QLineEdit()
        self.yStepSizeEdit.setMinimumSize(100,24)
        self.yStepSizeEdit.setMaximumSize(100,24)
        self.yStepSizeEdit.setText('100')
        self.connect(self.yStepSizeEdit, QtCore.SIGNAL('returnPressed()'), self.yStepSizeChanged)

        self.xPtsLabel = QtGui.QLabel()
        self.xPtsLabel.setText("X Scan Points")
        self.xPtsEdit = QtGui.QLineEdit()
        self.xPtsEdit.setMinimumSize(100,24)
        self.xPtsEdit.setMaximumSize(100,24)
        self.xPtsEdit.setText('40')

        self.yPtsLabel = QtGui.QLabel()
        self.yPtsLabel.setText("Y Scan Points")
        self.yPtsEdit = QtGui.QLineEdit()
        self.yPtsEdit.setMinimumSize(100,24)
        self.yPtsEdit.setMaximumSize(100,24)
        self.yPtsEdit.setText('40')

        self.enLabel = QtGui.QLabel()
        self.enLabel.setText("Photon Energy (eV)")
        self.enEdit = QtGui.QLineEdit()
        self.enEdit.setMaximumSize(100,24)
        self.enEdit.setText('750')
        self.connect(self.enEdit, QtCore.SIGNAL('returnPressed()'), self.updateEnergy)

        #self.bsLabel = QtGui.QLabel()
        #self.bsLabel.setText("Beamstop Transmission")
        #self.bsEdit = QtGui.QLineEdit()
        #self.bsEdit.setMaximumSize(100,24)
        #self.bsEdit.setText('0.01')

        self.defocusLabel = QtGui.QLabel()
        self.defocusLabel.setText("De-focus (microns)")
        self.defocusEdit = QtGui.QLineEdit()
        self.defocusEdit.setMaximumSize(100,24)
        self.defocusEdit.setText('0.0')

        self.space = QtGui.QFrame()
        self.space.setMaximumSize(QtCore.QSize(100,125))
        self.space.setMinimumSize(QtCore.QSize(100,116))

        serverGrid.addWidget(self.dateLabel,0,1,1,2,QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.dateEdit,0,0,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.scanNumLabel,1,1,1,2,QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.scanNumEdit,1,0,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.scanIDLabel,2,1,1,2,QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.scanIDEdit,2,0,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.bgScanNumLabel,3,1,1,2,QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.bgScanNumEdit,3,0,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.bgScanIDLabel,4,1,1,2,QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.bgScanIDEdit,4,0,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.binLabel,5,1,1,2,QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.binEdit,5,0,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.pixStepLabel,6,1,1,2,QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.pixStepEdit,6,0,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.pixnmLabel,7,1,1,2,QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.pixnmEdit,7,0,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.longExpLabel,8,1,1,2,QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.longExpEdit,8,0,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.shortExpLabel,9,1,1,2,QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.shortExpEdit,9,0,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.xStepSizeLabel,10,1,1,2,QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.xStepSizeEdit,10,0,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.yStepSizeLabel,11,1,1,2,QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.yStepSizeEdit,11,0,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.xPtsLabel,12,1,1,2,QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.xPtsEdit,12,0,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.yPtsLabel,13,1,1,2,QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.yPtsEdit,13,0,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.enEdit,14,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.enLabel,14,1,1,1,QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        #serverGrid.addWidget(self.bsEdit,15,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        #serverGrid.addWidget(self.bsLabel,15,1,1,3,QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.defocusEdit,16,0,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.defocusLabel,16,1,1,3,QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.loadScanButton,17,0,1,1,QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.loadConfigButton,17,1,1,1,QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.saveConfigButton,18,0,1,2,QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.loadCXIButton,18,1,1,2,QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.space,19,0,20,1,QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        return widget

	
    def updateEnergy(self):

        self.calcPupil()
        #getBSTransmission(np.float(self.enEdit.text()), self.bsData)
        #self.bsEdit.setText(np.str(round(getBSTransmission(np.float(self.enEdit.text()), self.bsData),4)))
        self.updateBSTransmission()

    def updateBSTransmission(self):

        getBSTransmission(np.float(self.enEdit.text()), self.bsData)
        self.bsEdit.setText(np.str(round(getBSTransmission(np.float(self.enEdit.text()), self.bsData),4)))

    def loadCXI(self):

        fileName = QFileDialog.getOpenFileName(self,
            "Open CXI File", DATAPATH, "CXI (*.cxi)")

        print os.path.dirname(str(fileName)) + '/'
        self.dirEdit.setText(os.path.dirname(str(fileName)) + '/')

        self.loadParamFromFile(fileName)
        #self.setParam(param)
	if fileName != '':        
	    self.updateEnergy()
	    self.reconEdit.setTabEnabled(0,True)
	    self.reconEdit.setTabEnabled(1,True)
	    self.sendCommandButton.setEnabled(True)

    def saveConfig(self):

        initialPath = QDir.currentPath() + '/param.txt'

        fileName = QFileDialog.getSaveFileName(self, "Save As",
            initialPath,
            "%s Files (*.%s);;" % ('Text','txt'))

	if fileName != '':
	    print fileName
            self.execute(fileName)

    def calcPixnm(self):

        newPixnm = self.xStepSizeEdit.text().toFloat()[0] / self.pixStepEdit.text().toFloat()[0]
        #print "Setting Pixel Size to %.2f nm." %newPixnm
        self.pixnmEdit.setText(str(round(newPixnm,3)))

    def calcPixPerStep(self):

        newPixStep = self.xStepSizeEdit.text().toFloat()[0] / self.pixnmEdit.text().toFloat()[0]
        print "Setting Pixels Per Step to %.2f nm." %newPixStep
        self.pixStepEdit.setText(str(round(newPixStep,3)))

    def xStepSizeChanged(self):

        self.yStepSizeEdit.setText(self.xStepSizeEdit.text())
        print "Set step size to %s nm." %self.xStepSizeEdit.text()
        self.calcPixnm()

    def yStepSizeChanged(self):

        self.xStepSizeEdit.setText(self.yStepSizeEdit.text())
        print "Set step size to %s nm." %self.yStepSizeEdit.text()
        self.calcPixnm()

    def loadScan(self):

        fileName = QFileDialog.getOpenFileName(self,
            "Open STXM Scan", DATAPATH, "HDR (*.hdr)")

        if fileName != '':
            self.stxm = readXIM(fileName)

            self.dateEdit.setText(self.stxm.date)
            self.xPtsEdit.setText(str(self.stxm.nxPoints))
            self.yPtsEdit.setText(str(self.stxm.nyPoints))
            self.xStepSizeEdit.setText(str(self.stxm.dx * 1000))
            self.yStepSizeEdit.setText(str(self.stxm.dy * 1000))
            self.blEdit.setText(self.stxm.bl)
            self.longExpEdit.setText(str(self.stxm.dwell))
            self.scanNumEdit.setText(self.stxm.scanNumber)
            self.bgScanNumEdit.setText(self.stxm.scanNumber)
            self.scanIDEdit.setText('001')
            self.bgScanIDEdit.setText('002')
            self.speFileEdit.setText('')
            self.speBGEdit.setText('')
            self.speDirEdit.setText('')
            self.calcPixnm()
            self.calcPupil()

    def loadConfig(self):

        fileName = QFileDialog.getOpenFileName(self,
            "Open Configuration File", os.environ["NSRECONSERVERROOT"] + 'tmp', "TXT (*.txt)")

        self.loadParamFromFile(fileName)
        #self.setParam(param)
        if fileName != '':
	    self.updateEnergy()
	    self.reconEdit.setTabEnabled(0,True)
	    self.reconEdit.setTabEnabled(1,True)
	    self.sendCommandButton.setEnabled(True)

    def loadParamFromFile(self, fileName):

        if fileName != '':
	    #print '\nLoading parameters from ' + fileName + '\n'
            if fileName.split('.')[-1] == 'txt':
                try: f = open(fileName,'r')
                except IOError:
                    print "IOError: could not open parameter file: %s" %(fileName)
                    return 0
                else: pass
                for item in f:
                    pStringList = [item.split('=')[0],item.split('=')[1].rstrip('\n')]
                    if len(pStringList) != 2:
                        pass
                    else:
                        param[pStringList[0]] = pStringList[1]
            elif fileName.split('.')[-1] == 'cxi':
                thisParam = readCXI(str(fileName)).process
                for key in thisParam.keys():
                    param[key] = thisParam[key]

            self.dateEdit.setText(param["scanDate"])
            self.dirEdit.setText(param["dataPath"])
            self.preprocessButton.setChecked(int(param["process"]))
            self.reconButton.setChecked(int(param["reconstruct"]))
            self.scanNumEdit.setText(param["scanNumber"])
            self.scanIDEdit.setText(param["scanID"])
            #self.dateEdit.setText(param["bgScanDate"])
            self.bgScanNumEdit.setText(param["bgScanNumber"])
            self.bgScanIDEdit.setText(param["bgScanID"])
            self.binEdit.setText(param["bin"])
            self.pixnmEdit.setText(str(round(float(param["pixnm"]),3)))
            self.sampleEdit.setText(param["sh_sample_y"])
            self.sampleEdit.setText(param["sh_sample_x"])
            self.filterEdit.setText(param["filter_width"])
            self.xStepSizeEdit.setText(param["ssx"])
            self.yStepSizeEdit.setText(param["ssy"])
            self.xPtsEdit.setText(param["xpts"])
            self.yPtsEdit.setText(param["ypts"])
            self.zdEdit.setText(param["zd"])
            self.drEdit.setText(param["dr"])
            self.enEdit.setText(param["e"])
            self.ccdpEdit.setText(param["ccdp"])
            self.ccdzEdit.setText(param["ccdz"])
            self.ntEdit.setText(param["nexp"])
            self.ntEdit.setText(param["nl"])
            self.xcEdit.setText(param["xcenter"])
            self.ycEdit.setText(param["ycenter"])
            self.longExpEdit.setText(param["t_long"])
            self.shortExpEdit.setText(param["t_short"])
            self.stEdit.setText(param["s_threshold"])
            self.blEdit.setText(param["bl"])
            self.ptEdit.setText(param["probeThreshold"])
            self.filterEdit.setText(param["filter_width"])
            self.gpuButton.setChecked(int(param["gpu"]))
            self.itEdit.setText(param["nIterations"])
            self.pmButton.setChecked(int(param["probeMask"]))
            self.prButton.setChecked(int(param["pR"]))
            self.brButton.setChecked(int(param["bR"]))
            self.vInvertButton.setChecked(int(param["fv"]))
            self.hInvertButton.setChecked(int(param["fh"]))
            self.transposeButton.setChecked(int(param["transpose"]))
            self.useStxmButton.setChecked(int(param["useStxm"]))
            self.usePreviousButton.setChecked(int(param["usePrevious"]))
            self.outliersButton.setChecked(int(param["removeOutliers"]))
            self.xshiftEdit.setText(param['beamstopXshift'])
            self.yshiftEdit.setText(param['beamstopYshift'])
            
            try: self.fCCDButton.setChecked(int(param["fCCD"]))
            except KeyError: self.fCCDButton.setChecked(0)
            self.pixStepEdit.setText(param['ssn'])
            self.defocusEdit.setText(param['defocus'])
            if param['version'] == '1':
                self.speDirEdit.setText(param['dataPath'])
                self.speFileEdit.setText(param['speFile'])
                self.speBGEdit.setText(param['bg_filename'])
            else:
                self.speDirEdit.setText('')
                self.speFileEdit.setText('')
                self.speBGEdit.setText('')
            #print "Loaded parameters."
            self.calcPupil()

            return 1

    def outputWidget(self):

        widget = QtGui.QWidget()
        Grid = QtGui.QGridLayout(widget)
        Grid.setSpacing(0)
        Grid.setMargin(2)
        
        self.stdCommandLabel = QtGui.QLabel()
        self.stdCommandLabel.setText('Current command string:')
        self.stdCommandEdit = QtGui.QTextEdit()
        self.stdCommandEdit.setReadOnly(True)
        self.stdCommandEdit.setMinimumSize(550,20)
        self.stdCommandEdit.setLineWrapMode(0)
        
        self.sendCommandButton = QtGui.QPushButton()
        self.sendCommandButton.setText('Send Command')
        self.sendCommandButton.clicked.connect(self.sendCommandButtonClicked)
        self.sendCommandButton.setMinimumSize(110,24)
        self.sendCommandButton.setMaximumSize(110,24)
        self.sendCommandButton.setEnabled(False)

        self.stdLabel = QtGui.QLabel()
        self.stdLabel.setText('Console')
        self.stdOutEdit = QtGui.QTextEdit()
        self.stdOutEdit.setReadOnly(True)
        self.stdOutEdit.setMinimumSize(550,180)
        self.stdOutEdit.setLineWrapMode(0)
        
        self.stdLabel2 = QtGui.QLabel()
        self.stdLabel2.setText('Processes')
        self.procsList = QtGui.QListWidget()
        self.procsList.setMinimumSize(550,180)
        
        self.showResultsButton = QtGui.QPushButton()
        self.showResultsButton.setText('Show current results')
        self.showResultsButton.clicked.connect(self.showResultsButtonClicked)
        self.showResultsButton.setMinimumSize(150,24)
        self.showResultsButton.setMaximumSize(150,24)
        
        Grid.addWidget(self.stdCommandLabel,0,0,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.stdCommandEdit,1,0, QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.sendCommandButton,2,0,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)      
        Grid.addWidget(self.stdLabel,3,0,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.stdOutEdit,4,0, QtCore.Qt.AlignTop | QtCore.Qt.AlignRight)
        Grid.addWidget(self.stdLabel2,6,0,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.procsList,7,0, QtCore.Qt.AlignTop | QtCore.Qt.AlignRight)
        Grid.addWidget(self.showResultsButton,8,0,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)

        return widget

class nsServers(QtGui.QFrame):

    def __init__(self, parent = None):

        super(nsServers, self).__init__()

        self.__bcsStatusColor = 'red'
        self.__bcsStatusText = 'Not Connected'
        self.__bcsCommand = 'ListCommands'
        self.__xpsStatusColor = 'red'
        self.__xpsStatusText = 'Not Connected'
        self.__xpsCommand = ''
        self.__ccdStatusColor = 'red'
        self.__ccdStatusText = 'Not Connected'
        self.__ccdCommand = ''
        self.outputEdit = self.outputWidget()
        self.serversWidget = self.nsServersWidget()
        self.__servicesThread = Services()

        w = QtGui.QWidget()
        wg = QtGui.QGridLayout(w)
        wg.addWidget(self.serversWidget,0,0,1,1, QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        wg.addWidget(self.outputEdit, 0,1,1,1, QtCore.Qt.AlignTop | QtCore.Qt.AlignRight)
        self.setLayout(wg)

        #####################################################################################
        #####################################################################################
        self.connect(self.__servicesThread, QtCore.SIGNAL("StatusColorUpdated"), self.__updateStatus)
        self.ns = nsControl()
        if self.ns.bcsStatus:
            self.__bcsStatusColor = 'green'
            self.__bcsStatusText = 'Connected'
        else:
            self.__bcsStatusColor = 'red'
            self.__bcsStatusText = 'Not Connected'
        sys.stdout = OutLog(self.stdOutEdit, sys.stdout)
        #self.__services = threading.Thread(name='servicesDaemon', target=self.__servicesDaemon)
        #self.__services.setDaemon(True)
        #self.__services.start()

    def __updateStatus(self):

        self.ns.getStatus(test = True)
        #print self.ns.bcsStatus
        if self.ns.bcsStatus:
            self.__bcsStatusColor = 'green'
            self.__bcsStatusText = 'Connected'
            self.bcsConnectButton.setText('Disconnect')
        else:
            self.__bcsStatusColor = 'red'
            self.__bcsStatusText = 'Not Connected'
            self.bcsConnectButton.setText('Connect')
        self.bcsStatusLabel.setText(self.__bcsStatusText)
        bg = "QtGui.QLabel {background-color:"+self.__bcsStatusColor+"}"
        self.bcsStatusLabel.setStyleSheet( bg )

    def nsServersWidget(self):

        widget = QtGui.QWidget()
        serverGrid = QtGui.QGridLayout(widget)
        serverGrid.setSpacing(5)
        serverGrid.setMargin(5)

        self.bcsLabel = QtGui.QLabel()
        self.bcsLabel.setText("Beamline Controls Server")

        self.bcsEdit = QtGui.QLineEdit()
        self.bcsEdit.setMinimumSize(256,24)
        self.bcsEdit.setText('BL901BCS.als.lbl.gov')

        self.bcsConnectButton = QtGui.QPushButton()
        self.bcsConnectButton.setText('Connect')
        self.bcsConnectButton.clicked.connect(self.connect2BCS)
        self.bcsConnectButton.setMinimumSize(128,24)
        self.bcsConnectButton.setMaximumSize(128,24)

        self.bcsCommandLabel = QtGui.QLabel()
        self.bcsCommandLabel.setText("Command")

        self.bcsCommandEdit = QtGui.QLineEdit()
        self.bcsCommandEdit.setMinimumSize(256,24)
        self.bcsCommandEdit.setText(self.__bcsCommand)

        self.bcsCommandButton = QtGui.QPushButton()
        self.bcsCommandButton.setText('Send')
        self.bcsCommandButton.clicked.connect(self.sendBCSCommand)
        self.bcsCommandButton.setMinimumSize(128,24)
        self.bcsCommandButton.setMaximumSize(128,24)

        self.bcsStatusLabel = QtGui.QLabel()
        self.bcsStatusLabel.setFrameStyle(2)
        self.bcsStatusLabel.setText(self.__bcsStatusText)
        bg = "QtGui.QLabel {background-color:"+self.__bcsStatusColor+"}"
        self.bcsStatusLabel.setStyleSheet( bg )

        self.hLine = QtGui.QFrame()
        self.hLine.setFrameShape(QtGui.QFrame.HLine)
        self.hLine.setFrameShadow(QtGui.QFrame.Sunken)
        self.hLine.setMinimumSize(128,30)

        self.xpsLabel = QtGui.QLabel()
        self.xpsLabel.setText("XPS Controller")

        self.xpsEdit = QtGui.QLineEdit()
        self.xpsEdit.setMinimumSize(256,24)
        self.xpsEdit.setText('192.168.0.254')

        self.xpsConnectButton = QtGui.QPushButton()
        self.xpsConnectButton.setText('Connect')
        self.xpsConnectButton.clicked.connect(self.connect2XPS)
        self.xpsConnectButton.setMinimumSize(128,24)
        self.xpsConnectButton.setMaximumSize(128,24)

        self.xpsCommandLabel = QtGui.QLabel()
        self.xpsCommandLabel.setText("Command")

        self.xpsCommandEdit = QtGui.QLineEdit()
        self.xpsCommandEdit.setMinimumSize(256,24)
        self.xpsCommandEdit.setText(self.__xpsCommand)

        self.xpsCommandButton = QtGui.QPushButton()
        self.xpsCommandButton.setText('Send')
        self.xpsCommandButton.clicked.connect(self.sendXPSCommand)
        self.xpsCommandButton.setMinimumSize(128,24)
        self.xpsCommandButton.setMaximumSize(128,24)

        self.xpsStatusLabel = QtGui.QLabel()
        self.xpsStatusLabel.setFrameStyle(2)
        self.xpsStatusLabel.setText(self.__xpsStatusText)
        bg = "QtGui.QLabel {background-color:"+self.__xpsStatusColor+"}"
        self.xpsStatusLabel.setStyleSheet( bg )

        self.hLine2 = QtGui.QFrame()
        self.hLine2.setFrameShape(QtGui.QFrame.HLine)
        self.hLine2.setFrameShadow(QtGui.QFrame.Sunken)
        self.hLine2.setMinimumSize(128,30)

        self.ccdLabel = QtGui.QLabel()
        self.ccdLabel.setText("Camera Interface Node")

        self.ccdEdit = QtGui.QLineEdit()
        self.ccdEdit.setMinimumSize(256,24)
        self.ccdEdit.setText('192.168.1.1')

        self.ccdConnectButton = QtGui.QPushButton()
        self.ccdConnectButton.setText('Connect')
        self.ccdConnectButton.clicked.connect(self.connect2CCD)
        self.ccdConnectButton.setMinimumSize(128,24)
        self.ccdConnectButton.setMaximumSize(128,24)

        self.ccdCommandLabel = QtGui.QLabel()
        self.ccdCommandLabel.setText("Command")

        self.ccdCommandEdit = QtGui.QLineEdit()
        self.ccdCommandEdit.setMinimumSize(256,24)
        self.ccdCommandEdit.setText(self.__ccdCommand)

        self.ccdCommandButton = QtGui.QPushButton()
        self.ccdCommandButton.setText('Send')
        self.ccdCommandButton.clicked.connect(self.sendCCDCommand)
        self.ccdCommandButton.setMinimumSize(128,24)
        self.ccdCommandButton.setMaximumSize(128,24)

        self.ccdStatusLabel = QtGui.QLabel()
        self.ccdStatusLabel.setFrameStyle(2)
        self.ccdStatusLabel.setText(self.__ccdStatusText)
        bg = "QtGui.QLabel {background-color:"+self.__ccdStatusColor+"}"
        self.ccdStatusLabel.setStyleSheet( bg )

        serverGrid.addWidget(self.bcsLabel,0,0,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.bcsEdit,1,0,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.bcsConnectButton,1,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignRight)
        serverGrid.addWidget(self.bcsStatusLabel,0,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignHCenter)
        serverGrid.addWidget(self.bcsCommandLabel,2,0,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.bcsCommandEdit,3,0,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.bcsCommandButton,3,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignRight)
        serverGrid.addWidget(self.hLine, 4,0,1,2,QtCore.Qt.AlignVCenter|QtCore.Qt.AlignHCenter)
        serverGrid.addWidget(self.xpsLabel,5,0,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.xpsEdit,6,0,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.xpsConnectButton,6,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignRight)
        serverGrid.addWidget(self.xpsStatusLabel,5,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignHCenter)
        serverGrid.addWidget(self.xpsCommandLabel,7,0,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.xpsCommandEdit,8,0,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.xpsCommandButton,8,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignRight)
        serverGrid.addWidget(self.hLine2, 9,0,1,2,QtCore.Qt.AlignVCenter|QtCore.Qt.AlignHCenter)
        serverGrid.addWidget(self.ccdLabel,10,0,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.ccdEdit,11,0,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.ccdConnectButton,11,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignRight)
        serverGrid.addWidget(self.ccdStatusLabel,10,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignHCenter)
        serverGrid.addWidget(self.ccdCommandLabel,12,0,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.ccdCommandEdit,13,0,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        serverGrid.addWidget(self.ccdCommandButton,13,1,QtCore.Qt.AlignTop | QtCore.Qt.AlignRight)
        return widget

    def sendBCSCommand(self):

        print "Send command to BCS: %s" %self.bcsCommandEdit.text() + '\r\n'
        print self.ns.sendCommand(str(self.bcsCommandEdit.text()) + '\r\n')

    def sendXPSCommand(self):

        print "Send command to XPS: %s" %self.xpsCommandEdit.text() + '\r\n'

    def sendCCDCommand(self):

        print "Send command to CCD: %s" %self.xpsCommandEdit.text() + '\r\n'

    def connect2CCD(self):

        print "No camera yet, sorry :("

    def connect2BCS(self):

        if not(self.ns.bcsStatus):
            self.ns.openBCS()
            if self.ns.bcsStatus:
                self.bcsConnectButton.setText('Disconnect')
        else:
            self.ns.closeBCS()
            if not(self.ns.bcsStatus):
                self.bcsConnectButton.setText('Connect')
        self.__updateStatus()

    def connect2XPS(self):

        if not(self.ns.xpsStatus):
            self.ns.openXPS()
            if self.ns.xpsStatus:
                self.xpsConnectButton.setText('Disconnect')
        else:
            self.ns.closeXPS()
            if not(self.ns.xpsStatus):
                self.xpsConnectButton.setText('Connect')
        self.__updateStatus()

    def outputWidget(self):

        widget = QtGui.QWidget()
        Grid = QtGui.QGridLayout(widget)
        Grid.setSpacing(0)
        Grid.setMargin(2)

        self.stdLabel = QtGui.QLabel()
        self.stdLabel.setText('Console')
        self.stdOutEdit = QtGui.QTextEdit()
        self.stdOutEdit.setReadOnly(True)
        self.stdOutEdit.setMinimumSize(600,400)

        Grid.addWidget(self.stdLabel,0,0,QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        Grid.addWidget(self.stdOutEdit,1,0, QtCore.Qt.AlignTop | QtCore.Qt.AlignRight)

        return widget


class NanoSurveyorGUI(QtGui.QWidget):

    def __init__(self, args):

        QtGui.QWidget.__init__(self)
        #self.setGeometry(0,0,600,400)
        self.setWindowTitle("NanoSurveyor 2.0")
        try: self.dbcon = sqlCon('localhost', 'nsReconServer','nsReconServer', 'nsReconServer')
        except: self.dbcon = None

        #tab_widget = QTabWidget()
        self.tab_widget = DetachableTabWidget()

        tab1 = qtDiffInspectorMain()
        tab2 = qtPtychoViewerMain()
        self.tab3 = ptychoServerViewer(self.dbcon,tab_tmp = tab2)
        tab4 = StreamViewer(args)
        tab5 = SimulationViewer()

        self.tab_widget.addTab(tab1, "Image Viewer")
        self.tab_widget.addTab(tab2, "Ptychography Viewer")
        self.tab_widget.addTab(self.tab3, "Reconstruction")
        self.tab_widget.addTab(tab4, "Streaming Visualization")
        self.tab_widget.addTab(tab5, "Simulation")

        self.tab_widget.currentChanged.connect(self.reconChosen)
        self.tab_widget.setCurrentWidget(tab4)

        # layout manager
        vbox = QtGui.QVBoxLayout()
        vbox.addWidget(self.tab_widget)
        self.setLayout(vbox)

    def reconChosen(self,numTab):
        if self.tab_widget.tabText(numTab) == 'Reconstruction':
            if not self.tab3.reconEdit.isTabEnabled(0):
                QtGui.QMessageBox.information(self, 'Message', "Load configuration file, CXI file or Scan to start!")



#app = QApplication([])
#frame = NanoSurveyorGUI()
#frame.show()
#app.exec_()


from PyQt4 import QtCore, QtGui, uic
import pyqtgraph as pg
import numpy as np
import os
from .. import utils
from aux.simexpwfccd4 import Simulator

currdir = os.path.dirname(os.path.realpath(__file__))
Ui_mainwindow, base = uic.loadUiType(currdir + '/ui/simulationviewer.ui')
Ui_visual,     base = uic.loadUiType(currdir + '/ui/visWidget.ui')


class VisualWidget(QtGui.QWidget, Ui_visual):
    def __init__(self, title):
        QtGui.QWidget.__init__(self)
        self.setupUi(self)

        #Header
        self.title.setText(title)
        self.maximize.setIcon(QtGui.QIcon(currdir + '/icons/maximize.svg'))
        self.maximize.setIconSize(QtCore.QSize(16,16))

        # Image
        self.image = np.zeros((256,256))
        
        # Canvas
        self.canvas.ci.layout.setContentsMargins(0, 0, 0, 0)
        self.canvas.ci.layout.setSpacing(0)
        self.view = self.canvas.addViewBox(row=0, col=0, lockAspect=False, enableMouse=False)
        self.imageitem = pg.ImageItem(self.image, autoDownsample=False)   
        self.view.addItem(self.imageitem)
        self.imageitem2 = pg.ImageItem()
        self.view.addItem(self.imageitem2)

        # Colormap
        self.setColorMap('jet')

        # Logarithmic axis
        #self.setLogAxis(False)
        
        # Levels
        #self.autolevel = True
        #self.vmin = None
        #self.vmax = None

    @QtCore.pyqtSlot()
    def setColorMap(self, name):
        self.cmap = getattr(utils, name)()

    def setImage(self):
        self.image = self.data
        
    def setData(self, data):
        self.data = np.rot90(data,3)
        self.setImage()
        
    def replot(self):
        self.imageitem.setImage(self.image, lut=self.cmap)
        #self.view.autoRange()
        
        
class SimulationViewer(QtGui.QMainWindow, Ui_mainwindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        self.setupUi(self)
        
        # Visualization Panels
        self.visFrames = [self.sampleFrame, self.filteredFrame, self.illuminatedFrame, self.rawFrame, self.cleanFrame, self.frame_6]
        self.visPanels = [self.sampleWidget, self.filteredWidget, self.illuminatedWidget, self.rawWidget, self.cleanWidget, self.widget_6]
        self.visTitles = ['Sample', 'Filtered', 'Illuminated', 'Raw', 'Clean', 'Frame 6']
        self.visColorMaps  = ['jet', 'jet', 'jet', 'jet', 'jet', 'jet']
        for i in range(6):
            self.visFrames[i].layout().removeWidget(self.visPanels[i])
            self.visPanels[i].setParent(None)
            self.visPanels[i] = VisualWidget(self.visTitles[i])
            self.visPanels[i].setColorMap(self.visColorMaps[i])
            self.visFrames[i].layout().addWidget(self.visPanels[i])
            self.visFrames[i].setAutoFillBackground(True)
            self.visPanels[i].maximize.clicked.connect(self.toggleMaximization)
            self.visPanels[i].invertY()
            self.maximized = False
        self.samplePanel, self.framePanel, self.illuminatedPanel, self.rawPanel, self.cleanPanel, self.frame6Panel = self.visPanels
        
        # Initializations
        self.init_connections()
        self.threadPool = []
        #self.init_timer()
        
        self.roi = pg.CircleROI([0, 0], [1, 1], pen=(10,1), movable=False)
        self.visPanels[0].view.addItem(self.roi)
        
    def init_connections(self):
        self.simulateButton.clicked.connect(self.simulate)
        self.stopButton.clicked.connect(self.stop)
    #    self.connectionPanel.disconnectAll.clicked.connect(self.disconnectAll)
    #    self.commandsPanel.start.clicked.connect(self.startNewRun)
    #    self.commandsPanel.stop.clicked.connect(self.stopRun)
    #    self.commandsPanel.save.clicked.connect(self.saveRun)
    #    self.controlPanel.parameters.clicked.connect(self.controlParameters)


    def plot(self, images):
        #Plot images
        i = 0
        for image in images:
            if (i<6):
                self.visPanels[i].setData(image)
                self.visPanels[i].replot()
            i=i+1
        
        self.roi.setPos([images[6],images[7]])
        self.roi.setSize([100, 200])
        
    def updateCurrentFrame(self, num):
    #Update info panel
        self.frameNumber.setText(str(num))
        
    def updateTotalFrames(self, num):
    #Update info panel
        self.totalFramesNumber.setText(str(num))

    @QtCore.pyqtSlot()
    def toggleMaximization(self):
        index = self.visPanels.index(self.sender().parent())
        if self.maximized:
            self.centralwidget.layout().setRowStretch(0,1)
            self.centralwidget.layout().setRowStretch(1,15)
            self.centralwidget.layout().setColumnStretch(0,3)
            self.centralwidget.layout().setColumnStretch(1,1)
        else:
            self.centralwidget.layout().setRowStretch(0,1)
            self.centralwidget.layout().setColumnStretch(0,3)
        for i in range(6):
            if i != index:
                self.visFrames[i].setVisible(self.maximized)
        self.maximized = not self.maximized

    @QtCore.pyqtSlot()
    def simulate(self):
        print 'Started Simulation!'
        
        self.threadPool.append( Simulator([int(self.scanSide.text()), float(self.pixelSize.text()), float(self.distance.text()), int(self.gain.text()), int(self.npixel.text()), int(self.npixelx.text()), int(self.npixely.text()), float(self.transmission.text()), float(self.radius.text()) ]) )
        self.connect( self.threadPool[0], QtCore.SIGNAL("updateGUI"), self.plot )
        self.connect( self.threadPool[0], QtCore.SIGNAL("updateCurrentFrame"), self.updateCurrentFrame )
        self.connect( self.threadPool[0], QtCore.SIGNAL("updateTotalFrames"), self.updateTotalFrames )
        self.threadPool[0].start()

    @QtCore.pyqtSlot()
    def stop(self):
        print 'Stoped Simulation!'
        
        self.threadPool[0].stop()

    def resizeEvent(self, event):
        QtGui.QMainWindow.resizeEvent(self, event)
        QtGui.QApplication.processEvents()


def main():
#    args = parse_cmdl_args()
    app = QtGui.QApplication([])
    frame = SimulationViewer()
    frame.resize(1300,830)
    frame.show()
    app.exec_()


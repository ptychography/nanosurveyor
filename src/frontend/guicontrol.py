import logging
import time
import zmq
import numpy as np
import sys, os
import socket
import argparse
import datetime
import imp
from PyQt4 import QtCore, QtGui
import json

from .. import utils
from ..comm.zmqsocket import ZmqSocket
from ..comm.qtsocket import QtSocket
from ..camera.framegrabber import Framegrabber
from ..camera.simulator import FccdSimulator
from ..camera.cincontrol import CINController
from nano_options import Login

class Guicontrol(QtCore.QObject):
    """
    Doc
    """
    statusMessage  = QtCore.pyqtSignal(str)
    transferinfo   = QtCore.pyqtSignal(float, float, float)
    frameinfo  = QtCore.pyqtSignal(str,str,str)
    framedata  = QtCore.pyqtSignal(np.ndarray)
    reconsinfo = QtCore.pyqtSignal(int,float,float)
    reconsdata = QtCore.pyqtSignal(np.ndarray)
    illumdata  = QtCore.pyqtSignal(np.ndarray)
    stxmdata   = QtCore.pyqtSignal(np.ndarray)
    simulationStopped = QtCore.pyqtSignal(bool)
    moduleStatus = QtCore.pyqtSignal(bool)
    backgroundStatus = QtCore.pyqtSignal(bool)
    illuminationStatus = QtCore.pyqtSignal(bool)
    ptychoStatus = QtCore.pyqtSignal(bool, bool)
    
    def __init__(self, params):    
        super(Guicontrol, self).__init__()   
        self.params = params
        self.threads = []
        self.status = None
        self.bytes_uploaded = 0
        self.bytes_downloaded = 0
        self.timestamp = time.time()
        self.backendConnected = False

    @QtCore.pyqtSlot(str)
    def appendStatus(self, status):
        self.statusMessage.emit(status)

    @QtCore.pyqtSlot(int)
    def append_upload(self, bytesize):
        self.bytes_uploaded += bytesize

    @QtCore.pyqtSlot(int)
    def append_download(self, bytesize):
        self.bytes_downloaded += bytesize

    def checkTransfer(self):
        self.timestamp = time.time()
        self.transferinfo.emit(self.timestamp, self.bytes_downloaded/1e6, self.bytes_uploaded/1e6)
        self.bytes_downloaded = 0
        self.bytes_uploaded = 0
            
    @QtCore.pyqtSlot()
    def startSimulation(self):
        self.startNewRun()
        self.appendStatus("<font color=\"green\">Starting simulation...</font>")
        self.simulation = FccdSimulator(self.params['simulation']['rawfile'],  self.params['simulation']['cam_address'],
                                        self.params['simulation']['cam_port'], self.params['simulation']['cam_delay'],
                                        self.params['simulation']['add_random_dark'])
        self.simulation.statusMessage.connect(self.appendStatus)
        self.simulation.simulationDone.connect(self.stopSimulation)
        self.simulation.createSendPacketsSocket()
        self.threads.append(self.simulation)
        self.simulation.start()

    @QtCore.pyqtSlot()
    def stopSimulation(self):
        self.appendStatus("<font color=\"red\">Stopping simulation...</font>")
        self.simulation.stop()
        self.simulationStopped.emit(True)

    @QtCore.pyqtSlot()
    def startFramegrabber(self):
        self.appendStatus("<font color=\"green\">Starting Framegrabber...</font>")
        self.framegrabber = Framegrabber(self.params['grabber']['inport'], self.params['grabber']['outport'])
        self.framegrabber.statusMessage.connect(self.appendStatus)
        self.framegrabber.sizeUploaded.connect(self.append_upload)
        self.framegrabber.createReadFrameSocket()
        self.framegrabber.createSendFrameSocket()
        self.threads.append(self.framegrabber)
        self.framegrabber.start()
        
    @QtCore.pyqtSlot()
    def stopFramegrabber(self):
        self.appendStatus("<font color=\"red\">Stopping Framegrabber...</font>")
        self.framegrabber.stop()

    @QtCore.pyqtSlot()
    def startCINController(self):
        self.appendStatus("<font color=\"green\">Starting CINController...</font>")
        self.cincontroller = CINController(self.params['cincontrol'])
        self.cincontroller.statusMessage.connect(self.appendStatus)
        self.cincontroller.newParam_int.connect(self.updateParam_int)
        self.cincontroller.newParam_float.connect(self.updateParam_float)
        self.cincontroller.newParam_str.connect(self.updateParam_str)
        self.cincontroller.startNewRun.connect(self.startNewRun)
        self.cincontroller.createSendCommandsSocket()
        self.cincontroller.createReceiveCommandsSocket_qtcpserver()
        #self.threads.append(self.cincontroller)
        #self.cincontroller.start()

    @QtCore.pyqtSlot(str, str, int)
    def updateParam_int(self, group, key, value):
        print "updating param int:", key, value
        self.params[str(group)][str(key)] = value

    @QtCore.pyqtSlot(str, str, float)
    def updateParam_float(self, group, key, value):
        print "updating param float:", key, value
        self.params[str(group)][str(key)] = value

    @QtCore.pyqtSlot(str, str, str)
    def updateParam_str(self, group, key, value):
        print "updating param str:", group, key, value
        self.params[str(group)][str(key)] = value        
        
    @QtCore.pyqtSlot()
    def stxmStartNewRun(self):
        self.params 
        
    @QtCore.pyqtSlot()
    def stopCINController(self):
        self.appendStatus("<font color=\"red\">Stopping CINController...</font>")
        self.cincontroller.stop()

    @QtCore.pyqtSlot()
    def connectBackend(self):
        self.appendStatus("<font color=\"red\">Connecting to backend...</font>")

        # Tunnel to backend
        if self.params['backend']['tunnel']:
            login = Login()
            if login.exec_() != QtGui.QDialog.Accepted:
                self.status.append("<font color=\"red\">Connection attempt to phasis cancelled...</font>")
                return False
            username = str(login.textName.text())
            password = str(login.textPass.text())
            server = str(login.machineName.text())
            ssh_tunnel = "%s@%s,%s" %(username, server, password)
        else:
            ssh_tunnel = False

        # Control the backend
        self.backend = QtSocket(zmq.REQ)
        self.backend.ready_read.connect(self.onCtrlRecv)
        self.backend.connect_socket("tcp://%s:%d" %(self.params['backend']['address'], self.params['backend']['ctrlport']), tunnel=ssh_tunnel)
        #self.backend.bind("tcp://*:%d" %(self.params['backend_ctrlport']))
        self.appendStatus("<font color=\"orange\">Controlling the backend (%s) on port %d...</font>"
                          %(self.params['backend']['address'], self.params['backend']['ctrlport']))
                          
        # Receive data from the backend
        self.readydata = QtSocket(zmq.PULL, self)
        self.readydata.ready_read.connect(self.onDataRecv)
        self.readydata.bind("tcp://*:%d" %(self.params['backend']['viewport']))
        self.appendStatus("<font color=\"orange\">Waiting for data from %s on port %d...</font>"
                          %(self.params['backend']['address'], self.params['backend']['viewport']))

        # Disable/Enable connect/disconnect button
        self.appendStatus("connect")

    @QtCore.pyqtSlot()
    def disconnectBackend(self):
        self.appendStatus("<font color=\"green\">Stopping the backend...</font>")
        self.readydata.ready_read.disconnect()
        self.readydata.disconnect()
        self.readydata.close()
        
        # Enable/Disable connect/disconnect button
        self.appendStatus("disconnect")

    @QtCore.pyqtSlot()
    def connectAll(self):
        self.connectBackend()
        self.startFramegrabber()
        self.startCINController()
        self.backendConnected = True
        self.statusReceived = True
        
    @QtCore.pyqtSlot()
    def disconnectAll(self):
        self.stopFramegrabber()
        self.stopCINController()
        self.disconnectBackend()
        self.backendConnected = False
        
    @QtCore.pyqtSlot()
    def startNewRun(self):
        self.appendStatus("<font color=\"green\">Starting New Run...</font>")
        self.sendCommand('newRun', self.params)
        
    @QtCore.pyqtSlot()
    def interrupt(self):
        self.sendCommand('interrupt', self.params)
        
    @QtCore.pyqtSlot()
    def onCtrlRecv(self):
        cmd, status = self.backend.recv_cmd()
        self.status = status
        self.statusReceived = True
        self.updateStatus()
        
    def updateStatus(self):
        self.modulesConfigured = self.status['backgroundConfigured'] and \
                                 self.status['cxiwriteConfigured'] and \
                                 self.status['ptychoConfigured'] and \
                                 self.status['dataConfigured']
        self.moduleStatus.emit(self.modulesConfigured)
        self.backgroundStatus.emit(self.status['backgroundFinished'])
        self.illuminationStatus.emit(self.status['dataFinished'])
        self.ptychoStatus.emit(self.status['ptychoInitialized'], self.status['ptychoFinished'])
        
            
    @QtCore.pyqtSlot()
    def onDataRecv(self):
        key = self.readydata.recv()
        if key == 'cleanframe':
            fid, frame, posx, posy = self.readydata.recv_frame_and_pos()
            self.frameinfo.emit(fid, posx, posy)
            self.framedata.emit(frame)
            self.append_download(frame.nbytes + len(fid) + len(posx) + len(posy))
        elif key == 'reconstruction':
            img, illumination, iteration, moduleError, overlapError = self.readydata.recv_recons()
            self.reconsinfo.emit(int(iteration), float(moduleError), float(overlapError))
            self.reconsdata.emit(img)
            self.illumdata.emit(illumination)
            self.append_download(img.nbytes + len(iteration) + len(moduleError) + len(overlapError))
        elif key == 'stxm':
            image = self.readydata.recv_array()
            self.stxmdata.emit(image)
            self.append_download(image.nbytes)
        else:
            print key
    
    def sendCommand(self, cmd, params=None):
        """Check if connections is alive and send command to backend.
        Otherwise raise Error.
        """
        if self.statusReceived:
            self.backend.send_cmd(cmd, params)
            self.statusReceived = False
            time.sleep(1) # Wait 1 second to give backend time to respond
        else:
            self.appendStatus("<font color=\"green\">Backend is not responding...</font>")
            self.disconnectAll()

def parse_cmdl_args():
    """Parsing command line arguments"""
    parser = argparse.ArgumentParser(description='nsguicontrol')
    parser.add_argument("-c", "--config", type=str, default=None, help="Configuration file")
    if(len(sys.argv) == 1):
        parser.print_help()
    return parser.parse_args()

def main():
    """Doc"""
    args = parse_cmdl_args()
    guicontrol = Guicontrol(args)
    guicontrol.connectAll()


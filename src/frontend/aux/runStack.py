__author__ = 'DAShapiro'
__author__ = 'nsptycho'

from aux.executeParam import readParamFile
from aux.energyStack import energyStack
from aux.readXIM import *

#######################################################################################################################
#######################################################################################################################
##setup of main path and load a parameter file
dataPath = '/global/groups/cosmic/Data/141017/141017001/' #stack base directory
scanFileName = 'NS_141017001.hdr'
paramFileName = 'param.txt'

######################################################################################################################
######################################################################################################################
##Reconstruction
nIterations = '300' #Number of iterations
preprocess = True #set True for pre-processing and GPU reconstruction
cpu = True #set True for final CPU reconstruction (requires preprocessing done previously)
saveOutput = True #set True to save TIFF files of the reconstructions

#######################################################################################################################
#######################################################################################################################
##Some labels
year = '2014'
month = '10'
day = '17'
scientist = 'Young-Sang, Maryam, and David'
sample = 'LFP micro-plate'
comment1 = 'Fe energy Scan'
comment2 = "Destined for greatness"

#######################################################################################################################
#######################################################################################################################
#Parts below are the functional part of the script
#DO NOT EDIT
#
#######################################################################################################################
#######################################################################################################################
param = readParamFile(dataPath + paramFileName)
stxmHDR = readXIM(dataPath + scanFileName)

if param:
    param["dataPath"] = dataPath ##set the path in the parameter list just in case...
    param["nIterations"] = nIterations
    param['Year'] = year
    param['Month'] = month
    param['Day'] = day
    param['Scientist'] = scientist
    param['Sample'] = sample
    param['Comment1'] = comment1
    param['Comment2'] = comment2
    param['useCluster'] = '1'
    param['nGPU'] = '8'  ##let's be conservative to try to avoid crashes
    param['nCPU'] = '50' ##49 processors + 1 for rank 0
    energyStack(param, stxmHDR, preprocess = preprocess, cpu = cpu, saveOutput = saveOutput)
else:
    print "Failed to open parameter file.  Check paths."
    exit()

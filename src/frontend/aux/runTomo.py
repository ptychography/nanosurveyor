__author__ = 'nsptycho'

from aux.executeParam import readParamFile
from aux.tomoScan import tomoscan

#######################################################################################################################
#######################################################################################################################
##setup of main path and load a parameter file
dataPath = '/global/groups/cosmic/Data/150131/tomoscan4/' #Tomo base directory
paramFileName = 'param.txt'

######################################################################################################################
######################################################################################################################
##Reconstruction
nIterations = '700' #Number of iterations
cluster = False #if you will run on phasis or false if you run locally
preprocess = False #set True for pre-processing and GPU reconstruction
reconstruct = True
saveOutput = False #set True to save TIFF files of the reconstructions
nGPU = 1
debug = False

#######################################################################################################################
#######################################################################################################################
##Some Meta Data
year = '2015'
month = '01'
day = '18'
scientist = 'Young-Sang, Maryam, and David'
sample = 'YSZ on carbon'
comment1 = 'Tomography Scan at 750 eV'
comment2 = "It's gonna work this time"

#######################################################################################################################
#######################################################################################################################
#
#DO NOT EDIT BELOW
#
#######################################################################################################################
#######################################################################################################################
param = readParamFile(dataPath + paramFileName)
if param:
    param["dataPath"] = dataPath 
    param["nIterations"] = nIterations
    param['Year'] = year
    param['Month'] = month
    param['Day'] = day
    param['Scientist'] = scientist
    param['Sample'] = sample
    param['Comment1'] = comment1
    param['Comment2'] = comment2
    param['nCPU'] = '17'
    param['nGPU'] = str(nGPU)

    tomoscan(param, preprocess = preprocess, reconstruct = reconstruct, cluster = cluster,\
	cpu = False, saveOutput = saveOutput, debug = debug)

else:
    print "Failed to open parameter file.  Check paths."
    exit()


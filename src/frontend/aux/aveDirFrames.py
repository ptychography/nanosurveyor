__author__ = 'david'

from imageIO import imload
from util import shift
from numpy import hstack, fliplr, flipud, zeros, sort, zeros_like
import os

def aveDirFrames(bgScanDir, multi = False, fCCD = True, fCCD_version = 2):

    if fCCD:
        bgFileList = sorted(os.listdir(bgScanDir))
        bgFileList = [bgFileList[i] for i in range(len(bgFileList)) if bgFileList[i].count('tif')]
        longExpList = bgFileList[1::2]
        shortExpList = bgFileList[0::2]
        npoints = len(shortExpList)
        nFrames = 0
        for a,b,i in zip(longExpList,shortExpList,xrange(npoints)):
            if i == 0:
                bg = imload(bgScanDir + a).astype('single')[500:1500,:]
                #if fCCD_version == 1: bg = hstack((bg[534:1000,:], fliplr(flipud(shift(bg[0:500-34,:],-1,0)))))
                #elif fCCD_version == 2: bg = hstack((bg[534:1000,:], fliplr(flipud(shift(bg[0:500-34,:],0,0)))))
                bgNorm = zeros(bg.shape)
                bgNorm[bg > 0.] = 1.
                if multi:
                    bg_short = imload(bgScanDir + b).astype('single')[500:1500,:]
                    if fCCD_version == 1: bg_short = hstack((bg_short[534:1000,:], fliplr(flipud(shift(bg_short[0:500-34,:],-1,0)))))
                    elif fCCD_version == 2: bg_short = hstack((bg_short[534:1000,:], fliplr(flipud(shift(bg_short[0:500-34,:],0,0)))))
                    bgShortNorm = zeros(bg_short.shape)
                nFrames += 1
            else:
                temp = imload(bgScanDir + a).astype('single')[500:1500,:]
                #if fCCD_version == 1: temp = hstack((temp[534:1000,:], fliplr(flipud(shift(temp[0:500-34,:],-1,0)))))
                #elif fCCD_version == 2: temp = hstack((temp[534:1000,:], fliplr(flipud(shift(temp[0:500-34,:],0,0)))))
                temp_short = imload(bgScanDir + b).astype('single')[500:1500,:]
                #if fCCD_version == 1: temp_short = hstack((temp_short[534:1000,:], fliplr(flipud(shift(temp_short[0:500-34,:],-1,0)))))
                #elif fCCD_version == 2: temp_short = hstack((temp_short[534:1000,:], fliplr(flipud(shift(temp_short[0:500-34,:],0,0)))))
                bg += temp
                bgNorm[temp > 0.] += 1
                if multi:
                    bg_short += temp_short
                    bgShortNorm += temp_short > 0.
                else:
                    bg += temp_short
                    bgNorm += temp_short > 0.
                nFrames += 2
        bgNorm[bgNorm == 0.] = 1.
        bg = bg / bgNorm
        if multi:
            bgShortNorm[bgShortNorm == 0.] = 1.
            bg_short = bg_short / bgShortNorm
        else: bg_short = None
    else:
        i = 0
        for item in sort(os.listdir(bgScanDir)):
            if item.split('.')[-1::][0] == 'tif':
                if i == 0:
                    bg = imload(bgScanDir + item).astype('single')
                    if multi: bg_short = zeros_like(bg)
                else:
                    if multi:
                        if (i % 2) == 0:
                            bg += imload(bgScanDir + item).astype('single')
                        else:
                            bg_short += imload(bgScanDir + item).astype('single')
                    else:
                        bg += imload(bgScanDir + item).astype('single')
                i += 1
        if i == 0:
            if verbose: print "Did not find any tif files in the directory: %s" %bgScanDir
        else:
            if multi:
                bg = bg / (i / 2)
                bg_short = bg_short / (i / 2)
            else:
                bg = bg / i
                bg_short = None
        nFrames = i

    return bg, bg_short, nFrames

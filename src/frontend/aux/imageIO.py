# -*- coding: utf-8 -*-
"""\
Saving to images, loading from images
"""

import colorsys
import numpy
import matplotlib.cm as cm
#import Image
import numpy as np
from PIL import Image, ImageQt
from numpy.lib.function_base import vectorize
from math import pi
from PyQt4.QtGui import QImage, QColor, QPixmap

# Numpy's vectorized function
vect_hsv_to_rgb = vectorize(colorsys.hsv_to_rgb)  
vect_rgb_to_hsv = vectorize(colorsys.rgb_to_hsv)  

def P1A_to_HSV_old(cin):
    """\
    Transform a complex array into an RGB image,
    mapping phase to hue, amplitude to value and
    keeping maximum saturation.

    Kept for documentation purposes
    """

    # TODO : Optimize this function for less memory usage
    # TODO : allow clipping of amplitude (using matplotlib limits maybe?)
    
    # HSV channels
    H = np.angle(cin)
    H /= (2.*pi)
    H += .5
    S = np.ones(cin.shape)
    V = np.absolute(cin)
    Vmax = V.max()
    V /= Vmax

    # Convert to RGB - inplace.
    # We need the ".flat" - I don't know why
    (H.flat, S.flat, V.flat) = vect_hsv_to_rgb(H.flat, S.flat, V.flat)

    # Rename arrays to avoid confusion
    R,G,B = H,S,V

    R *= 255.
    G *= 255.
    B *= 255.

    imout = np.zeros(cin.shape + (3,), 'uint8')
    imout[:,:,0] = R.astype('uint8')
    imout[:,:,1] = G.astype('uint8')
    imout[:,:,2] = B.astype('uint8')
    
    return imout
    
def P1A_to_HSV(cin):
    """\
    Transform a complex array into an RGB image,
    mapping phase to hue, amplitude to value and
    keeping maximum saturation.
    """

    # HSV channels
    h = .5 * np.angle(cin) / np.pi + .5
    v = np.abs(cin)
    v /= v.max()

    i = (6. * h).astype(int)
    f = (6. * h) - i
    q = v * (1. - f)
    t = v * f
    i0 = (i % 6 == 0)
    i1 = (i == 1)
    i2 = (i == 2)
    i3 = (i == 3)
    i4 = (i == 4)
    i5 = (i == 5)

    imout = np.zeros(cin.shape + (3,), 'uint8')
    imout[:,:,0] = 255 * (i0*v + i1*q + i4*t + i5*v)
    imout[:,:,1] = 255 * (i0*t + i1*v + i2*v + i3*q)
    imout[:,:,2] = 255 * (i2*t + i3*v + i4*v + i5*q)

    return imout
    

def imsave(a, filename=None, vmin=None, vmax=None,ct=None, get_obj = True, asFloat = False):

    """\
    imsave(a) converts array a into, and returns a PIL imagescipy toimage color map
    imsave(a, filename) returns the image and also saves it to filename
    imsave(a, filename, vmin, vmax) clips the array to values between vmin and vmax.
    """

    if a.dtype.kind == 'c':
        if vmin is None and vmax is None:
            b = a
        else:
            if vmin is None:
                vmin = -numpy.inf
            elif vmax is None:
                vmax = numpy.inf
            aa = abs(a)
            b = numpy.where(aa != 0.,(a / aa) * aa.clip(vmin,vmax), 0.)
        i = P1A_to_HSV(b)
        try:
            im = Image.fromarray(i, mode='RGB')
        except:
            im = Image.fromstring('RGB', a.shape[-1::-1], i.tostring())

    else:
        if vmin is None:
            vmin = a.min()
        if vmax is None:
            vmax = a.max()

        if ct=='bgr':
            R=a/a.max()
            G=numpy.where(R>0.5,abs(1.0-R),R)
            B=abs(R-1.)

            imout = numpy.zeros(b.shape + (3,))
            imout[:,:,0] = 255*R
            imout[:,:,1] = 255*G
            imout[:,:,2] = 255*B
            imout=imout.astype('uint8')
            im=Image.fromstring('RGB',a.shape[-1::-1],imout.tostring())
        elif ct=='stern':
            im = Image.fromarray(numpy.uint8(cm.gist_stern((a.clip(vmin,vmax)-vmin)/(vmax-vmin))*255))
        elif ct=='gray':
            im = Image.fromarray(numpy.uint8(cm.gray((a.clip(vmin,vmax)-vmin)/(vmax-vmin))*255))
        elif ct=='gist_yarg':
            im = Image.fromarray(numpy.uint8(cm.gist_yarg((a.clip(vmin,vmax)-vmin)/(vmax-vmin))*255))
        elif ct=='earth':
            im = Image.fromarray(numpy.uint8(cm.gist_earth((a.clip(vmin,vmax)-vmin)/(vmax-vmin))*255))
        elif ct=='hot':
            im = Image.fromarray(numpy.uint8(cm.hot((a.clip(vmin,vmax)-vmin)/(vmax-vmin))*255))
        elif ct=='jet':
            im = Image.fromarray(numpy.uint8(cm.jet((a.clip(vmin,vmax)-vmin)/(vmax-vmin))*255))
        elif ct=='bone':
            im = Image.fromarray(numpy.uint8(cm.bone((a.clip(vmin,vmax)-vmin)/(vmax-vmin))*255))
        elif ct=='bone_r':
            im = Image.fromarray(numpy.uint8(cm.bone_r((a.clip(vmin,vmax)-vmin)/(vmax-vmin))*255))
        else:
            im = Image.fromarray(numpy.uint8(cm.binary((a.clip(vmin,vmax)-vmin)/(vmax-vmin))*255))
        if asFloat == False:
            pass
        else:
            im = Image.fromarray(a)
            #pass

    if filename != None:
        if get_obj == True:
            try: im.save(filename)
            except: return False
            else: return im
        else:
            print "returning boolean"
            if filename is not None:
                try: im.save(filename)
                except: return False
                else: return True
            else:
                return False
    else:
        return im

def imload(filename,mode=None, verbose = False, ct = None):
    """\
    Load an image and returns a numpy array
    """
    im = Image.open(filename)
    if verbose: print "Image mode is: "+str(im.mode)

#    if (im.mode!='I;16'):
#        #print "mode = I;16"
#        a = numpy.array(im)
#    elif (mode==None):
#        #print "mode = None"
#        a = numpy.fromstring(im.tostring())
#        a.resize(im.size[-1::-1])
#    else:
#        #print "mode = "+mode
#    	a = numpy.fromstring(im.tostring(), dtype=mode)
#        a.resize(im.size[-1::-1])
    if ct == None:
        if im.mode == 'L':
            a = numpy.fromstring(im.tostring(), dtype='double')
            a.resize(im.size[-1::-1])
        elif im.mode == 'I;16':
            a = numpy.fromstring(im.tostring(), dtype='uint16')
            a.resize(im.size[-1::-1])
        elif im.mode == 'I;16B':
            a = numpy.fromstring(im.tostring(), dtype='uint16')
            a.resize(im.size[-1::-1])
            a.byteswap(True)
            #a = numpy.array(im)
        elif im.mode == 'I':
            a = numpy.fromstring(im.tostring(), dtype='uint32')
            a.resize(im.size[-1::-1])
        elif im.mode == 'F':
            a = numpy.fromstring(im.tostring(), dtype='float32')
            a.resize(im.size[-1::-1])
        elif im.mode == 'F;32BF':
            im.mode = 'F'
            a = numpy.fromstring(im.tostring(), dtype='float32')
            a.resize(im.size[-1::-1])
        elif im.mode == 'RGB':
            a = numpy.fromstring(im.tostring(), dtype='uint8')
            a.resize((im.size[1],im.size[0],3))
    elif ct == 'bw':
        if im.mode == 'L':
            a = numpy.fromstring(im.tostring(), dtype='uint8')
            a.resize(im.size[-1::-1])
        elif im.mode == 'LA':
            a = numpy.fromstring(im.tostring(), dtype='uint16')
            a.resize(im.size[-1::-1])
#    elif im.mode == 'LA':
#        a.resize((2,im.size[1],im.size[0]))
    elif im.mode == 'RGB':
        a = numpy.fromstring(im.tostring(), dtype='uint8')
        a.resize((im.size[1],im.size[0],3))
#    elif im.mode == 'RGBA':
#        a.resize((4,im.size[1],im.size[0]))
#    elif im.mode == 'F':
#        a.resize(im.size[-1::-1])
#    elif im.mode == 'I;16':
#        a.resize(im.size[-1::-1])
#    else:
#        raise RunTimeError('Unsupported image mode %s' % im.mode)

    return a

def hsv2rgb(image_hsv):

    """Convert RGB image to HSV image
    
    image should have the form (n,n,3)
    R = image_rgb[:,:,0]
    G = image_rgb[:,:,1]
    B = image_rgb[:,:,2]

    return image_hsv

    """
    n = len(image_hsv)
    image_hsv = image_hsv.astype('float32')
    image_rgb = image_hsv.copy()

    (image_rgb[:,:,0],image_rgb[:,:,1],image_rgb[:,:,2]) = \
        vect_hsv_to_rgb(image_hsv[:,:,0],image_hsv[:,:,1],image_hsv[:,:,2])

    return image_rgb

def rgb2hsv(image_rgb):
    """Convert RGB image to HSV image
    
    image should have the form (n,n,3)
    R = image_rgb[:,:,0]
    G = image_rgb[:,:,1]
    B = image_rgb[:,:,2]

    return image_hsv

    """
    n = len(image_rgb)
    image_rgb = image_rgb.astype('float32')
    image_hsv = image_rgb.copy()

    (image_hsv[:,:,0],image_hsv[:,:,1],image_hsv[:,:,2]) = \
        vect_rgb_to_hsv(image_rgb[:,:,0],image_rgb[:,:,1],image_rgb[:,:,2])

    return image_hsv

_bgra_rec = numpy.dtype({'b': (numpy.uint8, 0),
                         'g': (numpy.uint8, 1),
                         'r': (numpy.uint8, 2),
                         'a': (numpy.uint8, 3)})

def qimage2numpy(qimage):
    if qimage.format() in (QImage.Format_ARGB32_Premultiplied,
                           QImage.Format_ARGB32,
                           QImage.Format_RGB32):
        dtype = _bgra_rec
    elif qimage.format() == QImage.Format_Indexed8:
        dtype = numpy.uint8
    else:
        raise ValueError("qimage2numpy only supports 32bit and 8bit images")
    # FIXME: raise error if alignment does not match
    buf = qimage.bits().asstring(qimage.numBytes())
    return numpy.frombuffer(buf, dtype).reshape(
        (qimage.height(), qimage.width()))

def numpy2qimage(a, vmin = None, vmax = None, cmap = None):
    b = (255*(a.clip(a.min(),a.max())-a.min())/(a.max()-a.min())).astype('uint32')
    if a.dtype.kind == 'c':
        if vmin is None and vmax is None:
            b = a
        else:
            if vmin is None:
                vmin = -numpy.inf
            elif vmax is None:
                vmax = numpy.inf
            aa = abs(a)
            b = numpy.where(aa != 0.,(a / aa) * aa.clip(vmin,vmax), 0.)
        return rgb2qimage(P1A_to_HSV(b))
    elif numpy.ndim(b) == 2:
        #return gray2qimage(b)
        if cmap == 'gray': return ImageQt.ImageQt(Image.fromarray(numpy.uint8(cm.gray(b)*255)))
        elif cmap == 'hot': return ImageQt.ImageQt(Image.fromarray(numpy.uint8(cm.hot(b)*255)))
        elif cmap == 'jet': return ImageQt.ImageQt(Image.fromarray(numpy.uint8(cm.jet(b)*255)))
        elif cmap == 'stern': return ImageQt.ImageQt(Image.fromarray(numpy.uint8(cm.gist_stern(b)*255)))
        elif cmap == 'earth': return ImageQt.ImageQt(Image.fromarray(numpy.uint8(cm.gist_earth(b)*255)))
        elif cmap == 'bone': return ImageQt.ImageQt(Image.fromarray(numpy.uint8(cm.bone(b)*255)))
        elif cmap == None: return ImageQt.ImageQt(Image.fromarray(numpy.uint8(cm.gray(b)*255)))
        else: return ImageQt.ImageQt(Image.fromarray(numpy.uint8(cm.gray(b)*255)))
    elif numpy.ndim(b) == 3:
        return rgb2qimage(b)
    else: raise ValueError("can only convert 2D or 3D arrays")

def numpy2qpixmap(a, xsize = None, ysize = None):

    b = (255*(a.clip(a.min(),a.max())-a.min())/(a.max()-a.min())).astype('uint8')
    return QPixmap.fromImage(numpy2qimage(b))


def gray2qimage(gray):
	"""Convert the 2D numpy array `gray` into a 8-bit QImage with a gray
	colormap.  The first dimension represents the vertical image axis."""
	if len(gray.shape) != 2:
		raise ValueError("gray2QImage can only convert 2D arrays")

        gray = numpy.require(gray, numpy.uint8, requirements = ['C','A'])

        h, w = gray.shape

        result = QImage(gray.data, w, h, QImage.Format_Indexed8)
        result.ndarray = gray
        for i in range(256):
                result.setColor(i, QColor(i, i, i).rgb())
        return result

def rgb2qimage(rgb):
	"""Convert the 3D numpy array `rgb` into a 32-bit QImage.  `rgb` must
	have three dimensions with the vertical, horizontal and RGB image axes."""
	if len(rgb.shape) != 3:
		raise ValueError("rgb2QImage can expects the first (or last) dimension to contain exactly three (R,G,B) channels")
	if rgb.shape[2] != 3:
		raise ValueError("rgb2QImage can only convert 3D arrays")

	h, w, channels = rgb.shape

	# Qt expects 32bit BGRA data for color images:
	bgra = numpy.empty((h, w, 4), numpy.uint8, 'C')
	bgra[...,0] = rgb[...,2]
	bgra[...,1] = rgb[...,1]
	bgra[...,2] = rgb[...,0]
	bgra[...,3].fill(255)

	result = QImage(bgra.data, w, h, QImage.Format_RGB32)
	result.ndarray = bgra
	return result

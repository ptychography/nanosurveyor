import numpy as np
#from pyptycho.util import dist, shift

#near field calculation
def propnf(a, z, l):
    """
    propnf(a,z,l) where z and l are in pixel units
    """

    if a.ndim != 2:
        raise RunTimeError("A 2-dimensional wave front 'w' was expected")
    #if a.shape[0] != a.shape[1]:
    #    raise RunTimeError("Only square arrays are currently supported")
    
    n = len(a)
    ny,nx = a.shape
    qx,qy = np.meshgrid(np.linspace(-nx / 2, nx / 2, nx),np.linspace(-ny / 2, ny / 2, ny))
    q2 = np.fft.fftshift(qx**2 + qy**2)

    if n / np.sqrt(2.) < np.abs(z) * l:
        print "Warning: %.2f < %.2f: this calculation could fail." % ( n / np.sqrt(2.) , np.abs(z) * l )
        print "You could enlarge your array, or try the far field method: propff()"

    return np.fft.ifftn(np.fft.fftn(a) * np.exp(-2j * np.pi * (z / l) * (np.sqrt(1 - q2 * (l / n)**2) - 1)))


def propff(a, z, l):

    if a.ndim != 2:
        raise RunTimeError("A 2-dimensional wave front 'w' was expected")
    if a.shape[0] != a.shape[1]:
        raise RunTimeError("Only square arrays are currently supported")

    n = len(a)
    q2 = np.fft.fftshift(dist(n)**2)

    if n * np.sqrt(2.) > np.abs(z) * l:
        print "Warning: %.2f > %.2f: this calculation could fail." % ( n * \
            np.sqrt(2.) , np.abs(z) * l )
        print "You could try the near field method: propnf()"

    return -1j * np.fft.fftshift(np.exp(4j * np.pi * l * z * q2 / n**2) * \
        np.fft.fftn(a * np.exp(1j * np.pi * np.fft.fftshift(q2) / (l * z)))) / n

def prop2(a, x1, x2, z ,l):
    """
    prop2(a,x1,x2,l)
    a: input array
    x1: pixel size in starting plane
    x2: pixel size in ending plate
    l: wavelength, same units as pixel size
    """
    
    if a.ndim != 2:
        raise RunTimeError("A 2-dimensional wave front 'w' was expected")
    if a.shape[0] != a.shape[1]:
        raise RunTimeError("Only square arrays are currently supported")

    n = len(a)
    p1 = np.fft.fftshift(dist(n)) * x1

    if n * x1 * np.sqrt(2.) > np.abs(z * l):
        print "Warning: %.2f > %.2f: this calculation could fail." % ( n * \
            np.sqrt(2.) , np.abs(z) * l )
        print "You could try the near field method: propnf()"

    return np.fft.fftshift(-1j * (np.exp(2j * np.pi * z / l) * \
        np.fft.fftn(np.fft.fftshift(a) * np.exp(1j * np.pi * p1**2 / (l * z))))) / n

 #   return fft.fftshift(fft.fftn(fft.fftshift(a) * exp(1j * pi * p1**2 / (l * z)))) / n

# def iprop2(a, x1, x2, z ,l):
#     """
#     prop2(a,x1,x2,l)
#     a: input array
#     x1: pixel size in starting plane
#     x2: pixel size in ending plate
#     l: wavelength, same units as pixel size
#     """
#
#     if a.ndim != 2:
#         raise RunTimeError("A 2-dimensional wave front 'w' was expected")
#     if a.shape[0] != a.shape[1]:
#         raise RunTimeError("Only square arrays are currently supported")
#
#     n = len(a)
#     p2 = fft.fftshift(dist(n)) * x2
#
#     if n * x1 * sqrt(2.) > abs(z * l):
#         print "Warning: %.2f > %.2f: this calculation could fail." % ( n * \
#             sqrt(2.) , abs(z) * l )
#         print "You could try the near field method: propnf()"
#
#     return (fft.fftshift(-1j * (exp(2j * pi * z / l) * exp (1j * pi * p2**2 / (l * z)) * \
#         fft.fftn(fft.fftshift(a))))/n)[::-1,::-1]



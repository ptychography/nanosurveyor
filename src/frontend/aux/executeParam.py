__author__ = 'nsptycho'

from numpy import single, load
import subprocess
from param import Param
#from pyptycho.ptycho import prPtycho
from readCXI import *
from util import e2l
import os, shutil, logging


def loadParam(scriptPath, scriptName):

    param = {}
    try: f = open(scriptPath + scriptName,'r')
    except IOError:
        logging.error("IOError: could not open parameter file: %s" %(scriptPath + scriptName))
        return
    else: logging.info("Loading parameters from file %s" %(scriptPath + scriptName))
    for item in f:
        pStringList = [item.split('=')[0],item.split('=')[1].rstrip('\n')]
        if len(pStringList) != 2:
            pass
        else:
            param[pStringList[0]] = pStringList[1]
    return param

def readParamFile(fileName):

    try:
        p = Param()
        f = open(fileName,'r')
        for item in f:
            key = item.split('=')[0]
            value = item.split('=')[1].split('\n')[0]
            p.param[key] = value
            setattr(p, key, value)
        f.close()
        return p.param
    except: return 0

def saveParamFile(fileName, param):

    try: f = open(fileName, 'w')
    except:
        logging.error("Failed to write parameter file."); return 0
    else:
        for key in param.keys():
            f.write(key + '=' + param[key] + '\n')
        f.close()
        return 1


#def runParam(param):
def parseParam(param):

    param['paramFileName'] = param['scanDir'] + 'param.txt'
    if paramCheck(param):
        ###calculate the pixel size and save the parameter file
        param["pixnm"] = str(calcPixnm(single(param['ssx']), single(param['ssn'])))
        saveParamFile(param['paramFileName'], param)
        pwd = os.getcwd() + '/'
        ###execute mpi code

        ##load the probe from file if requested
        if param['probeFile'] != '':
            p = load(param['probeFile'])
            placeProbe(param['scanDir'] + param['dataFile'], p)
        if param['illuminationIntensitiesFile'] != '':
            p = load(param['illuminationIntensitiesFile'])
            placeIntensities(param['scanDir'] + param['dataFile'], p)

        if param['version'] == '2':
            if int(param['process']):
                hostString = " --hostfile ~/host_file " * int(param['useCluster'])
                remoteScriptString = int(param['useCluster']) * " python $PYPTYCHOROOT/pyptycho/readALSMPI.py "
                localScriptString =  (not(int(param['useCluster']))) * " python $PYPTYCHOROOT/pyptycho/readALSMPI.py "
                commandString = "mpirun --mca mpi_warn_on_fork 0 -n " + param['nCPU'] + hostString + localScriptString + remoteScriptString + \
                                param['paramFileName']
                #logging.info("Executing command: %s" %commandString)
                #subprocess.call(commandString, shell = True)
		return commandString

            if int(param['reconstruct']):
                if int(param['gpu']):
                    illuminationString = int(param['pR']) * (' -r ' + param['illuminationPeriod'] + ' ') +\
                                         int(param['illuminationIntensities']) * ' -I ' +\
                                         int(param['illuminationMask']) * ' -M '
                    betaString = ' -b ' + param['beta'] + ' '
                    fourierString = int(param['relaxedFourier']) * ' -B '
                    backgroundString = int(param['bR']) * (' -T ' + param['backgroundPeriod'] + ' ')
                    hostString = " --hostfile ~/host_file " * int(param['useCluster'])
                    outFileString = " -f " + param['scanDir'] + param['outputFilename']
                    commandString = "mpirun -n " + param['nGPU'] + hostString + " sharp -t -o 10 -i " +\
                                    param['nIterations'] + illuminationString + backgroundString +\
                                    param['scanDir'] + param['dataFile'] + outFileString + betaString
                    #logging.info("Executing command: %s" %commandString)
                    #subprocess.call(commandString, shell = True)
                    #getPtychopsOutput(pwd, param)
                    #print commandString
                    #subprocess.call(commandString, shell = True)
		    return commandString
                else:
                    logStr = " --log=ptychoLog.log"
                    modesString = " -m " + param['nModes']
                    illuminationString = int(param['pR']) * (' -r ' + param['illuminationPeriod'] + ' ') +\
                                         int(param['illuminationIntensities']) * ' -I ' +\
                                         int(param['illuminationMask']) * ' -M '
                    betaString = ' -b ' + param['beta'] + ' '
                    etaString = ' -e ' + param['eta'] + ' '
                    fourierString = int(param['relaxedFourier']) * ' -B '
                    backgroundString = int(param['bR']) * (' -T ' + param['backgroundPeriod'] + ' ')
                    mpiLoopString = ' -L ' + param['loopSize'] + ' '
                    usePreviousString = ' -P ' * int(param['usePrevious'])
                    hostString = " --hostfile ~/host_file " * int(param['useCluster'])
                    remoteScriptString = int(param['useCluster']) * " python $PYPTYCHOROOT/pyptycho/communicator.py "
                    localScriptString = (not(int(param['useCluster']))) * " python $PYPTYCHOROOT/pyptycho/communicator.py "
                    outFileString = " -f " + param['outputFilename']
                    commandString = "mpirun --mca mpi_warn_on_fork 0 -n " + param['nCPU'] + hostString + localScriptString + remoteScriptString + \
                                    param['scanDir'] + param['dataFile'] + ' -i ' + param['nIterations'] + mpiLoopString + betaString + illuminationString + \
                                    etaString + backgroundString + usePreviousString + logStr + modesString + outFileString
                    #logging.info("Executing command: %s" %commandString)
                    #subprocess.call(commandString, shell = True)
		    return commandString
                    #print commandString
                    #subprocess.call(commandString, shell = True)
        else:
            return None
    else:
        return None

def runParam(command):
    logging.info("Executing command: %s" %command)
    subprocess.call(command, shell = True)
    print "Done!"

#def runParam(param):

#    param['paramFileName'] = param['scanDir'] + 'param.txt'
#    if paramCheck(param):
#        ###calculate the pixel size and save the parameter file
#        param["pixnm"] = str(calcPixnm(single(param['ssx']), single(param['ssn'])))
#        saveParamFile(param['paramFileName'], param)
#        pwd = os.getcwd() + '/'
#        ###execute mpi code
#        if param['version'] == '2':
#            if int(param['process']):
#                hostString = " --hostfile ~/host_file " * int(param['useCluster'])
#                remoteScriptString = int(param['useCluster']) * " python $PYPTYCHOROOT/pyptycho/readALSMPI.py "
#                localScriptString =  (not(int(param['useCluster']))) * " python $PYPTYCHOROOT/pyptycho/readALSMPI.py "
#                commandString = "mpirun --mca mpi_warn_on_fork 0 -n " + param['nCPU'] + hostString + localScriptString + remoteScriptString + \
#                                param['paramFileName']
#                logging.info("Executing command: %s" %commandString)
#                subprocess.call(commandString, shell = True)

#            if int(param['reconstruct']):
#                if int(param['gpu']):
#                    illuminationString = int(param['pR']) * (' -r ' + param['illuminationPeriod'] + ' ') +\
#                                         int(param['illuminationIntensities']) * ' -I ' +\
#                                         int(param['illuminationMask']) * ' -M '
#                    betaString = ' -b ' + param['beta'] + ' '
#                    fourierString = int(param['relaxedFourier']) * ' -B '
#                    backgroundString = int(param['bR']) * (' -T ' + param['backgroundPeriod'] + ' ')
#                    hostString = " --hostfile ~/host_file " * int(param['useCluster'])
#                    commandString = "mpirun -n " + param['nGPU'] + hostString + " sharp -t -o 10 -i " +\
#                                    param['nIterations'] + illuminationString + backgroundString +\
#                                    param['scanDir'] + param['dataFile']
#                    logging.info("Executing command: %s" %commandString)
#                    subprocess.call(commandString, shell = True)
#                    #getPtychopsOutput(pwd, param)
#                else:
#                    logStr = " --log=ptychoLog.log"
#                    modesString = " -m " + param['nModes']
#                    illuminationString = int(param['pR']) * (' -r ' + param['illuminationPeriod'] + ' ') +\
#                                         int(param['illuminationIntensities']) * ' -I ' +\
#                                         int(param['illuminationMask']) * ' -M '
#                    betaString = ' -b ' + param['beta'] + ' '
#                    fourierString = int(param['relaxedFourier']) * ' -B '
#                    backgroundString = int(param['bR']) * ' -T 1 '
#                    mpiLoopString = ' -L ' + param['loopSize'] + ' '
#                    usePreviousString = ' -P ' * int(param['usePrevious'])
#                    hostString = " --hostfile ~/host_file " * int(param['useCluster'])
#                    remoteScriptString = int(param['useCluster']) * " python $PYPTYCHOROOT/pyptycho/communicator.py "
#                    localScriptString = (not(int(param['useCluster']))) * " python $PYPTYCHOROOT/pyptycho/communicator.py "
#                    commandString = "mpirun --mca mpi_warn_on_fork 0 -n " + param['nCPU'] + hostString + localScriptString + remoteScriptString + \
#                                    param['paramFileName'] + ' -i ' + param['nIterations'] + mpiLoopString + betaString + illuminationString + \
#                                    backgroundString + usePreviousString + logStr + modesString
#                    logging.info("Executing command: %s" %commandString)
#                    subprocess.call(commandString, shell = True)
#        else:
#            print "OOPSIE!!"

#            # if int(param['process']): read_als(param)
#            # if int(param['reconstruct']):
#            #     if int(param['gpu']):
#            #         illuminationString = int(param['pR']) * (' -r ' + param['illuminationPeriod'] + ' ') +\
#            #                              int(param['illuminationIntensities']) * ' -I ' +\
#            #                              int(param['illuminationMask']) * ' -M '
#            #         betaString = ' -b ' + param['beta'] + ' '
#            #         fourierString = int(param['relaxedFourier']) * ' -B '
#            #         backgroundString = int(param['bR']) * ' -T 1 '
#            #         hostString = " --hostfile ~/host_file " * int(param['useCluster'])
#            #         commandString = "mpirun -n " + param['nGPU'] + hostString + " ptychops -R -t -i " +\
#            #                         param['nIterations'] + illuminationString + backgroundString +\
#            #                         param['scanDir'] + param['dataFile']
#            #         print "Executing command: %s" %commandString
#            #         subprocess.call(commandString, shell = True)
#            #         getPtychopsOutput(pwd, param)
#            #     else:
#            #         illuminationString = int(param['pR']) * (' -r ' + param['illuminationPeriod'] + ' ') +\
#            #                              int(param['illuminationIntensities']) * ' -I ' +\
#            #                              int(param['illuminationMask']) * ' -M '
#            #         betaString = ' -b ' + param['beta'] + ' '
#            #         fourierString = int(param['relaxedFourier']) * ' -B '
#            #         backgroundString = int(param['bR']) * ' -T 1 '
#            #         mpiLoopString = ' -L ' + param['loopSize'] + ' '
#            #         usePreviousString = ' -P ' * int(param['usePrevious'])
#            #         hostString = " --hostfile ~/host_file " * int(param['useCluster'])
#            #         remoteScriptString = int(param['useCluster']) * " python $PYPTYCHOROOT/pyptycho/ptychoCommunicatorMPI.py "
#            #         localScriptString = (not(int(param['useCluster']))) * " python $PYPTYCHOROOT/pyptycho/ptychoCommunicatorMPI.py "
#            #         commandString = "mpirun --mca mpi_warn_on_fork 0 -n " + param['nCPU'] + hostString + localScriptString + remoteScriptString + \
#            #                         param['paramFileName'] + ' -i ' + param['nIterations'] + mpiLoopString + betaString + illuminationString + backgroundString + usePreviousString
#            #         print "Executing command: %s" %commandString
#            #         subprocess.call(commandString, shell = True)
#    print "Done!"

def paramCheck(param):
    ##first check that the relevant data files and directories exist
    paramCheckOK = 0
    if os.path.isdir(param['scanDir']): paramCheckOK = 1
    else: logging.error("paramCheck Error: could not find data path %s" %param['scanDir'])
    if int(param['process']) == 0:

        if os.path.isfile(param['scanDir'] + param['dataFile']):
            pass
        else: paramCheckOK = 0; logging.error("paramCheck Error: could not find CXI file %s" %param['dataFile'])

    ##next check the pixel size
    if int(param['fCCD']):
        ccdp = single(param['ccdp'])
        zpw = float(param['zpw'])
        dr = single(param['dr'])
        zd = single(param['zd'])
        l = e2l(single(param['e']))
        ccdz = calcCCDZ(zd, dr, l, ccdp, zpw)
        realNA = single(param['sh_crop']) * ccdp / ccdz / 1000. / 2.##assumes 512 pixels in a fCCD dataset
        requestedPixnm = single(param['ssx']) / single(param['ssn'])
        paddedNA = l / 2. / requestedPixnm
        if paddedNA < realNA:
            paramCheckOK = 0
            logging.error("paddedNA = %.4f and realNA = %.4f" %(paddedNA, realNA))
            logging.error("paramCheck Error: please increase ssn (pixels per scan step).")
    else:
        pass
        # ccdp = single(param['ccdp'])
        # zpw = single(param['zpw'])
        # dr = single(param['dr'])
        # zd = single(param['zd'])
        # l = e2l(single(param['e']))
        # ccdz = single(param['ccdz'])
        # realNA = single(param['sh_crop']) * ccdp / ccdz / 1000. / 2.##assumes 512 pixels in a fCCD dataset
        # requestedPixnm = single(param['ssx']) / single(param['ssn'])
        # paddedNA = l / 1. / requestedPixnm
        # if paddedNA < realNA:
        #     paramCheckOK = 0
        #     print "paddedNA = %.4f and realNA = %.4f" %(paddedNA, realNA)
        #     print "paramCheck Error: please increase ssn (pixels per scan step)."
    return paramCheckOK

def calcCCDZ(zd, dr, l, ccdp, pupilw):

    f = zd * dr / l #zp focal length microns
    na = zd / f / 2. #zp numerical aperture
    ccdz = ccdp * pupilw / 2. / na / 1000.
    #print "Setting CCDZ = %.4f mm" %ccdz
    return ccdz

def calcPixnm(ssx, ssn, verbose = False):

    newPixnm = ssx / ssn
    logging.info("Setting Pixel Size to %.2f nm." %newPixnm)
    return newPixnm


# def getPtychopsOutput(pwd,param):
#
#     from util import cropAndDetrend
#     shutil.move('run-00000-image.cxi', param['scanDir'] + 'run-00000-image.cxi')
#     os.chmod(param['scanDir'] + 'run-00000-image.cxi', 0664)
#
#     f1 = h5py.File(param['scanDir'] + param['dataFile'])
#     f2 = h5py.File(param['scanDir'] + 'run-00000-image.cxi')
#     psize =  f1['entry_1/instrument_1/detector_1/Probe'].value.shape
#     dshape = f2['entry_1/image_1/data'].value.shape
#
#     logging.info('getPtychopsOutput: Cropping image')
#     imageData = cropAndDetrend(data = f2['entry_1/image_1/data'].value, psize = psize)
#     if imageData != None:
#
#         logging.info('getPtychopsOutput: Saving data.')
#         if 'entry_1/image_1' in f1:
#             if 'entry_1/image_1/data' in f1:
#                 del f1['entry_1/image_1/data']
#                 dset = f1.create_dataset('entry_1/image_1/data', data = imageData)
#             else:
#                 dset = f1.create_dataset('data', data = imageData)
#         else:
#             entry_1 = f1['entry_1']
#             image_1 = entry_1.create_group('image_1')
#             image_1.create_dataset('data', data = imageData)
#
#     f1.close()
#     f2.close()


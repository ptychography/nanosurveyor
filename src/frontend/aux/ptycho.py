#! /usr/bin/python
# -*- coding: utf-8 -*-

# To change this template, choose Tools | Templates
# and open the template in the editor.

"""


"""

__author__="David Shapiro"
__date__ ="$Jul 15, 2010 9:05:40 AM$"

import numpy as np
from scipy import fftpack as fft
from util import shift, e2l, removePlane
from imageIO import rgb2hsv, imload, imsave
import matplotlib.pyplot as plt
import pylab
from matplotlib import cm
from scipy import rand
from scipy.linalg import norm
import time
import fftw3
from subprocess import call
import h5py
import subprocess, shutil, sys
from readCXI import cxi
from accumarray import accum, accum_np, accum_py, unpack
# from pyfft.cuda import Plan
# import pycuda.driver as cuda
# from pycuda.tools import make_default_context
# import pycuda.gpuarray as gpuarray


class prPtycho(cxi):

    """
    Main ptychography class that inherits from the CXI object and then expands with the various methods required
    reconstruction and basic analysis of the data.
    """

    def __init__(self, cxiObj):

        self.cxi = cxiObj

        ##private
        self.__param = self.cxi.process #metadata from the CXI file
        self.__dataPath = self.__param['scanDir'] #absolute path to the data file
        self.__fileName = self.__param['dataFile'] #data file name
        self.__pPosVec = [] #vector of probe positions in pixel units
        self.__pixnm = 1.
        self.__indices = [] #list of indices for the corners of each probe position on the total object
        self.__i = None #current iteration value
        self.__delta = 0.0001 #regularization term

        ##public
        self.dVec = None #diffraction data stack
        self.oVec = None #stack of frames in real space
        self.probe = None #either grab it from the CXI file or generate it from the data
        self.oSum = None #total object array generated from the CXI translation data
        self.probeRetrieval = False #boolean grabbed from param
        self.errhist = [] #list of error values
        self.probeMask = None #Fourier mask to apply to the probe, grabbed from the CXI file
        self.e = None #value of the error metric
        self.algorithm = self.__param['algorithm'] #algorithm choice, grabbed from the CXI file
        self.iGlobal = int(self.__param['itnGlobal']) #Global iteration value, synchronized in communicator.py
        self.saturationThreshold = np.sqrt(float(self.__param['s_threshold'])) ## saturation level, applied to sqrt(intensity)
        self.verbose = False
        self.stxm = None ##STXM image generated from the diffraction data, grabbed from the CXI file
        self.numpyFFT = False #Boolean, use numpy or FFTW
        self.bg = None #Retrieved background matrix
        self.bgRemoval = False #Boolean, background retrieval, grabbed from the CXI file
        #self.debug = int(self.__param['debug'])
        self.nit = int(self.__param['nIterations']) #number of local iterations, grabbed from the CXI file
        #self.nModes = np.int(self.cxi.process['nModes'])
        self.nModes = 1
        self.multiMode = True


        if 'probeMask' in self.__param: self.useProbeMask = int(self.__param['probeMask'])
        if 'beamStop' in self.__param: self.useBeamStop = int(self.__param['beamStop'])
        else: self.useBeamStop = 0
        self.algorithm = self.__param['algorithm']
        self.probeRetrieval = int(self.__param['pR'])
        if 'bR' in self.__param: self.bgRemoval = int(self.__param['bR'])
        else: self.bgRemoval = False
        if 'useStxm' in self.__param: self.useStxm = int(self.__param['useStxm'])
        else: self.useStxm = 0
        if 'usePrevious' in self.__param: self.usePrevious = int(self.__param['usePrevious'])
        else: self.usePrevious = 0
        if 'useIlluminationIntensities' in self.__param: self.useIlluminationIntensities = int(self.__param['useIlluminationIntensities'])
        else: self.useIlluminationIntensities = 0

        self.useStxm = 0 ##this is buggy

        self.beta = np.float(self.__param['beta'])
        self.eta = np.float(self.__param["eta"])

        self.removePhasePlane = int(self.__param["removePhasePlane"])
        if 'beamStopTransmission' in self.__param.keys(): self.beamStopTransmission = np.float(self.__param['beamStopTransmission'])
        else: self.beamStopTransmission = 0.015

        if self.verbose:
            if self.probeRetrieval: print "Doing probe retrieval"
        if self.verbose:
            if self.bgRemoval: print "Doing background retrieval"
        if self.verbose: print "Using BETA = ", self.beta
        if self.verbose: print "Using ETA = ", self.eta
        if self.verbose: print "Using %s modes" %self.nModes

        ########################################################################
        #read the configuration file and load in the diffraction data
        self.__readCXI()

    def __readCXI(self):

        """
        Private function which takes the CXI input and calculates various things needed by prPtycho.  Most important, the
        total object array and the list of shift indices are generated.  Also, a mask used on the diffraction data is also
        generated.  This mask indicates which pixels to enforce during the modulus projection.
        Input: CXI object
        Output: oSum, indices, dMask
        """

        ##load in some variables from the CXI object.  This really isn't needed.
        ##TODO, convert all variables to self.cxi values

        self.dVec = self.cxi.ccddata #f['/entry_1/instrument_1/detector_1/data'].value
        self.dVec = np.sqrt(fft.fftshift(self.dVec, axes = (2,1)))

        self.ny, self.nx = self.cxi.dataShape()[1:]
        self.nFramePixels = self.ny * self.nx

        ##If available, get probe mask, stxm image and beamstop from the CXI file
        if self.useProbeMask:
            self.probeMask = self.cxi.probemask #f['/entry_1/instrument_1/detector_1/Probe Mask'].value
            self.probeRmask = self.cxi.probeRmask
        if self.useStxm: self.stxm = self.cxi.stxm #f['/entry_1/instrument_1/detector_1/STXM'].value
        if self.useBeamStop: self.beamStop = self.cxi.beamstop #f['/entry_1/instrument_1/detector_1/Beamstop'].value
        if self.useIlluminationIntensities: self.illuminationIntensities = self.cxi.illuminationIntensities

        if self.usePrevious:
            try: self.previousImage = self.cxi.startImage #f['entry_1/image_1/data'].value
            except KeyError:
                self.previousImage = None
                self.usePrevious = 0

        ##TODO: check that the saved probe is loaded during the mpi loops!!

        ##Get the probe if available or generate if it's not
        self.probe = np.zeros((self.nModes, 1, self.ny, self.nx), complex)


        ##TODO: imageProge is always None in the CXI so this will always start from dataProbe
        if self.cxi.imageProbe == None:
            if len(self.cxi.probe.shape) == 4: self.probe[0,0] = self.cxi.probe[0,0]
            else: self.probe[0,0] = self.cxi.probe
        else:
            if len(self.cxi.imageProbe.shape) == 4: self.probe[0,0] = self.cxi.imageProbe[0,0]
            else: self.probe[0,0] = self.cxi.imageProbe

        ##initialize coherent modes to random fields
        for i in range(1,self.nModes):
            self.probe[i,0] = rand(self.ny, self.nx) * np.exp(-1j * rand(self.ny, self.nx) * np.pi / 2.)

        ##Generate the starting background array
        if self.cxi.bg != None:
            if self.verbose: print "Using previous background"
            self.bg = self.cxi.bg
        else:
            if self.verbose: print "Generating 0 background"
            self.bg = np.zeros((self.probe.shape[2:4]))

        ##Setup the mask for the Fourier modulus constraint
        if self.saturationThreshold == 0.:
            self.dMask = np.ones(self.dVec.shape)
        else:
            self.dMask = self.dVec < self.saturationThreshold
            self.dMask = self.dMask * (self.dVec > 0.)
            self.dMask = self.dMask + self.probeMask * (self.dVec < (np.sqrt(1. / self.beamStopTransmission) * self.saturationThreshold))

        if self.verbose: print "nFrames in data stack: ", self.dVec.shape

        self.__pPosVec = self.cxi.pixelTranslations()
        self.oSizeY, self.oSizeX = self.cxi.imageShape()

        self.__nFrames = len(self.dVec[:,0,0])
        self.oVec = np.zeros((self.nModes, self.__nFrames, self.ny, self.nx), complex)

        ## Setup the submatrix corner indices for each of the frame positions
        i = 0
        for p in self.__pPosVec:
            x1 = p[1] - self.nx / 2.
            x2 = p[1] + self.nx / 2.
            y1 = p[0] - self.ny / 2.
            y2 = p[0] + self.ny / 2.
            self.__indices.append((y1,y2,x1,x2))
            i += 1

        self.indices = self.__indices
        self.pPosVec = self.__pPosVec

        self.getStartImages()

        return

    def __crop(self):

        """
        Private function which crops the total object by 1/2 the probe width around the perimeter
        Input: Current total object
        Output: Cropped version of the current total object
        """

        npy, npx = self.probe.shape[2:4]
        oSizeY, oSizeX = self.oSum.shape[2:4]
        data = self.oSum[0, 0, npy / 2:oSizeY - npy / 2, npx / 2:oSizeX - npx / 2]
        return data

    def __phaseRamp(self, data):

        """
        Private function which removes a linear phase ramp from the total object
        Input: Current total object
        Output: Current total object with a linear phase ramp removed
        """

        return abs(data) * np.exp(1j * removePlane(np.angle(data)))

    def __errorMetric(self):

        """
        Private function which calculates the iteration error using the standard metric
        Input: Current total object and the diffraction data
        Output: Iteration error values
        """

        self.e = ((abs(self.__pFFT(self.__QP(self.__Q(self.oSum)))).sum(axis = 0) - self.dVec)**2).sum() / (self.dVec**2).sum()


    def __pFFT(self, a):

        """
        Private function which executes a 2D forward FFT on a 3D stack of frames, operating slice by slice along
        the 0 axis.
        Input: stack of frames in real space
        Output: Stack of frames in reciprocal space
        """

        dummy = np.zeros(a.shape,complex)
        for j in range(self.nModes):
            for i in range(self.__nFrames):
                if self.numpyFFT: dummy[j,i] = fft.fftn(a[j,i])
                else:
                    fftplan = fftw3.Plan(a[j,i],dummy[j,i],'forward')
                    fftplan()
        return dummy     
    
    def __pFFTi(self, a):

        """
        Private function which executes a 2D inverse FFT on a 3D stack of frames, operating slice by slice along
        the 0 axis.
        Input: stack of frames in reciprocal space
        Output: Stack of frames in real space
        """

        dummy = np.zeros(a.shape,complex)
        for j in range(self.nModes):
            for i in range(self.__nFrames):
                if self.numpyFFT: dummy[j,i] = fft.ifftn(a[j,i])
                else:
                    ifftplan = fftw3.Plan(a[j,i],dummy[j,i],'backward')
                    ifftplan()
        if not(self.numpyFFT): dummy = dummy / self.nFramePixels
        return dummy


    def __iterate(self, algorithm = 'dm'):

        """
        Private function which executes the main iteration
        Input: stack of frames
        Output: stack of frames times the probe
        """


        t0 = time.time()
        psi_o = self.__Pi_O(self.oVec) ##apply overlap constraint
        if self.verbose: print "Time for PO projection: %.4f" %(time.time() - t0)
        t0 = time.time()
        psi_f = self.__Pi_F(self.oVec) ##apply the Fourier modulus constraint

        if self.verbose: print "Time for PF projection: %.4f" %(time.time() - t0)
        if algorithm == "raar": psi_fo = self.__Pi_O(psi_f)
        if algorithm == "dm": psi_of = self.__Pi_F(2. * psi_o - self.oVec)
        t0 = time.time()

        if self.verbose: print "Time for probe retrieval: %.4f" %(time.time() - t0)
        t0 = time.time()
        if algorithm == "raar":
            dx = self.beta * (2. * psi_fo - psi_o) + (1. - 2. * self.beta) * psi_f + (self.beta - 1) * self.oVec
        if algorithm == "dm": dx = psi_of - psi_o
        if algorithm == "epie":
            self.oVec = self.oVec + (psi_f - self.oVec) * self.probe.conjugate() / (abs(self.probe)**2).max()

        if self.verbose: print "Time for %s: %.4f" %(algorithm, time.time() - t0)
        self.oVec = self.oVec + dx

        if self.probeRetrieval:
            if self.iGlobal > 10:
                if self.__i % int(self.__param['illuminationPeriod']) == 0:
                    #self.__Q_O()
                    #self.oSum = self.__QH(self.oVec)
                    self.__Pi_P(psi_f) #original probe retrieval
                    #self.__Pi_P(self.__Pi_F(self.oVec))


    def __Q(self, osum):

        """
        Private function which splits the total object image into a stack of frames
        Input: total object image
        Output: stack of frames
        """

        o = np.zeros(self.oVec.shape, complex)
        for k in range(self.nModes):
            for i in range(self.__nFrames):
                j = self.__indices[i]
                o[k,i] = osum[0, 0, j[0]:j[1], j[2]:j[3]]
        return o

    def __QP(self, o):

        """
        Private function which multiplies the frames by the probe
        Input: stack of frames
        Output: stack of frames times the probe
        """

        return o * self.probe

    def __QPH(self, o):

        """
        Private function which multiplies the frames by the conjugate probe.
        Input: stack of frames
        Output: stack of frames times the conjugate probe
        """

        return o * self.probe.conjugate()
    
    def __QH(self, ovec):

        """
        Private function which computes the overlap from stack of frames.
        Input: Stack translation indices and the stack of frames
        Output: Total object image
        """

        osum = np.zeros(self.oSum.shape, complex)
        for i in range(self.__nFrames):
            j = self.__indices[i]
            osum[:, 0, j[0]:j[1], j[2]:j[3]] = osum[:, 0, j[0]:j[1], j[2]:j[3]] + ovec[:,i]
        return osum

    def Q(self, oSum, oVec, indices):

        """
        Private function which splits the total object image into a stack of frames
        Input: total object image
        Output: stack of frames
        """

        o = np.zeros(self.oVec.shape, complex)
        for k in range(self.nModes):
            for i in range(self.__nFrames):
                j = self.__indices[i]
                o[k,i] = oSum[0, 0, j[0]:j[1], j[2]:j[3]]
        return o

    def QH(self, ovec, indices, pPosVec, oSumShape):

        """
        Public function which computes the overlap from stack of frames.
        Input: Stack translation indices and the stack of frames
        Output: Total object image
        """

        osum = np.zeros(self.oSum.shape, complex)
        for i in range(self.__nFrames):
            j = self.__indices[i]
            osum[:, 0, j[0]:j[1], j[2]:j[3]] = osum[:, 0, j[0]:j[1], j[2]:j[3]] + ovec[:,i]
        return osum

    def QP(self):

        """
        Private function which multiplies the frames by the probe
        Input: stack of frames
        Output: stack of frames times the probe
        """

        return self.oVec * self.probe

    def probeStack(self):

        """
        Generates a stack of probes.  Used in probe retrieval version 2.
        Input: None
        Output: NxMxM matrix with each frame containing the probe.
        """

        return (np.zeros(self.dVec.shape, complex) + (1 + 1j)) * self.probe

    def QHP(self):

        """
        Public function which computes the overlap from a stack of probes.  This is equivalent to the total illumination profile
        Input: Stack translation indices and the probe
        Output: Illumination profile
        """

        isum = np.zeros(self.oSum.shape)
        for i in range(self.__nFrames):
            j = self.__indices[i]
            isum[0, 0, j[0]:j[1],j[2]:j[3]] = isum[0, 0, j[0]:j[1],j[2]:j[3]] + abs(self.probe.sum(axis = 0)[0])
        return isum

    
    def __Qnorm(self):

        """
        Function which normalizes the current image with respect to the total illumination
        Input: frames in real space
        Output: Normalization term
        """

        #calculate the normalization term
        qnorm = self.__QPH(self.__QP(np.ones(self.oVec.shape, complex)))
        qnorm = self.__QH(qnorm) + self.__delta**2
        tr = 1. / (self.__delta * abs(self.probe).max()**2)
        qnorm = (1. / qnorm) * ((1. / qnorm) < tr)
        qnorm[np.isfinite(qnorm) == False] = 0.
        return qnorm

    def __Pi_O(self, o):

        """
        The overlap projection: updates the current image and then splits into frames multiplied by the probe.
        Input: frames in real space
        Output: frames in real space
        """

        self.oSum = self.__QH(self.__QPH(o)) * self.__Qnorm()
        return self.__QP(self.__Q(self.oSum))

    def __Q_O(self):

        """
        Updates the current image using the current frames in real space
        """

        self.oSum = self.__QH(self.__QPH(self.oVec)) * self.__Qnorm()
        
    def __probeStack(self):

        """
        Generates a stack of probes.  Used in probe retrieval version 2.
        Input: None
        Output: NxMxM matrix with each frame containing the probe.
        """

        return (np.zeros(self.dVec.shape, complex) + (1 + 1j)) * self.probe


    def __Pi_P(self, psi_f):

        """
        Updates the current probe using the current image and the previous stack satisfying the modulus projection.
        Input: frames in real space, after modulus projection
        Output: Updated probe
        """

        psi_o = self.__Q(self.oSum) #split the object into frames
        numerator = ((psi_f - self.__QP(psi_o)) * psi_o.conjugate()).sum(axis = 1) #numerator of the probe correction term
        denominator = (psi_o * psi_o.conjugate()).sum(axis = 1) + self.__delta #denominator of the probe correction term plus regularization term
        deltaProbe = numerator / denominator
        temp = self.probe + deltaProbe.reshape(self.nModes, 1, self.ny, self.nx) #add the correction term to the probe
        self.probe = temp * norm(self.probe)/ norm(temp) #renormalize

        self.__Pf_probe()
        #if self.useProbeMask: #multiply the FFT of the probe by a mask to filter out pathologies
        #    if self.probeRmask != None: self.probe *= self.probeRmask
        #    self.__Pf_probe()


    def __Pi_P2(self, psi_f):

        """
        Power Iteration
        Updates the current probe using the current image and the previous stack satisfying the modulus projection.
        Input: frames in real space, after modulus projection
        Output: Updated probe
        """

        probes = (np.ones(self.dVec.shape) * (1 + 1j)) * self.probe
        QQ = self.__Pi_O(abs(probes)**2)
        mm = (probes.conjugate() * psi_f).sum() / (abs(probes)**2).sum()
        frames -= mm * probes
        Qz = self.__QH(psi_f * probes.conjugate())
        Tz2 = self.__QH(abs(psi_f)**2)
        p = (psi_f * self.__Q(Qz).conjugate()).sum(axis = 0) / (self.__Q(Tz2)).sum(axis = 0)
        p = self.__Pf_probe(p = p)
        self.probe = p * norm(probes) / norm(p)

    def __Pf_probe(self, p = None):

        """
        Applies a Fourier mask on the probe.  This mask is supplied in the CXI file and is generally calculated as a
        threshold on the average diffraction data.  It is the pupil which is used to generate the initial estimate of
        the probe.  Only applied on the primary mode.
        Input: current probe and Fourier mask
        Output: Updated probe
        """

	
        if p == None:
            if self.probeRmask != None: self.probe *= self.probeRmask
            for i in range(self.nModes):
                if self.useIlluminationIntensities:
                    pf = np.fft.fftn(self.probe)
                    pf_norm = np.abs(pf).sum()
                    pf = pf * np.sqrt(self.illuminationIntensities) / np.abs(pf)
                    pf = pf * pf_norm / np.abs(pf).sum()
                    self.probe[i,0] = np.fft.ifftn(pf)
                elif self.useProbeMask: self.probe[i,0] = np.fft.ifftn(np.fft.fftn(self.probe[i,0]) * self.probeMask)
                else: pass
        else:
            if self.probeRmask != None: p *= self.probeRmask
            if self.useIlluminationIntensities:
                pf = np.fft.fftn(p)
                p[0,0] = fft.ifftn(pf * np.sqrt(self.illuminationIntensities) / np.abs(pf))
            elif self.useProbeMask: p[0,0] = np.fft.ifftn(np.fft.fftn(p[0,0]) * self.probeMask)
            else: pass
            return p


    def __Pi_F(self, o):

        """
        Fourier modulus projection with background retrieval option.
        Input: current stack of frames, current background, and the data stack.
        Output: Updated stack of frames and updated background
        """

        #calculate the Fourier transform (transforms are normalized)
        f = self.__pFFT(o) #G=mpsi_fyfft2(g); %Forward transform to Fourier space

        #matlab code in comments (smv is a bsxfun something or other...)
        if self.bgRemoval:
            if self.iGlobal > 10:
                if self.__i % int(self.__param['backgroundPeriod']) == 0:
                    fI = (abs(f)**2).sum(axis = 0) #G2=abs(G).^2; %Fourier intensity
                    d = self.dVec**2 - self.bg #d= smv(Idata,bkg);   %frames-2d background.  dVec is sqrt(data)
                    etai = (fI * d).sum(axis = 0) / (d**2).sum(axis = 0)
                    #etai = (d**2).sum(axis = 0) / (fI * d).sum(axis = 0) #sum(d.^2,3)./sum(G2.*d,3); %normalization factor
                    etai[etai < self.eta] = self.eta
                    self.bg += (d - fI / (etai)).mean(axis = 0)  #bkg=bkg+mean(d-stv(G2,etai),3); %update background
                    self.bg[self.bg < 0.] = 0.
                    self.bg[np.isnan(self.bg)] = 0. #clean up

                    if self.useProbeMask: self.bg = self.bg * (1. - self.probeMask)
                    else: pass

                    d = self.dVec**2 - self.bg #d= smv(Idata,bkg); %
                    d[d < 0.0] = 0.0
                    fScaleFactor = np.sqrt(d / fI)
                    self.dMask2 = (1. - self.dMask) * (abs(f) < self.saturationThreshold)
                    fScaleFactor = fScaleFactor * self.dMask + (1. - self.dMask)
                    f = f * fScaleFactor # * (self.dMask - self.dMask2)#; %Fourier magnitudes (enforce positivity)
                    f[np.isfinite(f) == False] = 0. #clean up

        else: #normal modulus projection
            self.dMask2 = (1. - self.dMask) * (abs(f) < self.saturationThreshold)
            f = self.dMask  * self.dVec * f / (abs(f)).sum(axis = 0) + (1 - self.dMask) * f # + self.dMask2 * self.saturationThreshold
            f[np.isfinite(f) == False] = 0. #clean up
        return self.__pFFTi(f) #return inverse transform

    def save(self, fileName = None):

        self.oSum = self.__crop()
        self.oSum = self.__phaseRamp(self.oSum)

        if fileName == None: fileName = self.__dataPath + self.__fileName
        f = h5py.File(fileName,'a')

        if list(f) == []:
            entry_1 = f.create_group("entry_1")

        entryList = [str(e) for e in list(f['entry_1'])]
        try: currentImageNumber = max(loc for loc, val in enumerate(entryList) if not(val.rfind('image')))
        except:
            print "No previous image data in cxi file.  Generating..."
            entry_1 = f['entry_1']
            image_1 = entry_1.create_group('image_1')
            image_1.create_dataset('data', data = self.oSum)
            process_1 = entry_1.create_group('image_1/process_1')
            process_1.create_dataset('final_background', data = self.bg)
            process_1.create_dataset('final_illumination', data = self.probe)
        else:
            if currentImageNumber == 0: currentImageNumber += 1
            newImageNumber = str(currentImageNumber + 1)
            print "Found image_%s in CXI file" %str(currentImageNumber)
            print "Adding new image_%s to CXI file" %newImageNumber
            newImage = 'entry_1/image_' + newImageNumber + '/'
            dset = f.create_dataset(newImage + 'data', data = self.oSum)
            dset = f.create_dataset(newImage + 'startImage', data = self.previousImage)
            newProcess = 'entry_1/image_' + newImageNumber + '/process_1/'
            dset = f.create_dataset(newProcess + 'final_background', data = self.bg)
            dset = f.create_dataset(newProcess + 'final_illumination', data = self.probe)
            del f['entry_1/instrument_1/detector_1/probe']
            dset = f.create_dataset('entry_1/instrument_1/detector_1/probe', data = self.probe)
        f.close()

        # temp = thisData.stxmInterp
        # im = scipy.misc.toimage(temp, high = temp.max(), low = temp.min(), mode = 'F')
        # fileName =  param['dataPath'] + 'tiffs/stxm/' + 'stxm_' + thisAngle + '.tif'
        # im.save(fileName)
        #
        # temp = np.angle(thisData.image)
        # temp = removePlane(temp)
        # imsave(temp,param['dataPath'] + 'pngs/phase/' + 'projection_phase_' + thisAngle + '.png', ct = 'bone')
        # im = scipy.misc.toimage(temp, high = temp.max(), low = temp.min(), mode = 'F')
        # fileName =  param['dataPath'] + 'tiffs/phase/' + 'projection_phase_' + thisAngle + '.tif'
        # im.save(fileName)
        #
        # temp = np.abs(thisData.image)**2
        # imsave(temp, param['dataPath'] + 'pngs/intensity/' + 'projection_intensity_' + thisAngle + '.png', ct = 'bone')
        # im = scipy.misc.toimage(temp, high = temp.max(), low = temp.min(), mode = 'F')
        # fileName =  param['dataPath'] + 'tiffs/intensity/' + 'projection_intensity_' + thisAngle + '.tif'
        # im.save(fileName)

        print "Saved results to file: %s" %(fileName)

    def getStartImages(self):
        # if self.useStxm:
        #     if self.verbose: print "Starting from STXM image."
        #     sy,sx = self.stxm.shape
        #     if (sy % 2) != 0: self.stxm = self.stxm[1:sy,:]
        #     if (sx % 2) != 0: self.stxm = self.stxm[:,1:sx]
        #     sy,sx = self.stxm.shape
        #     self.oSum = np.zeros((self.oSizeY,self.oSizeX),complex)
        #     self.oSum[self.oSizeY / 2 - sy / 2:self.oSizeY / 2 + sy / 2, self.oSizeX / 2 - \
        #         sx / 2:self.oSizeX / 2 + sx / 2] = np.sqrt(self.stxm) * np.exp(1j * np.pi / 2)# * rand(sy,sx))
        #     self.bg = np.zeros(self.oSizeY, self.oSizeX)

        if self.usePrevious:
            if self.verbose: print "Starting from previous reconstruction"
            self.oSum = self.previousImage.copy()
            self.oSizeY, self.oSizeX = self.oSum[0,0].shape

        else:
            if self.verbose: print "Starting from random field."
            self.oSum = rand(self.oSizeY, self.oSizeX) * np.exp(-1j * rand(self.nModes, 1, self.oSizeY, self.oSizeX) * np.pi / 2.)
            self.bg = np.zeros(self.oSizeY, self.oSizeX)

    def start(self, init = True, random_start = True, verbose = False):

        self.previousImage = self.oSum.copy()

        if self.probeMask != None:
            if verbose: print "Using Fourier mask on the probe."

        if self.algorithm in ["raar","dm"]:
            if verbose: print "Starting %i iterations of the %s algorithm..." %(self.nit,self.algorithm)
            self.oVec = self.__QP(self.__Q(self.oSum))
            for self.__i in range(self.nit):
                t0 = time.time()
                self.__iterate(self.algorithm)
                if ((self.__i + 1) % 10) == 0:
                    #self.oSum = self.__QH(self.__QPH(self.oVec)) * self.__Qnorm()
                    self.__errorMetric()
                    self.errhist.append(self.e)
                if self.verbose: print "Error for iteration %s of %s: %.4f" %(self.iGlobal, self.nit, self.e)
                self.iGlobal += 1

        else: print "Unsupported algorithm: %s" %self.algorithm

        return


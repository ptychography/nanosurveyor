import scipy
import numpy as np
import Image
from scipy.cluster.vq import kmeans2
import pylab as plt
import os
from dslib.dtfile.readXIM import *
import scipy
from scipy import misc
from dslib import saveTiffData, imload, shift

###User input
dataPath = '/home/david/scratch/LiFePO4-images/complex/good/aligned/'
dataPath = '/home/david/scratch/LiFePO4-images/ptycho-phase/'
dataPath = '/home/david/scratch/LFP-141017/tiffs/complex/aligned/'
#dataPath2 = '/home/david/scratch/LiFePO4-images/ptycho-intensity/'
imageType = 'complex' #'intensity' or 'phase' or 'stxm' or #complex
nClusters = 3 #1 more than the number of chemical components
background_threshold_value = 50  #Get this by opening an image in Nanosurveyor.  set to FALSE for AUTO
saveOutput = False #set to False if you don't want to save the output images
displayOutput = True #set to False if you don't want to display the output images
realImage = True

#########################################################################################################
#####Create directory to save the output
if os.path.isdir(dataPath + 'output'): outDir = dataPath + 'output/'
else: os.mkdir(dataPath + 'output'); outDir = dataPath + 'output/'

#########################################################################################################
#########################################################################################################
def loadImages(dataPath):
    if os.path.isdir(dataPath): print "Found data directory: %s" %dataPath
    else:
        print "Could not find data directory: %s" %dataPath
        return 0

    ##load the energies from the energies.txt file in the dataPath directory
    ev = []
    if os.path.isfile(dataPath + 'energies.txt'):
        f = open(dataPath + 'energies.txt', 'r')
        for item in f:
            ev.append(np.single(item))
        f.close()
    else:
        print "Could not locate energies file."
        return 0

    n_ev = len(ev)
    imageFileList = []
    for f in os.listdir(dataPath):
        if f.endswith('.tif'): imageFileList.append(f)
    if f.endswith('.xim'): imageFileList.append(f)

    if len(imageFileList) == 0:
        print "Could not locate any tif files in data directory."-55.00
        print "Make sure image files end with extension tif."
        return 0

    imageFileList = sorted(imageFileList)

    if len(imageFileList) != n_ev:
        print "Wrong number of energies in energies.txt file."
        return 0

    i = 0
    for item in imageFileList:
        if imageType == 'stxm':
            thisImage = misc.imread(dataPath + item).astype('float64')[::-1,:]
        else: 
            thisImage = misc.imread(dataPath + item).astype('float64')
        if i == 0:
            sh = thisImage.shape
            n_pixels = sh[0] * sh[1]
            if len(sh) == 3:
                images = np.zeros((sh[0],sh[1],n_ev), complex); realImage = False
            else: images = np.zeros((sh[0],sh[1],n_ev)); realImage = True
        if realImage: images[:,:,i] = thisImage[:,:]
        else:
            images[:,:,i] = thisImage[:,:,0] + 1j * thisImage[:,:,1]
        i += 1

#    if imageType != 'stxm':
#    #########intensity
#        realImage = 0
#        if os.path.isdir(dataPath2): print "Found data directory: %s" %dataPath2
#        else:
#            print "Could not find data directory: %s" %dataPath2
#            return 0
#
#        ##load the energies from the energies.txt file in the dataPath directory
#        ev = []
#        if os.path.isfile(dataPath2 + 'energies.txt'):
#            f = open(dataPath2 + 'energies.txt', 'r')
#            for item in f:
#                ev.append(np.single(item))
#            f.close()
#        else:
#            print "Could not locate energies file."
#            return 0
#
#        n_ev = len(ev)
#        imageFileList = []
#        for f in os.listdir(dataPath2):
#            if f.endswith('.tif'): imageFileList.append(f)
#        if f.endswith('.xim'): imageFileList.append(f)
#
#        if len(imageFileList) == 0:
#            print "Could not locate any tif files in data directory."-55.00
#            print "Make sure image files end with extension tif."
#            return 0
#
#        imageFileList = sorted(imageFileList)
#
#        if len(imageFileList) != n_ev:
#            print "Wrong number of energies in energies.txt file."
#            return 0
#
#        i = 0
#        for item in imageFileList:
#            if imageType == 'stxm':
#                thisImage = readXIM(dataPath + item).astype('float64')
#            else:
#                thisImage = imload(dataPath2 + item).astype('float64')
#            if i == 0:
#                sh = thisImage.shape
#                n_pixels = sh[0] * sh[1]
#                if len(sh) == 3:
#                    images2 = np.zeros((sh[0],sh[1],n_ev), complex); realImage = False
#                else: images2 = np.zeros((sh[0],sh[1],n_ev)); realImage = True
#            if realImage: images2[:,:,i] = thisImage[:,:]
#            else:
#                images[:,:,i] = thisImage[:,:,0] + 1j * thisImage[:,:,1]
#            i += 1
#
#        imagesComplex = np.zeros(images2.shape,complex)
#        print images2.shape, images.shape, n_ev
#        for i in range(n_ev):
#           #imagesComplex[:,:,i] = np.sqrt(images2[:,:,i]) * np.exp(1j * shift(images[5:187,1:279,i] / 1000.,-2,-4))
#            imagesComplex[:,:,i] = np.sqrt(images2[:,:,i]) * np.exp(1j * shift(images[:,:,i] / 1000.,-2,-4))
#        images = imagesComplex

    mask = np.abs(images).sum(axis = 2)
    mask = mask > 600

    if imageType == 'intensity': images = np.abs(images)
    elif imageType == 'phase':
        if realImage: print "Not taking angle"
        else: images = np.angle(images); print "Taking Angle"
    return images, mask, ev


success = 1
a = loadImages(dataPath)
if a == 0: success = 0
else: images, mask, ev = a

if background_threshold_value: i0Mask = images.mean(axis = 2) > background_threshold_value
else: pass

if success:
    n_ev = len(ev)
    sh = images.shape
    n_pixels  = sh[0] * sh[1]
    images = np.delete(images,1,axis = 2)
    ev.pop(1)
    n_ev -= 1

    if imageType == 'intensity':
        images[images < 0.] = 0.
        i0 = np.zeros((n_ev))
        for i in range(n_ev): i0[i] = (images[:,:,i] * i0Mask).sum() / i0Mask.sum()
        for i in range(n_ev): images[:,:,i] = -np.log(images[:,:,i]/i0[i])

    if imageType == 'stxm':
        images[images < 0.] = 0.
        i0 = np.zeros((n_ev))
        for i in range(n_ev): i0[i] = images[0,0:100,i].mean()#(images[:,:,i] * i0Mask).sum() / i0Mask.sum()
        for i in range(n_ev): images[:,:,i] = -np.log(images[:,:,i]/i0[i])

    if imageType == 'phase':
        i0 = np.zeros((n_ev))
        for i in range(n_ev): i0[i] = images[0,0:100,i].mean()#(images[:,:,i] * i0Mask).sum() / i0Mask.sum()
        for i in range(n_ev): images[:,:,i] = images[:,:,i] - i0[i]

    if imageType == 'complex':
        i0Int = np.zeros((n_ev))
        i0Phase = np.zeros((n_ev))
        i0Complex = np.zeros((n_ev),complex)
        for i in range(n_ev): i0Int[i] = (np.abs(images[:,:,i]) * i0Mask).sum() / i0Mask.sum()
        for i in range(n_ev): i0Phase[i] = (np.angle(images[:,:,i]) * i0Mask).sum() / i0Mask.sum()
        for i in range(n_ev): i0Complex[i] = (images[:,:,i]).sum() / i0Mask.sum()
        for i in range(n_ev): images[:,:,i] = -np.log(images[:,:,i] / i0Int[i])
        for i in range(n_ev): images[:,:,i] = abs(images[:,:,i]) * np.exp(-1j * (np.angle(images[:,:,i]) - i0Phase[i]))


    # for i in range(n_ev):
    #     images[:,:,i] = images[:,:,i] * mask + images.min()

    images[np.isinf(images)] = 0.

    p = np.reshape(images, (n_pixels, n_ev), order = 'F')
    covmatrix = np.dot(p.T, p.conjugate())
    eigenvals, eigenvecs = np.linalg.eigh(covmatrix)
    perm = np.argsort(-np.abs(eigenvals))
    eigenvals = eigenvals[perm]
    eigenvecs = eigenvecs[:,perm]
    pcaimages = np.dot(p, eigenvecs)
    pcaimages = np.reshape(pcaimages, (sh[0],sh[1],n_ev), order = 'F')

    clusters = np.zeros(n_pixels)
    res, clusters = kmeans2(np.reshape(pcaimages[:,:,1:nClusters + 1],(n_pixels, nClusters), order = 'F'), nClusters)
    clusters = np.reshape(clusters, (sh[0], sh[1]), order = 'F')

    spectra = []
    for i in range(nClusters):
        if imageType == 'complex': spectra.append(np.zeros((n_ev), complex))
        else: spectra.append(np.zeros((n_ev)))

    for i in range(n_ev):
        thisImage = images[:,:,i]
        for j in range(nClusters):
            thisVal = thisImage[clusters == j].mean()
            spectra[j][i] = thisVal

    target_spectra = np.vstack(spectra[0:-1])

    ###########
    ##SVD

    U, s, V = np.linalg.svd(target_spectra, full_matrices = False)
    mu_inverse = t_inverse = np.dot(np.dot(V.T, np.linalg.inv(np.diag(s))), U.T)
    target_svd_maps = np.dot(images, mu_inverse.conjugate())
    target_svd_maps = np.reshape(target_svd_maps, (sh[0], sh[1], nClusters - 1), order = 'F').real

    a = np.zeros((sh[0],sh[1],3))
    a[:,:,2] = target_svd_maps[::1,:,0]# * mask
    a[:,:,0] = target_svd_maps[::1,:,1]# * mask
    im = scipy.misc.toimage(a)

    meanContrast = (a[:,:,0] * mask).sum() / mask.sum()
    stdContrast = np.sqrt(((a[:,:,0] - meanContrast)**2 * mask).sum() / mask.sum())

    if displayOutput:
        plt.figure()
        plt.subplot(331)
        plt.imshow(clusters)
        plt.title('Clusters')
        plt.subplot(332)
        plt.imshow(images[:,:,10].real)
        plt.title('OD')
        plt.subplot(333)
        plt.imshow(pcaimages[:,:,0].real)
        plt.title('PCA-0')
        plt.subplot(334)
        plt.imshow(pcaimages[:,:,1].real)
        plt.title('PCA-1')
        plt.subplot(335)
        plt.imshow(pcaimages[:,:,2].real)
        plt.title('PCA-2')
        plt.subplot(336)
        plt.imshow(target_svd_maps[:,:,0])
        plt.title('SVD-0')
        plt.subplot(337)
        plt.imshow(target_svd_maps[:,:,1])
        plt.title('SVD-1')
        plt.subplot(338)
        plt.imshow(im)
        plt.title('SVD Map')
        plt.subplot(339)
        #plt.plot(ev,specFit1,'r-')
        #plt.plot(ev,specFit2,'b-')
        # plt.plot(ev,specFit3,'g-')
        # plt.legend(('LFP','FP'),loc = 'upper right')
        # plt.title('Spectra')
        # plt.axis((703,716,0.2,1.5))
        plt.show()

    if saveOutput:
        saveTiffData(clusters, outDir + "clusters.tif")
        for i in range(nClusters - 1):
            saveTiffData(pcaimages[:,:,i], outDir + "PCA-" + str(i) + ".tif")
        for i in range(nClusters - 1):
            saveTiffData(target_svd_maps[:,:,i], outDir + "SVD-" + str(i) + ".tif")



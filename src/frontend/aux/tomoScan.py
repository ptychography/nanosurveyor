__author__ = 'nsptycho'

from aux.executeParam import *
from aux.param import *
from aux.readCXI import *
from aux.imageIO import imsave
from aux.readXIM import *
from aux.util import removePlane
import numpy as np
import os
import scipy

def tomoscan(param, preprocess = True, reconstruct = True, cluster = False, cpu = True, saveOutput = True, debug = False):

    if not(os.path.isdir(param['dataPath'])):
        print "Could not locate tomography scan directory."
        return 1


    #######################################################################################################################
    #######################################################################################################################
    ##Load available HDR files and parse some meta data
    hdrFiles = []
    tomoScans = []
    bgScans = []
    bgScansList = []
    tomoAngles = []
    bgAngles = []

    #check to see if there is a progress file from other runs
    if os.path.isfile(param['dataPath'] + 'progress.txt'): os.remove(param['dataPath'] + 'progress.txt')

    #make a list of directories containing tomo scans
    scanList = os.listdir(param["dataPath"])
    tomoScans = []
    bgScans = []

    for item in scanList:
        if os.path.isdir(param['dataPath'] + item):
            if len(item) == 9:
                thisScanList = sorted(os.walk(param['dataPath'] + item).next()[1])
                tomoScans.append([item + '/' + x + '/' for x in thisScanList[0::2]])
                bgScans.append([item + '/'  + x + '/' for x in thisScanList[1::2]])

    #reshape the list
    tomoScans = [scan for item in tomoScans for scan in item]
    bgScans = [scan for item in bgScans for scan in item]
    ignoring = []

    #get the angles from the text files in the sub-directories and check number of tiffs
    for item in tomoScans:
        scanHDR = open(param['dataPath'] + item + 'NS_' + item.split('/')[0] + '_' + '001' + '.txt','r')
        nFrames = len([tifFile for tifFile in os.listdir(param['dataPath'] + item) if tifFile.count('tif')])
        if nFrames < int(param['ypts']) * int(param['xpts']):
            print "Scan %s has only %i frames but exptected %i. Ignoring." %(item, nFrames, int(param['ypts']) * int(param['xpts']))
            ignoring.append(item)
        else:
            tomoAngles.append(round(single(scanHDR.readline().split(',')[1].split('=')[1]),2))
        scanHDR.close()
    for item in ignoring: tomoScans.remove(item)


    #######################################################################
    ########################################################################
    ##Open a file to report the status
    # f = open(param['dataPath'] + 'progress.txt', 'w')
    # f.close()

    ########################################################################
    ########################################################################
    ##This function just executes the parameter list from the subprocesses started below
    def callExecuteParam(param,scanDir = None,dataFile = None, ang = None, prelim = False):
        if scanDir != None: param['scanDir'] = param['dataPath'] + scanDir
        if dataFile != None: param['dataFile'] = dataFile
        if ang != None: param['angle'] = ang
        ##############################################################param["interferometer"] = '1'
        ##############################################################
        #Check, Execute, save parameters, and display
        runParam(param)

    paramList = []
    #######################################################################
    ########################################################################
    ##Go through the list of tomo scans and pre-process plus initial GPU reconstruction in series
    for scan,bgScan,angle in zip(tomoScans,bgScans,tomoAngles):
        param["angle"] = str(angle)
        param["scanDate"] = scan[:6] #date string of the tomo scan
        param["scanNumber"] = scan[6:9]
        param["scanID"] = scan[-4:-1]
        param["bgScanNumber"] = bgScan[6:9]
        param["bgScanID"] = bgScan[-4:-1]
        param['scanDir'] = param['dataPath'] + scan #directory with the tomo scan for a given angle
        param['bgScanDir'] = param['dataPath'] + bgScan #directory with the BG scan for that same angle
        param['dataFile'] = 'NS_' + scan.split('/')[0] + '_' + scan.split('/')[1] + '.cxi' #CXI file name #resulting CXI file
        param['outputFilename'] = param['dataFile']  
        param["gpu"] = '1' #GPU flag: set '1' for GPU and '0' otherwise
        param["saveFile"] = '0' #set to filename for custom file name, otherwise leave as '0'
        param['paramFileName'] = '0'
        if cluster: param['useCluster'] = '1'
        else: param['useCluster'] = '0'
        if preprocess: param['process'] = '1'
        else: param['process'] = '0'
        if reconstruct: param['reconstruct'] = '1'
        else: param['reconstruct'] = '0'
        param["interferometer"] = '0'

        ###append to the parameter List
        paramList.append(param.copy())

    ###Sort the parameter list according to angle
    indices = sorted(range(len(tomoAngles)), key = lambda k: tomoAngles[k])
    paramList = [paramList[i] for i in indices]

    if debug:
        for p in paramList:
            print "Angle: %s, Data dir: %s, Data file: %s, BG dir: %s" %(p['angle'],p['scanDir'], p['dataFile'], p['bgScanDir'])

    #############################################################
    #############################################################
    ##############Check, Execute, save parameters
    if not(debug):
        for param in paramList: callExecuteParam(param, prelim = True)

    if cpu:
        ##############################################################
        ##############################################################
        ##Apply switches for final reconstruction using the CPU
        for param in paramList:
            param['process'] = '0'
            param['reconstruct'] = '1'
            param['gpu'] = '0'

        ###########################################################################
        ###########################################################################
        ##Generate a pool of subprocesses and start CPU reconstructions in parallel, 10 at a time
        for param in paramList: callExecuteParam(param)


    ###########################################################################
    ###########################################################################
    ###Generate tiff files and exit
    if saveOutput:
        minAngle = min(tomoAngles)
        print "Generating output images..."
        if not(os.path.isdir(param['dataPath'] + 'tiffs')): print "Making tiff directory."; os.mkdir(param['dataPath'] + 'tiffs')
        if not(os.path.isdir(param['dataPath'] + 'pngs')): print "Making png directory."; os.mkdir(param['dataPath'] + 'pngs')
        if not(os.path.isdir(param['dataPath'] + 'tiffs/phase')): print "Making tiff directory."; os.mkdir(param['dataPath'] + 'tiffs/phase')
        if not(os.path.isdir(param['dataPath'] + 'tiffs/intensity')): print "Making tiff directory."; os.mkdir(param['dataPath'] + 'tiffs/intensity')
        if not(os.path.isdir(param['dataPath'] + 'tiffs/stxm')): print "Making tiff directory."; os.mkdir(param['dataPath'] + 'tiffs/stxm')
        if not(os.path.isdir(param['dataPath'] + 'pngs/phase')): print "Making png directory."; os.mkdir(param['dataPath'] + 'pngs/phase')
        if not(os.path.isdir(param['dataPath'] + 'pngs/intensity')): print "Making png directory."; os.mkdir(param['dataPath'] + 'pngs/intensity')

        ###Open a text file to save the list of angles
        f = open(param['dataPath'] + 'angles.txt', 'w')

        ##Loop through and save the tiffs and pngs
        i = 0
        for param in paramList:
            f.write(param['angle'] + '\n')
            thisAngle = str(float(param['angle']) - minAngle)
            thisData = readCXI(param['scanDir'] + param['dataFile'])

            temp = thisData.stxmInterp
            im = scipy.misc.toimage(temp, high = temp.max(), low = temp.min(), mode = 'F')
            fileName =  param['dataPath'] + 'tiffs/stxm/' + 'stxm_' + thisAngle + '.tif'
            im.save(fileName)

            temp = np.unwrap(np.angle(thisData.image))
            temp = removePlane(temp)
            imsave(temp,param['dataPath'] + 'pngs/phase/' + 'projection_phase_' + thisAngle + '.png', ct = 'bone')
            im = scipy.misc.toimage(temp, high = temp.max(), low = temp.min(), mode = 'F')
            fileName =  param['dataPath'] + 'tiffs/phase/' + 'projection_phase_' + thisAngle + '.tif'
            im.save(fileName)

            temp = np.abs(thisData.image)**2
            imsave(temp, param['dataPath'] + 'pngs/intensity/' + 'projection_intensity_' + thisAngle + '.png', ct = 'bone')
            im = scipy.misc.toimage(temp, high = temp.max(), low = temp.min(), mode = 'F')
            fileName =  param['dataPath'] + 'tiffs/intensity/' + 'projection_intensity_' + thisAngle + '.tif'
            im.save(fileName)
            i += 1
        f.close()

    ###########################################################################
    ###########################################################################
    ###Finish
    print "Done!"

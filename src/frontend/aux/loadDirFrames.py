__author__ = 'david'

import os, sys
from imageIO import imload
from numpy import zeros

def loadDirFrames(scanDir, filePrefix, expectedFrames, cropSize, multi = False, fCCD = True, \
                  frame_offset = 0, verbose = True):

    if multi: nexp = 2
    else: nexp = 1
    numJobs = 0
    i = 0
    while (i < expectedFrames):
        sys.stdout.write("Loading frame %i \r" %i)
        sys.stdout.flush()
        frame_num = i + frame_offset
        if fCCD:
            filename = scanDir + filePrefix + str(frame_num) + ".tif"
        else:
            frame_num += 1
            if frame_num < 10:
                fileNumStr = '00' + str(frame_num)
                filename = scanDir + filePrefix + fileNumStr + ".tif"
            elif frame_num < 100:
                fileNumStr = '0' + str(frame_num)
                filename = scanDir + filePrefix + fileNumStr + ".tif"
            else:
                fileNumStr = str(frame_num)
                filename = scanDir + filePrefix + fileNumStr + ".tif"
        if fCCD:
            data = imload(filename).astype('single')[500:1500,:]
        else:
            data = imload(filename).astype('single')
        if i == 0:
            if fCCD: ny, nx = cropSize, cropSize
            else: ny, nx = data.shape
            if verbose: print "Frames are %i x %i pixels (H x V)" %(nx,ny)
            dataStack = zeros((expectedFrames, data.shape[0],data.shape[1]))

        ###These lines are because STXM control sometimes generates non-square arrays when using PVCAM
        ###One index is usually 1-2 pixels shorter than the other
        ###not really sure why it has to be square though
        ####TODO: allow for non-square input arrays
#        if nx > ny: data = data[:,0:ny]
#        elif ny > nx: data = data[0:nx,:]
        #####
        #####

        dataStack[i,:,:]= data
        i += 1
        loadedFrames = i

    return dataStack, loadedFrames
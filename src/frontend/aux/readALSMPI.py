__author__ = 'david'
import time, os, datetime, sys
from util import dist, shift, circle
from imageIO import imload
from numpy import *
from scipy.misc import imsave
from scipy import random, ndimage, interpolate
import Image
import fftw3
from scipy.interpolate import interp1d
from scipy import polyfit, misc, ndimage, mgrid, rand
from scipy.ndimage.filters import gaussian_filter, maximum_filter, minimum_filter
from processFCCD import fitOverscan, processfCCDFrame
from propagate import propnf
from loadDirFrames import *
from aveDirFrames import *
from mpi4py import MPI
from param import Param
from executeParam import *
import numpy as np
from piUtils import readSpe
# from pycallgraph import PyCallGraph
# from pycallgraph.output import GraphvizOutput

##TODO: MPI4PY comm.send() fails if the datasets are bigger than 200x512x512.  I need to queue larger datasets so the chunks are small enough

def processor(comm, verbose = False):

    if verbose: print "Processor %i started" %(comm.Get_rank())

    ###############################################################################################
    ###get data from the master process
    dataStruct = comm.recv(source=0, tag = 11)

    ###############################################################################################
    ###set up some parameters
    nSlices = len(dataStruct[0])
    bg = dataStruct[1]
    sh_pad = dataStruct[2]
    sh_sample = dataStruct[3]
    filter = dataStruct[4]
    xc = dataStruct[5]
    yc = dataStruct[6]
    dfilter = dataStruct[7]
    t_exp = dataStruct[8]
    saturation_threshold = dataStruct[9]
    noise_threshold = dataStruct[10]
    stxmMask = dataStruct[11]
    tiff = dataStruct[12]
    numpyFFT = dataStruct[13]
    bl = dataStruct[14]
    fCCD = dataStruct[15]
    fv = dataStruct[16]
    fh = dataStruct[17]
    tr = dataStruct[18]
    multi = dataStruct[19]
    verbose = False#dataStruct[20]
    fCCD_version = dataStruct[21]
    fNorm = dataStruct[22]
    i1, i2 = dataStruct[23]
    indexd = dataStruct[24]
    indexo = dataStruct[25]
    cropSize = dataStruct[26]
    os_bkg = dataStruct[27]
    bg_short = dataStruct[28]
    low_pass = dataStruct[29]

    y,x = dataStruct[0][0].shape
    ny, nx = sh_pad
    nyn, nxn = sh_sample
    df = dfilter
    nl = noise_threshold


    ###############################################################################################
    ###define the array for the processed data
    data = np.zeros((len(dataStruct[0]), cropSize, cropSize))

    ###############################################################################################
    ###for fastCCD data launch the fCCD pre-processor for overscan removal and background subtraction
    if fCCD:
        if multi:
            for i in range(len(dataStruct[0])):
                data[i,:,:] = processfCCDFrame(dataStruct[0][i,:,:], dataStruct[0][i + 1,:,:], bg, bg_short, os_bkg,\
                    indexd, indexo, t_exp, saturation_threshold,noise_threshold, xc,yc,fNorm,i,cropSize,fCCD_version)
                i += 1
        else:
            for i in range(len(dataStruct[0])):
                data[i,:,:] = processfCCDFrame(dataStruct[0][i,:,:], None, bg, bg_short, os_bkg, indexd, indexo, t_exp, \
                    saturation_threshold, noise_threshold, xc, yc, fNorm, i, cropSize, fCCD_version)

    ###############################################################################################
    #if verbose:
    if verbose: sys.stdout.write("Processing frames...");sys.stdout.flush()

    ###############################################################################################
    ###background subtraction is already done so just remove the final "noise level" offset
    if fCCD:
        ###redefine the center since the fastCCD preprocessor centers the data
        y,x = data[0].shape
        xc,yc = data[0].shape[1] / 2, data[0].shape[0] / 2

    ##############################################################################################
    ###Background subtract and merge for non-fastCCD data
    elif multi:
        nSlices = nSlices / 2
        data = dataStruct[0]
        data[0::2,:,:] -= bg ##subtract background from long exposures
        data[0::2,:,:] *= (data[0::2,:,:] > nl)
        data[1::2,:,:] -= bg_short ##subtract background from short exposures
        data[1::2,:,:] *= (data[1::2,:,:] > nl)
        mask = (data[0] > saturation_threshold)
        mask = 1. - mask / mask.max()
        data[0::2,:,:] = mask * data[0::2,:,:] + (1 - mask) * data[1::2,:,:] * t_exp[0] / t_exp[1]
        data = data[0::2,:,:]

    else:
        data = dataStruct[0]
        data -= (bg + nl)
        data *= data > 0.

    dy, dx = float(nyn) / float(ny),float(nxn) / float(nx)
    dyn, dxn = int(float(y) * dy), int(float(x) * dx)
    data -= nl
    data = data * (data > 0.)

    ###############################################################################################
    ####Low Pass Filter
    if df:
        if verbose: print "Applying filter on data transform"

        data = data * (1+0j)

        #############Do it this way to reuse the plan, requires some data copies
        input_array = zeros_like(data[0])
        output_array = zeros_like(data[0])
        fftForwardPlan = fftw3.Plan(input_array,output_array,'forward')
        fftBackwardPlan = fftw3.Plan(input_array,output_array,'backward')
        for i in range(len(data)):
            copyto(input_array, data[i])
            fftw3.guru_execute_dft(fftForwardPlan, input_array, output_array)
            copyto(input_array, output_array * filter)
            fftw3.guru_execute_dft(fftBackwardPlan, input_array, output_array)
            copyto(data[i], output_array)

        data = abs(data) / (data[0].size) #renormalize after transforms

    ###############################################################################################
    ###RESAMPLE and ZERO PAD
    if dyn < dxn:
        q = dxn
    else: q = dyn

    dataStruct[0] = zeros((nSlices,nyn, nxn))
    yc = int(float(yc) * float(q) / float(y)) + 1
    xc = int(float(xc) * float(q) / float(x)) + 1

    p1 = nyn / 2 - yc + 1
    p2 = p1 + q
    p3 = nxn / 2 - xc + 1
    p4 = p3 + q

    if p1 < 0 or p3 < 0 or p2 > nyn or p4 > nxn:
        print "Zero-padding failed, try increasing pixels per scan step."
        return

    ##downsample using bivariate spline interpolation
    for i in range(len(data)):
        x, y = np.linspace(0, data[i].shape[0], data[i].shape[0]),np.linspace(0, data[i].shape[1], data[i].shape[1])
        sp = interpolate.RectBivariateSpline(x, y, data[i])
        yn, xn = np.meshgrid(np.linspace(0, data[i].shape[1], q), np.linspace(0, data[i].shape[0], q))
        dataStruct[0][i][p1:p2, p3:p4] = sp.ev(xn.flatten(), yn.flatten()).reshape(q,q)

    if low_pass:
        f = np.exp(-dist(nyn)**2 / 2. / (nyn / 6)**2)
        for i in range(len(data)):
            dataStruct[0][i] = dataStruct[0][i] * f

    ###Add some small background noise or offset zeros with a constant value
    if fCCD: pass
    else: dataStruct[0] *= (dataStruct[0] > nl)
    #dataStruct[0] += (dataStruct[0] == 0.) * random.poisson(1000. * ones(dataStruct[0].shape)) * nl / 200. / 1000.
    #dataStruct[0][dataStruct[0] < 0.0001] = 0.0001

    ####Return
    if fv: dataStruct[0] = dataStruct[0][:,::-1,:]
    if fh: dataStruct[0] = dataStruct[0][:,:,::-1]
    if tr: dataStruct[0] = transpose(dataStruct[0], axes = (0,2,1))
    if fCCD: dataStruct[0] = transpose(rot90(transpose(dataStruct[0])))
    if verbose: print "Sending data from Rank %i" %(comm.Get_rank())
    comm.send(dataStruct, dest = 0, tag = 12)

def main(param, comm, verbose = True):

    ######################################################################################################
    ###load some parameters from the dictionary
    dataPath = param['dataPath']
    scanDate = param['scanDate']
    scanNumber = param['scanNumber']
    scanID = param['scanID']
    bgScanDate = param['bgScanDate']
    bgScanNumber = param['bgScanNumber']
    bgScanID = param['bgScanID']
    sh_sample = int(param['sh_sample_y']),int(param['sh_sample_x']) #size to which the raw data is resampled
    filter_width = float(param['filter_width'])
    ss = float(param['ssy']),float(param['ssx'])
    xpts = int(param['xpts'])
    ypts = int(param['ypts'])
    zd = float(param['zd'])
    dr = float(param['dr'])
    e = float(param['e']) #photon energy
    ccdp = float(param['ccdp'])
    ccdz = float(param['ccdz']) * 1000. #convert to microns
    nexp = int(param['nexp'])
    bin = int(param['bin'])
    png = False #int(param['png'])
    xshift = 2
    yshift = 4
    xc = int(param['xcenter'])# + xshift
    yc = int(param['ycenter'])# + yshift
    t_exp = float(param['t_long']),float(param['t_short'])
    saturation_threshold = float(param['s_threshold'])
    nProcesses = int(param['nProcesses'])
    bl = param['bl']
    xpixnm,ypixnm = single(param['pixnm']),single(param['pixnm'])
    interferometer = int(param['interferometer'])
    xaxis = param['xaxis']
    yaxis = param['yaxis']
    xaxisNo = str(int(scanID) - 1)
    yaxisNo = str(int(scanID) - 1)
    pThreshold = float(param['probeThreshold'])
    removeOutliers = int(param['removeOutliers'])
    fCCD = int(param['fCCD'])
    dfilter = int(param['lowPassFilter'])
    nl = float(param['nl'])
    cropSize = int(param['sh_crop']) #size to which the raw data is cropped
    noise_threshold = nl
    process = 15
    verbose = True
    fstep = 1
    tiff = 0
    logscale = 1
    indexd = None
    indexo = None
    gpu = int(param['processGPU'])
    fv = int(param['fv'])
    fh = int(param['fh'])
    tr = int(param['transpose'])
    fCCD_version = int(param['fCCD_version'])
    low_pass = int(param['low_pass'])
    multiCore = True
    bsNorm = 1. / float(param['beamStopTransmission'])
    defocus = float(param['defocus'])
    bsXshift = int(param['beamstopXshift'])
    bsYshift = int(param['beamstopYshift'])


    if gpu:
        from pyfft.cuda import Plan
        import pycuda.driver as cuda
        from pycuda.tools import make_default_context5
        import pycuda.gpuarray as gpuarray
        cuda.init()
        context = make_default_context()
        stream = cuda.Stream()

    if nexp == 2:
        multi = True
    else: multi = False

    ######################################################################################################
    ###setup some of the file name prefixes
    if not(int(param['sim'])):
        scanDir = param["scanDir"]
        if fCCD:
            filePrefix = 'image'
            namePrefix = 'NS_'
        else:
            if param['bl'] == 'ns':
                namePrefix = 'NS_'
                filePrefix = namePrefix + scanDate + scanNumber + '_'
            if param['bl'] == 'mes':
                namePrefix = '11_'
                filePrefix = namePrefix + scanDate + scanNumber + '_'
            if param['bl'] == 'K':
                namePrefix = 'K'
                filePrefix = namePrefix + scanDate + scanNumber + '_'
    else:
        scanDir = dataPath
        if not(int(param['saveFile'])): saveFile = param['dataFile']
        else: saveFile = param['saveFile']
        filePrefix = param['filePrefix']

    if bgScanDate == '0': bgScanDate = scanDate
    if bgScanNumber != '0':
        bg_subtract = True
        bgScanDir = param["bgScanDir"]
        bg_prefix = 'NS_' + bgScanDate + bgScanNumber + '_'
    else: bg_subtract = False

    ######################################################################################################
    ###load interferometer data if requested
    if interferometer:
        xaxisDataFile = namePrefix + scanDate + scanNumber + '_' + xaxis + xaxisNo + '.xim'
        yaxisDataFile = namePrefix + scanDate + scanNumber + '_' + yaxis + yaxisNo + '.xim'
        print "X Axis data file: ", xaxisDataFile
        print "Y Axis data file: ", yaxisDataFile
        xdataFile = dataPath + scanDate + '/' + xaxisDataFile
        ydataFile = dataPath + scanDate + '/' + yaxisDataFile
        try: xshiftData = readXIM(xdataFile)
        except:
            print "Could not open interferometer X data file: %s" %xdataFile
            interferometer = 0
        else: xshiftData = (xshiftData - xshiftData.mean()) * 1000. #convert to nanometers
        try: yshiftData = readXIM(ydataFile)
        except:
            print "Could not open interferometer Y data file: %s" %ydataFile
            interferometer = 0
        else: yshiftData = (yshiftData - yshiftData.mean()) * 1000. #convert to nanometers



    ######################################################################################################
    ###calculate the number of pixels to use for raw data (the "crop" size)
    nyn, nxn = sh_sample
    ssy, ssx = ss
    fw = filter_width
    data_ave = zeros((nyn,nxn))
    l = e2l(e)
    f = zd * dr / l #zp focal length microns
    na = zd / f / 2. #zp numerical aperture
    xtheta = l / 2. / xpixnm #scattering angle
    nx = 2 * round(ccdz * (xtheta) / ccdp)
    ytheta = l / 2. / ypixnm
    ny = 2 * round(ccdz * (ytheta) / ccdp)
    sh_pad = ny, nx  #size to which the raw data is zero padded

    verbose = True

    if verbose:
        if fCCD: print "Using fastCCD version: %s" %fCCD_version
        else: print "Not using fastCCD"
        print "Scan date: %s, Scan Number: %s, Scan ID: %s" %(scanDate, scanNumber, scanID)
        print "Output file: %s" %param['dataFile']
        print "Photon energy: %.2f eV" %e
        print "Wavelength: %.2f nm" %l
        print "Zone plate focal length: %.2f mm" %f
        print "sample-CCD distance: %.2f mm" %(ccdz / 1000.)
        print "Requested pixel size (nm): %.2f" %(xpixnm)
        print "Will pad the data to: %i x %i (HxV)" %(nx,ny)
        print "Will downsample data to: %i x %i (HxV)" %(nxn,nyn)
        print "Probe step in pixels (x,y): %.2f,%.2f" %(ssx / xpixnm, ssy / ypixnm)
        print "Intensity threshold percent for probe calculation: %i" %(100. * pThreshold)
        print "Beamstop transmission: %.4f" %(1. / bsNorm)
        print "Beamstop normalization factor: %.2f" %bsNorm

    zpw = round(2. * na * ccdz / ccdp)
    stxmImage = zeros((ypts, xpts))
    stxmMask = circle(nxn,zpw * float(nxn) / float(nx),nxn/2,nxn/2)

    expectedFrames = xpts * ypts * bin * nexp# - nexp * fCCD
    if verbose: print "Expecting %i frames." %(expectedFrames)

    ######################################################################################################
    ###load the list of tiff file names
    if fCCD:
        fileList = sorted(os.listdir(scanDir))
        tifFiles = [i for i in range(len(fileList)) if fileList[i].count('tif')]
        indices = [int(fileList[tifFiles[i]].split('.')[0].split('image')[1]) for i in range(len(tifFiles))]
        frame_offset = array(indices).min()# + 1
        #if nexp == 2: frame_offset = 2 ###JUST TESTING!!!
        if verbose: print "Starting frame is: %i" %(frame_offset)
        if multi: fNorm = 1.
    else:
        frame_offset = 0
        frame_start = 0

    ######################################################################################################
    ###load and average the background frames
    ###this alternates long and short frames regardless of "multi", should be fixed
    if bg_subtract:
        if verbose: print "Averaging background frames..."
        if param['bg_filename'] == '':
            bg, bg_short, nframes = aveDirFrames(bgScanDir, multi, fCCD, fCCD_version)
        else:
            bg = np.asarray(readSpe(scanDir + param['bg_filename'])['data'])
            nframes = len(bg) / nexp
            bg_short = bg[1::2].mean(axis = 0)
            bg = bg[2::2].mean(axis = 0)
        if verbose: print "Done. Averaged %i frames." %nframes

        if fCCD:
            osmask = zeros(bg.shape)
            sh = bg.shape
            if fCCD_version == 1:
                osmask[:,0::12] = 1
                osmask[:,1::12] = 1
            elif fCCD_version == 2:
                #osmask[:,0::12] = 1
                #osmask[:,11::12] = 1
                osmask[0:sh[0]/2,0::12] = 1
                osmask[0:sh[0]/2,11::12] = 1
                osmask[sh[0]/2:sh[0],0::12] = 1
                osmask[sh[0]/2:sh[0],11::12] = 1
                #osmask = vstack((flipud(fliplr(osmask[:,1152:2304])),osmask[:,0:1152]))
            #bg = vstack((flipud(fliplr(bg[:,1152:2304])),bg[:,0:1152]))
            osmask = vstack((osmask[1:467,:],osmask[534:1000,:]))
            #bg = vstack((bg[1:467,:],1. * bg[534:1000,:]))
            dmask = 1 - osmask
            indexd = where(dmask)
            indexo = where(osmask)
            #bg = reshape(bg[indexd],(932,960))
            os_bkg = 0.
    else:
        bg = None
        bg_short = None
        if verbose: print "No background data submitted!"

    t0 = time.time()

    ######################################################################################################
    ###Load the data from the directory of tiff frames
    if param['speFile'] == '':
        dataStack, loadedFrames = loadDirFrames(scanDir, filePrefix, expectedFrames, cropSize, multi, fCCD, frame_offset)
    else:
        dataStack = np.asarray(readSpe(scanDir + param['speFile'])['data'])
        loadedFrames = len(dataStack)

    ######################################################################################################
    ###Generate a mask to cover bad regions of the detectors
    a,b,c = dataStack.shape
    dataMask = ones((b,c))
    if fCCD:
        dataMask[500:1000,0:150] = 0.
    dataStack = dataStack * dataMask

    ######################################################################################################
    ###set up the frame shift data
    pixIndex = [(i / xpts, i % xpts) for i in range(expectedFrames / nexp)]
    if interferometer:
        shiftData = [(-yshiftData[i / xpts, i % xpts] * 1000., xshiftData[i / xpts, i % xpts] * 1000.) for i in range(xpts * ypts)]
    else:
        shiftData = [(-(float(pixIndex[i][0]) - float(ypts - 1) / 2) * ssy, (float(pixIndex[i][1]) - float(xpts - 1) / 2) * ssx) for i in range(expectedFrames / nexp)]

    ######################################################################################################
    ###set up the low pass filter and beamstop normalization
    if fCCD:
        fsh = cropSize, cropSize
        bs_r = 40 #was 10 at BL901

        bs = 1. + (bsNorm * circle(cropSize, bs_r, cropSize / 2, cropSize / 2) - (bsNorm + 0.5) * circle(cropSize, 10, cropSize / 2, cropSize / 2))
        bs = 1. + ((bsNorm - 1) * circle(cropSize, bs_r, cropSize / 2, cropSize / 2))
        #bs[250:256,240:246] = 1
        fNorm = shift(gaussian_filter(bs, sigma = 0.5), bsYshift, bsXshift) #we multiply the data by this to normalize

    else: fsh = dataStack[0].shape; fNorm, indexd, indexo, os_bkg = None, None, None, None
    dy, dx = float(sh_pad[0]) / float(sh_sample[0]),float(sh_pad[1]) / float(sh_sample[1])
    dyn, dxn = int(float(fsh[0]) / dy), int(float(fsh[1]) / dx)

    if dfilter:
        x,y = meshgrid(abs(arange(fsh[1]) - fsh[1] / 2),abs(arange(fsh[0]) - fsh[0] / 2))
        filter = exp(-x**2 / 2. / float(fw)**2 -\
                     y**2 / 2. / float(fw)**2)
        filter = fft.fftshift(filter).astype("float64")
    else: filter = False

    tRead = time.time() - t0
    t0 = time.time()
    if verbose: print "Read frames at: %.2f Hz" %(float(i) / tRead)
    if verbose: print "Loaded %i frames" %loadedFrames
    if expectedFrames == loadedFrames:
        if verbose: print "Loaded correct number of frames.  Good!"
    else:
        if verbose: print "Expected %i frames but loaded %i. Exiting!" %(expectedFrames, loadedFrames)
        return

    ######################################################################################################
    ###split up the data stack into chunks and send to the processors
    if comm.Get_size() > 1: nProcessors = comm.Get_size() - 1
    else: print "readALSMPI requires at least 2 MPI processes. Exiting."; return
    if nProcessors == 1:
        startIndices = [0]
        stopIndices = [len(dataStack) - 1]
    else:
        interval = len(dataStack) / nProcessors
        if interval % 2 != 0: interval += 1  ##make it even for double exposure mode
        stopIndices = arange(nProcessors) * interval + interval
        startIndices = arange(nProcessors) * interval
        if stopIndices[-1] < len(dataStack): stopIndices[-1] = len(dataStack) #make the last one longer to get the stragglers
    for rank in range(1,comm.Get_size()):
        i1 = startIndices[rank - 1]
        i2 = stopIndices[rank - 1]
        indexTuple = i1, i2
        dataStruct = [dataStack[i1:i2,:,:], bg, sh_pad, sh_sample, filter, xc,\
                 yc, dfilter, t_exp, saturation_threshold, noise_threshold, stxmMask, \
                 tiff, False, bl, fCCD, fv, fh, tr, multi, verbose, fCCD_version, fNorm, \
                 indexTuple, indexd, indexo, cropSize, os_bkg, bg_short, low_pass]
        comm.send(dataStruct, rank, tag = 11)

    ######################################################################################################
    ###retrieve the chunks back and put them into the right spot in the stack (re-initialized to the new smaller size)
    nPoints = xpts * ypts
    dataStack = zeros((nPoints, nyn, nxn))
    for rank in range(1, comm.Get_size()):
        dataStruct = comm.recv(source = rank, tag = 12)
        i1,i2 = dataStruct[23]  ##these are wrong for double exposure mode
        if multi: i1, i2 = i1 / 2, i2 / 2
        dataStack[i1:i2] = dataStruct[0]

    tProcess = time.time() - t0
    if verbose: print "Done!"
    if verbose: print "Processed frames at: %.2f Hz" %(float(loadedFrames / nexp) / tProcess)
    if verbose: print "Total time per frame: %i ms" %(1000. * (tProcess + tRead) / float(loadedFrames / nexp))

    ######################################################################################################
    ###Calculate the STXM image and make some arrays for the translations
    ihist = array([(dataStack[i] * (dataStack[i] > 10.)).sum() for i in range(nPoints)])
    indx = arange(nPoints)
    stxmImage = reshape(ihist,(ypts,xpts))[::-1,:]

    #####################################################################################################
    ###Remove outliers based on STXM intensities
    x = array(shiftData)[:,1]
    y = array(shiftData)[:,0]


    if removeOutliers:
        #identify bad files by checking for total intensities more than 3 sigma from the mean
        Imax = ihist.mean() + 4.0 * ihist.std()
        hifiles = where(ihist > Imax)
        Imin = ihist.mean() - 4.0 * ihist.std()
        lofiles = where(ihist < Imin)
        nanfiles = where(isnan(ihist))
        bad_files_indices = concatenate((hifiles[0],lofiles[0],nanfiles[0]))
        bad_files_indices.sort() #sort the list to ascending order
        if verbose: print "Removing outliers"
        k = 0
        if len(bad_files_indices) > 0:
            for item in bad_files_indices:
                i = item / stxmImage.shape[1]
                j = item % stxmImage.shape[1]
                stxmImage[i,j] = stxmImage.mean()
                indx[item] = 0
                indx[item+1:nPoints] = indx[item+1:nPoints] - 1
                x = delete(x, item - k)
                y = delete(y, item - k)
                dataStack = delete(dataStack, item - k, axis = 0)
                k += 1

    translationX, translationY = (x - x.min()) * 1e-9, (y - y.min()) * 1e-9

    ######################################################################################################
    ###Calculate the probe, center of mass and probe mask
    if verbose: print "Calculating probe and mask"
    dataAve = dataStack.sum(axis = 0) #average dataset
    #locate the center of mass of the average and shift to corner
    dataAve = dataAve * (dataAve > 10.) #threshold, 10 is about 1 photon at 750 eV
    dataSum = dataAve.sum() #sum for normalization
    xIndex = arange(dataAve.shape[1])
    yIndex = arange(dataAve.shape[0])

    xc = np.int(np.round((dataAve.sum(axis = 0) * xIndex).sum() / dataSum))
    yc = np.int(np.round((dataAve.sum(axis = 1) * yIndex).sum() / dataSum))
    print "Center positions: %.2f, %.2f" %(xc,yc)

    ##Calculate the probe from the average diffraction pattern
    dataStack = np.roll(np.roll(dataStack, dataStack.shape[1] / 2 - yc, axis = 1), dataStack.shape[2] / 2 - xc, axis = 2)  ##center the data
    ##zero the center
    #j,k,l = dataStack.shape
    #dataStack = dataStack * (1. - circle(k, 9, k / 2, k / 2))
    dataAve = np.roll(np.roll(dataAve, -yc, axis = 0), -xc, axis = 1) ##center the average data
    #dataAve = dataAve * (1. - circle(k, 9, k / 2, k / 2))
    pMask = (dataAve > pThreshold * dataAve.max())
    p = sqrt(dataAve) * pMask
    p = fft.ifftshift(fft.ifftn(p)) #we can propagate this to some defocus distance if needed.
    ###put propagation code here
    if defocus != 0:
        p = propnf(p, defocus * 1000. / xpixnm, l / xpixnm)

    ######################################################################################################
    ###Apply a mask to the data
    # ny,nx = dataStack.shape[1:3]
    # m = np.ones((ny,nx))# - circle(nx, 2, nx / 2, nx / 2)
    # m[:,190:nx] = 0.
    # m[175:205,140:nx] = 0.
    # dataStack *= m

    ##########################################################
    ####interpolate STXM image onto the same mesh as the reconstructed image
    if verbose: print "Interpolating STXM image"
    yr,xr = ssy * ypts, ssx * xpts
    y,x = meshgrid(linspace(0,yr,ypts),linspace(0,xr,xpts))
    y,x = y.transpose(), x.transpose()
    yp,xp = meshgrid(linspace(0,yr, ypts * (ssy / ypixnm)),linspace(0,xr, xpts * (ssx / xpixnm)))
    yp,xp = yp.transpose(), xp.transpose()
    x0 = x[0,0]
    y0 = y[0,0]
    dx = x[0,1] - x0
    dy = y[1,0] - y0
    ivals = (xp - x0)/dx
    jvals = (yp - y0)/dy
    coords = array([ivals, jvals])
    stxmImageInterp = ndimage.map_coordinates(stxmImage.transpose(), coords)
    stxmImageInterp = stxmImageInterp * (stxmImageInterp > 0.)

    stride_x = 1
    stride_y = 1
    start_x = 0
    start_y = 0
    end_x = xpts
    end_y = ypts

    ######################################################################################################
    ###Generate the CXI file
    translation = column_stack((translationX, translationY, zeros(translationY.size))) #(x,y,z) in meters
    corner_position = [float(sh_pad[1]) * ccdp / 2. / 1e6, float(sh_pad[0]) * ccdp / 2. / 1e6, ccdz / 1e6] #(x,y,z) in meters

    outputFile = param['scanDir'] + param['outputFilename']
    if verbose: print "Writing data to file: %s" %outputFile

    ########################################################################################################
    ########################################################################################################

    cxiObj = cxi()
    cxiObj.process = param
    cxiObj.probe = p
    cxiObj.beamline = bl
    cxiObj.energy = e * 1.602e-19
    cxiObj.ccddata = dataStack
    cxiObj.probemask = pMask
    cxiObj.probeRmask = circle(pMask.shape[0], pMask.shape[0] / 4, pMask.shape[0] / 2, pMask.shape[0] / 2)
    cxiObj.datamean = dataAve
    cxiObj.illuminationIntensities = dataAve
    cxiObj.stxm = stxmImage
    cxiObj.stxmInterp = stxmImageInterp
    cxiObj.xpixelsize = float(sh_pad[1]) / float(nxn) * float(ccdp) / 1e6
    cxiObj.ypixelsize = float(sh_pad[0]) / float(nxn) * float(ccdp) / 1e6
    cxiObj.corner_x, cxiObj.corner_y, cxiObj.corner_z = corner_position
    cxiObj.translation = translation
    cxiObj.indices = indx
    writeCXI(cxiObj, fileName = outputFile)

    ######################################################################################################
    ######################################################################################################

    if verbose: print "Done."
    if removeOutliers:
        if verbose: print "Located %i bad frames..." %(len(bad_files_indices))

    #os.chmod(outputFile, 0664)
    return 1


if __name__ == '__main__':

    comm = MPI.COMM_WORLD
    commSize = comm.Get_size()
    rank = comm.Get_rank()
    graph = False

    if rank == 0:
        print "Welcome to readALS MPI!"
        if len(sys.argv) == 1:
            print "Parameter file argument needed."
            sys.exit()
        elif len(sys.argv) > 1: paramFile = sys.argv[1]
        try: f = open(paramFile,'r')
        except IOError:
            print "IOError: could not open parameter file: %s" %(paramFile)
            sys.exit()
        else:
            param = Param().param
            for item in f:
                pStringList = [item.split('=')[0],item.split('=')[1].rstrip('\n')]
                if len(pStringList) != 2:
                    pass
                else:
                    param[pStringList[0]] = pStringList[1]

        #####Generate CXI object from param and call the main function
        # graph == False
        # if graph:
        #     with PyCallGraph(output=GraphvizOutput()): main(param, comm)
        main(param, comm)

    else:
        processor(comm)

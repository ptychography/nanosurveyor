from pylab import matshow,show
from numpy import *
import numpy as np
from util import shift
from imageIO import imload
from scipy.interpolate import interp1d
import os
from scipy.ndimage.filters import gaussian_filter
import sys
import fftw3

def fitOverscan(a,mask):

    ll = a.shape[0]
    xxb = matrix(linspace(0,1,ll)).T
    w = ones((ll))
    w[0:100] = 0
    w = matrix(diag(w))
    Q = matrix(hstack((power(xxb,8),power(xxb,6),power(xxb,4),power(xxb,2),(xxb * 0. + 1.))))
    bnfw = Q * (Q.T * w * Q).I * (w * Q).T * matrix(a * mask)
    bnfw1 = rot90(bnfw,2)

    jkjk = array(bnfw1) # / rot90(mask,2)
    jkjk1 = zeros((2*ll,192))
    jkjk1[::2,0:192] = jkjk[:,::2]
    jkjk1[1::2,0:192] = jkjk[:,1::2]

    tt = reshape(linspace(1,12*ll,12*ll),(ll,12))
    ttd = tt[:,9::-1]
    tts = tt[:,12:9:-1]

    f = interp1d(tts.flatten(), jkjk1, 'linear',axis = 0,bounds_error = False,fill_value = jkjk1.mean())
    bkg = rot90(reshape(f(ttd),(ll,192 * 10)),2)
    bkg = f(ttd)
    bkg1 = zeros((ll,1920))

    for i in xrange(10):
        bkg1[:,i::10] = bkg[:,i,:]

    return rot90(bkg1,2)

def processfCCDFrame(data, data_short, bkg, bkg_short, os_bkg, indexd, indexo, t_exp, saturation_threshold,noise_threshold, xc,yc,fNorm,framenum, dSize, version = 1, verbose = True, fullProcess = True):

    if yc > 970:
        xc, yc = xc - 84, yc - 572
    else:
        xc, yc = xc - 84, yc - 500

    if verbose:
        sys.stdout.write("Extracting fCCD data frame %i \r" %framenum)
        sys.stdout.flush()

    #fullProcess = False

    if fullProcess:
        #if version == 2: data = hstack((data[534:1000,:], fliplr(flipud(shift(data[1:467,:],0,2)))))
        #elif version == 1: data = hstack((data[534:1000,:], fliplr(flipud(shift(data[1:467,:],-1,2)))))
        data = hstack((data[534:1000,:], fliplr(flipud(shift(data[1:467,:],0,2)))))
        bkg = hstack((bkg[534:1000,:], fliplr(flipud(shift(bkg[1:467,:],0,2)))))
        ll = data.shape[0]
        data = data - 0.999 * bkg
        osmask = zeros(bkg.shape)
        osmask[:,0::12] = 1
        osmask[:,11::12] = 1
        dmask = 1 - osmask
        indexd = where(dmask)

        indexo = where(osmask)
        dd = reshape(data[indexd],(466,10*192))
        os = reshape(data[indexo],(466,2*192))
        osmask = os > 0. #ignores the dropped packets, I hope

        if data_short != None:
            data_short = hstack((data_short[534:1000,:], fliplr(flipud(shift(data_short[0:466,:],-1,0)))))
            data_short -= bkg_short
            dd_short = reshape(data_short[indexd],(466,10*192))
            osshort = reshape(data_short[indexo],(466,2*192))
            os = os * osmask + (1 - osmask) * osshort
        else: os = os * osmask

        smask = dd < saturation_threshold

        ## bkg1 = fitOverscan(os, osmask)

        bkg1 = zeros(dd.shape)
        for i in range(192):
            j = i * 10
            k = i * 2
            temp = os[:,k:(k+2)]
            bkg1[:,j:(j+10)] = temp.mean()

        dd -= bkg1
        dd = dd * (dd > 0)

        ll = dd.shape[0]
        lw = dd.shape[1]

        jk = reshape(transpose(reshape(dd,(ll,10,192)),(0, 1, 2)), (ll * 10, 192))
        jkf=(fft.fftn((jk*(abs(jk)<4.0))))#; % only when it is background...
        msk = abs(jkf) > 400.; msk[0:500,:] = 0 #; % Fourier mask, keep low frequencies.
        jkif = reshape(transpose(reshape(fft.ifftn(jkf * msk),(10,ll,192)),(0, 1, 2)),(ll,1920))
        dd -= jkif.real

        dd[0,9::10] = 0

        if data_short != None: dd = dd * smask + (1 - smask) * dd_short * t_exp[1] / t_exp[0]

        dd =  vstack((flipud(fliplr(dd[:,960:1920])),dd[:,0:960]))

    else:
        dd = vstack((data[0:466,:], shift(data[534:1000,:],0,0)))
        dd = reshape(dd[indexd],(932,960))
        dd -= reshape(vstack((bkg[0:466,:], shift(bkg[534:1000,:],0,0)))[indexd],(932,960))

    if version == 1:
        ##do some shifting because the CCD de-scrambling is incorrect
        dd[466:932,9::10] = shift(dd[466:932,9::10],0,1)
        dd[0:466,0::10] = shift(dd[0:466,0::10],0,-1)
        dd[0:467,:] = shift(dd[0:467,:],0,-4)
        dd = dd[yc - dSize / 2:yc + dSize / 2, xc - dSize / 2: xc + dSize / 2]

    elif version == 2:
        dd[0:470,:] = shift(dd[0:470,:],0,-8)
        dd[468,8::10] = dd[468,0::10]
        dd[468,9::10] = dd[468,3::10]
        dd[470,0::10] = dd[471,0::10]
        dd[470,1::10] = dd[470,2::10]
        dd[469,:] = (dd[468,:] + dd[470,:]) / 2.
        # temp = dd[:,0:50].mean(axis = 1)
        # dd = dd - reshape(temp,(len(temp),1))
        ##crop
        dd = dd[yc - dSize / 2:yc + dSize / 2, xc - dSize / 2: xc + dSize / 2]

    ##remove negatives and multiply by the beamstop scale factor
    dd *= fNorm
    dd -= (fNorm - 1) * noise_threshold
    dd *= dd > noise_threshold

    if isnan(dd.min()): print "Nan encountered!"
    return dd




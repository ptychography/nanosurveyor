__author__ = 'DAShapiro'
__author__ = 'nsptycho'

from aux.executeParam import *
from aux.param import *
from aux.readCXI import *
from aux.imageIO import imsave
from aux.util import removePlane
import numpy as np
import os
import scipy
import time

def energyStack(param, stxmHDR, preprocess = True, cpu = True, saveOutput = True):

    if not(os.path.isdir(param['dataPath'])):
        print "Could not locate energy scan directory."
        return 1


    #######################################################################################################################
    #######################################################################################################################
    debug = False

    #######################################################################################################################
    #######################################################################################################################
    ##Load available HDR files and parse some meta data
    hdrFiles = []
    ptychoScans = []
    bgScans = []
    bgScansList = []
    ptychoEnergies = []
    bgEnergies = []

    #check to see if there is a progress file from other runs
    if os.path.isfile(param['dataPath'] + 'progress.txt'): os.remove(param['dataPath'] + 'progress.txt')

    #make a list of directories containing ptycho scans
    scanList = os.listdir(param["dataPath"])

    thisScanList = sorted(os.walk(param['dataPath']).next()[1])
    ptychoScans.append([x + '/' for x in thisScanList[0::2]])
    bgScans.append([x + '/' for x in thisScanList[1::2]])

    #reshape the list
    ptychoScans = [scan for item in ptychoScans for scan in item]
    bgScans = [scan for item in bgScans for scan in item]
    ignoring = []

    #check the number of frames per scan
    i = 0
    for item in ptychoScans:
        nFrames = len([tifFile for tifFile in os.listdir(param['dataPath'] + item) if tifFile.count('tif')])
        if nFrames < int(param['ypts']) * int(param['xpts']):
            print "Scan %s has only %i frames but exptected %i. Ignoring." %(item, nFrames, int(param['ypts']) * int(param['xpts']))
            ignoring.append(item)
        else:
            ptychoEnergies.append(np.float(stxmHDR.energies[i]))
        i += 1
    for item in ignoring: ptychoScans.remove(item)


    #######################################################################
    ########################################################################
    ##Open a file to report the status
    f = open(param['dataPath'] + 'progress.txt', 'w')
    f.close()

    ########################################################################
    ########################################################################
    ##This function just executes the parameter list from the subprocesses started below
    def callExecuteParam(param, scanDir = None, dataFile = None, ang = None, prelim = False):
        if scanDir != None: param['scanDir'] = param['dataPath'] + scanDir
        if dataFile != None: param['dataFile'] = dataFile
        if ang != None: param['angle'] = ang
        ##############################################################param["interferometer"] = '1'
        ##############################################################
        #Check, Execute, save parameters, and display
        runParam(param)

    paramList = []
    
    #######################################################################
    ########################################################################
    ##Go through the list of ptycho scans and pre-process plus initial GPU reconstruction in series
    for scan,bgScan,energy in zip(ptychoScans,bgScans,ptychoEnergies):
        param["scanDate"] = stxmHDR.date #scan[:6] #date string of the ptycho scan
        param["scanNumber"] = stxmHDR.scanNumber
        param["scanID"] = scan[0:3]
        param['scanDir'] = param['dataPath'] + scan #directory with the tomo scan for a given angle
        param['bgScanDir'] = param['dataPath'] + bgScan #directory with the BG scan for that same angle
        param['dataFile'] = 'NS_' + param["scanDate"] + param["scanNumber"] + '_' + param["scanID"] + '.cxi' #CXI file name #resulting CXI file
        param["gpu"] = '1' #GPU flag: set '1' for GPU and '0' otherwise
        param["saveFile"] = '0' #set to filename for custom file name, otherwise leave as '0'
        param['paramFileName'] = '0'
        param['process'] = '1'
        param['reconstruct'] = '1'
        param["interferometer"] = '0'
        param["e"] = str(energy)

        ###append to the parameter List
        paramList.append(param.copy())

    ###Sort the parameter list according to angle
    indices = sorted(range(len(ptychoEnergies)), key = lambda k: ptychoEnergies[k])
    paramList = [paramList[i] for i in indices]

    if debug:
        for p in paramList:
            print p['angle'],p['scanDir'], p['bgScanDir']


    #############################################################
    #############################################################
    ##############Check, Execute, save parameters
    if preprocess:
        for param in paramList: callExecuteParam(param, prelim = True)

    if cpu:
        ##############################################################
        ##############################################################
        ##Apply switches for final reconstruction using the CPU
        for param in paramList:
            param['process'] = '0'
            param['reconstruct'] = '1'
            param['gpu'] = '0'

        ###########################################################################
        ###########################################################################
        ##Generate a pool of subprocesses and start CPU reconstructions in parallel, 10 at a time
        for param in paramList: callExecuteParam(param)


    ###########################################################################
    ###########################################################################
    ###Generate tiff files and exit
    if saveOutput:
        minEnergy = min(ptychoEnergies)
        print "Generating output images..."
        if not(os.path.isdir(param['dataPath'] + 'tiffs')): print "Making tiff directory."; os.mkdir(param['dataPath'] + 'tiffs')
        if not(os.path.isdir(param['dataPath'] + 'pngs')): print "Making png directory."; os.mkdir(param['dataPath'] + 'pngs')
        if not(os.path.isdir(param['dataPath'] + 'tiffs/phase')): print "Making tiff directory."; os.mkdir(param['dataPath'] + 'tiffs/phase')
        if not(os.path.isdir(param['dataPath'] + 'tiffs/intensity')): print "Making tiff directory."; os.mkdir(param['dataPath'] + 'tiffs/intensity')
        if not(os.path.isdir(param['dataPath'] + 'tiffs/stxm')): print "Making tiff directory."; os.mkdir(param['dataPath'] + 'tiffs/stxm')
        if not(os.path.isdir(param['dataPath'] + 'pngs/phase')): print "Making png directory."; os.mkdir(param['dataPath'] + 'pngs/phase')
        if not(os.path.isdir(param['dataPath'] + 'pngs/intensity')): print "Making png directory."; os.mkdir(param['dataPath'] + 'pngs/intensity')

        ###Open a text file to save the list of angles
        f = open(param['dataPath'] + 'energies.txt', 'w')

        ##Loop through and save the tiffs and pngs
        i = 0
        for param in paramList:
            f.write(param['e'] + '\n')
            thisEnergy = str(float(param['e']) - minEnergy)
            thisData = readCXI(param['scanDir'] + param['dataFile'])

            temp = thisData.stxmInterp
            im = scipy.misc.toimage(temp, high = temp.max(), low = temp.min(), mode = 'F')
            fileName =  param['dataPath'] + 'tiffs/stxm/' + 'stxm_' + thisEnergy + '.tif'
            im.save(fileName)

            temp = np.angle(thisData.image)
            temp = removePlane(temp)
            imsave(temp,param['dataPath'] + 'pngs/phase/' + 'projection_phase_' + thisEnergy + '.png', ct = 'bone')
            im = scipy.misc.toimage(temp, high = temp.max(), low = temp.min(), mode = 'F')
            fileName =  param['dataPath'] + 'tiffs/phase/' + 'projection_phase_' + thisEnergy + '.tif'
            im.save(fileName)

            temp = np.abs(thisData.image)
            imsave(temp,param['dataPath'] + 'pngs/intensity/' + 'projection_intensity_' + thisEnergy + '.png', ct = 'bone')
            im = scipy.misc.toimage(temp, high = temp.max(), low = temp.min(), mode = 'F')
            fileName =  param['dataPath'] + 'tiffs/intensity/' + 'projection_intensity_' + thisEnergy + '.tif'
            im.save(fileName)
            i += 1
        f.close()

    ###########################################################################
    ###########################################################################
    ###Finish
    print "Done!"

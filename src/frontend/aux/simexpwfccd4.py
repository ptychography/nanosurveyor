from __future__ import division
import scipy.misc
from scipy.interpolate import interp2d
import scipy.special
from collections import namedtuple
import numpy as np
import os
#import pycuda.driver as cuda
#import pycuda.autoinit
#from pycuda.compiler import SourceModule
from PIL import Image
from scitools.all import *
import matplotlib.pyplot as plt
import h5py

from PyQt4 import QtCore

class Source():
    def __init__(self, nphotons, distance, energy, hc):
        self.nphotons = nphotons
        self.distance = distance
        self.energy = energy
        self.lambda_ = hc/energy
        
class Zp():
    def __init__(self, outermostzonewidth, diameter, stop, nzone=None, focallength=None, focaldistance=None, dof=None):
        self.outermostzonewidth = outermostzonewidth
        self.diameter = diameter
        self.stop = stop
        self.nzone = nzone
        self.focallength = focallength
        self.focaldistance = focaldistance
        self.dof = dof
        
class Ccd():
    def __init__(self, pixel_size, distance, hnu, npixel, npixelx,npixely):
        self.pixel_size = pixel_size
        self.distance = distance
        self.hnu = hnu
        self.npixel = npixel
        self.npixelx = npixelx
        self.npixely = npixely
        
class Attenuator():
    def __init__(self, transmission, radius):
        self.transmission = transmission
        self.radius = radius


class Simulator(QtCore.QThread):
    def __init__(self, parameters):
        QtCore.QThread.__init__(self)
        self.scanSide = parameters[0]
        self.ccd = Ccd(parameters[1],parameters[2],parameters[3],parameters[4],parameters[5],parameters[6])
        self.attenuator = Attenuator(parameters[7],parameters[8])
        
    def __del__(self):
        self.wait()
        
    def stop(self):
        if (self.isRunning()):
            self.terminate()
        
    def _cropmat(self, a,s):
        c0 = np.floor((a.shape[0] - s[0])/2)
        c1 = np.floor((a.shape[1] - s[1])/2)
        return a[c0:c0+s[0],c1:c1+s[1]]

    def illuminationshift(self, illumination, dx, dy, xx, yy):
        M = shape(illumination)[0]
        N = shape(illumination)[1]
        f = interp2d(arange(N),arange(M),illumination,fill_value=0)
        toEvalX = (xx+dx).ravel()
        toEvalY = (yy+dy).ravel()
        interp_res = f(arange(toEvalX[0],toEvalX[-1]+1),arange(toEvalY[0],toEvalY[-1]+1))
        return interp_res

    def zp2illumination(self, zp=None,source=None,dz=None,*args,**kwargs):
        h_plank=4.1356668e-15
        c_light=299792458.0
        j2ev=6.24150934e+18
        hc=h_plank * c_light
        source.lambda_=hc / source.energy
        lambda_=source.lambda_
        zp.nzone = ceil(zp.diameter / 4 / zp.outermostzonewidth)
        zp.focallength = zp.diameter * zp.outermostzonewidth / source.lambda_
        zp.focaldistance = 1 / (1 / zp.focallength - 1 / source.distance)
        zpmag=self.ccd.distance / zp.focaldistance
        zp_aperture=np.zeros(2)
        zp_aperture[0] = zp.stop / 2 
        zp_aperture[1] = zp.diameter / 2
        zp_aperture=zp_aperture * zpmag / self.ccd.pixel_size
        xi=linspace(0,self.ccd.npixelx - 1,self.ccd.npixelx) - self.ccd.npixelx / 2
        yi=linspace(0,self.ccd.npixely - 1,self.ccd.npixely) - self.ccd.npixely / 2
        dosepp=source.nphotons / (pi * ceil(zp_aperture[1] ** 2))
        yi.shape = (shape(yi)[0],1)
        rr2=np.add(xi**2, yi**2)
        Fprobe=(rr2 < (zp_aperture[1]) ** 2)
        Fprobe=np.logical_and(rr2 > (zp_aperture[0]) ** 2, Fprobe)
        filter_=(rr2 > (self.attenuator.radius / self.ccd.pixel_size) ** 2) + (rr2 <= (self.attenuator.radius / self.ccd.pixel_size) ** 2) * self.attenuator.transmission
        fx2=((self.ccd.pixel_size) / self.ccd.distance) ** 2 / lambda_ / 2
        fy2=((self.ccd.pixel_size) / self.ccd.distance) ** 2 / lambda_ / 2
        Dh=lambda dz: np.multiply(exp(- dz * 1j * fx2 * (xi ** 2)),exp(- dz * 1j * fy2 * (yi ** 2)))
        fftshift2D=np.multiply((- 1) ** xi,(- 1) ** yi)
        propagate=lambda x,dz: np.multiply(fftshift2D , (np.fft.ifft2( np.multiply(np.multiply(Dh(dz),x) , fftshift2D))))
        ipropagate=lambda x,dz: np.multiply(fftshift2D  , (  np.fft.fft2(  np.multiply(np.multiply(np.conj(Dh(dz)),x), fftshift2D )  )  ))
        probe=propagate(Fprobe,dz)
        return probe,Fprobe,filter_,dosepp,zp

    # Still hardcoded stuff
    def rowXgccd(self, data):
        temp = np.hstack([np.rot90(data[960:,:],2), data[:960,:]])
        out = np.zeros((1000,2304))
        out[10:970,191:2111] = temp
        return out

    def writecxi1(self, cxiname,frames,backgrounds, image,illumination,illumination_mask,translations,energy,det_distance, corner_pos,pixel_size):
        if (os.path.exists(cxiname)):
            os.remove(cxiname)       
        f = h5py.File(cxiname, "w")
        f.create_dataset("cxi_version",data=140)
        entry1 = f.create_group("entry_1")
        data1 = entry1.create_group("data_1")
        image1 = entry1.create_group("image_1")
        instrument1 = entry1.create_group("instrument_1")
        process1 = entry1.create_group("process_1")
        sample1 = entry1.create_group("sample_1")
        detector1 = instrument1.create_group("detector_1")
        source1 = instrument1.create_group("source_1")
        geometry1 = sample1.create_group("geometry_1")
        source1.create_dataset("energy",data=energy)
        geometry1.create_dataset("translation",data=translations.transpose())
        detector1.create_dataset("distance",data=det_distance)
        detector1.create_dataset("corner_position",data=corner_pos)
        detector1.create_dataset("x_pixel_size",data=pixel_size)
        detector1.create_dataset("y_pixel_size",data=pixel_size)
        detector1.create_dataset("illumination_mask",data=uint32(illumination_mask))
        detector1.create_dataset("data",data=uint16(frames.T))
        detector1.create_dataset("data_dark",data=uint16(backgrounds.T))
        image1.create_dataset("data",data=image)
        image1.create_dataset("illumination",data=illumination)
        data1["data"] = h5py.SoftLink('/entry_1/instrument_1/detector_1/data')
        data1["translation"] = h5py.SoftLink('/entry_1/sample_1/geometry_1/translation')    
        detector1["translation"] = h5py.SoftLink('/entry_1/sample_1/geometry_1/translation')
        f.close()
    
     
    def run(self):
        
        images = [np.zeros((256,256)), np.zeros((256,256)), np.zeros((256,256)), np.zeros((256,256)), np.zeros((256,256)), np.zeros((256,256)),0,0]
        
        displayraw=False
        displayclean=True

        # ccd
        #ccd = Ccd(3e-05, 0.05, 15, 960, 960, 2*960)

        #gdescramble_filter_setup
        nbcol=12
        nb=16
        nmux=12
        nadccol=10
        nrows=1000
        ngrows=960
        nadc=nb * nmux
        ncols=nbcol * nb * nmux
        ngcols=nadccol * nadc
        nbmux=nb * nmux
        qq1=arange(nbmux)
        q1=((mod(nbmux - (qq1+1),nmux) * nb + nb - 4 * floor(qq1 / (nmux * 4))) - mod(floor((nbmux - (qq1+1)) / nmux),4))-1
        q1=q1.astype('uint16')
        descramblemux=lambda data: data[q1,0:]
        ii=arange(0,shape(q1)[0])
        iq1=np.zeros(shape(ii))
        iq1[q1]=ii
        iq1 = iq1.astype('uint16')
        scramblemux=lambda data: data[iq1,0:]

        clockXraw1=lambda data: descramblemux(data.reshape(nb * nmux,nbcol * nrows)).transpose()
        rawXclock1=lambda data: scramblemux(data.transpose());

        rowXclock=lambda data_adc: np.reshape(np.transpose(np.reshape(data_adc,(nbcol, nrows, nadc), order='F'), [1,0,2]), (nrows, nadc*nbcol), order='F')

        ccdXrow=lambda bd: np.vstack([bd[0:,ncols / 2 + (arange(0,ncols / 2))],np.rot90(bd[0:,arange(0,ncols / 2)],2)])

        clockXrow=lambda data: np.reshape(np.transpose(np.reshape(data,(nrows, nbcol, nadc), order='F'),[1,0,2]), (nrows * nbcol, nadc), order='F')

        rowXccd=lambda img: np.hstack( [ np.rot90(img[nrows+arange(0,nrows),0:],2), img[0:nrows,0:] ] )

        rowXtif=lambda a: np.reshape(np.roll(np.reshape(rowXccd(a), (nrows, nbcol, nadc)), -1, axis=2),(nrows, nbcol*nadc))
        clockXtif=lambda a: clockXrow(rowXtif(a))

        gii=np.arange(ngcols / 2)
        gii=(gii + np.floor(gii / nadccol) * 2).astype(np.int)
        gccdXrow = lambda data: np.roll(np.vstack([data[5:5+ngrows, (ncols/2+gii).astype(np.int)],np.rot90(data[5:5+ngrows, gii],2)]),80,1)

        indxr=np.reshape(arange(0,nrows * ncols),(nrows,ncols),order='F')
        rindxr=gccdXrow(indxr)

        clockXgccd=lambda data: clockXrow(self.rowXgccd(data))

        gccdXraw=lambda data: gccdXrow(rowXclock(clockXraw1(data)))
        rawXgccd=lambda img: rawXclock1(clockXgccd(img))


        Mx=960
        My=960
        mx=128
        my=128

        #fftshifted prefactors
        temp1 = np.power(- 1, ((arange(0,self.ccd.npixely)).transpose()))
        temp1.shape = (shape(temp1)[0],1)
        temp2 = np.power(- 1, (arange(0,self.ccd.npixelx)))
        temp2.shape = (1,shape(temp2)[0])
        ftshift2dccd=temp1 * temp2
        temp1 = np.power(-1, ((arange(0, Mx)).transpose()))
        temp1.shape = (shape(temp1)[0],1)
        temp2 =  np.power(-1, (arange(0, Mx)))
        temp2.shape = (1,shape(temp2)[0])
        ftshift2d=temp1 * temp2
        temp1 = np.power(-1, ((arange(0, mx)).transpose()))
        temp1.shape = (shape(temp1)[0],1)
        temp2 =  np.power(-1, (arange(0, mx)))
        temp2.shape = (1,shape(temp2)[0])
        ftshift2dmx=temp1 * temp2


        # crop input frames (limit resolution)
        cropF=lambda x: self._cropmat(x,(Mx,My))
        # the filter, downsampling by cropping in real space
        cropframe0=lambda frame: self._cropmat(frame,(mx,my))

        # smooth crop 
        # make an image = r^2  of size mx
        temp1 = np.power(arange(- mx / 2 + 1,mx / 2+1),2)
        temp1.shape = (1,shape(temp1)[0])
        temp2 = (np.power(arange(- mx / 2 + 1,mx / 2+1), 2)).T
        temp2.shape = (shape(temp2)[0],1)
        r2=temp1 + temp2
        # smooth crop at the border for anti-aliasing
        msk=(scipy.special.erf(((mx / 2) * 0.7 - sqrt(r2)) / 10) + 1) / 2
        cropframe=lambda frame: np.multiply(cropframe0(frame), msk)

        # fftshift factors
        ift2shifted=lambda data2: np.fft.ifft2(np.multiply(ftshift2d, data2))
        ft2shifted=lambda data2: np.multiply(ftshift2dmx, np.fft.fft2(data2))

        downsample=lambda data: ft2shifted(cropframe(ift2shifted(data)))
        filter_=lambda data: downsample(cropF(data))

        ## pre-filter for correlated detctor noise
        ##
        # time of the ADC clocks
        tadc=arange(1,nbcol * nrows+1)

        # prepare first filter: remove background from offset column
        # select the offset values


        #- this stuff can be pre-computed
        # these are the offset columns+ extra reads
        #msktoffset= mskt&((mod(tadc-1,nbcol)>9) |tadc>((nrows-35)*nbcol)); % );
        mskt=(tadc > ((2) * nbcol))
        # these are the offset columns
        msktoffset1=np.logical_and(mskt, ((mod(tadc - 1,nbcol) > 9)))
        # we also remove some on top
        msktoffset=np.logical_and(msktoffset1, tadc < ((nrows - 100) * nbcol))

        msktbgoffset=np.logical_and(msktoffset, (tadc > 800 * nbcol))
        mskt1=np.logical_and(msktoffset, (tadc < nrows / 2 * nbcol))

        #create vandermonde filter.. here we split into two regions tt1 and tt2
        t1=tadc - min(tadc)
        t1.shape = (1,shape(t1)[0])
        t1=(t1 / t1.max() - 0.5)
        tt1=0.5 - t1
        toffset=0.6
        tt2=np.multiply(np.multiply((tt1 - toffset), ((tt1 > toffset))), scipy.special.erf((tt1 - toffset) * 2))

        vanderX=np.vstack( (np.power(tt1,0) , tt1, np.power(tt1,2) , tt2, np.power(tt2,2)) )

        VS=vanderX[:,msktoffset]

        vanderfilterX=np.mat(vanderX.transpose()) * np.mat((np.linalg.solve((np.mat(VS)*np.mat(VS.transpose())),VS)))

        vanderfilter=lambda data: (np.mat(vanderfilterX) * np.mat(data))

        #gvanderfilterX=gpuArray(single(vanderfilterX))
        vanderfilterX_float = vanderfilterX.astype(np.float32)
        #vanderfilterX_gpu = cuda.mem_alloc(vanderfilterX_float.nbytes)
        #cuda.memcpy_htod(vanderfilterX_gpu, vanderfilterX_float)

        dname='src/frontend/aux/testdata/tiffs/002/'
        tifnames = os.listdir(dname)
        tifnames.sort()

        fasttifread=lambda ii: np.array(Image.open(dname+tifnames[ii+1]))

        nframes=shape(tifnames)[0] - 1
        bg_avg=0
        backgrounds=np.zeros((nbmux,nmux * nrows,nframes),dtype='uint16')
        print('averaging backgrounds...')

        for ii in arange(nframes):
            #print(ii)
            data_c=(clockXtif((fasttifread(ii))))
            data_clock=data_c.astype('double')
            backgrounds[:,:,ii]=rawXclock1(data_c)
            bg_avg=bg_avg + data_clock
            
        bg_avg=bg_avg / (shape(tifnames)[0] - 1)
        bgraw=rawXclock1(bg_avg)
        print('done averaging backgrounds')

        adcstd=np.mean(((clockXraw1(np.std(backgrounds,2)))),0)
        badadc=adcstd < 40
        tadc=arange(nbcol * nrows)
        mskrowst=(tadc > ((2) * nbcol))

        msktf=lambda data: np.multiply(babadc, np.multiply(data, mskrowst.transpose()))


        xi=linspace(0,self.ccd.npixelx - 1,self.ccd.npixelx) - self.ccd.npixelx / 2
        yi=linspace(0,self.ccd.npixely - 1,self.ccd.npixely) - self.ccd.npixely / 2
        xi1=xi + self.ccd.npixelx / 2
        yi1=yi + self.ccd.npixely / 2
        xi1=xi1.astype('uint16')
        yi1=yi1.astype('uint16')
        xx,yy=meshgrid(xi1,yi1)
        xx=xx.astype('uint16')
        yy=yy.astype('uint16')

        splitframe=lambda psi,x,y: psi[(yi1 + y)[0]:(yi1 + y)[-1]+1,(xi1 + x)[0]:(xi1 + x)[-1]+1]

        illuminatesplitframe=lambda psi,illumination,x,y: np.multiply(splitframe(psi,floor(x),floor(y)),self.illuminationshift(illumination,floor(x) - x,floor(y) - y,xx, yy))
        generate_framepx=lambda psi,illumination,x,y: np.power(np.absolute(((np.fft.fft2(np.multiply(ftshift2dccd, illuminatesplitframe(psi,illumination,x,y)))))),2)
        generate_frame=lambda psi,illumination,x,y: np.power(np.absolute((np.fft.fft2(np.multiply(ftshift2dccd, illuminatesplitframe(psi,illumination,x,y))))),2)

        #########################################3



        # scan geometry
        scan_side=self.scanSide
        step_size_x=8.61e-09
        step_size_y=8.61e-09

        # experimental parameters
        #-------------------------
        #physical constants
        #-------------------------
        h_plank=4.1356668e-15
        c_light=299792458.0
        j2ev=6.24150934e+18
        hc=h_plank * c_light

        #--------------------------
        # source
        source = Source(10000000.0,1,1000,hc)
        lambda_=(hc / source.energy)

        # zone plate lens
        zp = Zp(2.4e-08, 0.00024, 9e-05)
        dz=2e-06

        # diffraction limited resolution from ccd NA
        dlresx=lambda_ * self.ccd.distance / (self.ccd.npixelx * self.ccd.pixel_size)
        dlresy=lambda_ * self.ccd.distance / (self.ccd.npixely * self.ccd.pixel_size)

        fname='src/frontend/aux/semfiles/Mag13K.tif'
        a=scipy.misc.imread(fname)
        img=a[0:650,0:]-37
        img=img.astype(double)
        img=img*1e-09
        nrefractive=complex(0.00051057653,- 8.30131394e-05) * 2.0
        sample_pixelsize = dlresx
        # resample image according to detector and scan width
        Nx=ceil((step_size_x * scan_side / dlresx + self.ccd.npixelx))
        Ny=ceil((step_size_y * scan_side / dlresy + self.ccd.npixely))
        #resample=lambda a: interp2linear( a.astype(double) , shape(a)[1]/2 + (arange(0,Nx) - Nx/2)*dlresx/sample_pixelsize , shape(a)[0]/2 + ((arange(0,Ny)).T - Ny/2)*dlresy/sample_pixelsize )

        M = shape(img)[0]
        N = shape(img)[1]
        f = interp2d(arange(N),arange(M),img,fill_value=0)
        temp1 = shape(a)[1]/2 + (arange(0,Nx) - Nx/2)*dlresx/sample_pixelsize
        temp2 = shape(a)[0]/2 + ((arange(0,Ny)).transpose() - Ny/2)*dlresy/sample_pixelsize
        interp_res = f(temp1,temp2)

        image2sample=lambda img,nrefractive: exp(2 * pi / (lambda_) * nrefractive * interp_res)

        # "true sample"
        psi0=image2sample(img,nrefractive)
        psi0 = np.roll(psi0,-80,0) # there is more stuff near the middle if you shift the sample...

        # attenuator
        #attenuator = Attenuator(1e-2, 1.8e-3);

        ## generate illumination and attenuator
        illumination,Fillumination,fltr, dosepp,zp=self.zp2illumination(zp,source,dz)
        attenuate=lambda x: np.multiply(x, fltr)
        deattenuate=lambda x: np.divide(x, fltr)
        illumination_small=self._cropmat( np.multiply(ftshift2d, np.fft.ifft2( self._cropmat(np.fft.fft2(np.multiply(illumination,ftshift2dccd)),(Mx, My)) )) , (mx, my) )

        # frame in real space
        generate_framer=lambda psi,illumination,x,y: illuminatesplitframe(psi,illumination,x/dlresx,y/dlresy)

        # |Fourier frame|
        generate_frame=lambda psi,illumination,x,y: generate_framepx(psi,illumination, x/dlresx,y/dlresy)
        # generate frames
        add_poisson_noise=lambda img: np.random.poisson(img) + img
        frames_noisy=lambda psi,x,y: (add_poisson_noise(attenuate(np.multiply(generate_frame(psi,illumination,x,y),dosepp)).astype('float'))) * self.ccd.hnu # poisson

        frames_rawf=lambda psi,x,y: rawXgccd(frames_noisy(psi0,x,y))+bgraw # generate scrambled frames

        new_pixel_size=self.ccd.pixel_size*self.ccd.npixelx/mx
        dlresnew= lambda_*self.ccd.distance/(mx * new_pixel_size)

        ##
        print 'photons at the sample=%f photons/pixels on ccd=%f' %(sum(np.power(abs(Fillumination),2))*dosepp,dosepp)
        print 'zone plane focus=%f (mm), sample out of focus by %f (microns)' %(zp.focaldistance*1e3,dz*1e6)
        print 'diffraction limited resolution (%f,%f) nm.' %(dlresnew*1e9, dlresnew*1e9)
        print 'simulating experiment...'

        ## scan
        illumination_position_r_x,illumination_position_r_y = ndgrid(linspace(0,(scan_side-1)*step_size_x,scan_side),linspace(0,(scan_side-1)*step_size_x,scan_side));

        # initialize frames
        nframes1 = shape(illumination_position_r_x)[0]
        nframes2 = shape(illumination_position_r_x)[1]
        nframes=size(illumination_position_r_x)
        frames=np.zeros((mx,my,nframes))
        frames_raw=np.zeros((nbmux,nmux*nrows,nframes),dtype='uint16')

        self.emit( QtCore.SIGNAL("updateTotalFrames"), nframes1*nframes2 )

        count=0
        for jj in arange(nframes2):
            for ii in arange(nframes1): 
                data=frames_noisy(psi0,illumination_position_r_x[ii,jj], illumination_position_r_y[ii,jj])
                #remove noise and filter
                frames[:,:,count]=abs(filter_(deattenuate(data)))
                #scramble it up and add noise
                frames_raw[:,:,count]=rawXgccd(data)    
                #str=(sprintf('frame=%g,\n position=[%g, %g]', ii,illumination_position_r_x(ii),illumination_position_r_y(ii)));
                #if displayraw
                #    showraw(frames_raw(:,:,ii));
                #    textraw(str);
                if displayclean:  # crop, filter, downsample
                    frame = np.power(frames[:,:,ii],2)
                    images[4] = frame
                    #ax5.imshow(frame)
                illuminated_frame=self._cropmat((illuminatesplitframe(psi0,illumination,floor(illumination_position_r_x[ii,jj]/dlresx),floor(illumination_position_r_y[ii,jj]/dlresy))),(mx, my*dlresy/dlresx))
                #ax2.imshow(abs(illuminated_frame))
                images[2] = abs(illuminated_frame)
                filteredframe=self._cropmat((splitframe(psi0,floor(illumination_position_r_x[ii,jj]/dlresx),floor(illumination_position_r_y[ii,jj]/dlresy))),(mx, my*dlresy/dlresx))
                #ax3.imshow(abs(filteredframe))
                images[1] = abs(filteredframe)
                frameraw = frames_raw[:,:,count]
                #ax4.imshow(abs(gccdXraw(frameraw)))
                images[3] = abs(gccdXraw(frameraw))
                #ax1.imshow(abs(psi0))
                images[0] = abs(psi0)
                images[6] = illumination_position_r_x[ii,jj]/dlresx+self.ccd.npixelx/2
                images[7] = illumination_position_r_y[ii,jj]/dlresy+self.ccd.npixely/2
                #ax1.plot(illumination_position_r_x[ii,jj]/dlresx+ccd.npixelx/2, illumination_position_r_y[ii,jj]/dlresy+ccd.npixely/2, 'ro', markersize=15.0,fillstyle='none')
                #fig.canvas.draw()
                #fig.clf()
                self.emit( QtCore.SIGNAL("updateGUI"), images )
                self.emit( QtCore.SIGNAL("updateCurrentFrame"), count )
                count = count+1
                

        Fillumination_f=filter_(Fillumination)
        Fillumination_mask_f=abs(Fillumination_f)>(abs(Fillumination_f)).max()/2
        ## save everything
        energy=source.energy/j2ev
        real_translation=np.hstack([np.reshape(illumination_position_r_x,(size(illumination_position_r_x),1)),np.reshape(illumination_position_r_y,(size(illumination_position_r_y),1))])
        ### save raw
        det_distance=self.ccd.distance
        step_size=step_size_x
        corner_pos = np.hstack([np.array([self.ccd.npixelx, self.ccd.npixely])/2*self.ccd.pixel_size,self.ccd.distance])
        image=psi0
        illumination_mask=abs(np.fft.fftshift(Fillumination))>np.spacing(1)
        cxiname='python_sim_15x15.cxi'
        self.writecxi1(cxiname,frames_raw,backgrounds,image,illumination,Fillumination_mask_f,real_translation,energy,det_distance, corner_pos,self.ccd.pixel_size)

        
        #for i in range(6):
        #    time.sleep(0.3) # artificial time delay
        #    self.emit( QtCore.SIGNAL('update(QString)'), "from work thread " + str(i) )
        return



__author__ = 'David Shapiro'

import time, os, glob
from setproctitle import *
import subprocess
from multiprocessing import Process
from ptycho import *
from executeParam import *
from datetime import date
import time, shutil
from util import e2l


def cleanDirs(path, file):

    if not(os.path.isdir(path + 'tmp')):
        os.mkdir(path + 'tmp')
    os.rename(path + file, path + 'tmp/' + str(time.time()) + '_param.txt')

def runServer(conn = None):

#######################################################################################################################
#######################################################################################################################

    scriptPath = os.environ['NSRECONSERVERROOT']
    scriptName = 'param.txt'
    setproctitle('nsTmpProcess')

    startServer = 1

    if startServer:
        setproctitle('nsReconServer')
        print "Welcome to nsReconServer!  Now waiting for new scripts in %s" %scriptPath
        while True:
            a = glob.glob(scriptPath + scriptName)
            if len(a) == 0:
                pass
            elif len(a) > 1:
                print "Found ambiguous file list"
            else:
                print "Found file: %s" %a[0]
                p = Process(target = runParam, args = (loadParam(scriptPath, scriptName),))
                cleanDirs(os.environ['NSRECONSERVERROOT'], 'param.txt')
                p.start()
                p.join()
                print "Waiting for new scripts..."

            time.sleep(0.2)

            if conn != None:
                if conn.poll():
                    if conn.recv():
                        print "nsReconServer received kill signal.  Exiting."
                        return


    else: print "Exiting."

runServer()

__author__ = 'David Shapiro, LBNL'

from numpy import array, single, round
import os

def readASCIIMatrix(filename,separator='\t'):

    data = []
    f = open(filename,'r')
    for line in f:
        row = line.split(separator)
        data.append(row[0:len(row)-1])
    return array(data).astype('float')


class xIM(object):

    def __init__(self,filePrefix):

        self.filePrefix = filePrefix
        self.data = {}

        self.__readHDR()
        # self.__loadChannels()

    def __readHDR(self):

        hdr = open(self.filePrefix + '.hdr').read().split('\r\n')
        temp = hdr[1].split(';')
        self.date = temp[0].split('_')[1][0:6]
        self.scanNumber = temp[0].split('_')[1][6:9]
        self.bl = temp[0].split('_')[0][-2::].lower()
        self.dwell = int(temp[3].split('=')[1].lstrip())
        self.xPoints = hdr[5].split(',')
        self.yPoints = hdr[9].split(',')
        self.nxPoints = int(self.xPoints[0].split('(')[1])
        self.nyPoints = int(self.yPoints[0].split('(')[1])
        self.xPoints = self.xPoints[1::1]
        self.yPoints = self.yPoints[1::1]
        self.xPoints = [float(self.xPoints[i].split(');')[0].lstrip()) for i in xrange(self.nxPoints)]
        self.yPoints = [float(self.yPoints[i].split(');')[0].lstrip()) for i in xrange(self.nyPoints)][::-1]
        self.dx = abs(self.xPoints[1] - self.xPoints[0])
        self.dy = abs(self.yPoints[1] - self.yPoints[0])
        self.energyList = hdr[23].split('=')[1]
        self.nEnergies = self.energyList.split(',')[0].lstrip(' (')
        self.energies = [item.rstrip(');') for item in self.energyList.split(',')[1:]]

        # self.nChannels = int(hdr[17].split('(')[1].split(',')[0])
        ##find the line with the SmarAct stage for the angle
        angleStr = [s for s in hdr if 'SmartAct' in s]
        if any("ShutterAutomatic = false" in s for s in hdr): self.shuttermode = 'closed'
        else: self.shuttermode = 'auto'
        if len(angleStr): self.angle = round(single(angleStr[0].split(';')[1].split('=')[1]),2)

        # if self.nChannels > 0:
        #    self.recordedChannels = [hdr[18 + i].split(';')[0].split('=')[1].lstrip().strip('"') \
        #                                   for i in xrange(self.nChannels)]
        #    self.__labels = map(chr,range(97, 97 + self.nChannels))

    def __loadChannels(self):
        for channel in self.recordedChannels:
            filename = self.filePrefix + "_" + self.__labels[self.recordedChannels.index(channel)] + '.xim'
            self.data[channel] = readASCIIMatrix(filename)

def readXIM(filename):

    if os.path.isfile(filename):

        if filename.split('.')[-1] == 'xim':
            return readASCIIMatrix(filename)
        elif filename.split('.')[-1] == 'hdr':
            filePrefix = filename.split('.')[0]
            return xIM(filePrefix)
        else:
            print "readXIM Error: file type not supported."
            return

    else:
        print "readXIM Error: no such file"
        return 1
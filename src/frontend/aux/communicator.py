__author__ = 'davidshapiro'
from mpi4py import MPI
import sys
from readCXI import readCXI
import numpy as np
from util import *
from ptycho import prPtycho
from param import *
from executeParam import *
import logging
import time, os
from scipy import rand, misc
from optparse import OptionParser
import cProfile
# from pycallgraph import PyCallGraph
# from pycallgraph.output import GraphvizOutput
import warnings
from imageIO import *

warnings.simplefilter("ignore", RuntimeWarning)

def processor(comm):

    """
    Processor function which receives data from RANK 0 and executes the reconstruction.
    This function must know how many reconstruction loops to execute so it knows whether
    or not it should be waiting for data.  It receives the number of loops first, then
    waits for CXI objects that number of times.

    Input: CXI object (received from RANK 0 prior to reconstruction)
    Output: CXI object (sent to RANK 0 after reconstruction)
    """

    nLoops = comm.recv(source = 0, tag = 11)
    i = 0
    while i < nLoops:
        if comm.recv(source = 0, tag = 11):
            break
        ptychoObj = comm.recv(source=0, tag = 11)
        ptychoObj.start()
        comm.send(ptychoObj, dest = 0, tag = 12)
        i += 1
    return

def getStartImage(ptychoObj, randomImage = False):

    """
    Generate the starting image from the input CXI object.  This image can be either a flat field
    or a random image.  It gets the correct image size from the translation values in the ptychoObj.

    Input: ptychoObj (ptycho object)
    Output: ptychoObj.QH() (aligned image)
    """

    if randomImage: print "Generating random start image."

    ny, nx = ptychoObj.cxi.ccddata[0].shape
    pPosVec = ptychoObj.cxi.pixelTranslations()

    nFrames = len(ptychoObj.cxi.ccddata)
    oSizeY, oSizeX = ptychoObj.cxi.imageShape()

    if randomImage:
        oVec = np.ones((ptychoObj.nModes, nFrames, ny, nx), complex) * rand(ny,nx) * np.exp(-1j * rand(ny,nx) * np.pi / 2.)
    else: oVec = np.ones((ptychoObj.nModes, nFrames, ny, nx), complex)

    indices = []
    for p in pPosVec:
        x1 = p[1] - nx / 2.
        x2 = p[1] + nx / 2.
        y1 = p[0] - ny / 2.
        y2 = p[0] + ny / 2.
        indices.append((y1,y2,x1,x2))

    return ptychoObj.QH(oVec, indices, pPosVec, (oSizeY, oSizeX))

def main(cxiObj, comm):

    """
    This function takes the CXI object as input, splits the data according to the number of processors,
    generates a CXI object for each data chunk, sends each chunk to the processors for reconstruction,
    retrieves the results, combines into a single image, splits and repeats, saves the reconstruction into
    the given CXI file.

    Input: CXI object
    Output: none
    """

    cxiObj.process['itnGlobal'] = '0' #Keeps track of the global iteration number
    nIterations = int(cxiObj.process['nIterations']) #total number of iterations
    loopSize = int(cxiObj.process['loopSize']) #number of iterations between synchronizations
    nLoops = nIterations / loopSize #total number of synchronizations to do
    nProcessors = comm.Get_size() - 1 #user must request processors equal to a total square plus 1.
    nBlocks = np.sqrt(nProcessors) #number of subdivisions per dimension, same for each
    nGroups = nProcessors  #nBlocks**2, total processes always a perfect square
    nSlices = len(cxiObj.translation) #number of frames in the dataset
    pixm = cxiObj.pixm()
    threshError = float(cxiObj.process['threshError'])
    useThreshError = threshError != 0.

    logging.info("Using %i CPUs." %nGroups)
    logging.info("Doing %i loops of %i iterations..." %(nLoops, loopSize))
    logging.info("Using BETA: %.2f" %np.float(cxiObj.process['beta']))
    logging.info("Using ETA: %.2f" %np.float(cxiObj.process['eta']))
    logging.info("Using %s modes" %cxiObj.process['nModes'])
    logging.info("Using defocus: %s microns" %cxiObj.process['defocus'])
    if int(cxiObj.process['pR']): logging.info("Doing probe retrieval.")
    if int(cxiObj.process['bR']): logging.info("Doing background retrieval.")

    ##defocus the probe
    if float(cxiObj.process['defocus']) != 0.:
        print "Defocusing probe by ", float(cxiObj.process['defocus']), float(cxiObj.process['defocus']) / pixm / 1e6, e2l(cxiObj.energy / 1.6e-19) / pixm / 1e9
        cxiObj.probe = propnf(cxiObj.dataProbe, float(cxiObj.process['defocus']) / pixm / 1e6, e2l(cxiObj.energy / 1.6e-19) / pixm / 1e9)

    for rank in range(nProcessors):
        comm.send(nLoops, rank + 1, tag = 11)

    def splitImage(cxiO):

        """
        Split the main CXI object into smaller chunks according to the number of processors
        requested.  The splitting logic currently works best for square scan grids.  Circular grids will
        work but the load balance won't be uniform.

        Input: CXI object (full dataset)
        Output: List of CXI objects (sub-datasets)
        """

        subImageCenterTranslations = np.zeros((nGroups, 2))
        indexGroups = {}

        #TODO: optimize this for circular/spiral scans.

        ##Locate the center positions of the different blocks
        xmin, xmax = cxiO.translation[:,0].min(), cxiO.translation[:,0].max()
        ymin, ymax = cxiO.translation[:,1].min(), cxiO.translation[:,1].max()
        blockSizeY, blockSizeX = cxiO.imageShape()
        blockSizeY, blockSizeX = blockSizeY * pixm / nBlocks, blockSizeX * pixm / nBlocks
        xCenters = np.linspace(xmin + blockSizeX / 4, xmax - blockSizeX / 4, nBlocks)
        yCenters = np.linspace(ymin + blockSizeY / 4, ymax - blockSizeY / 4, nBlocks)

        for i in range(nGroups):
            subImageCenterTranslations[i] = xCenters[i % len(xCenters)], yCenters[i / len(xCenters)]
            key = 'grp' + str(i)
            indexGroups[key] = []

        ##for each position in the translation list, put it in the closest block
        d = np.zeros((nGroups))
        for i in range(nSlices):
            d = (subImageCenterTranslations[:,0] - cxiO.translation[i,0])**2 + (subImageCenterTranslations[:,1] - cxiO.translation[i,1])**2
            indexGroups['grp' + str(d.argmin())].append(i)

        for i in range(len(indexGroups)):
            key = 'grp' + str(i)
            if len(indexGroups[key]) == 0: logging.warning("0 entries! Discarding index group"); del indexGroups[key]

        results = []

        ##set up the individual CXI objects for each block reconstruction
        for key in indexGroups.keys():
            thisCXI = cxi()
            thisCXI.process = cxiO.process
            thisCXI.process['gpu'] = '0'
            thisCXI.process['nIterations'] = loopSize
            thisCXI.ccddata = cxiO.ccddata[indexGroups[key]]
            thisCXI.translation = cxiO.translation[indexGroups[key]]
            thisCXI.probe = cxiO.probe
            thisCXI.bg = cxiO.bg
            thisCXI.probemask = cxiO.probemask
	    thisCXI.probeRmask = cxiO.probeRmask
	    thisCXI.illuminationIntensities = cxiO.illuminationIntensities
            thisCXI.corner_x, thisCXI.corner_y, thisCXI.corner_z = cxiO.corner_x, cxiO.corner_y, cxiO.corner_z
            thisCXI.ccddistance = cxiO.ccddistance
            thisCXI.stxm = cxiO.stxm
            thisCXI.startImage = cxiO.startImage
            thisCXI.process['usePrevious'] = '1'
            thisCXI.corner = cxiO.corner
            thisCXI.energy = cxiO.energy
            results.append(prPtycho(thisCXI))

        return results

    def syncImage(ptychoList):

        """
        Recombine the small CXI objects into a single dataset.  The images are merged and
        then normalized by the illumination profile.

        Input: List of CXI objects
        Output: Combined image, average probe, average background
        """

        imageNorm = np.zeros((ptychoList[0].oSum.shape))
        image = np.zeros((ptychoList[0].oSum.shape), complex)
        probe = np.zeros((ptychoList[0].probe.shape), complex)
        bg = np.zeros((ptychoList[0].bg.shape))

        for item in ptychoList:
            thisIllumination = item.QHP()
            thisIllumination = thisIllumination / thisIllumination.max()
            imageNorm += thisIllumination
            image += item.oSum * thisIllumination
            probe += item.probe / len(ptychoList)
            bg += item.bg / len(ptychoList)

        image = image / imageNorm
        image[np.isnan(image)] = 0.
        image[np.isinf(image)] = 0.

        for item in ptychoList:
            item.oSum = image.copy()
            item.previousImage = image.copy()
            item.bg = bg
            item.probe[0,0] = probe[0,0]

        #return image, probe, bg
        return ptychoList, image, probe, bg

    cxiObj.process['usePrevious'] = '0'
    if cxiObj.process == None: cxiObj.process = Param().param #default parameter list if not present in cxi
    ptychoObj = prPtycho(cxiObj)
    ptychoObj.useGPU = 0
    ptychoObj.nit = 1
    ptychoObj.usePrevious =  False
    if cxiObj.process['usePrevious'] == '0': cxiObj.startImage = getStartImage(ptychoObj, randomImage = True)
    if cxiObj.startImage == None: cxiObj.startImage = getStartImage(ptychoObj, randomImage = True)
    ptychoObj.previousImage = cxiObj.startImage.copy()
    results = splitImage(cxiObj)
    nGroups = len(results)

    amDone = False

    print "Sending data to processors and starting iteration."
    t0 = time.time()
    prevError = 1e15
    for i in range(nLoops):
        thisError = 0
        t1 = time.time()
        for rank in range(nGroups):
            comm.send(amDone, rank + 1, tag = 11)
        if amDone:
            break
        for rank in range(nGroups):
            comm.send(results[rank], rank + 1, tag = 11)
        for rank in range(nGroups):
            results[rank] = comm.recv(source = rank + 1, tag = 12)
            thisError += results[rank].e
        thisError /= nGroups
        print "Error for iteration %s of max %s: %.4f" %(loopSize * (i + 1), nIterations, thisError)
        results, ptychoObj.oSum, ptychoObj.probe, ptychoObj.bg = syncImage(results)
        logging.info("Completed loop %i of %i in %.2f seconds" %(i + 1, nLoops, time.time() - t1))
        deltaError = prevError - thisError
        if useThreshError and deltaError < threshError:
            amDone = True
            print "deltaError is now less than threshError! Stopping iteration..."
        prevError = thisError

    # ##save and exit
    outFile = cxiObj.process['scanDir'] + cxiObj.process['outputFilename']
    print "Saving results to file %s" %(cxiObj.process['scanDir'] + cxiObj.process['outputFilename'])
    ptychoObj.save(fileName = outFile)


    # misc.toimage((np.abs(ptychoObj.oSum))).save('image_abs.tiff')
    # misc.toimage((np.angle(ptychoObj.oSum))).save('image_phase.tiff')
    # misc.toimage((np.log(np.abs(ptychoObj.probe[0,0])))).save('probe_abs.tiff')

    print "Iteration time: %.2f seconds" %(time.time() - t0)


if __name__ == '__main__':

    comm = MPI.COMM_WORLD
    commSize = comm.Get_size()
    rank = comm.Get_rank()

    if rank == 0:

        ###Check for in input file or CLI options
        if len(sys.argv) == 1:
            print "Parameter or CXI file argument needed."
            print "Use the -h option for a full list of options"
            sys.exit()
        elif len(sys.argv) > 1: inputFile = sys.argv[1]

        parser = OptionParser()
        parser.add_option("-b", "--beta", dest="beta", default = '0.75',
                          help="BETA parameter for RAAR.  Defaults to 0.75")
        parser.add_option("-e", "--eta", dest="eta", default = '1.1',
                          help="ETA parameter for background retrieval.  Defaults to 1.1")
        parser.add_option("-i", "--iterations", dest="nIterations", default = '1500',
                          help="Maximum Number of Iterations.  Defaults to 1500")
        parser.add_option("-r", dest='illuminationPeriod', default = '0',
                          help="Illumination refinement period.  Defaults to 0 (off)")
        parser.add_option("-T", dest='backgroundPeriod', default = '0',
                          help="Background refinement period.  Defaults to 0 (off)")
        parser.add_option("-M", dest = 'probeMask', default = '0', action = "store_true",
                          help="Enforce Fourier mask when refining illumination. Defaults to Off")
        parser.add_option("-L", dest='loopSize', default = '10',
                          help="Period of global synchronization of MPI processes.  Defaults to 10")
        parser.add_option("-P", dest='usePrevious', default = '0', action = "store_true",
                          help="Start iterating from the previously stored reconstruction.  Defaults to off")
        parser.add_option("-R", dest='roundPixels', default = '0', action = "store_true",
                          help="Round pixel sizes up.  Defaults to off")
        parser.add_option("-s", dest='s_threshold', default = '0',
                          help="Upper limit on the diffraction intensity.  Defaults to 2000.")
        parser.add_option("-d", dest='defocus', default = '0', help="Probe defocus.  Defaults to 0.")
        parser.add_option("-f", dest='outputFilename', default = '', help="Output file name.  Defaults to input file.")
        parser.add_option("-I", dest = 'useIlluminationIntensities', default = '0', action = "store_true", help = "Enforce illumination intensities.  Defaults to OFF")
        parser.add_option("-m", "--modes", dest='nModes', default = '1', help="Number of coherent modes. Defaults to 1")
        parser.add_option("--log", dest = 'logFile', default = 'ptychoLog.txt', help = "Log file")
        parser.add_option("--log_level", dest = 'logLevel', default = '0', help = "Logging level. 0 = DEBUG, 1 = INFO, 2 = WARNING, 3 = ERROR")
        parser.add_option("-E", dest='threshError', default = '0.0', help = "Threshold Error. Any change in error less than this value will end iteration. Defaults to 0.0")

        ##Get the parser args and convert to a dict
        (options, args) = parser.parse_args()
        options = vars(options)
        options['probeMask'] = str(int(options['probeMask']))
        options['usePrevious'] = str(int(options['usePrevious']))
#        options['logFile'] = os.environ["PTYCHODATAROOT"] + '/' + options['logFile']

        ###Log the CLI parameters
        for option in options.keys():
            logging.info("%s: %s" %(option,options[option]))

        ###set the global logging level
        if options['logLevel'] == '0':
            LL = logging.DEBUG
        if options['logLevel'] == '1':
            LL = logging.INFO
        if options['logLevel'] == '2':
            LL = logging.WARNING
        if options['logLevel'] == '3':
            LL = logging.ERROR

        ###Start the logger
        logging.basicConfig(format='%(asctime)s %(message)s', filename = options['logFile'], level = LL)
        logging.debug("Welcome to prPtycho MPI!")


        ##Check if the input is a param.txt or a cxi file and handle accordingly
        if inputFile.split('.')[-1] == 'txt':
            param = readParamFile(inputFile)
            inputFile = param['scanDir'] + param['dataFile']
            cxiObj = readCXI(inputFile)
        elif inputFile.split('.')[-1] == 'cxi':
            cxiObj = readCXI(inputFile)
        logging.info("Read CXI file: %s" %inputFile)

        cxiObj.process['dataFile'] = inputFile.split('/')[-1]
        cxiObj.process['scanDir'] = inputFile.split(cxiObj.process['dataFile'])[0]
        cxiObj.process['nBlocks'] = str(int(np.sqrt(commSize - 1)))
        cxiObj.process['nCPU'] = str(commSize - 1)
        cxiObj.process['gpu'] = '0'

        for key in options.keys():
            cxiObj.process[key] = options[key]
        if cxiObj.process['backgroundPeriod'] == '0':
            cxiObj.process['bR'] = '0'
        else: cxiObj.process['bR'] = '1'
        if cxiObj.process['illuminationPeriod'] == '0':
            cxiObj.process['pR'] = '0'
        else: cxiObj.process['pR'] = '1'
        if cxiObj.process['outputFilename'] == '': cxiObj.process['outputFilename'] = cxiObj.process['dataFile']
        if cxiObj.illuminationIntensities == None:
            print "No illumination intensities found in CXI file"
            cxiObj.process['useIlluminationIntensities'] = '0'

        #####Generate CXI object from param and call the main function
        #with PyCallGraph(output=GraphvizOutput()): main(cxiObj, comm)

        cxiObj.translation[:,1] = cxiObj.translation[:,1] - (cxiObj.translation[:,1]).min()

        ##In not already available, generate the datamean, probe, and probemask
        if cxiObj.datamean == None: cxiObj.datamean = cxiObj.ccddata.sum(axis = 0)
        if cxiObj.probe == None:
            cxiObj.probe = np.sqrt(cxiObj.datamean) * cxiObj.probemask
            cxiObj.probe = np.fft.ifftshift(np.fft.ifftn(cxiObj.probe))
        if cxiObj.probemask == None:
            cxiObj.probemask = np.fft.fftshift((cxiObj.datamean > 0.05 * cxiObj.datamean.max()))

        main(cxiObj, comm)
    else:
        processor(comm)


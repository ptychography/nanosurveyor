from scipy.misc import imread
from scipy import ndimage
from code.readCXI import *
import sys
import numpy as np

#################################################################
### Initial setup, paths, and initiate the CXI object with some parameters
inputPath = '../data/frames/' #location of the data frames
outputFile = 'test.cxi' #output file name
ccdz = 0.015487 #sample to detector distance in meters
xSteps = 30 #x scan points
ySteps = 30 #y scan points
stepSize = 52.82 #scan step size in nm
en = 710. #energy in eV
nFrames = xSteps * ySteps #number of data frames measured, assumes regular grid
filePrefix = 'image_' #prefix to the file name counter
stxmThreshold = 500.

#################################################################

cxiObj = cxi() #initiate the CXI object
cxiObj.process = {} #the meta-data is an empty dictionary since this is simulation
cxiObj.beamline = 'ALS 5.3.2.1' #beamline string
cxiObj.energy = en * 1.602e-19 #energy in eV converted to Joules
cxiObj.xpixelsize = 20.e-6 #detector pixel size in meters
cxiObj.ypixelsize = 20.e-6 #detector pixel size in meters

#################################################################
### Load the frames
i = 0 ##file name counter starts at 0
while (i < nFrames): 

	sys.stdout.write('Loading frame %i \r' %i)
	sys.stdout.flush()
	fileName = inputPath + filePrefix + str(i) + '.tif'
	if i == 0:
		frame = imread(fileName)
		ny, nx = frame.shape
		cxiObj.ccddata = np.zeros((nFrames,ny, nx))
		cxiObj.ccddata[i,:,:] = frame
	else:
		cxiObj.ccddata[i,:,:] = imread(fileName)
	i += 1
	
#################################################################
### Calculate the probe and probe Fourier mask
cxiObj.datamean = np.fft.fftshift(cxiObj.ccddata.sum(axis = 0) / nFrames)
cxiObj.probemask = cxiObj.datamean > 0.1 * cxiObj.datamean.max()
cxiObj.probe = np.fft.ifftshift(np.fft.ifftn(np.sqrt(cxiObj.datamean * cxiObj.probemask)))
cxiObj.probe /= abs(cxiObj.probe).max()


################################################################
### Calculate the STXM image, assumes regular grid
cxiObj.corner_x, cxiObj.corner_y, cxiObj.corner_z = nx / 2 * cxiObj.xpixelsize, ny / 2 * cxiObj.ypixelsize, ccdz #detector corner position
l_nm = 1239.852 / en #lambda in nm
theta = np.arctan(cxiObj.corner_x / cxiObj.corner_z) #max scattering angle
xpixnm = l_nm / 2 / theta #pixel size in nm
xR, yR = stepSize * xSteps, stepSize * ySteps
cxiObj.stxm = np.zeros((ySteps, xSteps))
for k in range(nFrames):
	i = ySteps - k / xSteps - 1
	j = k % xSteps
	cxiObj.stxm[i,j] = (cxiObj.ccddata[k] * (cxiObj.ccddata[k] > stxmThreshold)).sum()

ssx, ssy = stepSize, stepSize
y,x = np.meshgrid(np.linspace(0,yR,ySteps),np.linspace(0,xR,xSteps))
y,x = y.transpose(), x.transpose()
yp,xp = np.meshgrid(np.linspace(0,yR,ySteps * (ssy / xpixnm)),np.linspace(0,xR,xSteps * (ssx / xpixnm)))
yp,xp = yp.transpose(), xp.transpose()
x0 = x[0,0]
y0 = y[0,0]
dx = x[0,1] - x0
dy = y[1,0] - y0
ivals = (xp - x0)/dx
jvals = (yp - y0)/dy
coords = np.array([ivals, jvals])
cxiObj.stxmInterp = ndimage.map_coordinates(cxiObj.stxm.transpose(), coords)

################################################################
### Setup the scan translation geometries, assuming regular scan grid
cxiObj.indices = np.arange(nFrames) #indices for the GUI
xR, yR = xSteps * stepSize, ySteps * stepSize #scan range in nm
xp, yp = np.meshgrid(np.arange(0 , xSteps) * stepSize, np.arange(0, xSteps) * stepSize)
xpts, ypts = xp.flatten(), yp.flatten()[::-1]
cxiObj.translation = np.column_stack((xpts * 1e-9, ypts * 1e-9, np.zeros(xpts.size)))


###############################################################
### Write CXI file
writeCXI(cxiObj, fileName = inputPath + outputFile)


# -*- coding: utf-8 -*-
##Import statements
from pyptycho.util import circle, dist, shift, congrid, addNoise, genProbe
from pyptycho.imageIO import imload, imsave
import numpy as np
from scipy import rand, ndimage, misc
import pylab
import scipy.stats.distributions as poisson
from random import gauss
from scipy.ndimage.interpolation import rotate
import os
import time, os, glob
from setproctitle import *
from datetime import date
import time, shutil
import sys, os
from scipy.ndimage.morphology import binary_dilation
from scipy import ndimage, misc
from pyptycho.param import *
from pyptycho.executeParam import *
from pyptycho.readCXI import *

##setup the input file 
imageFile = os.environ['PYPTYCHOROOT'] + '/pyptycho/examples/composite.npy'
outputFile = os.getcwd() + '/test.cxi'
saveFrames = False
saveOutput = True
generateData = True
photons_per_s = 1e9 #probably a good value to stick with
exposure_s = 0.01
saturate = False #saturates high intensities
beamstop = False #attenuates the central portion of the diffraction data
bg_offset = False #constant background offset
bg_scale = 0.01 #ratio of total background signal to diffraction signal

##not so interesting stuff here
propDistance = 0.  ##defocus of the probe in nanometers
dZ = 0.0 ##percentage error on the CCDZ distance,can be negative, also doesn't have much affect
dE = 0.0 ##percentage error on the energy, can be negative, also doesn't have much affect
t_exp = 0.01 * exposure_s, exposure_s #seconds, only needed for dual exposure mode
transmission_error = 0.0 #percentage error in beamstop transmission, doesn't actually have much affect
save_data = True #this tells the program to save all data
shearError = False #adds a shear error to the positions, don't mess with for now
randomError = False #adds a random error to the positions, don't mess with for now
errorSize = 10 #size of the position error in pixels, don't mess with for now
dual_exposure = False #probably not so interesting now
partialCoherence = False

##############################################
####################
readoutLevel = 50.
noise_threshold = 50.
beamstop_transmission = 0.01
saturation_threshold = 5e4

nStepsX = 10 #number of steps in the x-direction
nStepsY = 10 #number of steps in the y-direction

#optical constants and energy
## for the matrix, let's use either vacuum (delta,beta)=(1,0) or carbon.  Get the values from carbon from the
##CXRO website.  For LFP, let's use Young-Sang's numbers.
l_nm = e2l(710.) #wavelength to use for nearfield case
matrix_delta, matrix_beta = 1., 0.
matrix_t = 1.0
lfp_delta, lfp_beta = 0.00014, 0.000085
fp_delta, fp_beta = 0.00028, 0.0000425
fp_t = 1000.0 #thickness in nanometers, probably doesn't need to change, just sets the scale

##load the pre-determined sample image
print "Loading the image data..."
temp = np.load(imageFile)
o = np.zeros(temp.shape, complex)

#replace the integer values with the optical constants
o[temp == 0] = 0. + 0j #exp(-2. * pi * matrix_t * (matrix_delta + 1j * matrix_beta) / l_nm)
o[temp == 1] = np.exp(2. * np.pi * (fp_t * (lfp_delta + 1j * lfp_beta)) / l_nm)
o[temp == 2] = np.exp(2. * np.pi * (fp_t * (fp_delta + 1j * fp_beta)) / l_nm)

#blur it a bit
#o = ndimage.filters.gaussian_filter(o.real, sigma = 1) + 1j * ndimage.filters.gaussian_filter(o.imag, sigma = 0.65)
n = len(o)

##set up the probe, make it like a STXM beam
print "Generating probe using genProbe function"
p_window = 256  # probe is contained within an array of this size
p = genProbe(n = p_window,x = 10., d = 160.,e = 700.,z = propDistance, a = 80., sigma = 1, saveFile = None)
ptemp = p.copy()
n_photons = exposure_s * photons_per_s
pshape = p.shape
psum = np.zeros(o.shape)
osum = psum.copy()
p = p / abs(p).max()

##set up the scan points
p_step = 5.
xR, yR = nStepsX * p_step, nStepsY * p_step
dX, dY = 0., 0.
if shearError:
    dX, dY = np.linspace(-errorSize / 2, errorSize / 2, nStepsX), 0. * linspace(-errorSize / 2, errorSize / 2, nStepsY)
    dX, dY = np.meshgrid(dX, dY)
    dX, dY = dX.flatten(), dY.flatten()
elif randomError:
    dX, dY = errorSize * rand(nStepsX), errorSize * rand(nStepsY)
    dX, dY = np.meshgrid(dX, dY)
    dX, dY = dX.flatten(), dY.flatten()

xp = np.round(np.linspace(-xR / 2 + p_step / 2, xR / 2 - p_step / 2, nStepsX))
yp = np.round(np.linspace(-yR / 2 + p_step / 2, yR / 2 - p_step / 2, nStepsY))

x,y = np.meshgrid(xp,yp)
xpts,ypts = x.flatten(),y.flatten()
npoints = len(xpts)
indx = np.arange(npoints)

##set up the constant background offset
bg = 0.000001 * np.ones(pshape)
if bg_offset: 
    #bg = ndimage.filters.gaussian_filter(rand(p_window,p_window), sigma = 5.)
    bg = shift(np.exp(-dist(p_window)**2 / 3.), 100, 100)

data = np.zeros((npoints,pshape[0],pshape[1]))
data_ave = np.zeros(p.shape)

##set up the beamstop
if beamstop:
    print "Generating beamstop"
    beamstop = (np.ones(pshape) - ndimage.filters.gaussian_filter(circle(pshape[0],15,pshape[0]/2,pshape[0]/2) * (1. - beamstop_transmission), sigma = 0.5))
    ibeamstop = 1. / (np.ones(pshape) - ndimage.filters.gaussian_filter(circle(pshape[0],15,pshape[0]/2 + 0,pshape[0]/2) * (1. - (1. - transmission_error) * beamstop_transmission), sigma = 0.5))
else:
    print "Not using beamstop"
    beamstop = np.ones(pshape)
    ibeamstop = np.ones(pshape)

##normalize the probe
p = np.sqrt(n_photons) * p / np.abs(p).sum()
probe = p.copy()

if partialCoherence:
    xsig, ysig = 15., 15.
    xl,yl = np.meshgrid(np.linspace(0,pshape[0],256),np.linspace(0,pshape[1],256))
    g = np.exp(-2. * np.pi * (xl**2 / xsig**2 + yl**2 / ysig**2))
    gf = np.fft.ifftn(g)

##generate the diffraction data
if generateData:
    for k in range(np.int(npoints)):

        sys.stdout.write(" Doing frame %i of %i.  \r" %(k+1,npoints))
        sys.stdout.flush()
        x, y = np.int(xpts[k]), np.int(ypts[k])
        m = len(p)

        psi = shift(o,-x,-y)[n / 2 - pshape[0]/2:n / 2 + pshape[0]/2, n / 2 - pshape[1]/2:n / 2 + pshape[1]/2]
        psi = psi * p
        temp = np.abs(psi)
        if (k==0): dx,dy = 0.,0.
        if (k>0): dx,dy = x - np.int(xpts[k-1]),y - np.int(ypts[k-1])
        psum = shift(psum, -dx, dy)
        psum[n / 2 - pshape[0]/2:n / 2 + pshape[0]/2, n / 2 - pshape[1]/2:n / 2 + pshape[1]/2] += abs(p)**2
        psif = np.fft.ifftshift(np.fft.fftn(np.fft.fftshift(psi))) ##go to reciprocal space
        psif = np.abs(psif * psif.conjugate()) ##get intensity
        bg = bg * (bg_scale * n_photons) / bg.sum()
        bg_mean = bg.mean()
        if partialCoherence: psif = np.abs(np.fft.fftn(np.fft.ifftn(psif) * gf))
        psi_long = n_photons * psif / psif.sum() * beamstop + bg * (bg_scale * n_photons) / bg.sum() * beamstop
        psi_long = addNoise(psi_long, readout = readoutLevel) ##add photon statistics and read noise
        psi_long -= noise_threshold ##subtract the read noise
        psi_long -= bg_mean
        psi_long = np.clip(psi_long, 0., psi_long.max() - saturate * (psi_long.max() - saturation_threshold))
        psi_long *= ibeamstop ##renormalize the beamstop

        if bg_offset: psi_long += bg

        if dual_exposure:
            psi_short = (addNoise((n_photons * t_exp[0] / t_exp[1] * psif / psif.sum()), readout = readoutLevel)).astype('float')
            psi_short -= noise_threshold
            psi_short = np.clip(psi_short, 0., psi_short.max())
            if bg_offset: psi_short += bg * t_exp[0] / t_exp[1]
            saturation_mask = psi_long < saturation_threshold
            psi_long = psi_long * saturation_mask + psi_short * (1 - saturation_mask) * t_exp[1] / t_exp[0]

        if bg_offset:
            pass

        #put psi into 3D data matrix that will get saved to CXI file
        data[k,:,:] = psi_long

        if saveFrames:
            im = misc.toimage(psi_long, high = psi_long.max(), low = psi_long.min(), mode = 'F')
            fileName =  'image_' + str(k) + '.tif'
            im.save(fileName)


    ##################################################################################
    ##################################################################################
    # defined in param.txt instead for experimental data
    #calculate the positions needed for the CXI file
    x_pixel_size = 20.e-6
    y_pixel_size = 20.e-6
    ccdz = 0.015487 * (1. + dZ)
    ccd_corner_x = x_pixel_size * pshape[0] / 2.
    ccd_corner_y = y_pixel_size * pshape[1] / 2.
    theta = ccd_corner_x / ccdz
    xpixnm = l_nm / 2 / theta
    e = 1240./l_nm
    e = (1. + dE) * e
    corner_position = [ccd_corner_x, ccd_corner_y, ccdz]
    xpts,ypts = np.array(xpts + dX).astype('float32'),np.array(ypts + dY).astype('float32')
    translationX, translationY = (xpts - xpts.min()) * xpixnm * 1e-9,(ypts - ypts.min())[::-1] * xpixnm * 1e-9
    translation = np.column_stack((translationX, translationY, np.zeros(translationY.size)))
    bl = 'sim'

    #-----------------------------------------------
    data_ave = data.sum(axis = 0)
    data_ave = np.fft.fftshift(data_ave)
    pMask = (data_ave > 0.05 * data_ave.max())
    p = np.sqrt(data_ave) * pMask
    p = np.fft.ifftshift(np.fft.ifftn(p))
    p = p / abs(p).max()

    ##################################################################################
    ##################################################################################
    # generate the stxm image
    stxmImage = np.zeros((nStepsY, nStepsX))
    for k in range(nStepsX * nStepsY):
        i = k / stxmImage.shape[1]
        j = k % stxmImage.shape[1]
        stxmImage[i,j] = (data[k] * np.fft.fftshift(pMask)).sum()
    stxmImage = stxmImage[::-1,:]

    ##########################################################
    ####interpolate STXM image onto the same mesh as the reconstructed image
    ssx, ssy = xpixnm * p_step, xpixnm * p_step
    yr,xr = ssy * nStepsY, ssx * nStepsX
    y,x = np.meshgrid(np.linspace(0,yr,nStepsY),np.linspace(0,xr,nStepsX))
    y,x = y.transpose(), x.transpose()
    yp,xp = np.meshgrid(np.linspace(0,yr, nStepsY * (ssy / xpixnm)),np.linspace(0,xr, nStepsX * (ssx / xpixnm)))
    yp,xp = yp.transpose(), xp.transpose()
    x0 = x[0,0]
    y0 = y[0,0]
    dx = x[0,1] - x0
    dy = y[1,0] - y0
    ivals = (xp - x0)/dx
    jvals = (yp - y0)/dy
    coords = np.array([ivals, jvals])
    stxmImageInterp = ndimage.map_coordinates(stxmImage.transpose(), coords)
    stxmImageInterp = stxmImageInterp * (stxmImageInterp > 0.)

    param = Param().param
    param["s_threshold"] = saturation_threshold
    param["process"] = '0'
    param["reconstruct"] = '1'
    param["dataPath"] = '0'
    param["scanDate"] = '0'
    param['Year'] = '0'
    param['Month'] = '0'
    param['Day'] = '0'
    param["scanNumber"] = '0'
    param["scanID"] = '0'
    param["bgScanDate"] = '0'
    param["bgScanNumber"] = '0'
    param["bgScanID"] = '0'
    param["bin"] = '0'
    param["pixnm"] = str(xpixnm)
    param["sh_sample_y"] = '0'
    param["sh_sample_x"] = '0'
    param["ssx"] = str(xpixnm * p_step)
    param["ssy"] = str(xpixnm * p_step)
    param["xpts"] = str(nStepsX)
    param["ypts"] = str(nStepsY)
    param["zd"] = '0'
    param["dr"] = '0'
    param["e"] = str(e)
    param["ccdp"] = '0'
    param["ccdz"] = '0'
    if dual_exposure: param["nexp"] = '2'
    else: param['nexp'] = '1'
    param["nl"] = '0'
    param["xcenter"] = '0'
    param["ycenter"] = '0'
    param["t_long"] = str(t_exp[1])
    param["t_short"] = str(t_exp[0])
    param["s_threshold"] = str(saturation_threshold)
    param["bl"] = bl
    param["cxi"] = '1'
    param["gpu"] = '0'
    param["nIterations"] = '100'
    param["probeMask"] = '1'
    param["pR"] = '1'
    param["bR"] = '1'
    param["useStxm"] = '0'
    param["usePrevious"] = '1'
    param["fv"] = '0'
    param["fh"] = '0'
    param["transpose"] = '0'
    param["fCCD_version"] = '0'
    param["removeOutliers"] = '0'
    param["removePhasePlane"] = '1'
    param['ssn'] = '0'

if saveOutput:
    print 'Saving data to file %s' %outputFile
    cxiObj = cxi()
    cxiObj.process = param
    cxiObj.probe = p
    cxiObj.beamline = bl
    cxiObj.energy = e * 1.602e-19
    cxiObj.ccddata = data
    cxiObj.probemask = pMask
    cxiObj.datamean = data_ave
    cxiObj.stxm = stxmImage
    cxiObj.stxmInterp = stxmImageInterp
    cxiObj.xpixelsize = x_pixel_size
    cxiObj.ypixelsize = x_pixel_size
    cxiObj.corner_x, cxiObj.corner_y, cxiObj.corner_z = corner_position
    cxiObj.translation = translation
    cxiObj.indices = indx
    writeCXI(cxiObj, fileName = outputFile)


    # f = h5py.File(outputFile, "w")
    # f.create_dataset("cxi_version",data=130)
    # entry_1 = f.create_group("entry_1")
    # entry_1.create_dataset("start_time",data=datetime.date.today().isoformat())
    #
    # instrument_1 = entry_1.create_group("instrument_1")
    # instrument_1.create_dataset("name",data=bl)
    # source_1 = instrument_1.create_group("source_1")
    # source_1.create_dataset("energy", data=e*1.602e-19) # in J
    # source_1.create_dataset("name",data="Simulation")
    #
    # detector_1 = instrument_1.create_group("detector_1")
    # ccd_data = detector_1.create_dataset("data",data=data.astype('float32'),compression='gzip')
    # ccd_data.attrs['axes'] = "translation:y:x"
    # detector_1.create_dataset("Probe", data = p)
    # detector_1.create_dataset("Probe Mask", data = pMask)
    # detector_1.create_dataset("Data Average", data = data_ave)
    # detector_1.create_dataset("Beamstop", data = np.fft.fftshift(beamstop))
    # detector_1.create_dataset("STXM", data = stxmImage)#not CXI standard
    # detector_1.create_dataset("STXMInterp", data = stxmImageInterp)#not CXI standard
    # detector_1.create_dataset("distance", data=ccdz) # in meters
    # detector_1["translation"] = h5py.SoftLink('/entry_1/sample_1/geometry_1/translation')
    # detector_1.create_dataset("data_dark",data=0.*data.astype('float32'))
    # #if(has_no_sample):
    # #    detector_1.create_dataset("data_white",data=ns)
    # detector_1.create_dataset("x_pixel_size", data=x_pixel_size) # in meters
    # detector_1.create_dataset("y_pixel_size", data=x_pixel_size) # in meters
    # detector_1.create_dataset("corner_position", data=corner_position) # in meters
    # sample_1 = entry_1.create_group("sample_1")
    # geometry_1 = sample_1.create_group("geometry_1")
    # geometry_1.create_dataset("translation", data = translation)
    #
    # data_1 = entry_1.create_group("data_1")
    # data_1["data"] = h5py.SoftLink('/entry_1/instrument_1/detector_1/ccd_data')
    # data_1["translation"] = h5py.SoftLink('/entry_1/sample_1/geometry_1/translation')
    #
    # process_1 = entry_1.create_group("process_1")
    # process_1.create_dataset("indices", data = indx)
    # paramGroup = process_1.create_group('Param')  #not CXI standard
    # dsets = []
    # for item in param:
    #     dsets.append(paramGroup.create_dataset(item,data = param[item]))
    # f.close()
    # print "Wrote data to file: %s" %outputFile


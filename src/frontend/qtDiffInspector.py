import sys, os, time
from PyQt4.QtCore import *
from PyQt4.QtGui import *
import matplotlib
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QTAgg as NavigationToolbar
from matplotlib.figure import Figure
from PIL import Image
from aux.imageIO import numpy2qimage, imload, imsave
from aux.readXIM import readXIM
from aux.util import congrid, rot_ave
from aux.util import e2l, l2e
import numpy as np
from matplotlib import rc, rcParams
from aux.util import filtered_auto
from reportlab.pdfgen import canvas
from subprocess import call
import scipy.optimize as optimize
from scipy.misc import toimage


class imageWindow(QWidget):
    def __init__(self, parent = None, size = None, pressFunction = None, moveFunction = None, debug = False):
        QWidget.__init__(self, parent)

        self.pressCallBackFunction = pressFunction
        self.moveCallBackFunction = moveFunction
        self.imStartPt = QPoint()
        self.imStartPt.setX(0)
        self.imStartPt.setY(0)
        self.image = QImage()
        self.startPixel = QPoint()
        self.startDataPt = QPoint()
        self.currentPixel = QPoint()
        self.currentDataPt = QPoint()
        self.selectedRect = QRect()
        self.selectedRectTL = QPoint()
        self.selectedRectBR = QPoint()
        self.dataRect = QRect()
        self.dataRectTL = QPoint()
        self.dataRectBR = QPoint()
        self.currentI = None
        self.imageSize = size.width(),size.height()
        self.imageData = None
        self.currentData = None
        self.zoomSize = None
        self.__currentX, self.__currentY = 0, 0
        self.__xstart, self.__ystart = 0, 0
        self.isPressed = False
        self.gotROI = False
        self.box = QRect(0,0,size.width(),size.height())
        self.key = None
        self.slice = QLine()
        self.gotSlice = False
        self.sliceRect1, self.sliceRect2 = QRect(),QRect()
        self.dragRect1, self.dragRect2 = False, False
        self.drawFilter = False
        self.gotFilter = False
        self.dragFilter = False
        self.drawPolygon = False
        self.drawPoints = False
        self.filter = QRect(0,0,0,0)
        self.polygon = QPolygon()
        self.polygonFit = QPolygon()
        self.filterWidth = 0
        self.filterCenterX = 0
        self.filterCenterY = 0
        self.debug = debug
        self.pointsRectList = []
        
    def displayImage(self, array):

        self.gotROI = False
        self.gotSlice = False
        self.imageData = array
        self.image = array
        self.update()

    def paintEvent(self, event):

        t0 = time.time()
        painter = QPainter()
        painter.begin(self)
        painter.setRenderHint(QPainter.Antialiasing,False)
        painter.setBrush(QBrush(Qt.SolidPattern))
        painter.drawRect(0,0,self.imageSize[0],self.imageSize[1])
        painter.drawImage(self.imStartPt, self.image)
        painter.setBrush(QBrush(Qt.NoBrush))
        painter.setPen(QPen(Qt.red,1))
        if self.gotROI: painter.drawRect(self.box)
        elif self.gotSlice:
            painter.setPen(QPen(Qt.green,1))
            painter.drawText(self.slice.p1()+QPoint(-5,-7),'1')
            painter.setRenderHint(QPainter.Antialiasing,False)
            painter.drawRect(self.sliceRect1)
            painter.setRenderHint(QPainter.Antialiasing,True)
            painter.drawLine(self.slice.p1(), self.slice.p2())
            painter.drawText(self.slice.p2()+QPoint(-5,-7),'2')
            painter.setRenderHint(QPainter.Antialiasing,False)
            painter.drawRect(self.sliceRect2)
            painter.setRenderHint(QPainter.Antialiasing,True)
        elif self.drawFilter:
            painter.setRenderHint(QPainter.Antialiasing,True)
            painter.setPen(QPen(Qt.red,1))
            painter.setBrush(QColor(255,0,0,50))
            painter.drawEllipse(self.filter)
        elif self.drawPolygon:
            painter.setRenderHint(QPainter.Antialiasing,True)
            painter.setPen(QPen(Qt.red,1))
            painter.setBrush(QColor(255,0,0,50))
            painter.drawPolygon(self.polygon)
        elif self.drawPoints:
            painter.setRenderHint(QPainter.Antialiasing,True)
            painter.setPen(QPen(Qt.red,2))
            #if self.polygon.size() != 0: painter.drawRect(QRect(self.polygon[0].x()-5,self.polygon[0].y()-5,10,10))
            for i in range(self.polygon.size()):
                painter.setPen(QPen(Qt.red,2))
                painter.drawRect(QRect(self.polygon[i].x()-5,self.polygon[i].y()-5,10,10))#self.pointsRectList[i + 1])
                painter.setPen(QPen(Qt.green,2))
                if i > 0: painter.drawLine(self.polygonFit.point(i - 1),self.polygonFit.point(i))
        else:
            painter.setRenderHint(QPainter.Antialiasing,False)
            painter.setPen(QPen(Qt.red,1))
            painter.drawRect(self.currentPixel.x()-5,self.currentPixel.y()-5,10,10)
            painter.drawLine(self.currentPixel-QPoint(0,15),self.currentPixel+QPoint(0,15))
            painter.drawLine(self.currentPixel-QPoint(15,0),self.currentPixel+QPoint(15,0))

        painter.end()
        if self.debug: print "paint event took %.6f seconds" %(time.time()-t0)

    def loadImage(self, event):
        painter = QPainter()
        painter.begin(self)
        painter.drawImage(QPoint(0,0), self.image)
        painter.end()

    def mousePressEvent(self, event):
        """
      This method should update the current pixel clicked and plot the associated x and y slices of
      the displayed data.  The registration of the image pixels and the data should be updated
      after each zoom so we should come here knowing which subset of data is currently displayed

      First update the selected rectangle (may be zero area), the startPixel (pressed position),
      and the currentPixel (starts as pressed position but ends as released position).  Then
      update the data point and the plot vectors using the appropriate transformations.
      """
        self.startPolygon = self.polygon
        self.startPixel = event.pos()
        self.currentPixel = event.pos()
        self.gotROI = False
        self.gotSlice = False
        self.update()
        if self.sliceRect1.contains(event.pos()):
            self.dragRect1 = True
            self.gotSlice = True
        elif self.sliceRect2.contains(event.pos()):
            self.dragRect2 = True
            self.gotSlice = True
        else: self.gotSlice = False
        self.box = QRect(self.startPixel, QSize(0,0))

        x1 = self.box.left()
        x2 = x1 + self.box.width()-1
        y1 = self.box.top()
        y2 = y1 + self.box.height()-1

        if self.key == 16777249: #this is the QT value for the ctrl key for some reason...
            self.slice = QLine()
            self.gotSlice = True
            self.gotFilter = False
            self.sliceRect1 = QRect(self.startPixel.x()-5,self.startPixel.y()-5,10,10)
            self.sliceRect2 = QRect(self.startPixel.x()-5,self.startPixel.y()-5,10,10)
            self.slice.setP1(self.startPixel)
            self.slice.setP2(self.startPixel)
        elif self.drawPolygon:
            if event.buttons() & Qt.RightButton:
                if self.polygon.count() > 0:
                    self.polygon.remove(0)
                else:
                    pass
            else: self.polygon.prepend(event.pos())
        elif self.drawPoints:
            if event.buttons() & Qt.LeftButton:
                self.polygon.prepend(event.pos())
                apply(self.pressCallBackFunction, (event.x(),event.x(),event.y(),event.y()))
                self.pointsSignal()
                #self.pointsRectList.insert(0,QRect(self.startPixel.x()-5,self.startPixel.y()-5,10,10))
        elif self.gotSlice == False:
            t0=time.time()
            if self.pressCallBackFunction != None: apply(self.pressCallBackFunction, (x1,x2,y1,y2))
            #print "Time for callback %.6f seconds" %(time.time()-t0)


    def mouseMoveEvent(self, event):

        self.currentPixel = event.pos()
        x = self.currentPixel.x()
        y = self.currentPixel.y()

        if self.key == 16777249:
            self.gotSlice = True
            self.sliceRect2 = QRect(self.currentPixel.x()-5,self.currentPixel.y()-5,10,10)
            self.slice.setP2(self.currentPixel)
            if self.moveCallBackFunction != None: apply(self.moveCallBackFunction, \
                (self.startPixel.x(),x, self.startPixel.y(),y))
            if np.sqrt(self.slice.dx()**2 + self.slice.dy()**2) > 10:
                self.update()
        elif self.dragRect1:
            self.gotSlice = True
            self.sliceRect1 = QRect(self.currentPixel.x()-5,self.currentPixel.y()-5,10,10)
            self.slice.setP1(self.currentPixel)
            if self.moveCallBackFunction != None: apply(self.moveCallBackFunction, \
                (self.startPixel.x(),x, self.startPixel.y(),y))
            if np.sqrt(self.slice.dx()**2 + self.slice.dy()**2) > 10:
                self.update()
        elif self.dragRect2:
            self.gotSlice = True
            self.sliceRect2 = QRect(self.currentPixel.x()-5,self.currentPixel.y()-5,10,10)
            self.slice.setP2(self.currentPixel)
            if self.moveCallBackFunction != None: apply(self.moveCallBackFunction, \
                (self.startPixel.x(),x, self.startPixel.y(),y))
            if np.sqrt(self.slice.dx()**2 + self.slice.dy()**2) > 10:
                self.update()
        elif self.drawFilter:
            if self.dragFilter:
                dx = x-self.startPixel.x()
                dy = y-self.startPixel.y()
                newx = self.startFilter.left()+dx
                newy = self.startFilter.top()+dy
                self.filter = QRect(newx,newy,self.filter.width(),self.filter.height())
                self.update()
            else:
                if (event.buttons() & Qt.LeftButton):
                    if abs(x-self.startPixel.x())>30:
                        if abs(y-self.startPixel.y())>30:
                            self.gotFilter = True
                            self.filter = QRect(self.startPixel, QSize(x - \
                                self.startPixel.x(),y - self.startPixel.y()))
                            self.update()

        else:
            if (event.buttons() & Qt.RightButton):
                if (x < self.imageSize[0]) & (y < self.imageSize[1]) & (x >= 0) & (y >= 0):
                    if self.moveCallBackFunction != None: apply(self.moveCallBackFunction, \
                        (self.startPixel.x(),x, self.startPixel.y(),y))
            elif (event.buttons() & Qt.LeftButton):
                if abs(x-self.startPixel.x())>10:
                    if abs(y-self.startPixel.y())>10:
                        self.gotROI = True
                        self.box = QRect(self.startPixel, QSize(x - \
                            self.startPixel.x(),y - self.startPixel.y()))
                        self.update()
            else: self.mouseReleaseEvent(event, mplot = False)

    def mouseReleaseEvent(self, event, mplot = True):

        self.currentPixel = event.pos()
        self.key = None
        if self.gotROI:
            x1 = self.box.left()
            x2 = x1 + self.box.width()-1
            y1 = self.box.top()
            y2 = y1 + self.box.height()-1
            self.update()
        elif self.gotSlice:
            x1 = self.slice.p1().x()
            x2 = self.slice.p2().x()
            y1 = self.slice.p1().y()
            y2 = self.slice.p2().y()
        else:
            x1 = self.currentPixel.x()
            y1 = self.currentPixel.y()
            x2 = 0.
            y2 = 0.
        if self.pressCallBackFunction != None:
            apply(self.pressCallBackFunction, (x1,x2,y1,y2, mplot))
        
        if self.currentPixel == self.startPixel:
            if self.drawFilter: self.clearSignal()
            self.filter = QRect(0,0,0,0)
            self.drawFilter = False
            self.gotFilter = False
            self.dragFilter = False
        elif self.gotFilter:
            self.startFilter = self.filter
            self.dragFilter = True
        else:
            if self.drawFilter: self.clearSignal()
            self.filter = QRect(0,0,0,0)
            self.drawFilter = False
            self.gotFilter = False
            self.dragFilter = False



        self.dragRect1 = False
        self.dragRect2 = False


    def clearSignal(self):
        self.emit(SIGNAL("clearToggles"),())

    def pointsSignal(self):
        self.emit(SIGNAL("calcPoints"),())

 
class infoWidget(QFrame):
    def __init__(self, parent = None):
        QFrame.__init__(self, parent)

        frame = QFrame(self)
        grid = QGridLayout(frame)
        grid.setSpacing(0)
        grid.setHorizontalSpacing(2)
        grid.setMargin(0)
        self.plotSize = QSize(256,192)
        self.plotType = 'histogram'
        self.energy = 0.
        self.wavelength = 0.
        self.pixelSize = 0.
        self.ccdZ = 0.
        self.roiSize = 0

#################################################################################
        self.main_frame = QWidget()

        # Create the mpl Figure and FigCanvas objects.
        # 5.5x4 inches, 100 dots-per-inch
        #
        rcParams['font.size'] = 10
        self.dpi = 100
        self.fig = Figure((6.0, 4.0), dpi=self.dpi,facecolor='white')
        self.fig.subplots_adjust(left = 0.12, bottom=0.1, right = 0.97, top = 0.93)
        self.canvas = FigureCanvas(self.fig)
        self.canvas.setParent(self.main_frame)

        # Since we have only one plot, we can use add_axes
        # instead of add_subplot, but then the subplot
        # configuration tool in the navigation toolbar wouldn't
        # work.
        #
        self.axes = self.fig.add_subplot(111)

        # Bind the 'pick' event for clicking on one of the bars
        #
        #self.canvas.mpl_connect('pick_event', self.on_pick)

        # Create the navigation toolbar, tied to the canvas
        #
        self.mpl_toolbar = NavigationToolbar(self.canvas, self.main_frame)
################################################################################

        self.info = self.infoWidget()
        self.control = self.controlWidget()

        grid.addWidget(self.canvas,0,0,1,2,Qt.AlignTop | Qt.AlignHCenter)
        grid.addWidget(self.mpl_toolbar,1,0,1,2,Qt.AlignTop | Qt.AlignLeft)
        grid.addWidget(self.info,2,0,1,1,Qt.AlignTop | Qt.AlignLeft)
        grid.addWidget(self.control,2,1,1,1,Qt.AlignTop | Qt.AlignRight)
        self.setLayout(grid)
        self.roiBox.setCurrentIndex(0)
        self.roiBoxChanged()

    def controlWidget(self):
        
        widget = QWidget(self)
        grid = QGridLayout(widget)
        grid.setSpacing(0)
        grid.setMargin(0)   

        self.roiLabel = QLabel("ROI:")
        self.roiBox = QComboBox()
        self.roiBox.addItem("Bounding Box")
        self.roiBox.addItem("Center+Size")
        self.roiBox.setMaximumSize(128,24)
        self.roiBox.setMinimumSize(128,24)
        self.roiSizeLabel = QLabel("Size")
        self.roiSizeEdit = QLineEdit()
        self.roiSizeEdit.setMaximumSize(64,24)
        self.roiCenterXLabel = QLabel("X Center")
        self.roiCenterXEdit = QLineEdit()
        self.roiCenterXEdit.setMaximumSize(64,24)
        self.roiCenterYLabel = QLabel("Y Center")
        self.roiCenterYEdit = QLineEdit()
        self.roiCenterYEdit.setMaximumSize(64,24)
        self.roiLeftLabel = QLabel("Left")
        self.roiLeftEdit = QLineEdit()
        self.roiLeftEdit.setMaximumSize(64,24)
        self.roiRightLabel = QLabel("Right")
        self.roiRightEdit = QLineEdit()
        self.roiRightEdit.setMaximumSize(64,24)
        self.roiTopLabel = QLabel("Top")
        self.roiTopEdit = QLineEdit()
        self.roiTopEdit.setMaximumSize(64,24)
        self.roiBottomLabel = QLabel("Bottom")
        self.roiBottomEdit = QLineEdit()
        self.roiBottomEdit.setMaximumSize(64,24)

        self.applyRoiButton = QPushButton()
        self.applyRoiButton.setText('Apply')
        self.applyRoiButton.setMinimumSize(128,24)
        self.applyRoiButton.setMaximumSize(128,24)

        self.mouseCenterButton = QPushButton()
        self.mouseCenterButton.setText('Center at Mouse')
        self.mouseCenterButton.clicked.connect(self.mouseCenter)
        self.mouseCenterButton.setMinimumSize(128,24)
        self.mouseCenterButton.setMaximumSize(128,24)

        self.energyLabel = QLabel("Energy (eV)")
        self.energyEdit = QLineEdit()
        self.energyEdit.setMaximumSize(64,24)
        self.wavelengthLabel = QLabel("Wavelength (nm)")
        self.wavelengthEdit = QLineEdit()
        self.wavelengthEdit.setMaximumSize(64,24)
        self.pixelSizeLabel = QLabel('Pixel Size ('+u'\u03BC'+'m)')
        self.pixelSizeEdit = QLineEdit()
        self.pixelSizeEdit.setMaximumSize(64,24)
        self.ccdZLabel = QLabel("Detector Distance (mm)")
        self.ccdZEdit = QLineEdit()
        self.ccdZEdit.setMaximumSize(64,24)

        self.plotBox = QComboBox()
        self.plotBox.addItem("Plot Pixels")
        self.plotBox.addItem("Plot Q")
        self.plotBox.setMaximumSize(128,24)
        self.plotBox.setMinimumSize(128,24)

        self.roiSizeEdit.textChanged.connect(self.roiSizeChanged)
        self.roiCenterXEdit.textChanged.connect(self.roiCenterXChanged)
        self.roiCenterYEdit.textChanged.connect(self.roiCenterYChanged)
        self.roiLeftEdit.textChanged.connect(self.roiLeftChanged)
        self.roiRightEdit.textChanged.connect(self.roiRightChanged)
        self.roiTopEdit.textChanged.connect(self.roiTopChanged)
        self.roiBottomEdit.textChanged.connect(self.roiBottomChanged)
        self.roiBox.activated.connect(self.roiBoxChanged)
        self.applyRoiButton.clicked.connect(self.refresh)
        self.connect(self.energyEdit, SIGNAL('returnPressed()'), self.energyChanged)
        self.connect(self.wavelengthEdit, SIGNAL('returnPressed()'), self.wavelengthChanged)
        self.connect(self.pixelSizeEdit, SIGNAL('returnPressed()'), self.pixelSizeChanged)
        self.connect(self.ccdZEdit, SIGNAL('returnPressed()'), self.ccdZChanged)
        self.energyEdit.setText(str(0.00))
        self.wavelengthEdit.setText(str(0.00))
        self.pixelSizeEdit.setText(str(0.00))
        self.ccdZEdit.setText(str(0.00))

        grid.addWidget(self.roiLabel,0,0,1,2,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.roiBox,0,2,1,2,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.roiSizeLabel,1,0,1,1,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.roiSizeEdit,1,1,1,1,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.mouseCenterButton,1,2,1,2,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.roiCenterXLabel,2,0,1,1,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.roiCenterXEdit,2,1,1,1,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.roiCenterYLabel,2,2,1,1,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.roiCenterYEdit,2,3,1,1,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.roiLeftLabel,3,0,1,1,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.roiLeftEdit,3,1,1,1,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.roiRightLabel,3,2,1,1,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.roiRightEdit,3,3,1,1,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.roiTopLabel,4,0,1,1,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.roiTopEdit,4,1,1,1,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.roiBottomLabel,4,2,1,1,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.roiBottomEdit,4,3,1,1,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.applyRoiButton,5,2,1,2,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.hLine3,6,0,1,4,Qt.AlignTop | Qt.AlignHCenter)
        grid.addWidget(self.energyLabel,7,1,1,2,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.energyEdit,7,3,1,1,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.wavelengthLabel,8,1,1,2,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.wavelengthEdit,8,3,1,1,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.pixelSizeLabel,9,1,1,2,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.pixelSizeEdit,9,3,1,1,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.ccdZLabel,10,1,1,2,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.ccdZEdit,10,3,1,1,Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.plotBox,11,2,1,2,Qt.AlignVCenter | Qt.AlignRight)

        return widget

    def infoWidget(self):

        widget = QWidget()
        infoGrid = QGridLayout(widget)
        infoGrid.setSpacing(0)
        infoGrid.setMargin(0)

        self.shapeLabel = QLabel()
        self.shapeLabel.setText("Shape (H x V):")
        self.shapeLabel.setAlignment(Qt.AlignBottom | Qt.AlignRight)

        self.shapeVal = QLabel()
        self.shapeVal.setText("")
        self.shapeVal.setAlignment(Qt.AlignBottom | Qt.AlignRight)

        self.maxLabel = QLabel()
        self.maxLabel.setText("Maximum:")
        self.maxLabel.setAlignment(Qt.AlignBottom | Qt.AlignRight)

        self.maxVal = QLabel()
        self.maxVal.setText("")
        self.maxVal.setAlignment(Qt.AlignBottom | Qt.AlignRight)

        self.minLabel = QLabel()
        self.minLabel.setText("Minimum:")
        self.minLabel.setAlignment(Qt.AlignBottom | Qt.AlignRight)

        self.minVal = QLabel()
        self.minVal.setText("")
        self.minVal.setAlignment(Qt.AlignBottom | Qt.AlignRight)

        self.meanLabel = QLabel()
        self.meanLabel.setText("Mean:")
        self.meanLabel.setAlignment(Qt.AlignBottom | Qt.AlignRight)

        self.meanVal = QLabel()
        self.meanVal.setText("")
        self.meanVal.setAlignment(Qt.AlignBottom | Qt.AlignRight)

        self.stdLabel = QLabel()
        self.stdLabel.setText("Variance:")
        self.stdLabel.setAlignment(Qt.AlignBottom | Qt.AlignRight)

        self.stdVal = QLabel()
        self.stdVal.setText("")
        self.stdVal.setAlignment(Qt.AlignBottom | Qt.AlignRight)

        self.totLabel = QLabel()
        self.totLabel.setText("Total")
        self.totLabel.setAlignment(Qt.AlignBottom | Qt.AlignRight)

        self.totVal = QLabel()
        self.totVal.setText("")
        self.totVal.setAlignment(Qt.AlignBottom | Qt.AlignRight)

        self.fileName = QLabel()
        self.fileName.setText("File Name:")
        self.fileName.setAlignment(Qt.AlignBottom | Qt.AlignRight)

        self.fileCreated = QLabel()
        self.fileCreated.setText("Created:")
        self.fileCreated.setAlignment(Qt.AlignBottom | Qt.AlignRight)

        self.fileModified = QLabel()
        self.fileModified.setText("Modified:")
        self.fileModified.setAlignment(Qt.AlignBottom | Qt.AlignRight)

        self.fileNameVal = QLabel()
        self.fileNameVal.setText("")
        self.fileNameVal.setAlignment(Qt.AlignBottom | Qt.AlignRight)

        self.fileCreatedVal = QLabel()
        self.fileCreatedVal.setText("")
        self.fileCreatedVal.setAlignment(Qt.AlignBottom | Qt.AlignRight)

        self.fileModifiedVal = QLabel()
        self.fileModifiedVal.setText("")
        self.fileModifiedVal.setAlignment(Qt.AlignBottom | Qt.AlignRight)

        self.hLine = QFrame()
        self.hLine.setFrameShape(QFrame.HLine)
        self.hLine.setFrameShadow(QFrame.Sunken)
        self.hLine.setMinimumSize(QSize(self.plotSize.width(),10))
        self.hLine2 = QFrame()
        self.hLine2.setFrameShape(QFrame.HLine)
        self.hLine2.setFrameShadow(QFrame.Sunken)
        self.hLine2.setMinimumSize(QSize(self.plotSize.width(),10))
        self.hLine3 = QFrame()
        self.hLine3.setFrameShape(QFrame.HLine)
        self.hLine3.setFrameShadow(QFrame.Sunken)
        self.hLine3.setMinimumSize(QSize(self.plotSize.width(),10))

        self.xLabel = QLabel()
        self.xLabel.setText('X Pixel:')
        self.xVal = QLabel()
        self.yLabel = QLabel()
        self.yLabel.setText('Y Pixel:')
        self.yVal = QLabel()
        self.ILabel = QLabel()
        self.ILabel.setText('Intensity:')
        self.IVal = QLabel()

        infoGrid.addWidget(self.shapeLabel, 0,0,1,1,Qt.AlignTop | Qt.AlignLeft)
        infoGrid.addWidget(self.shapeVal, 0,1,1,1,Qt.AlignTop | Qt.AlignRight)
        infoGrid.addWidget(self.maxLabel, 1,0,1,1,Qt.AlignTop | Qt.AlignLeft)
        infoGrid.addWidget(self.maxVal, 1,1,1,1,Qt.AlignTop | Qt.AlignRight)
        infoGrid.addWidget(self.minLabel, 2,0,1,1,Qt.AlignTop | Qt.AlignLeft)
        infoGrid.addWidget(self.minVal, 2,1,1,1,Qt.AlignTop | Qt.AlignRight)
        infoGrid.addWidget(self.meanLabel, 3,0,1,1,Qt.AlignTop | Qt.AlignLeft)
        infoGrid.addWidget(self.meanVal, 3,1,1,1,Qt.AlignTop | Qt.AlignRight)
        infoGrid.addWidget(self.stdLabel, 4,0,1,1,Qt.AlignTop | Qt.AlignLeft)
        infoGrid.addWidget(self.stdVal, 4,1,1,1,Qt.AlignTop | Qt.AlignRight)
        infoGrid.addWidget(self.totLabel, 5,0,1,1,Qt.AlignTop | Qt.AlignLeft)
        infoGrid.addWidget(self.totVal, 5,1,1,1,Qt.AlignTop | Qt.AlignRight)
        infoGrid.addWidget(self.hLine,6,0,1,2,Qt.AlignTop | Qt.AlignHCenter)
        infoGrid.addWidget(self.xLabel,7,0,1,1, Qt.AlignTop | Qt.AlignLeft)
        infoGrid.addWidget(self.yLabel,8,0,1,1, Qt.AlignTop | Qt.AlignLeft)
        infoGrid.addWidget(self.ILabel,9,0,1,1, Qt.AlignTop | Qt.AlignLeft)
        infoGrid.addWidget(self.xVal,7,1,1,1, Qt.AlignTop | Qt.AlignRight)
        infoGrid.addWidget(self.yVal,8,1,1,1, Qt.AlignTop | Qt.AlignRight)
        infoGrid.addWidget(self.IVal,9,1,1,1, Qt.AlignTop | Qt.AlignRight)
        infoGrid.addWidget(self.hLine2,10,0,1,2,Qt.AlignTop | Qt.AlignHCenter)
        infoGrid.addWidget(self.fileName,11,0,1,1,Qt.AlignTop | Qt.AlignLeft)
        infoGrid.addWidget(self.fileCreated,12,0,1,1,Qt.AlignTop | Qt.AlignLeft)
        infoGrid.addWidget(self.fileModified,13,0,1,1,Qt.AlignTop | Qt.AlignLeft)
        infoGrid.addWidget(self.fileNameVal,11,1,1,1,Qt.AlignTop | Qt.AlignRight)
        infoGrid.addWidget(self.fileCreatedVal,12,1,1,1,Qt.AlignTop | Qt.AlignRight)
        infoGrid.addWidget(self.fileModifiedVal,13,1,1,1,Qt.AlignTop | Qt.AlignRight)

        return widget

    def mouseCenter(self):

        self.roiCenterX = int(self.xVal.text())
        self.roiCenterXEdit.setText(str(self.roiCenterX))
        self.roiCenterY = int(self.yVal.text())
        self.roiCenterYEdit.setText(str(self.roiCenterY))
        self.refresh()

    def pixelSizeChanged(self):

        try: self.pixelSize = round(float(self.pixelSizeEdit.text()),2)
        except ValueError: print "Pixel size must be a floating point number"
        else:
            self.pixelSizeEdit.setText(str(round(self.pixelSize,2)))

    def ccdZChanged(self):

        try: self.ccdZ = round(float(self.ccdZEdit.text()),2)
        except ValueError: print "CCD Z-distance must be a floating point number"
        else:
            self.ccdZEdit.setText(str(round(self.ccdZ,2)))

    def energyChanged(self):

        try: self.energy = round(float(self.energyEdit.text()),2)
        except ValueError: print "Energy must be a floating point number"
        else:
            self.wavelength = e2l(self.energy)
            self.wavelengthEdit.setText(str(round(self.wavelength,2)))
            self.energyEdit.setText(str(round(self.energy,2)))

    def wavelengthChanged(self):

        try: self.wavelength = round(float(self.wavelengthEdit.text()),2)
        except ValueError: print "Wavelength must be a floating point number"
        else:
            self.energy = e2l(self.wavelength)
            self.energyEdit.setText(str(round(self.energy,2)))
            self.wavelengthEdit.setText(str(round(self.wavelength,2)))

    def roiBoxChanged(self):

        if self.roiBox.currentIndex() == 0:
            self.roiSizeEdit.setEnabled(False)
            self.roiCenterXEdit.setEnabled(False)
            self.roiCenterYEdit.setEnabled(False)
            self.roiLeftEdit.setEnabled(True)
            self.roiRightEdit.setEnabled(True)
            self.roiTopEdit.setEnabled(True)
            self.roiBottomEdit.setEnabled(True)
        elif self.roiBox.currentIndex() == 1:
            self.roiSizeEdit.setEnabled(True)
            self.roiCenterXEdit.setEnabled(True)
            self.roiCenterYEdit.setEnabled(True)
            self.roiLeftEdit.setEnabled(False)
            self.roiRightEdit.setEnabled(False)
            self.roiTopEdit.setEnabled(False)
            self.roiBottomEdit.setEnabled(False)

    def roiSizeChanged(self):

        try: self.roiSize = int(str(self.roiSizeEdit.text()))
        except ValueError: print "ROI Size must be an integer"
        #else: print self.roiSize

    def roiCenterXChanged(self):

        try: self.roiCenterX = int(str(self.roiCenterXEdit.text()))
        except ValueError: print "Center X must be an integer"
        #else: print self.roiCenterX

    def roiCenterYChanged(self):

        try: self.roiCenterY = int(str(self.roiCenterYEdit.text()))
        except ValueError: print "Center Y must be an integer"
        #else: print self.roiCenterY

    def roiLeftChanged(self):

        try: self.roiLeft = int(str(self.roiLeftEdit.text()))
        except ValueError: print "ROI Left must be an integer"
        #else: print self.roiLeft

    def roiRightChanged(self):

        try: self.roiRight = int(str(self.roiRightEdit.text()))
        except ValueError: print "ROI Right must be an integer"
        #else: print self.roiRight

    def roiTopChanged(self):

        try: self.roiTop = int(str(self.roiTopEdit.text()))
        except ValueError: print "ROI Top must be an integer"
        #else: print self.roiTop

    def roiBottomChanged(self):

        try: self.roiBottom = int(str(self.roiBottomEdit.text()))
        except ValueError: print "ROI Bottom must be an integer"
        #else: print self.roiBottom

    def setHistPlot(self):

        self.plotType = 'histogram'
        self.refresh()

    def setHorLinePlot(self):

        self.plotType = 'horLine'
        self.refresh()

    def setVerLinePlot(self):

        self.plotType = 'verLine'
        self.refresh()

    def setRadialPlot(self):

        self.plotType = 'radial'
        self.refresh()

    def setSpecialLinePlot(self):

        self.plotType = 'specialLine'
        self.refresh()

    def updatePlot(self):

        self.plot.drawPath()

    def setRoiText(self):

        self.roiLeftEdit.setText(str(self.roiLeft))
        self.roiRightEdit.setText(str(self.roiRight))
        self.roiTopEdit.setText(str(self.roiTop))
        self.roiBottomEdit.setText(str(self.roiBottom))
        self.roiCenterXEdit.setText(str(self.roiCenterX))
        self.roiCenterYEdit.setText(str(self.roiCenterY))
        self.roiSizeEdit.setText(str(self.roiSize))

    def refresh(self):

        self.emit(SIGNAL("refresh"), ())


class plotWindow(QFrame):
    def __init__(self, parent = None, size = None):
        QFrame.__init__(self, parent)

        self.imageSize = size
        self.gradient = True
        self.penColor = Qt.black
        self.gradientColor = Qt.gray
        self.setBackgroundRole(QPalette.Base)
        self.path = QPainterPath()
        self.penSize = 3
        self.borderColor = Qt.gray
        self.borderWidth = 3
        self.maxValue, self.minValue = 0., 0.
        self.printValues = False

        if self.gradient:
            if self.imageSize.height() > self.imageSize.width():
                self.gradientMap = QLinearGradient(self.penSize, 0, self.imageSize.width()-self.penSize, 0)
            else:
                self.gradientMap = QLinearGradient(0, self.imageSize.height()-self.penSize, 0, 0)
            self.gradientMap.setColorAt(0.0, Qt.white)
            self.gradientMap.setColorAt(1.0, self.gradientColor)

    def minimumSizeHint(self):
        return self.imageSize

    def sizeHint(self):
        return self.imageSize

    def paintEvent(self, event):
        painter = QPainter()
        painter.begin(self)
        painter.setRenderHint(QPainter.Antialiasing,True)
        painter.setBrush(QBrush(Qt.black))
        painter.setPen(QPen(self.borderColor, self.penSize,
                Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin))
        painter.drawRect(0, 0, self.imageSize.width(), self.imageSize.height())
        painter.setPen(QPen(Qt.white, 1.0, Qt.SolidLine, Qt.RoundCap))
        #if self.gradient: painter.setBrush(QBrush(self.gradientMap))
        painter.drawPath(self.path)
        painter.setPen(QPen(Qt.red, 1.0, Qt.SolidLine, Qt.RoundCap))
        if self.printValues:
            painter.drawText(QPoint(2,13), str(round(self.maxValue,4)))
            painter.drawText(QPoint(2,93), str(round(self.minValue,4)))
        painter.end()

    def drawPath(self):

        self.update()

class orbitPopup(QMainWindow):

    def __init__(self):
        super(orbitPopup, self).__init__()
        self.setCentralWidget(self.pointsWidget())
        self.setWindowTitle('Orbit Calculator')

    def pointsWidget(self):
        widget = QWidget()
        Grid = QGridLayout(widget)
        Grid.setSpacing(5)
        Grid.setMargin(5)

        self.pEdits = []
        self.valLabels = []

        self.xmotLabel = QLabel()
        self.xmotLabel.setText("X Motor")

        self.xmotcalcLabel = QLabel()
        self.xmotcalcLabel.setText("X Calc")

        self.zmotcalcLabel = QLabel()
        self.zmotcalcLabel.setText("Z Calc")

        self.rmotLabel = QLabel()
        self.rmotLabel.setText("R Motor")

        self.a1Label = QLabel()
        self.a1Label.setText("a1")
        self.a2Label = QLabel()
        self.a2Label.setText("a2")
        self.a3Label = QLabel()
        self.a3Label.setText("a3")
        self.a4Label = QLabel()
        self.a4Label.setText("a4")
        self.a5Label = QLabel()
        self.a5Label.setText("a5")
        self.a6Label = QLabel()
        self.a6Label.setText("a6")
        self.a7Label = QLabel()
        self.a7Label.setText("a7")
        self.a1ValLabel = QLabel()
        self.a1ValLabel.setText("0.0")
        self.a1ValLabel.setMinimumSize(72,24)
        self.valLabels.append(self.a1ValLabel)
        self.a2ValLabel = QLabel()
        self.a2ValLabel.setText("0.0")
        self.a2ValLabel.setMinimumSize(72,24)
        self.valLabels.append(self.a2ValLabel)
        self.a3ValLabel = QLabel()
        self.a3ValLabel.setText("0.0")
        self.a3ValLabel.setMinimumSize(72,24)
        self.valLabels.append(self.a3ValLabel)
        self.a4ValLabel = QLabel()
        self.a4ValLabel.setText("0.0")
        self.a4ValLabel.setMinimumSize(72,24)
        self.valLabels.append(self.a4ValLabel)
        self.a5ValLabel = QLabel()
        self.a5ValLabel.setText("0.0")
        self.a5ValLabel.setMinimumSize(72,24)
        self.valLabels.append(self.a5ValLabel)
        self.a6ValLabel = QLabel()
        self.a6ValLabel.setText("0.0")
        self.a6ValLabel.setMinimumSize(72,24)
        self.valLabels.append(self.a6ValLabel)
        self.a7ValLabel = QLabel()
        self.a7ValLabel.setText("0.0")
        self.a7ValLabel.setMinimumSize(72,24)
        self.valLabels.append(self.a7ValLabel)


        self.calcButton = QPushButton()
        self.calcButton.setText('Calculate')
        self.calcButton.clicked.connect(self.fitOrbit)

        self.pEstLabel = QLabel()
        self.pEstLabel.setText("Estimate")
        self.pEstxEdit = QLineEdit()
        self.pEstxEdit.setMaximumSize(72,24)
        self.pEstxcalcEdit = QLineEdit()
        self.pEstxcalcEdit.setMaximumSize(72,24)
        self.pEstrEdit = QLineEdit()
        self.pEstrEdit.setMaximumSize(72,24)
        self.pEstzcalcEdit = QLineEdit()
        self.pEstzcalcEdit.setMaximumSize(72,24)

        self.p1Label = QLabel()
        self.p1Label.setText("Point 1")
        self.p1xEdit = QLineEdit()
        self.p1xEdit.setMaximumSize(72,24)
        self.p1xcalcEdit = QLineEdit()
        self.p1xcalcEdit.setMaximumSize(72,24)
        self.p1rEdit = QLineEdit()
        self.p1rEdit.setMaximumSize(72,24)
        self.p1zcalcEdit = QLineEdit()
        self.p1zcalcEdit.setMaximumSize(72,24)
        self.pEdits.append((self.p1xEdit,self.p1rEdit,self.p1xcalcEdit,self.p1zcalcEdit))

        self.p2Label = QLabel()
        self.p2Label.setText("Point 2")
        self.p2xEdit = QLineEdit()
        self.p2xEdit.setMaximumSize(72,24)
        self.p2xcalcEdit = QLineEdit()
        self.p2xcalcEdit.setMaximumSize(72,24)
        self.p2rEdit = QLineEdit()
        self.p2rEdit.setMaximumSize(72,24)
        self.p2zcalcEdit = QLineEdit()
        self.p2zcalcEdit.setMaximumSize(72,24)
        self.pEdits.append((self.p2xEdit,self.p2rEdit,self.p2xcalcEdit,self.p2zcalcEdit))

        self.p3Label = QLabel()
        self.p3Label.setText("Point 3")
        self.p3xEdit = QLineEdit()
        self.p3xEdit.setMaximumSize(72,24)
        self.p3xcalcEdit = QLineEdit()
        self.p3xcalcEdit.setMaximumSize(72,24)
        self.p3rEdit = QLineEdit()
        self.p3rEdit.setMaximumSize(72,24)
        self.p3zcalcEdit = QLineEdit()
        self.p3zcalcEdit.setMaximumSize(72,24)
        self.pEdits.append((self.p3xEdit,self.p3rEdit,self.p3xcalcEdit,self.p3zcalcEdit))

        self.p4Label = QLabel()
        self.p4Label.setText("Point 4")
        self.p4xEdit = QLineEdit()
        self.p4xEdit.setMaximumSize(72,24)
        self.p4xcalcEdit = QLineEdit()
        self.p4xcalcEdit.setMaximumSize(72,24)
        self.p4rEdit = QLineEdit()
        self.p4rEdit.setMaximumSize(72,24)
        self.p4zcalcEdit = QLineEdit()
        self.p4zcalcEdit.setMaximumSize(72,24)
        self.pEdits.append((self.p4xEdit,self.p4rEdit,self.p4xcalcEdit,self.p4zcalcEdit))

        self.p5Label = QLabel()
        self.p5Label.setText("Point 5")
        self.p5xEdit = QLineEdit()
        self.p5xEdit.setMaximumSize(72,24)
        self.p5xcalcEdit = QLineEdit()
        self.p5xcalcEdit.setMaximumSize(72,24)
        self.p5rEdit = QLineEdit()
        self.p5rEdit.setMaximumSize(72,24)
        self.p5zcalcEdit = QLineEdit()
        self.p5zcalcEdit.setMaximumSize(72,24)
        self.pEdits.append((self.p5xEdit,self.p5rEdit,self.p5xcalcEdit,self.p5zcalcEdit))

        self.p6Label = QLabel()
        self.p6Label.setText("Point 6")
        self.p6xEdit = QLineEdit()
        self.p6xEdit.setMaximumSize(72,24)
        self.p6xcalcEdit = QLineEdit()
        self.p6xcalcEdit.setMaximumSize(72,24)
        self.p6rEdit = QLineEdit()
        self.p6rEdit.setMaximumSize(72,24)
        self.p6zcalcEdit = QLineEdit()
        self.p6zcalcEdit.setMaximumSize(72,24)
        self.pEdits.append((self.p6xEdit,self.p6rEdit,self.p6xcalcEdit,self.p6zcalcEdit))

        self.p7Label = QLabel()
        self.p7Label.setText("Point 7")
        self.p7xEdit = QLineEdit()
        self.p7xEdit.setMaximumSize(72,24)
        self.p7xcalcEdit = QLineEdit()
        self.p7xcalcEdit.setMaximumSize(72,24)
        self.p7rEdit = QLineEdit()
        self.p7rEdit.setMaximumSize(72,24)
        self.p7zcalcEdit = QLineEdit()
        self.p7zcalcEdit.setMaximumSize(72,24)
        self.pEdits.append((self.p7xEdit,self.p7rEdit,self.p7xcalcEdit,self.p7zcalcEdit))


        Grid.addWidget(self.rmotLabel,0,0,1,2,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.xmotLabel,0,2,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.xmotcalcLabel,0,3,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.zmotcalcLabel,0,4,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.p1Label,1,0,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.p1rEdit,1,1,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.p1xEdit,1,2,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.p1xcalcEdit,1,3,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.p1zcalcEdit,1,4,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.p2Label,2,0,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.p2rEdit,2,1,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.p2xEdit,2,2,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.p2xcalcEdit,2,3,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.p2zcalcEdit,2,4,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.p3Label,3,0,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.p3rEdit,3,1,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.p3xEdit,3,2,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.p3xcalcEdit,3,3,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.p3zcalcEdit,3,4,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.p4Label,4,0,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.p4rEdit,4,1,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.p4xEdit,4,2,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.p4xcalcEdit,4,3,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.p4zcalcEdit,4,4,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.p5Label,5,0,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.p5rEdit,5,1,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.p5xEdit,5,2,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.p5xcalcEdit,5,3,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.p5zcalcEdit,5,4,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.p6Label,6,0,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.p6rEdit,6,1,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.p6xEdit,6,2,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.p6xcalcEdit,6,3,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.p6zcalcEdit,6,4,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.p7Label,7,0,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.p7rEdit,7,1,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.p7xEdit,7,2,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.p7xcalcEdit,7,3,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.p7zcalcEdit,7,4,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.pEstLabel,8,0,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.pEstrEdit,8,1,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.pEstxEdit,8,2,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.pEstxcalcEdit,8,3,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.pEstzcalcEdit,8,4,1,1,Qt.AlignTop | Qt.AlignRight)
        Grid.addWidget(self.calcButton,9,0,1,2,Qt.AlignVCenter | Qt.AlignLeft)
        Grid.addWidget(self.a1Label,1,5,1,1,Qt.AlignVCenter | Qt.AlignRight)
        Grid.addWidget(self.a1ValLabel,1,6,1,1,Qt.AlignVCenter | Qt.AlignRight)
        Grid.addWidget(self.a2Label,2,5,1,1,Qt.AlignVCenter | Qt.AlignRight)
        Grid.addWidget(self.a2ValLabel,2,6,1,1,Qt.AlignVCenter | Qt.AlignRight)
        Grid.addWidget(self.a3Label,3,5,1,1,Qt.AlignVCenter | Qt.AlignRight)
        Grid.addWidget(self.a3ValLabel,3,6,1,1,Qt.AlignVCenter | Qt.AlignRight)
        Grid.addWidget(self.a4Label,4,5,1,1,Qt.AlignVCenter | Qt.AlignRight)
        Grid.addWidget(self.a4ValLabel,4,6,1,1,Qt.AlignVCenter | Qt.AlignRight)
        Grid.addWidget(self.a5Label,5,5,1,1,Qt.AlignVCenter | Qt.AlignRight)
        Grid.addWidget(self.a5ValLabel,5,6,1,1,Qt.AlignVCenter | Qt.AlignRight)
        Grid.addWidget(self.a6Label,6,5,1,1,Qt.AlignVCenter | Qt.AlignRight)
        Grid.addWidget(self.a6ValLabel,6,6,1,1,Qt.AlignVCenter | Qt.AlignRight)
        Grid.addWidget(self.a7Label,7,5,1,1,Qt.AlignVCenter | Qt.AlignRight)
        Grid.addWidget(self.a7ValLabel,7,6,1,1,Qt.AlignVCenter | Qt.AlignRight)
        self.setLayout(Grid)
        return widget

    def getPoints(self):

        self.pX,self.pR = [],[]
        for pEdit in self.pEdits:
            if (pEdit[0].text() == '') or (pEdit[1].text() == ''):
                pass
            else:
                self.pX.append(float(pEdit[0].text()))
                self.pR.append(float(pEdit[1].text()))


    def fitOrbit(self):

        self.getPoints()
        self.nPoints = len(self.pX)
        p0 = [0. for i in range(self.nPoints)]

        #p0 = [1.,1.,1.,1.,1.,1.,1.]
        #self.nPoints = 0
        #self.nPoints = 7
        #self.pX = [1640.2,1636.2,1635.5,1634.9,1637.1,1639.6,1647.4]
        #self.pR = [-60.,-40.,-20.,0.,20.,40.,60.]

        if self.nPoints == 3: fitfunc = lambda p, x: p[0] * np.sin(np.pi * x / 180. + p[1] * np.pi / 180.) + p[3]
        elif self.nPoints == 4: fitfunc = lambda p, x: p[0] * np.sin(np.pi * x / 180. + p[1] * np.pi / 180.) * np.cos(p[2] * np.pi / 180.) + p[3]
        elif self.nPoints == 5: fitfunc = lambda p, x: p[0] * np.sin(np.pi * x / 180. + p[1] * np.pi / 180.) * np.cos(p[2] * np.pi / 180.) + p[3] + p[4] * x
        elif self.nPoints == 6: fitfunc = lambda p, x: p[0] * np.sin(np.pi * x / 180. + p[1] * np.pi / 180.) * np.cos(p[2] * np.pi / 180.) + p[3] + p[4] * x
        elif self.nPoints == 7: fitfunc = lambda p, x: p[0] * np.cos(np.pi * x / 180. + p[1] * np.pi / 180.) + p[2]#* np.cos(p[2] * np.pi / 180.) + p[3] + p[4] * x
        #elif self.nPoints == 7: fitfunc = lambda p, x: np.sqrt(p[0]**2 + p[1]**2) * np.cos(np.pi * x / 180. + np.arctan(p[1]/p[0])) + p[2]
        zfitfunc = lambda p, x: -np.sign(p[2]) * (p[0] * np.cos(np.pi * x / 180. - p[1] * np.pi / 180.) - p[0] * np.cos(p[1] * np.pi / 180.))
        errfunc = lambda p, x, y: fitfunc(p, x) - y
        if self.nPoints > 2:
            p1, success = optimize.leastsq(errfunc, p0[:], args = (np.array(self.pR), np.array(self.pX)))
            self.pXcalc = fitfunc(p1, np.array(self.pR))
            self.pZcalc = zfitfunc([p1[0],p1[1], fitfunc(p1,0) - p1[3],0.,0.,0.,0.],np.array(self.pR))
            for pEdit,pX,pZ in zip(self.pEdits,self.pXcalc,self.pZcalc):
                pEdit[2].setText(str(round(pX,2)))
                pEdit[3].setText(str(round(pZ,2)))

            for i in range(self.nPoints):
                self.valLabels[i].setText(str(round(p1[i],4)))

        if self.pEstrEdit.text() != '':
            if self.pEstxEdit.text() == '':
                self.pEstxcalcEdit.setText(str(round(fitfunc(p1,float(self.pEstrEdit.text())),2)))
                self.pEstzcalcEdit.setText(str(round(zfitfunc([p1[0],p1[1], fitfunc(p1,0) - p1[3],0.,0.,0.,0.],float(self.pEstrEdit.text())),2)))
            else:
                dx = float(self.pEstxEdit.text()) - fitfunc(p1,0.) #this dx'
                r2 = p1[0] - dx * np.sqrt((1. + 1. / np.tan(p1[1] * np.pi / 180.)**2))# this is a0*
                print dx, r2, p1[0]
                p2 = [r2,p1[1],p1[2],p1[3],p1[4],p1[5],p1[6]]
                self.pEstxcalcEdit.setText(str(round(fitfunc(p2,float(self.pEstrEdit.text())),2)))
                self.pEstzcalcEdit.setText(str(round(zfitfunc([p2[0],p2[1], fitfunc(p2,0) - p2[3],0.,0.,0.,0.],float(self.pEstrEdit.text())),2)))



class qtDiffInspectorMain(QMainWindow):
    def __init__(self, parent = None, dataFile = None):
        
        super(qtDiffInspectorMain, self).__init__()

        self.histNBins = 10
        self.__thumbSize = 600
        self.mainImageSize = QSize(self.__thumbSize, self.__thumbSize)
        self.setWindowTitle('qtDiffInspector')        
        self.sidePlotSize = QSize(96,self.__thumbSize)
        self.bottomPlotSize = QSize(self.__thumbSize,96)
        self.selectedRect = QRect()
        self.selectedRectTL = QPoint()
        self.selectedRectBR = QPoint()
        self.dataRect = QRect()
        self.dataStartRect = QRect()
        self.imageRect = QRect()
        self.dataRectTL = QPoint()
        self.dataRectBR = QPoint()
        self.imStartPt = QPoint()
        self.__zoomX = self.__thumbSize
        self.__zoomY = self.__thumbSize
        self.dataMin = None
        self.dataMax = None
        self.dataX, self.dataY = 0,0
        self.dataSlice = None
        self.plotType = 'horLine'
        self.dataXform = None
        self.gotXform = False
        self.drawFilter = False
        self.drawPolygon = False
        self.isComplex = False
        self.filePath = os.environ['PTYCHODATAROOT']
        self.homePath = os.environ['PYPTYCHOROOT'] + '/pyptycho/gui/'
        self.fileList = []
        self.fileIndex = 0
        self.fileTypes = 'tiff','tif','TIFF','TIF','npy','xim','hdr'
        self.gotFit = False
        self.x0 = 1536.
        self.cmap = 'bone'

        #inspectorWidget = self.inspectorWidget()
        self.setCentralWidget(self.inspectorWidget())
        self.layout().setSizeConstraint(QLayout.SetFixedSize)

        open = QAction(QIcon(self.homePath + 'icons/open.png'), 'Open', self)
        open.setShortcut('Ctrl+O')
        open.setStatusTip('Open File')
        self.connect(open, SIGNAL('triggered()'), self.chooseFile)
        
        saveData = QAction('Save Data', self)
        saveData.setShortcut('Ctrl+S')
        saveData.setStatusTip('Save Data File')
        self.connect(saveData, SIGNAL('triggered()'), self.saveFile)
        
        saveImage = QAction('Save Image', self)
        saveImage.setShortcut('Ctrl+I')
        saveImage.setStatusTip('Save Image File')
        self.connect(saveImage, SIGNAL('triggered()'), self.saveImage)

        reload = QAction('Reload', self)
        reload.setShortcut('Ctrl+R')
        reload.setStatusTip('Reload File')
        self.connect(reload, SIGNAL('triggered()'), self.reload)

        exit = QAction(QIcon(self.homePath + 'icons/exit.png'), 'Exit', self)
        exit.setShortcut('Ctrl+Q')
        exit.setStatusTip('Exit application')
        self.connect(exit, SIGNAL('triggered()'), SLOT('close()'))

        openPolygon = QAction('Open Polygon', self)
        openPolygon.setStatusTip('Open Polygon')
        self.connect(openPolygon, SIGNAL('triggered()'), self.openPolygon)

        savePolygon = QAction('Save Polygon', self)
        savePolygon.setStatusTip('Save Polygon')
        self.connect(savePolygon, SIGNAL('triggered()'), self.savePolygon)

        applyPolygon = QAction('Apply Polygon', self)
        applyPolygon.setStatusTip('Apply Polygon')
        self.connect(applyPolygon, SIGNAL('triggered()'), self.applyPolygon)

        clearPolygon = QAction('Clear Polygon', self)
        clearPolygon.setStatusTip('Clear Polygon')
        self.connect(clearPolygon, SIGNAL('triggered()'), self.clearPolygon)

        fitPolynomial = QAction('Fit Polynomial', self)
        fitPolynomial.setStatusTip('Fit Polynomial')
        self.connect(fitPolynomial, SIGNAL('triggered()'), self.fitPolynomial)

        levels = QAction('Levels', self)
        levels.setStatusTip('Set Thresholds')
        self.connect(levels, SIGNAL('triggered()'), self.levels)

        self.statusBar()

        # menubar = self.menuBar()
        # file = menubar.addMenu('&File')
        # file.addAction(open)
        # file.addAction(saveData)
        # file.addAction(saveImage)
        # file.addAction(reload)
        # file.addAction(exit)
        # tools = menubar.addMenu('&Tools')
        # polygon = tools.addMenu('&Polygon')
        # polygon.addAction(openPolygon)
        # polygon.addAction(savePolygon)
        # polygon.addAction(applyPolygon)
        # polygon.addAction(clearPolygon)
        # polygon.addAction(fitPolynomial)
        # tools.addAction(levels)

    def savePolynomial(self):
        pass

    def fitPolynomial(self):
        self.mainImage.polygonFit = QPolygon()
        self.xImagePixelValues = np.insert(self.xImagePixelValues,len(self.xImagePixelValues),self.xpt)
        self.yImagePixelValues = np.insert(self.yImagePixelValues,len(self.yImagePixelValues),self.ypt)
        self.xDataPixelValues = np.insert(self.xDataPixelValues,len(self.xDataPixelValues),self.dataX - 1)
        self.yDataPixelValues = np.insert(self.yDataPixelValues,len(self.yDataPixelValues),self.dataY - 1)
        self.yfitDataPixelValues = np.arange(140)
        p0 = [1000.,1.,1.] # Initial guess for the parameters
        if self.mainImage.polygon.size() > len(p0) - 1:
            self.gotFit = True
            #fitfunc = lambda p, x: p[0]*np.cos(np.pi * x / 180. + p[1]) + p[2] # Target function
            fitfunc = lambda p, x: p[0] * np.sin(np.pi * x / 180. + p[1]) + p[2]# + p[2] * x + p[3] * x**2 + p[4] * x**3 # Target function
            #zfitfunc = lambda p, x: p[0]*np.sin(np.pi * x / 180. + p[1]) + p[2] # Target function
            zfitfunc = lambda p, x: p[0] * np.cos(np.pi * x / 180. + p[1]) #+ p[0] + p[2] * x + p[3] * x**2 + p[4] * x**3 #
            errfunc = lambda p, x, y: fitfunc(p, x) - y # Distance to the target function
            p1, success = optimize.leastsq(errfunc, p0[:], args = (self.yDataPixelValues, self.xDataPixelValues))
            self.xfitDataPixelValues = fitfunc(p1, self.yDataPixelValues)
            self.xfitImagePixelValues = ((np.single(self.xfitDataPixelValues + 1 - self.dataRect.left()) / float(self.xsize) * \
                                             float(self.__zoomX) + (float(self.mainImageSize.width())/2. - \
                                                                    float(self.__zoomX)/2.))).round().astype('int')
            self.yfitImagePixelValues = ((np.single(self.yfitDataPixelValues + 1 - self.dataRect.top()) / float(self.ysize) *\
                                          float(self.__zoomY) + (float(self.mainImageSize.height())/2. -\
                                                                 float(self.__zoomY)/2.))).round().astype('int')
            print self.yfitImagePixelValues
            print self.yImagePixelValues
            for i in range(len(self.xfitImagePixelValues)):
                self.mainImage.polygonFit.prepend(QPoint(self.xfitImagePixelValues[i],self.yImagePixelValues[i]))
            self.updateImage()

            if self.STXMdata:
                xMotorPoints = np.array(self.STXMdata.xPoints)[self.xDataPixelValues.astype('int')]
                yMotorPoints = np.array(self.STXMdata.yPoints)[self.yDataPixelValues.astype('int')]
                #p0 = [1000.,1.,1.,1.,1.,1.] # Initial guess for the parameters
                p1, success = optimize.leastsq(errfunc, p0[:], args = (yMotorPoints, xMotorPoints))
                xfitMotorPoints = fitfunc(p1, yMotorPoints)
                dz = zfitfunc([p1[0],p1[1]],yMotorPoints) - zfitfunc([p1[0],p1[1]],0)

                dX0 = 531. - p1[1]

                print "Fitting coefficients:"
                print p1[0],p1[2],p1[1]*180./np.pi#,p1[2],p1[3],p1[4]
                print "Theta - X Actual - X Fit - Residual - Defocus Correction"
                for i in range(len(xfitMotorPoints)):
                    print yMotorPoints[i], xMotorPoints[i],xfitMotorPoints[i],abs(xMotorPoints[i]-xfitMotorPoints[i]), -dz[i]

        else:
            print "Polynomial fit requires more than 1 point"

    def updatePoints(self):

#        self.yImagePixelValues = ((np.single(self.yDataPixelValues + 1 - self.dataRect.top()) / float(self.ysize) *\
#                                   float(self.__zoomY) + (float(self.mainImageSize.height())/2. -\
#                                                          float(self.__zoomY)/2.))).round().astype('int')
#        self.xImagePixelValues = ((np.single(self.xDataPixelValues + 1 - self.dataRect.left()) / float(self.xsize) *\
#                                   float(self.__zoomX) + (float(self.mainImageSize.width())/2. -\
#                                                          float(self.__zoomX)/2.))).round().astype('int')
#
#        if self.gotFit:
#            self.mainImage.polygonFit = QPolygon()
#            self.xfitImagePixelValues = ((np.single(self.xfitDataPixelValues + 1 - self.dataRect.left()) / float(self.xsize) *\
#                                          float(self.__zoomX) + (float(self.mainImageSize.width())/2. -\
#                                                                 float(self.__zoomX)/2.))).round().astype('int')

        for i in range(len(self.xImagePixelValues)):
            self.mainImage.polygon.setPoint(i,self.xImagePixelValues[i],self.yImagePixelValues[i])
            if self.gotFit: self.mainImage.polygonFit.prepend(QPoint(self.xfitImagePixelValues[i],self.yImagePixelValues[i]))



    def openPolygon(self):

        pass

    def savePolygon(self):

        pass

    def applyPolygon(self):

        pass

    def clearPolygon(self):

        pass

    def levels(self):

        self.diffLevels = levelsWindow(data = self.data)
        self.connect(self.diffLevels, SIGNAL("Levels"), self.applyLevels)
        self.connect(self.diffLevels, SIGNAL("Reset"),self.clear)
        self.diffLevels.show()
        sys.exit(self.exec_())

    def applyLevels(self):

        if self.diffLevels.maskButton.isChecked():
            print "Applying mask with values [lo,hi]=[%.2f,%.2f]" %(self.diffLevels.lowValue,self.diffLevels.hiValue)
            self.mask = (self.data > self.diffLevels.lowValue) * (self.data < self.diffLevels.hiValue)
        else:
            self.mask = np.ones(self.data.shape)
            self.dataMin = self.diffLevels.lowValue
            self.dataMax = self.diffLevels.hiValue
        self.updateImage()
        self.imageClick(x1 = self.xpt, x2 = None, y1 = self.ypt, y2 = None)

    def keyPressEvent(self, event):

        self.mainImage.key = event.key()

    def keyReleaseEvent(self, event):

        self.mainImage.key = None

    def inspectorWidget(self):

        widget = QWidget(self)
        grid = QGridLayout(widget)
        grid.setSpacing(4)
        grid.setMargin(4)
        self.mainImage = imageWindow(widget,size = self.mainImageSize, \
            pressFunction = self.imageClick, moveFunction = self.imageDrag)
        self.mainImage.setMouseTracking(True)
        self.bottomPlot = plotWindow(widget,size = self.bottomPlotSize)
        self.bottomPlot.printValues = True
        self.sidePlot = plotWindow(widget,size = self.sidePlotSize)

        bg = "QLabel {background-color:white}"
        self.sidePlot.setStyleSheet( bg )

        bg = "QLabel {background-color:white}"
        self.bottomPlot.setStyleSheet( bg )

        self.connect(self.mainImage, SIGNAL("clearToggles"), self.clearToggles)
        self.connect(self.mainImage, SIGNAL("calcPoints"), self.fitPolynomial)

        self.sideInfo = infoWidget(widget)
        self.connect(self.sideInfo, SIGNAL("refresh"), self.placeHolder)#self.updateInfoPlot)
        self.connect(self.sideInfo,SIGNAL("refresh"),self.updateROI)

        self.logButton = QRadioButton()
        self.logButton.setText('Log Scale')
        self.logButton.clicked.connect(self.logButtonClicked)

        self.zoomButton = QPushButton()
        self.zoomButton.setText('Zoom')
        self.zoomButton.clicked.connect(self.zoom)

        self.scaleButton = QPushButton()
        self.scaleButton.setText('Autoscale')
        self.scaleButton.setGeometry(10,10,100,30)
        self.scaleButton.clicked.connect(self.autoscale)

        self.tB = self.toolBar()

        grid.addWidget(self.tB, 0, 0, 1, 3, Qt.AlignVCenter | Qt.AlignLeft)
        grid.addWidget(self.mainImage, 1, 0,1,1)#, Qt.AlignTop | Qt.AlignHCenter)
        grid.addWidget(self.sidePlot, 1, 1,1,1)#, Qt.AlignTop | Qt.AlignHCenter)
        grid.addWidget(self.bottomPlot, 2, 0,3,1)#, Qt.AlignTop | Qt.AlignHCenter)
        grid.addWidget(self.sideInfo, 1, 2, 4, 2, Qt.AlignTop | Qt.AlignHCenter)
        grid.addWidget(self.logButton, 2,1,1,1,Qt.AlignBottom | Qt.AlignHCenter)
        grid.addWidget(self.zoomButton, 3,1,1,1,Qt.AlignBottom | Qt.AlignHCenter)
        grid.addWidget(self.scaleButton, 4,1,1,1,Qt.AlignBottom | Qt.AlignHCenter)
        #self.setLayout(grid)
	widget.setLayout(grid)
        return widget

    def placeHolder(self):

        pass

    def logButtonClicked(self):

        self.updateImage()


    def updateROI(self):

        if self.sideInfo.roiBox.currentIndex() == 0:
            self.dataRect.setLeft(self.sideInfo.roiLeft)
            self.dataRect.setRight(self.sideInfo.roiRight)
            self.dataRect.setTop(self.sideInfo.roiTop)
            self.dataRect.setBottom(self.sideInfo.roiBottom)
        elif self.sideInfo.roiBox.currentIndex() == 1:
            self.dataRect.setLeft(self.sideInfo.roiCenterX - self.sideInfo.roiSize / 2)
            self.dataRect.setRight(self.sideInfo.roiCenterX + self.sideInfo.roiSize / 2)
            self.dataRect.setTop(self.sideInfo.roiCenterY - self.sideInfo.roiSize / 2)
            self.dataRect.setBottom(self.sideInfo.roiCenterY + self.sideInfo.roiSize / 2)

        if (self.dataRect.width() > self.dataRect.height()):
            factor = float(self.dataRect.height())/float(self.dataRect.width())
            self.__zoomX = self.mainImageSize.width()
            self.__zoomY = factor * self.__zoomX
        elif (self.dataRect.width() < self.dataRect.height()):
            factor = float(self.dataRect.width())/float(self.dataRect.height())
            self.__zoomY = self.mainImageSize.height()
            self.__zoomX = factor * self.__zoomY
        else:
            self.__zoomX, self.__zoomY = self.mainImageSize.width(),self.mainImageSize.height()
        self.setRoiText()
        self.updateImage()

    def orbitClicked(self, checked=None):
        self.w = orbitPopup()
        #self.w.setGeometry(QRect(100, 100, 400, 400))
        self.w.show()

    def toolBar(self):

        widget = QWidget(self)
        grid = QGridLayout(widget)
        grid.setSpacing(0)
        grid.setMargin(0)

        self.openButton = QPushButton(QIcon(self.homePath + 'icons/open.png'),'')
        self.openButton.clicked.connect(self.chooseFile)
        self.openButton.setMaximumSize(32, 32)
        #self.openButton.setMinimumSize(32, 32)
        self.openButton.setToolTip('Open New File')

        self.saveButton = QPushButton(QIcon(self.homePath + 'icons/save.png'),'')
        self.saveButton.clicked.connect(self.saveFile)
        self.saveButton.setMaximumSize(32, 32)
        self.saveButton.setToolTip('Save Data File')

        self.saveImageButton = QPushButton(QIcon(self.homePath + 'icons/saveImage.png'),'')
        self.saveImageButton.clicked.connect(self.saveImage)
        self.saveImageButton.setMaximumSize(32, 32)
        self.saveImageButton.setToolTip('Save Image File')

        self.printButton = QPushButton(QIcon(self.homePath + 'icons/print.png'),'')
        self.printButton.clicked.connect(self.printFile)
        self.printButton.setMaximumSize(32, 32)
        self.printButton.setToolTip('Print to PDF')

        self.vLine = QFrame(self)
        self.vLine.setFrameShape(QFrame.VLine)
        self.vLine.setFrameShadow(QFrame.Sunken)
        self.vLine.setMinimumSize(QSize(2,32))

        self.clearButton = QPushButton(QIcon(self.homePath + 'icons/clear.png'),'')
        self.clearButton.clicked.connect(self.clear)
        self.clearButton.setMaximumSize(32, 32)
        self.clearButton.setToolTip('Reset')

        self.reloadButton = QPushButton(QIcon(self.homePath + 'icons/reload.png'),'')
        self.reloadButton.clicked.connect(self.reload)
        self.reloadButton.setMaximumSize(32, 32)
        self.reloadButton.setToolTip('Reload Current File')

        setHist = QAction('Histogram', self,triggered=self.setHistPlot)
        setHist.setStatusTip('Histogram Plot')

        setRadial = QAction('Radial Average', self, triggered=self.setRadialPlot)
        setRadial.setStatusTip('Radial Average')

        setHorLine = QAction('Horizontal Lineout', self, triggered=self.setHorLinePlot)
        setHorLine.setStatusTip('Horizontal Lineout')

        setVerLine = QAction('Vertical Lineout', self,triggered=self.setVerLinePlot)
        setVerLine.setStatusTip('Vertical Lineout')

        setSpecialLine = QAction('Slice', self,triggered=self.setSpecialLinePlot)
        setSpecialLine.setStatusTip('Slice')

        self.plotTypeButton = QPushButton()
        self.plotTypeButton.setText('Plot Type')
        self.plotTypeButton.setMinimumSize(64, 26)

        plotMenu = QMenu()
        plotMenu.addAction(setHist)
        plotMenu.addAction(setHorLine)
        plotMenu.addAction(setVerLine)
        plotMenu.addAction(setRadial)
        plotMenu.addAction(setSpecialLine)
        self.plotTypeButton.setMenu(plotMenu)

        setGray = QAction('Gray', self,triggered=self.setGray)
        setGray.setStatusTip('Colormap: gray')

        setJet = QAction('Jet', self,triggered=self.setJet)
        setJet.setStatusTip('Colormap: jet')

        setHot = QAction('Hot', self,triggered=self.setHot)
        setHot.setStatusTip('Colormap: hot')

        setStern = QAction('Stern', self,triggered=self.setStern)
        setStern.setStatusTip('Colormap: Stern')

        setEarth = QAction('Earth', self,triggered=self.setEarth)
        setEarth.setStatusTip('Colormap: Earth')

        setBone = QAction('Bone', self,triggered=self.setBone)
        setBone.setStatusTip('Colormap: Bone')

        self.colorMapButton = QPushButton()
        self.colorMapButton.setText('Color map')
        self.colorMapButton.setMinimumSize(64, 26)

        colorMenu = QMenu()
        colorMenu.addAction(setGray)
        colorMenu.addAction(setJet)
        colorMenu.addAction(setHot)
        colorMenu.addAction(setStern)
        colorMenu.addAction(setEarth)
        colorMenu.addAction(setBone)
        self.colorMapButton.setMenu(colorMenu)

        self.orbitButton = QPushButton()
        self.orbitButton.setText('Orbit')
        self.orbitButton.clicked.connect(self.orbitClicked)
        self.orbitButton.setMaximumSize(64,32)
        self.orbitButton.setToolTip('Calculate Sample Orbit')


        self.fftshiftButton = QPushButton(QIcon(self.homePath + 'icons/fftshift.png'),'')
        self.fftshiftButton.clicked.connect(self.fftshift)
        self.fftshiftButton.setMaximumSize(32,32)
        self.fftshiftButton.setToolTip('0 Frequency Shift Data')

        self.fftButton = QPushButton(QIcon(self.homePath + 'icons/fft.png'),'')
        self.fftButton.clicked.connect(self.fft)
        self.fftButton.setMaximumSize(32, 32)
        self.fftButton.setToolTip('Fourier Transform Data')

        self.unzoomButton = QPushButton(QIcon(self.homePath + 'icons/unzoom.png'),'')
        self.unzoomButton.clicked.connect(self.unzoom)
        self.unzoomButton.setMaximumSize(32, 32)
        self.unzoomButton.setToolTip('Zoom Out')

        self.filterButton = QPushButton(QIcon(self.homePath + 'icons/filter.png'),'')
        self.filterButton.clicked.connect(self.setFilter)
        self.filterButton.setMaximumSize(32, 32)
        self.filterButton.setToolTip('Draw FFT Filter')
        self.filterButton.setCheckable(True)

        self.polygonButton = QPushButton(QIcon(self.homePath + 'icons/polygon.png'),'')
        self.polygonButton.clicked.connect(self.setPolygon)
        self.polygonButton.setMaximumSize(32, 32)
        self.polygonButton.setToolTip('Draw Free Polygon')
        self.polygonButton.setCheckable(True)

        self.pointsButton = QPushButton(QIcon(self.homePath + 'icons/points.png'),'')
        self.pointsButton.clicked.connect(self.setPoints)
        self.pointsButton.setMaximumSize(32, 32)
        self.pointsButton.setToolTip('Draw Points')
        self.pointsButton.setCheckable(True)

        self.nextButton = QPushButton(QIcon(self.homePath + 'icons/next.png'),'')
        self.nextButton.clicked.connect(self.openNext)
        self.nextButton.setMaximumSize(32, 32)
        self.nextButton.setToolTip('Open Next File in Directory')

        self.previousButton = QPushButton(QIcon(self.homePath + 'icons/previous.png'),'')
        self.previousButton.clicked.connect(self.openPrevious)
        self.previousButton.setMaximumSize(32, 32)
        self.previousButton.setToolTip('Open Previous File in Directory')

        if sys.platform == 'darwin': grid.setHorizontalSpacing(13)
        else: grid.setHorizontalSpacing(1)
        grid.addWidget(self.openButton,0,0,1,1,Qt.AlignVCenter | Qt.AlignLeft)
        grid.addWidget(self.saveButton,0,1,1,1,Qt.AlignVCenter | Qt.AlignLeft)
        grid.addWidget(self.saveImageButton,0,2,1,1,Qt.AlignVCenter | Qt.AlignLeft)
        grid.addWidget(self.printButton,0,3,1,1,Qt.AlignVCenter | Qt.AlignLeft)
        grid.addWidget(self.vLine,0,4,1,1,Qt.AlignVCenter | Qt.AlignLeft)
        grid.addWidget(self.previousButton,0,5,1,1, Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.nextButton,0,6,1,1, Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.unzoomButton,0,7,1,1, Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.clearButton,0,8,1,1,Qt.AlignVCenter | Qt.AlignLeft)
        grid.addWidget(self.reloadButton,0,9,1,1, Qt.AlignVCenter | Qt.AlignLeft)
        grid.addWidget(self.plotTypeButton,0,15,1,1, Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.colorMapButton,0,16,1,1, Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.orbitButton,0,17,1,1, Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.fftshiftButton,0,10,1,1, Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.fftButton,0,11,1,1, Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.filterButton,0,12,1,1, Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.polygonButton,0,13,1,1, Qt.AlignVCenter | Qt.AlignRight)
        grid.addWidget(self.pointsButton,0,14,1,1, Qt.AlignVCenter | Qt.AlignRight)
        #grid.addWidget(self.filesButton,0,0,1,1, Qt.AlignVCenter | Qt.AlignRight)
        #self.setLayout(grid)
	widget.setLayout(grid)
        return widget

    def setGray(self):

        self.cmap = 'gray'
        self.unzoom()

    def setHot(self):

        self.cmap = 'hot'
        self.unzoom()

    def setJet(self):

        self.cmap = 'jet'
        self.unzoom()

    def setStern(self):

        self.cmap = 'stern'
        self.unzoom()

    def setEarth(self):

        self.cmap = 'earth'
        self.unzoom()

    def setBone(self):

        self.cmap = 'bone'
        self.updateImage()

    def openNext(self):
        
        if (self.fileIndex + 1) < self.nFiles:
            self.openFile(self.filePath + self.fileList[self.fileIndex + 1])
        else:
            pass

    def openPrevious(self):

        if (self.fileIndex - 1) > 0:
            self.openFile(self.filePath + self.fileList[self.fileIndex - 1])
        else:
            pass

    def fileViewer(self):

        window = Window(path = self.filePath)
        window.show()
        sys.exit(self.exec_())

    def setFilter(self):

        if self.filterButton.isChecked() == False:
            self.mainImage.drawFilter = False
            self.clearToggles()
        else:
            self.mainImage.drawFilter = True
            self.mainImage.drawPolygon = False
            self.mainImage.drawPoints = False
            self.mainImage.polygon = QPolygon()
            self.polygonButton.setChecked(False)
            self.pointsButton.setChecked(False)
            
    def setPolygon(self):

        if self.polygonButton.isChecked() == False:
            self.mainImage.drawPolygon = False
            self.clearToggles()
        else:
            self.mainImage.drawPolygon = True
            self.mainImage.drawFilter = False
            self.mainImage.drawPoints = False
            self.filterButton.setChecked(False)
            self.pointsButton.setChecked(False)
            self.mainImage.polygon = QPolygon()
            self.mainImage.update()

    def setPoints(self):

        if self.pointsButton.isChecked() == False:
            self.mainImage.drawPoints = False
            self.mainImage.points = QPolygon()
            self.clearToggles()
        else:
            self.mainImage.drawPoints = True
            self.mainImage.drawFilter = False
            self.mainImage.drawPolygon = False
            self.filterButton.setChecked(False)
            self.polygonButton.setChecked(False)
            self.mainImage.polygon = QPolygon()
            self.mainImage.update()

    def clearToggles(self):
            self.xImagePixelValues = np.array(())
            self.yImagePixelValues = np.array(())
            self.xDataPixelValues = np.array(())
            self.yDataPixelValues = np.array(())
            self.gotFit = False
            self.mainImage.drawFilter = False
            self.mainImage.dragFilter = False
            self.mainImage.filter = QRect()
            self.filterButton.setChecked(False)
            self.mainImage.drawPolygon = False
            self.mainImage.polygon = QPolygon()
            self.mainImage.polygonFit = QPolygon()
            self.polygonButton.setChecked(False)
            self.mainImage.drawPoints = False
            self.mainImage.points = QPolygon()
            self.pointsButton.setChecked(False)
            self.mainImage.pointsRectList = []
            self.mainImage.update()

    def unzoom(self):

        if self.dataShape[1] > self.dataShape[0]:
            self.__zoomX, self.__zoomY = self.__thumbSize, round(float(self.dataShape[0]) / float(self.dataShape[1]) * self.__thumbSize)
        else:
            self.__zoomX, self.__zoomY = round(float(self.dataShape[1]) / float(self.dataShape[0]) * self.__thumbSize), self.__thumbSize

        self.sideInfo.roiLeft = 0
        self.sideInfo.roiTop = 0
        self.sideInfo.roiRight = self.dataShape[1]
        self.sideInfo.roiBottom = self.dataShape[0]
        self.sideInfo.roiCenterX = self.dataShape[1]/2
	#if (self.sideInfo.roiCenterX) % 2 != 0: self.sideInfo.roiCenterX += 1
        self.sideInfo.roiCenterY = self.dataShape[0]/2
	#if (self.sideInfo.roiCenterY) % 2 != 0: self.sideInfo.roiCenterY += 1
        self.sideInfo.roiSize = min(self.dataShape[0],self.dataShape[1])
        self.logOffset = 0.0001 * self.data.max()
        self.dataRect.setRect(0,0,self.dataShape[1]+1,self.dataShape[0]+1)
        self.mainImage.box.setRect(0,0,self.dataShape[1]+1,self.dataShape[0]+1)
        self.imageClick(self.mainImageSize.width() / 2, 0,self.mainImageSize.height() / 2,0)
        self.updatePlots()
        #self.updateInfo()
        self.setRoiText()
        self.updateImage()

    def fft(self):

        if self.mainImage.gotFilter:
            self.dataXform = abs(filtered_auto(self.data, diameter = self.filterWidth, \
                bp = True, center = (self.filterY,self.filterX)))
            self.gotXform = True
            self.autoscale()
        else:
            self.dataXform = abs(np.fft.fftshift(np.fft.fftn(self.data[\
                self.dataRect.top():self.dataRect.bottom(),self.dataRect.left():\
                self.dataRect.right()]*self.mask[self.dataRect.top():\
                self.dataRect.bottom(),self.dataRect.left():self.dataRect.right()])))
            self.gotXform = True
            self.autoscale()
        self.mainImage.gotFilter = False
        self.mainImage.drawFilter = False
        self.mainImage.drawPolygon = False
        #self.mainImage.gotPolygon = False
        self.polygonButton.setChecked(False)
        self.filterButton.setChecked(False)

    def fftshift(self):

        self.data = np.fft.fftshift(self.data)
        self.mask = np.fft.fftshift(self.mask)
        self.updateImage()

    def setHistPlot(self):

        self.plotType = 'histogram'
        self.updateInfoPlot()

    def setHorLinePlot(self):

        self.plotType = 'horLine'
        self.updateInfoPlot()

    def setVerLinePlot(self):

        self.plotType = 'verLine'
        self.updateInfoPlot()

    def setRadialPlot(self):

        self.plotType = 'radial'
        self.updateInfoPlot()

    def setSpecialLinePlot(self):

        self.plotType = 'specialLine'
        self.updateInfoPlot()

    def saveFile(self):

        fileFormat = 'tif'
        initialPath = QDir.currentPath() + '/untitled.' + 'tif'

        fileName = QFileDialog.getSaveFileName(self, "Save As",
                initialPath,
                "%s Files (*.%s);;All Files (*)" % (fileFormat.upper(), fileFormat))

        temp = self.data[self.dataRect.top():self.dataRect.bottom(),self.dataRect.left():self.dataRect.right()]
        im = toimage(temp, high = temp.max(), low = temp.min(), mode = 'F')
        im.save(str(fileName))

    def clear(self):
        
        self.logButton.setChecked(False)
        if self.dataShape[1] > self.dataShape[0]:
            self.__zoomX, self.__zoomY = self.__thumbSize, round(float(self.dataShape[0]) / float(self.dataShape[1]) * self.__thumbSize)
        else:
            self.__zoomX, self.__zoomY = round(float(self.dataShape[1]) / float(self.dataShape[0]) * self.__thumbSize), self.__thumbSize
        self.logOffset = 0.0001 * self.data.max()
        self.dataRect.setRect(0,0,self.dataShape[1]+1,self.dataShape[0]+1)
        self.mainImage.box.setRect(0,0,self.dataShape[1]+1,self.dataShape[0]+1)
        self.dataXform = None
        self.gotXform = False
        self.imageClick(self.mainImageSize.width() / 2, 0,self.mainImageSize.height() / 2,0)
        self.mask = np.ones(self.data.shape)
        self.autoscale()
        self.updatePlots()

    def saveImage(self):

        initialPath = QDir.currentPath() + '/untitled.png'

        fileName = QFileDialog.getSaveFileName(self, "Save As",
                initialPath,
                "%s Files (*.%s);; %s Files (*.%s);; All Files (*)" % ('PNG','png','TIFF','tiff'))
        if fileName:
            if fileName.split('.')[-1::][0] == 'png':
                if self.logButton.isChecked():
                    return imsave(np.log(self.data[self.dataRect.top():\
                        self.dataRect.bottom(),self.dataRect.left():self.dataRect.right()] + self.logOffset),\
                        str(fileName),ct = self.cmap)
                else:
                    return imsave(np.clip(self.data[self.dataRect.top():self.dataRect.bottom(),\
                    self.dataRect.left():self.dataRect.right()],self.dataMin,self.dataMax),\
                        str(fileName),ct = self.cmap)
            elif fileName.split('.')[-1::][0] == 'tiff':
                temp = Image.fromarray(abs(self.data[self.dataRect.top():self.dataRect.bottom(),\
                                           self.dataRect.left():self.dataRect.right()]))
                temp.save(str(fileName))
            else: print "Unrecognized file format: %s" %(fileName.split('.')[-1::][0])

        return False

    def printFile(self):

        def hello(c):
                c.drawString(100,500,"Hello World")

        fileFormat = 'pdf'
        initialPath = QDir.currentPath() + '/untitled.' + fileFormat

        fileName = QFileDialog.getSaveFileName(self, "Save As",
                initialPath,
                "%s Files (*.%s);;All Files (*)" % (fileFormat.upper(), fileFormat))
        if fileName:
            c = canvas.Canvas("hello.pdf")
            hello(c)
            c.showPage()
            c.save()
            call(["evince","hello.pdf"])


    def updateInfoPlot(self):

        dummy = (self.sideInfo.energy * self.sideInfo.wavelength * self.sideInfo.pixelSize * self.sideInfo.ccdZ != 0.)
        q = (self.sideInfo.plotBox.currentIndex() == 1)

        if self.plotType == 'histogram':
            if self.gotXform:
                histVec, binEdges = np.histogram(self.dataXform[self.dataRect.top():self.dataRect.bottom(),\
                    self.dataRect.left():self.dataRect.right()], self.histNBins)
            else:
                histVec, binEdges = np.histogram(self.data[self.dataRect.top():self.dataRect.bottom(),\
                    self.dataRect.left():self.dataRect.right()], self.histNBins)
            histVec = np.log10(histVec)
            binEdges = binEdges / 1000.
            self.sideInfo.axes.clear()
            self.sideInfo.axes.grid(True,color = 'blue')
            self.sideInfo.axes.bar(binEdges[:-1],histVec,binEdges.max()/20.)
            self.sideInfo.axes.set_xlabel(r'Pixel Values ($\times$1000)')
            self.sideInfo.axes.set_ylabel(r'log$_{10}$(\# of Pixels)')
            self.sideInfo.axes.set_title(r'Histogram')
            self.sideInfo.canvas.draw()
        elif self.plotType == 'horLine':
            if self.gotXform: hVec = self.dataXform[self.dataY,self.dataRect.left():self.dataRect.right()]
            else: hVec = self.data[self.dataY,self.dataRect.left():self.dataRect.right()]
            if self.STXMdata:
                x = np.array(self.STXMdata.xPoints)[self.dataRect.left():self.dataRect.right()]
            else: x = np.arange(self.dataRect.left(),self.dataRect.right())
            self.sideInfo.axes.clear()
            self.sideInfo.axes.grid(True,color = 'blue')
            if dummy * q:
                x = 1. / self.sideInfo.wavelength * (x - self.dataShape[1] / 2.) * self.sideInfo.pixelSize / self.sideInfo.ccdZ / 1000.
                y = 1. / self.sideInfo.wavelength * (-self.dataY + self.dataShape[0]/2.) * self.sideInfo.pixelSize / self.sideInfo.ccdZ/1000.
                self.sideInfo.axes.set_xlabel(r'$Q_{x}$ (nm$^{-1}$)')
                self.sideInfo.axes.set_title(r'Horizontal Slice: $Q_{y}$ = %.4f' %(y))
            else:
                self.sideInfo.axes.set_xlabel(r'Horizontal Pixel Location')
                self.sideInfo.axes.set_title(r'Horizontal Slice: Y = %i' %self.dataY)
            self.sideInfo.axes.plot(x,hVec)
            self.sideInfo.axes.set_ylabel(r'Data Value')
            self.sideInfo.axes.axis((x.min(),x.max(),hVec.min(),hVec.max()))#+0.05*hVec.max()))
            self.sideInfo.canvas.draw()
        elif self.plotType == 'verLine':
            if self.gotXform: vVec = self.dataXform[self.dataRect.top():self.dataRect.bottom(),self.dataX]
            else: vVec = self.data[self.dataRect.top():self.dataRect.bottom(),self.dataX]
            if self.STXMdata:
                y = np.array(self.STXMdata.yPoints)[self.dataRect.top():self.dataRect.bottom()]
            else: y = np.arange(self.dataRect.top(),self.dataRect.bottom())
            self.sideInfo.axes.clear()
            self.sideInfo.axes.grid(True,color = 'blue')
            if dummy * q:
                x = 1. / self.sideInfo.wavelength * (-self.dataX + self.dataShape[1] / 2.) * self.sideInfo.pixelSize / self.sideInfo.ccdZ / 1000.
                y = 1. / self.sideInfo.wavelength * (y - self.dataShape[0]/2.) * self.sideInfo.pixelSize / self.sideInfo.ccdZ / 1000.
                self.sideInfo.axes.set_xlabel(r'$Q_{y}$ (nm$^{-1}$)')
                self.sideInfo.axes.set_title(r'Vertical Slice: $Q_{x}$ = %.4f' %(x))
            else:
                self.sideInfo.axes.set_xlabel(r'Vertical Pixel Location')
                self.sideInfo.axes.set_title(r'Vertical Slice: X = %i' %self.dataX)
            self.sideInfo.axes.plot(y,vVec)
            self.sideInfo.axes.set_ylabel(r'Data Value')
            self.sideInfo.axes.set_title(r'Vertical Slice')
            self.sideInfo.axes.axis((y.min(),y.max(),vVec.min(),vVec.max()))#+0.05*vVec.max()))
            self.sideInfo.canvas.draw()
        elif self.plotType == 'specialLine':
            sVec = self.dataSlice
            p = np.sqrt((self.xSlice - self.xSlice[0]).astype('float32')**2 + \
                (self.ySlice - self.ySlice[0]).astype('float32')**2).astype('int16')
            self.sideInfo.axes.clear()
            self.sideInfo.axes.grid(True,color = 'blue')
            self.sideInfo.axes.plot(p,sVec)
            self.sideInfo.axes.set_xlabel(r'Slice Pixel Distance')
            self.sideInfo.axes.set_ylabel(r'Data Value')
            self.sideInfo.axes.set_title(r'Slice')
            self.sideInfo.axes.axis((p.min(),p.max(),sVec.min(),sVec.max()))#+0.05*sVec.max()))
            self.sideInfo.canvas.draw()
        elif self.plotType == 'radial':
            if self.gotXform: rVec, a = rot_ave(self.dataXform[self.dataRect.top():self.dataRect.bottom()+\
                1,self.dataRect.left():self.dataRect.right()+1],1.,1.)
            else:
                if dummy * q:
                    rVec, a = rot_ave(self.data[self.dataRect.top():self.dataRect.bottom()+\
                        1,self.dataRect.left():self.dataRect.right()+1], \
                        self.sideInfo.pixelSize / 1000., self.sideInfo.ccdZ)
                    a = 2. * a / self.sideInfo.wavelength
                else:
                    rVec, a = rot_ave(self.data[self.dataRect.top():self.dataRect.bottom()+\
                        1,self.dataRect.left():self.dataRect.right()+1],1.,1.)
                    a = a / a.max() * len(self.data) * np.sqrt(2.)
            self.sideInfo.axes.clear()
            self.sideInfo.axes.grid(True,color = 'blue')
            self.sideInfo.axes.loglog(a,rVec)
            if dummy * q: self.sideInfo.axes.set_xlabel(r'Spatial Frequency (nm$^{-1}$)')
            else: self.sideInfo.axes.set_xlabel(r'Pixel Distance')
            self.sideInfo.axes.set_ylabel(r'Average Intensity (CCD Counts)')
            self.sideInfo.axes.set_title(r'Azimuthal Average')
            self.sideInfo.axes.axis((a.min(),a.max(),rVec.min(),rVec.max()))#+0.05*rVec.max()))
            self.sideInfo.canvas.draw()

    def updatePlots(self):

        self.bottomPlot.drawPath()
        self.sidePlot.drawPath()

    def autoscale(self):

        if self.mainImage.gotROI:
            this_x_size = self.dataRect.width() #num pixels in current data sub-array
            this_y_size = self.dataRect.height()

            #get size of new data sub-array by scaling with fractional size of image selection
            new_x_size=int(round(float(this_x_size)*float(self.selectedRect.width())/float(self.__zoomX)))
            new_x_size = new_x_size - new_x_size % 4 #size must be divisible by 4 for QImage conversion
            new_y_size=int(round(float(this_y_size)*float(self.selectedRect.height())/float(self.__zoomY)))
            new_y_size = new_y_size - new_y_size % 4
            xpix=float(self.selectedRect.left())-(float(self.mainImageSize.width())/2.-float(self.__zoomX)/2.)
            ypix=float(self.selectedRect.top())-(float(self.mainImageSize.height())/2.-float(self.__zoomY)/2.)
            y1 = int(round((ypix/float(self.__zoomY))*float(this_y_size - 1)))+self.dataRect.top()
            x1 = int(round((xpix/float(self.__zoomX))*float(this_x_size - 1)))+self.dataRect.left()
            y2 = y1 + new_y_size
            x2 = x1 + new_x_size
        else:
            x1 = self.dataRect.left()
            x2 = self.dataRect.left() + self.dataRect.width()
            y1 = self.dataRect.top()
            y2 = self.dataRect.top() + self.dataRect.height()
        if self.gotXform: self.dataMin = self.dataXform[y1:y2,x1:x2].min()
        else: self.dataMin = self.data[y1:y2,x1:x2].min()
        if self.gotXform: self.dataMax = self.dataXform[y1:y2,x1:x2].max()
        else: self.dataMax = self.data[y1:y2,x1:x2].max()
        self.updateImage()

    def zoom(self):

        if self.mainImage.gotROI:
            #update the bounding box in data space and generate the subarray
            this_x_size = self.dataRect.width() #num pixels in current data sub-array
            this_y_size = self.dataRect.height()

            #get size of new data sub-array by scaling with fractional size of image selection
            new_x_size=int(round(float(this_x_size)*float(self.selectedRect.width())/float(self.__zoomX)))
            new_x_size = new_x_size - new_x_size % 4 #size must be divisible by 4 for QImage conversion
            new_y_size=int(round(float(this_y_size)*float(self.selectedRect.height())/float(self.__zoomY)))
            new_y_size = new_y_size - new_y_size % 4
            xpix=float(self.selectedRect.left())-(float(self.mainImageSize.width())/2.-float(self.__zoomX)/2.)
            ypix=float(self.selectedRect.top())-(float(self.mainImageSize.height())/2.-float(self.__zoomY)/2.)
            y1 = int(round((ypix/float(self.__zoomY))*float(this_y_size - 1)))+self.dataRect.top()
            x1 = int(round((xpix/float(self.__zoomX))*float(this_x_size - 1)))+self.dataRect.left()
            y2 = y1 + new_y_size
            x2 = x1 + new_x_size

            self.dataRect.setLeft(x1)
            self.dataRect.setRight(x2)
            self.dataRect.setTop(y1)
            self.dataRect.setBottom(y2)
            imageWidth = float(self.selectedRect.width())
            imageHeight = float(self.selectedRect.height())

            if (new_x_size > new_y_size):
                factor = float(self.mainImageSize.width()) / imageWidth
                self.__zoomX, self.__zoomY = self.mainImageSize.width(), int(round(factor * imageHeight))
            elif (new_y_size > new_x_size):
                factor = float(self.mainImageSize.height()) / imageHeight
                self.__zoomX, self.__zoomY = int(round(factor * imageWidth)), self.mainImageSize.height()
            else:
                self.__zoomX, self.__zoomY = self.mainImageSize.width(),self.mainImageSize.height()
            self.imageRect.setLeft(self.__thumbSize / 2 - self.__zoomX / 2)
            self.imageRect.setRight(self.__thumbSize / 2 + self.__zoomX / 2)
            self.imageRect.setTop(self.__thumbSize / 2 - self.__zoomY / 2)
            self.imageRect.setBottom(self.__thumbSize / 2 + self.__zoomY / 2)
            self.setRoiText()
            self.updateImage()
            #self.updateInfo()

    def updateImage(self):
        if self.gotXform == False:
            if self.logButton.isChecked(): self.image = numpy2qimage(np.log(\
                self.data[self.dataRect.top():self.dataRect.bottom(),\
                self.dataRect.left():self.dataRect.right()] * self.mask[self.dataRect.top():self.dataRect.bottom(),\
                self.dataRect.left():self.dataRect.right()] + \
                self.logOffset),cmap = self.cmap).scaled(self.__zoomX, self.__zoomY)
            else: self.image = numpy2qimage(np.clip(self.data[self.dataRect.top():self.dataRect.bottom(),\
                self.dataRect.left():self.dataRect.right()],self.dataMin,self.dataMax) * self.mask[self.dataRect.top():self.dataRect.bottom(),\
                self.dataRect.left():self.dataRect.right()],cmap = self.cmap).scaled(self.__zoomX, self.__zoomY)
        else:
            if self.logButton.isChecked(): self.image = numpy2qimage(np.log(\
                self.dataXform[self.dataRect.top():self.dataRect.bottom(),\
                self.dataRect.left():self.dataRect.right()] + \
                self.logOffset),cmap = self.cmap).scaled(self.__zoomX, self.__zoomY)
            else: self.image = numpy2qimage(np.clip(self.dataXform[self.dataRect.top():self.dataRect.bottom(),\
                self.dataRect.left():self.dataRect.right()],self.dataMin, self.dataMax),cmap = self.cmap).scaled(self.__zoomX, self.__zoomY)
        self.mainImage.imStartPt.setX(self.mainImageSize.width() / 2 - self.__zoomX / 2)
        self.mainImage.imStartPt.setY(self.mainImageSize.height() / 2 - self.__zoomY / 2)
        self.mainImage.displayImage(self.image)

    def reload(self):

        self.dataXform = None
        self.gotXform = False
        self.openFile(str(self.path))

    def chooseFile(self):
        """ Provides a dialog window to allow the user to specify an image file.
            If a file is selected, the appropriate function is called to process
            and display it.
        """
        dataFile = QFileDialog.getOpenFileName(self,
                "Choose a data file to open", self.filePath, "TIFF \
                    (*.tiff *.tif *.TIF);; HDR (*.hdr);;HDF5 (*.h5);;Numpy (*.npy);;XIM (*.xim)")

        if dataFile != '':
            self.openFile(str(dataFile))
            self.path = dataFile

    def openFile(self, dataFile):
        self.STXMdata = None
        if os.path.exists(dataFile): self.path = dataFile
        else:
            print "qtDiffInspector Error: no such data file"
            return
        if dataFile.split('.')[len(dataFile.split('.'))-1]=='TIF':
            self.data=np.double(imload(dataFile))
            self.dataShape = self.data.shape
        elif dataFile.split('.')[len(dataFile.split('.'))-1]=='tif':
            self.data=np.double(imload(dataFile))
            self.dataShape = self.data.shape
        elif dataFile.split('.')[len(dataFile.split('.'))-1]=='tiff':
            self.data=np.double(imload(dataFile))
            self.dataShape = self.data.shape
        elif dataFile.split('.')[len(dataFile.split('.'))-1]=='npy':
            self.data=np.load(dataFile)
            self.dataShape = self.data.shape
        elif dataFile.split('.')[len(dataFile.split('.'))-1]=='xim':
            self.data = readXIM(dataFile)
            self.dataShape = self.data.shape
        elif dataFile.split('.')[len(dataFile.split('.'))-1]=='hdr':
            self.STXMdata = readXIM(dataFile)
            self.data = self.STXMdata.data["PhotoDiode"][::-1,:]
            self.dataShape = self.data.shape
        if self.data.dtype.kind == 'c':
            self.isComplex = True

        if self.data != None:
            self.clearToggles()
            if (self.data.shape[0] % 2) != 0: self.data = self.data[1:self.data.shape[0],:]
            if (self.data.shape[1] % 2) != 0: self.data = self.data[:,1:self.data.shape[1]]
            if ((self.data.shape[0] / 2) % 2) != 0: self.data = self.data[2:self.data.shape[0],:]
            if ((self.data.shape[1] / 2) % 2) != 0: self.data = self.data[:,2:self.data.shape[1]]
            self.dataShape = self.data.shape
            if self.dataShape[1] > self.dataShape[0]:
                self.__zoomX, self.__zoomY = self.__thumbSize, round(float(self.dataShape[0]) / float(self.dataShape[1]) * self.__thumbSize)
            else:
                self.__zoomX, self.__zoomY = round(float(self.dataShape[1]) / float(self.dataShape[0]) * self.__thumbSize), self.__thumbSize
            self.mask = np.ones(self.data.shape)
            self.dataXform = None
            self.gotXform = False
            self.setWindowTitle('qtDiffInspector: '+ dataFile)
            self.logOffset = 0.0001 * self.data.max()
            self.dataRect.setRect(0,0,self.dataShape[1]+1,self.dataShape[0]+1)
            self.mainImage.box.setRect(0,0,self.dataShape[1]+1,self.dataShape[0]+1)
            self.fileName = dataFile.split('/')[-1::][0]
            self.filePath = dataFile.split(self.fileName)[0]
            self.getFileList(sorted(os.listdir(self.filePath)))
            self.fileCreated = time.ctime(os.path.getctime(dataFile))
            self.fileModified = time.ctime(os.path.getmtime(dataFile))
            self.roiLeft = self.dataRect.left()
            self.roiRight = self.dataRect.right()
            self.roiTop = self.dataRect.top()
            self.roiBottom = self.dataRect.bottom()
            self.imageClick(self.mainImageSize.width() / 2, 0,self.mainImageSize.height() / 2,0)
            self.updatePlots()
            self.updateInfo()
            self.autoscale()
            self.updateROI()
            self.imageRect.setLeft(self.__thumbSize / 2 - self.__zoomX / 2)
            self.imageRect.setRight(self.__thumbSize / 2 + self.__zoomX / 2)
            self.imageRect.setTop(self.__thumbSize / 2 - self.__zoomY / 2)
            self.imageRect.setBottom(self.__thumbSize / 2 + self.__zoomY / 2)

    def getFileList(self,fileList):

        self.fileList = []
        for filename in fileList:
            if filename.split('.')[::-1][0] in self.fileTypes:
                self.fileList.append(filename)
        i = 0
        for filename in self.fileList:
            if filename.find(self.fileName) == 0:
                self.fileIndex = i
            else: i += 1
        self.nFiles = len(self.fileList)

    def imageDrag(self, x1,x2, y1,y2):

        if self.mainImage.gotSlice:
            pass
        else:
            x, y = x1 - x2, y1 - y2
            dy = round(float(y) / float(self.__zoomY) * float(self.dataRect.height()))
            dx = round(float(x) / float(self.__zoomX) * float(self.dataRect.width()))
            y = float(self.__zoomY) * float(dy) / float(self.dataRect.height())
            x = float(self.__zoomX) * float(dx) / float(self.dataRect.width())

            yT = self.dataStartRect.top() + dy
            yB = self.dataStartRect.bottom() + dy
            xL = self.dataStartRect.left() + dx
            xR = self.dataStartRect.right() + dx

            if yT < 0:
                yT = 0.
                yB = self.dataRect.height() - 1
            elif yB > self.dataShape[0]:
                yT = self.dataShape[0] - self.dataRect.height()
                yB = self.dataShape[0] - 1
            else:
                self.yImagePixelValues = self.yImagePixelStartValues - y
            if xL < 0:
                xL = 0.
                xR = self.dataRect.width() - 1
            elif xR > self.dataShape[1]:
                xL = self.dataShape[1] - self.dataRect.width()
                xR = self.dataShape[1] - 1
            else:
                self.xImagePixelValues = self.xImagePixelStartValues - x
                if self.gotFit: self.xfitImagePixelValues = self.xfitImagePixelStartValues - x

            self.dataRect.setTop(yT)
            self.dataRect.setLeft(xL)
            self.dataRect.setBottom(yB)
            self.dataRect.setRight(xR)
            self.setRoiText()
            self.updatePoints()
            self.updateImage()

    def setRoiText(self):

        self.sideInfo.roiLeft = self.dataRect.left()
        self.sideInfo.roiRight = self.dataRect.right()
        self.sideInfo.roiTop = self.dataRect.top()
        self.sideInfo.roiBottom = self.dataRect.bottom()
        self.sideInfo.roiCenterX = (self.dataRect.right() + self.dataRect.left()) / 2
        if (self.sideInfo.roiCenterX) % 2 != 0: self.sideInfo.roiCenterX += 1
        self.sideInfo.roiCenterY = (self.dataRect.top() + self.dataRect.bottom()) / 2
        if (self.sideInfo.roiCenterY) % 2 != 0: self.sideInfo.roiCenterY += 1
        self.sideInfo.setRoiText()

    def imageClick(self, x1 = None, x2 = None, y1 = None, y2 = None, mplot = False):
        if hasattr(self, 'xImagePixelStartValues') == False:
            return

        if self.mainImage.gotSlice != True:
            x,y = x1, y1
            self.xpt,self.ypt=x,y
            sidePlotPath = QPainterPath()
            bottomPlotPath = QPainterPath()
            this_x_size = self.dataRect.width() - 1#num pixels in current data sub-array
            this_y_size = self.dataRect.height() - 1
            self.xsize = this_x_size
            self.ysize = this_y_size
            xpix=float(x)-(float(self.mainImageSize.width() - 1)/2.-float(self.__zoomX)/2.)
            ypix=float(y)-(float(self.mainImageSize.height() - 1)/2.-float(self.__zoomY)/2.)
            self.dataY = int((ypix/float(self.__zoomY)*float(this_y_size - 1)))+self.dataRect.top()
            self.dataX = int((xpix/float(self.__zoomX)*float(this_x_size - 1)))+self.dataRect.left()

            self.xImagePixelStartValues = self.xImagePixelValues
            if self.gotFit: self.xfitImagePixelStartValues = self.xfitImagePixelValues
            self.yImagePixelStartValues = self.yImagePixelValues

            if self.STXMdata:
                self.sideInfo.xVal.setText('%.2f' %self.STXMdata.xPoints[self.dataX])
                self.sideInfo.yVal.setText('%.2f' %self.STXMdata.yPoints[self.dataY])
            else:
                self.sideInfo.xVal.setText('%i' %self.dataX)
                self.sideInfo.yVal.setText('%i' %self.dataY)
            self.sideInfo.IVal.setText('%i' %self.data[self.dataY,self.dataX])
            if self.gotXform:
                vVector = congrid(self.dataXform[self.dataRect.top():self.dataRect.top()+\
                    self.dataRect.height(),self.dataX],(self.sidePlotSize.height()+1,))
                vVector -= vVector.min()
                hVector = congrid(self.dataXform[self.dataY,self.dataRect.left():self.dataRect.left()+\
                    self.dataRect.width()],(self.bottomPlotSize.width()+1,))
                hVector -= hVector.min()
            elif self.isComplex:
                vVector = congrid(abs(self.data[self.dataRect.top():self.dataRect.top()+\
                    self.dataRect.height(),self.dataX]),(self.sidePlotSize.height()+1,))
                vVector -= vVector.min()
                hVector = congrid(abs(self.data[self.dataY,self.dataRect.left():self.dataRect.left()+\
                    self.dataRect.width()]),(self.bottomPlotSize.width()+1,))
                hVector -= hVector.min()
            else:
                vVector = congrid(self.data[self.dataRect.top():self.dataRect.top()+\
                    self.dataRect.height(),self.dataX] * self.mask[self.dataRect.top():self.dataRect.top()+\
                    self.dataRect.height(),self.dataX],(self.sidePlotSize.height()+1,))
                vVector -= vVector.min()
                hVector = congrid(self.data[self.dataY,self.dataRect.left():self.dataRect.left()+\
                    self.dataRect.width()] * self.mask[self.dataY,self.dataRect.left():self.dataRect.left()+\
                    self.dataRect.width()],(self.bottomPlotSize.width()+1,))
                hVector -= hVector.min()
            vVector = vVector / vVector.max() * (self.sidePlotSize.width() - 4.)
            hVector = hVector / hVector.max() * (self.bottomPlotSize.height() - 4.)
            sidePlotPath.moveTo(2,0)
            for i in range(len(vVector)):
                sidePlotPath.lineTo(vVector[i], i)
            sidePlotPath.lineTo(2,self.sidePlotSize.height())
            self.sidePlot.path = sidePlotPath
            bottomPlotPath.moveTo(0,self.bottomPlotSize.height()-2)
            for i in range(len(hVector)):
                bottomPlotPath.lineTo(i,self.bottomPlotSize.height() - 2. - hVector[i])
            bottomPlotPath.lineTo(self.bottomPlotSize.width(),self.bottomPlotSize.height() - 2)
            self.bottomPlot.maxValue = self.data[self.dataY,self.dataRect.left():self.dataRect.left()+\
                                                                                 self.dataRect.width()].max()
            self.bottomPlot.minValue = self.data[self.dataY,self.dataRect.left():self.dataRect.left()+\
                                                                                 self.dataRect.width()].min()
            self.bottomPlot.path = bottomPlotPath
            self.updatePlots()

        if self.mainImage.gotFilter:
            x = (self.mainImage.filter.left() + self.mainImage.filter.right())/ 2
            y = (self.mainImage.filter.top() + self.mainImage.filter.bottom())/ 2
            self.filterX = float(x) / float(self.mainImageSize.width()) * float(self.dataRect.width()) + self.dataRect.left()-1
            self.filterY = float(y) / float(self.mainImageSize.height()) * float(self.dataRect.height()) + self.dataRect.top()-1
            if (self.mainImage.filter.width() > self.mainImage.filter.height()):
                self.filterWidth = self.mainImage.filter.width()
            else:
                self.filterWidth = self.mainImage.filter.height()

        self.dataStartRect.setLeft(self.dataRect.left())
        self.dataStartRect.setRight(self.dataRect.right())
        self.dataStartRect.setTop(self.dataRect.top())
        self.dataStartRect.setBottom(self.dataRect.bottom())

        if self.mainImage.gotROI:
            if x1 < x2:
                self.selectedRect.setLeft(x1)
                self.selectedRect.setRight(x2)
            else:
                self.selectedRect.setLeft(x2)
                self.selectedRect.setRight(x1)
            if y1 < y2:
                self.selectedRect.setTop(y1)
                self.selectedRect.setBottom(y2)
            else:
                self.selectedRect.setTop(y2)
                self.selectedRect.setBottom(y1)

            self.selectedRectTL.setX(self.selectedRect.left())
            self.selectedRectTL.setY(self.selectedRect.top())
            self.selectedRectBR.setX(self.selectedRect.right())
            self.selectedRectBR.setY(self.selectedRect.bottom())
        elif self.mainImage.gotSlice:
            sliceX1 = float(x1)/float(self.mainImageSize.width()) * float(self.dataRect.width()) + float(self.dataRect.left())
            sliceY1 = float(y1)/float(self.mainImageSize.height()) * float(self.dataRect.height()) + float(self.dataRect.top())
            sliceX2 = float(x2)/float(self.mainImageSize.width()) * float(self.dataRect.width()) + float(self.dataRect.left())
            sliceY2 = float(y2)/float(self.mainImageSize.height()) * float(self.dataRect.height()) + float(self.dataRect.top())
            l = int(np.sqrt(abs(sliceY2 - sliceY1)**2 + abs(sliceX2 - sliceX1)**2))
            self.xSlice = np.linspace(sliceX1,sliceX2,l).astype('int16')
            self.ySlice = np.linspace(sliceY1,sliceY2,l).astype('int16')
            if self.gotXform: self.dataSlice = self.dataXform[(self.ySlice,self.xSlice)]
            else: self.dataSlice = self.data[(self.ySlice,self.xSlice)]

        if mplot:
            if self.plotType != 'histogram':
                if self.plotType != 'radial': self.updateInfoPlot()


    def updateInfo(self):
        
        self.sideInfo.shapeVal.setText('%ix%i' %(self.dataRect.width()-1,self.dataRect.height()-1))
        self.sideInfo.maxVal.setText('%.2f' %(self.data[self.dataRect.top():self.dataRect.bottom(),self.dataRect.left():self.dataRect.right()]).max())
        self.sideInfo.minVal.setText('%.2f' %(self.data[self.dataRect.top():self.dataRect.bottom(),self.dataRect.left():self.dataRect.right()]).min())
        self.sideInfo.meanVal.setText('%.2f' %(self.data[self.dataRect.top():self.dataRect.bottom(),self.dataRect.left():self.dataRect.right()]).mean())
        self.sideInfo.stdVal.setText('%.2f' %(self.data[self.dataRect.top():self.dataRect.bottom(),self.dataRect.left():self.dataRect.right()]).std())
        self.sideInfo.totVal.setText('%i' %(self.data[self.dataRect.top():self.dataRect.bottom(),self.dataRect.left():self.dataRect.right()]).sum())
        self.sideInfo.fileNameVal.setText(self.fileName)
        self.sideInfo.fileCreatedVal.setText(self.fileCreated)
        self.sideInfo.fileModifiedVal.setText(self.fileModified)
        self.updateInfoPlot()

        self.sideInfo.roiLeftEdit.setText(str(self.roiLeft))
        self.sideInfo.roiRightEdit.setText(str(self.roiRight))
        self.sideInfo.roiTopEdit.setText(str(self.roiTop))
        self.sideInfo.roiBottomEdit.setText(str(self.roiBottom))


#app = QApplication(sys.argv)
#qtDiffInspector = qtDiffInspectorMain()
#qtDiffInspector.show()
#sys.exit(app.exec_())

#!/usr/bin/env python
import h5py, argparse, sys, glob
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from scipy import signal
from libtiff import TIFFfile
from nanosurveyor.camera import fccd
from nanosurveyor.camera import pyptycho
from nanosurveyor.algorithms import stxm, stat

def preprocessing(args):

    # Read the parameters
    param = {}
    f = open(args.paramfile, 'r')
    for item in f:
        pStringList = [item.split('=')[0],item.split('=')[1].rstrip('\n')]
        if len(pStringList) != 2:
            pass
        else:
            param[pStringList[0]] = pStringList[1] 

    # Detector object (including MATLAB preprocessor)
    det = fccd.FCCD(nrows=1000, width=2000)

    # PYTPTYCHO preprocessor
    pyptycho_preprocessing = pyptycho.preprocessing
    
    # List of sorted TIFF files
    fnames_dark = sorted(glob.glob(args.darkdir + '/*.tif'))
    fnames_data = sorted(glob.glob(args.datadir + '/*.tif'))

    # Count the number of available files
    ndark = len(fnames_dark)
    ndata = len(fnames_data)

    # Read and average dark frames
    st = stat.Stats()
    for i in range(1,ndark-1):
        print "Reading TIFF file (dark): ", fnames_dark[i]
        f = TIFFfile(fnames_dark[i])
        darktiff = f.get_samples()[0][0][0]
        darkraw  = det.scramble(darktiff)
        print "Processing dark frame %d / %d\r" %(i+1, ndark),
        sys.stdout.flush()
        st.add(darkraw[:11640,:].ravel().astype(np.int))
        #st.add(darktiff)
        #plt.figure()
        #plt.imshow(darktiff, vmin=0, vmax=1)
        #plt.colorbar()
        #plt.title(fnames_dark[i])
        #plt.show()
        #print darktiff[640:642]
    meandark = st.mean()
    stddark  = st.std()
    print "Processed %d / %d dark frames" %(i+1, ndark)

    # Bad ADC mask
    adcmask = det.adcmask(stddark)
    
    # Read one data frame
    index = 0
    print "Converting TIFF file (data): ", fnames_data[index]
    f = TIFFfile(fnames_data[index])
    datatiff = f.get_samples()[0][0][0]
    dataraw  = det.scramble(datatiff)
    print "Processing data frame %d / %d\r" %(index+1, ndata),
    sys.stdout.flush()

    # Preprocessing based on MATLAB code
    # ----------------------------------
    
    # 1. Descrambling / Dark subtraction / Masking
    clockframe = det.descramble(dataraw[:11640,:].ravel() - meandark, adcmask, mask=True)

    # 2. Filtering / Downsampling etc...
    cleanframe_matlab = det.preprocessing(clockframe)
    
    # Preprocessing based on PYPTYCHO code
    # ------------------------------------

    # 1. Descrambling of data and dark independently
    clockdata = det.descramble(dataraw[:11640,:].ravel())
    clockdark = det.descramble(meandark)
    
    # 2. Convert back to TIFF format (input expected by pytptycho)
    tiffdata = det.assemble2(clockdata)
    tiffdark = det.assemble2(clockdark)

    # 3. Compare to original TIFF
    assert (tiffdata-datatiff).sum() == 0, "Something is wrong with the scrambling/descrambling"
    
    # 4. Call the pyptycho preprocessor
    cleanframe_pyptycho = pyptycho_preprocessing(tiffdata, tiffdark, param, debug=False, scanDir=args.datadir+'/', bgScanDir=args.darkdir+'/')
    
    # Load frame from CXI file for comparison
    with h5py.File(args.cxifile, 'r') as f:
        compareframe = f['entry_1/data_1/data'][0]

    #compareframe = tiffdark[500:1500]
    
    # Template matching of 2 images
    #corr = signal.correlate2d(compareframe[100:156,100:156], cleanframe_pyptycho[100:156,100:156], boundary='symm', mode='same')
    #y, x = np.unravel_index(np.argmax(corr), corr.shape) # find the match
    #dy = y - 56/2 + 1
    #dx = x - 56/2 + 1
    dx = 0
    dy = -1
    #print dx,dy
    cleanframe_pyptycho = np.roll(np.roll(cleanframe_pyptycho, dy, axis=0), dx, axis=1)
        
    # Plotting the cleaned frames
    fig = plt.figure(figsize=(20,4))
    ax1  = fig.add_subplot(131)
    ax1.set_title('From CXI file')
    ax1.imshow(compareframe, interpolation='none', norm=LogNorm(vmin=1e-2, vmax=1e6))
    #ax2  = fig.add_subplot(132)
    #ax2.set_title('Based on MATLAB code')
    #im2 = ax2.imshow(cleanframe_matlab, interpolation='none', norm=LogNorm(vmin=1e-2, vmax=1e5))
    ax2  = fig.add_subplot(132)
    ax2.set_title('Based on PYPTYCHO code')
    im2 = ax2.imshow(cleanframe_pyptycho, interpolation='none', norm=LogNorm(vmin=1e-2, vmax=1e6))
    fig.colorbar(im2)
    ax3  = fig.add_subplot(133)
    ax3.set_title('Difference')
    im3 = ax3.imshow(cleanframe_pyptycho - compareframe, interpolation='none', vmin=None, vmax=None)#, norm=LogNorm(vmin=1e-2, vmax=1e5))
    fig.colorbar(im3)
    fig.subplots_adjust()
    fig.savefig('compare.png', bbox_inches='tight')
    plt.show()
    
def parse_cmdl_args():
    """Parsing command line arguments"""
    parser = argparse.ArgumentParser(description='preprocessing.py')
    parser.add_argument("datadir", type=str, help="Path to directory with data TIFFs.")
    parser.add_argument("darkdir", type=str, help="Path to directory with dark TIFFs.")
    parser.add_argument("cxifile", type=str, help="Path to a CXI file with original data (for comparison).")
    parser.add_argument("paramfile", type=str, help="Path to pyptycho param file")
    if(len(sys.argv) == 1):
        parser.print_help()
    return parser.parse_args()

def main():
    """Entry point for script."""
    args = parse_cmdl_args()
    preprocessing(args)
    
if __name__ == '__main__':
    main()

# Current issues with streaming

## 1. The FCCD still needs CINControl to setup things (Send 2 files to the camera)
this should be replaced by either a python script or done within the framegrabber

## 2. STXM control should a feature to send background before the scan
Lee is working on that (there will be a separate STXM control with a new version number)

## 3. isBackground depends on the previos fid -> change that
use the reset() function instead introducing a flag there saying that we 
are waiting for the first frame

## 4. When starting the 2nd scan, the backround worker is not responding
it probably crashes before, because it is dividing by zero when a background frame is requested (after the 1st run)

## 5. Work on communication between nanosruveyor and STXM control
Test what Lee has already implemented, one can send requests for certain parameters to STXM control

## 6. Fix pre-processing pipeline
center finding, proper mask (dynamic)....

## Enable MPI for SHARP inside nanosurveyor
we need this, so that we can run bigger scans which do not fit on 1 GPU
for this we should introduce a main worker that coordinates things.

Frontend
=======

Signals
-------
- startCamera() -> streaming-backend
- stopCamera() -> streamng-backend
- startRun(exposure_time, scan_pattern, scan_type, ...) -> streaming-backend
- stopRun() -> streaming-backend
- requestImage(image_type) -> streaming/recons-backend

Slots
-----
- setCameraOn(bool)
- setShutterOpen(bool)	
- setStreamingOn(bool)
- setReconsRunning(bool)
- setReconsFinished(bool)
- setRunFinished(bool)
- setScanPosX(int)
- setScanPosY(int)
- setImageForDisplay(image)

Streaming - backend
================

Signals
---------
- startCamera() -> fccd-simulator.py
- stopCamera() -> fccd-simulator.py
- uploadFirmware() -> fccd-simulator.py

---
-  openShutter() -> beamline-simulator.py
-  startScan(scan_pattern) -> beamline-simulator.py

---
- cameraOn(bool) -> frontend
- shutterOpen(bool) -> frontend
- streamingOn(bool) -> frontend
- runFinished(bool) -> frontend
- scanPosX(int) -> frontend
- scanPosY(int) -> frontend
- imageForDisplay(image) -> frontend

---
- imageForWriting(image) -> recons


Slots
-------
- setStartCamera()
- setStopCamera()
- setStartRun()
- setStopRun()
- setRequestImage(image_type)
- setCameraReady(bool)

Reconstruction - backend
===================

Signals
----------


Slots
------



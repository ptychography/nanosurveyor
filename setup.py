import subprocess
from setuptools import setup, find_packages
from setuptools.command.develop import develop
from distutils.core import setup, Extension

class CustomDevelopCommand(develop):
    """
    Custom develop setup creating/removing a symlink to src.
    This is necessary because of this unsolved problem of setuptools:
    https://bitbucket.org/pypa/setuptools/issue/230
    """
    def run(self):
        if self.uninstall:
            self.multi_version = True
            self.uninstall_link()
            try:
                subprocess.check_output(["rm", "nanosurveyor"], stderr=subprocess.STDOUT)
            except subprocess.CalledProcessError:
                pass
        else:
            self.install_for_development()
            self.warn_deprecated_options()
            try:
                subprocess.check_output(["ln", "-s", "src", "nanosurveyor"], stderr=subprocess.STDOUT)
            except subprocess.CalledProcessError:
                pass

udpframereader = Extension('nanosurveyor.camera.udpframereader',
                           sources = ['src/camera/udpframereader.c'],
                           extra_compile_args=['-std=c99','-march=native','-O3'],
                           extra_link_args=['-std=c99','-march=native'])

setup(name='nanosurveyor',
      version='0.1',
      description='',
      url='https://bitbucket.org/ptychography/nanosurveyor',
      author='ptychography',
      author_email='ptychography@lbl.gov',
      license='...',
      cmdclass={'develop':CustomDevelopCommand},
      package_dir={'nanosurveyor':'src'},
      package_data={'nanosurveyor':['frontend/ui/*.ui', 'frontend/icons/*.svg', 'frontend/stylesheets/*.qss', 'frontend/default.conf']},
      packages=['nanosurveyor',
                'nanosurveyor.camera',
                'nanosurveyor.backend',
                'nanosurveyor.algorithms',
                'nanosurveyor.comm',
                'nanosurveyor.io',
                'nanosurveyor.utils',
                'nanosurveyor.frontend',
                'nanosurveyor.frontend.ui',
                'nanosurveyor.frontend.aux'],
      entry_points = {
          'console_scripts':
          ['nsgui=nanosurveyor.nanosurveyor:main',
           'nstream=nanosurveyor.frontend.streamviewer:main',
           'nsworkers=nanosurveyor.backend.workers:main',
           'nsbackend=nanosurveyor.backend.handler:main',
           'nsbackground=nanosurveyor.backend.background:main',
           'nspreproc=nanosurveyor.backend.preproc:main',
           'nsptycho=nanosurveyor.backend.ptycho:main',
           'nsptychompi=nanosurveyor.backend.ptycho_mpi:main',
           'nscxiwrite=nanosurveyor.backend.cxiwrite:main',
           'nsdata=nanosurveyor.backend.data:main',
           'nslog=nanosurveyor.backend.logger:main',
           'nsguicontrol=nanosurveyor.frontend.guicontrol:main',
           'nscin=nanosurveyor.camera.cin:main']
          },
      scripts = ['scripts/nstiff2raw', 'scripts/nsraw2cxi', 'testing/preprocessing.py', 'scripts/nscxi2cxi'],
      install_requires=["pyqtgraph>=0.9.8", "numpy", "scipy", "pyzmq", "afnumpy"],
      ext_modules = [udpframereader],
      test_suite='nose.collector',
      tests_require=['nose'],
      zip_safe=False)

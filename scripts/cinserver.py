import SocketServer

server = None

class MyTCPHandler(SocketServer.BaseRequestHandler):
    """
    The request handler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """

    def handle(self):
        while(1):
          # self.request is the TCP socket connected to the client
          self.data = self.request.recv(1024).strip()
          #end of data...
          if self.data == '':
            break
          print "{} wrote:".format(self.client_address[0])
          print self.data
          # just send back the same data, but upper-cased
          #self.request.sendall(self.data.upper())
          values = self.data.split(" ")
          print values[0]
          print "Command: " + values[0] + "\n\r"
          self.request.sendall("Command: " + values[0] + "\n\r")

if __name__ == "__main__":
    HOST, PORT = "10.0.2.56", 8880

    # Create the server, binding to localhost on port 9999
    server = SocketServer.TCPServer((HOST, PORT), MyTCPHandler)

    # Activate the server; this will keep running until you
    # interrupt the program with Ctrl-C
    server.serve_forever()

